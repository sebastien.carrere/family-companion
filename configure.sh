#!/bin/bash

error=0;
local=0;

echo -e "\e[1m\e[100m";

if [[ $1 == *"local"* ]]; then
  echo -e "\e[33m>>>> [HELP] - Local installation: login/password set to guest/guest - Sendmail not activated\nData directory set to $HOME/.family-companion\e[39m";
  FC_ADMIN='guest'
  FC_SMTP='local'
  FC_PORT='1977'
  FC_HOSTNAME='localhost'
  FC_DIR="$HOME/.family-companion"
  mkdir -p $FC_DIR
  local=1
fi

if [ -z ${FC_DIR+x} ]
then 
	echo -e "\n\e[31m[ERROR] - FC_DIR is unset\e[39m";
	error=1;
else
	if [ ! -d $FC_DIR/ ]
	then
		echo -e "\n\e[31m[ERROR] - FC_DIR >$FC_DIR< DOES NOT EXIST\e[39m";
		error=1;
	fi
fi

if [ -z ${FC_ADMIN+x} ]
then 
	echo -e "\n\e[31m[ERROR] - FC_ADMIN is unset : MUST BE SET TO Admin email address\e[39m";
	error=1;
fi

if [ ! -e $FC_DIR/data/cfg/ssmtp.conf ]
then
	if [ -z ${FC_SMTP+x} ]
	then
		echo -e "\n\e[31m[ERROR] - FC_SMTP is unset and SSMTP not configured: >$FC_DIR/data/cfg/ssmtp.conf< DOES NOT EXIST";
		echo -e "\e[33m>>>> [HELP] - Set FC_SMTP to your SMTP server address , FC_SMTP_SECURITY protocol (STARTLS or TLS) and FC_SMTP_LOGIN and FC_SMTP_PASSWD if authentication is required\e[39m";
		error=1;
	fi
	if [ ! -z ${FC_SMTP_SECURITY+x} ]
	then
		if [[ "$FC_SMTP_SECURITY" =~ ^(STARTTLS|TLS)$ ]]
		then
			echo "$FC_SMTP_SECURITY is in the list"
		else
			echo "\n\e[31m[ERROR] - FC_SMTP_SECURITY is not in the valid list : STARTTLS|TLS\e[39m";
			error=1
		fi
	fi
fi

if [ -z ${FC_PORT+x} ]
then 
	echo -e "\n\e[31m[ERROR] - FC_PORT is unset : MUST BE SET TO the port you want to access family-companion app\e[39m";
	error=1;
fi

if [ -z ${FC_HOSTNAME+x} ]
then
	echo -e "\e[33m>>>> [HELP] - FC_HOSTNAME not set - default setting is localhost\e[39m";
	FC_HOSTNAME='localhost';
fi

if [ $error -ne 1 ]
then
	if [ ! -d $FC_DIR/data ]
	then 
		mkdir -p $FC_DIR/data
		chown www-data.www-data  $FC_DIR/data
	fi
	if [ ! -d $FC_DIR/etc ]
	then 
		mkdir -p $FC_DIR/etc;
		chown www-data.www-data  $FC_DIR/etc
	fi
	if [ ! -d $FC_DIR/data/cfg ]
	then 
		mkdir $FC_DIR/data/cfg;
	fi

	echo -e "portal_web_server=http://$FC_HOSTNAME\n" > $FC_DIR/data/cfg/env.cfg;
	echo -e "portal_port=$FC_PORT\n" >>  $FC_DIR/data/cfg/env.cfg;
	
	if [ ! -e $FC_DIR/data/cfg/ssmtp.conf ]
	then
		cp site/cfg/ssmtp.conf.template $FC_DIR/data/cfg/ssmtp.conf
		echo -e "#LOCAL CONFIG\n\nmailhub=$FC_SMTP\n" >> $FC_DIR/data/cfg/ssmtp.conf;
		if [ ! -z ${FC_SMTP_SECURITY+x} ]
		then
			echo -e "Use$FC_SMTP_SECURITY=YES\n" >> $FC_DIR/data/cfg/ssmtp.conf;
		else
			echo -e "UseSTARTTLS=YES\n" >> $FC_DIR/data/cfg/ssmtp.conf;
		fi
		if [ ! -z ${FC_SMTP_LOGIN+x} ]
		then
			echo -e "AuthUser=$FC_SMTP_LOGIN\n" >> $FC_DIR/data/cfg/ssmtp.conf;
		fi
		if [ ! -z ${FC_SMTP_PASSWD+x} ]
		then
			echo -e "AuthPass=$FC_SMTP_PASSWD\n" >> $FC_DIR/data/cfg/ssmtp.conf;
		fi
	fi

	if [ ! -e $FC_DIR/data/cfg/local.cfg ]
	then
		echo -e "admin_mail=$FC_ADMIN\n" > $FC_DIR/data/cfg/local.cfg;
	fi
	if [ ! -e $FC_DIR/data/cfg/.htaccess ]
	then
		echo -e "Require all denied\n" > $FC_DIR/data/cfg/.htaccess
	fi

	if [ ! -e $FC_DIR/etc/.htaccess ]
	then
		echo -e "Require all denied\n" > $FC_DIR/etc/.htaccess
	fi

	echo -e "\n\n\e[32mfamily-companion is properly configured\n\n\e[39m";
else
	echo -e "\n\n\e[31m[ERROR] - Service not properly configured\n\n\e[39m";
fi

cp site/cfg/authentication-docker.cfg site/cfg/authentication.cfg
cp site/cfg/site-docker.cfg site/cfg/site.cfg
chown www-data.www-data $FC_DIR/data/cfg/local.cfg

if  [ $local -eq 1 ]
then
	./bin/int/admin_addUser.pl --email=$FC_ADMIN --password=guest --common_name='guest' --outdir=$FC_DIR/etc
	echo -e "mode=local\n" >> $FC_DIR/data/cfg/local.cfg;
fi

echo -e "\e[49m";
echo -e "\e[0m";
