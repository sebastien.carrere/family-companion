#
# $Id: WebBuilder.pm 1084 2013-03-11 12:39:44Z llegrand $
#

package WebBuilder;

# Emmanuel.Courcelle@toulouse.inra.fr
# Jerome.Gouzy@toulouse.inra.fr

# This software is a perl module whose purpose is to provide a
# perl framework for building web sites

# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".

# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.

# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

=pod

=head1 NAME

WebBuilder - An object to manage CGI based web server design 
	
=head1 DESCRIPTION

- structure of the web site is provided by a XML document
- layout is provided by a css file
- by default, only variables with name containing the prefix __wb_ are propagated through the menu options

=head1 BEHAVIOURS

The following behaviours may be set/unset (see the SetBehaviour, UnsetBehaviour, GetBehaviour functions).
Have a look to the $H_DEFBEHAVIOUR initializations to know default values for those behaviours

 encoded_url
   When set, the url is encoded, so that a human cannot decrypt the different parameters
   This works ONLY if ParamParser is able to uuencode/uudecode the parameters,  ie if the perl module Convert::UU is installed
   If Convert::UU is not installed, this behaviour is gently ignored.

 display_title
   When set, the title is displayed in the wb_header1 div, just after the logos

 use_substitution_table
   When set, special characters (ie %0-%9, %a-%z, %A-%Z) found in the xml file may be substituted by variables
   Have a look to the New function

 use_exceptions
   When set, LipmError based exceptions are thrown instead of calling die

 use_session
   When set, the method InitSession is called at start of Realize()
   use_session is automatically set when the tmp XML tag is provided in the description file
   
 session_autoclean
   When set, Webbuilder.pm automatically cleans the tmp directory, removing the directories which are older than $OLDEST_SESSION
   session_autoclean is automatically set when the tmp XML tag is provided in the description file
   NOTE - the method SetOldestSession may be used to modify the maximum age of the session

   WARNING - This works ONLY when use_session is set AND when www-data or any user under which the web server is executed is allowed to use at or batch
             command - See the files /etc/at.allow or /etc/at.deny

 send_parameters
   When set, the ParamParser object is sent to the client, in json format.
   NOTES - 1/ The javascript object will be stored in the client in a global variable called O_PARAM
           2/ If the paramater __wba_index is defined, THIS BEHAVIOUR IS IGNORED
              __wba_index is related to the object LipmAjax, it is used to send "partial requests"
           3/ If the parameter __wba_ajax is set to 'j', THIS BEHAVIOUR IS IGNORED
              __wba_ajax=j means we just want to retrieve some data in json format (cf. LipmAjaxSimpleClass)
           4/ Several reserved parameters (__wb_url, __wb_cookies etc.) are NOT sent
           5/ The parameters are written AFTER the callback called by Realize is executed, so that it is legal to 
              change a parameter from the callback function
           6/ This functionality is specially useful for working with LipmAjaxFormClass objects. The following keys are used by both objects:
              wba_ajax, wba_index, wba_start, wba_end
              wba_ajax set to 1 by LipmAjaxFormClass to go to Ajax mode
                       may be reset to n (>1) by the server to trigger partial Ajax requests
              wba_start, wba_end may be set by the server to define bounds for the partial Ajax requests
              wba_index is set by the client during the partial Ajax requests

 using_yui
   Set this behaviour when you are using the YUI toolkit: the body tag will belong to the yui-skin-sam class, which is very useful then

 Automatic behaviour when the __wba_popup parameter is set and != 0
   When the parameter __wba_popup is set AND != 0, the realize function is modified as follows:
      1/ The html headers are sent as usual
      2/ The logo and the menus are NOT sent
      3/ The patience javascript and icon is NOT sent
      4/ The div id="__wb_content"> is REPLACED with a <div id="__wb_popup"
   This parameter should be used to signify WebBuilder that we are displaying a separate window... ie a popup
   
=head1 XML TAGS

 The following tags with their attributes may be specified inside the XML file:

 <site></<site>
   The whole file must be enclosed between those tags

 <tmp path="path/to/tmp/dir" url="path/to/tmp/url" />
   the path to the tmp directory and url
   OPTIONAL - If tmp does not exist, AND if the behaviour use_session is unset, the session will NOT BE initialized
              BUT If tmp does not exist and use_session is set, you'll get an error

 <stylesheet>   
   <css id="some_id" url="some_url "/>
   <css id="some_other_id" url="some_other_url" msie="if lt IE 7" /> 
 </stylesheet>
   OPTIONAL - The style sheets to introduce. They are sorted by id, the optional comment is used for writing the copyright
              If the attribute msie is specified, the stylesheet will be declared like this:
              <!--[if lt IE 7]>
              <link rel="stylesheet" type="text/css" href="some_other_url" />
              <![endif]-->
              This is a very dirty but very convenient fix for MSIE

              The file attribute is used in two occurrences:
                  1/ If using cssgroup tags (see later)
                  2/ You may specify a directory as url/file attributes, WebBuilder will include ALL .css files found inside the directory
                     It is thus easy to add a css file just be depositing the file in the directory
 
 <stylesheet>
   <cssgroup id="an_id" url="http://.../concatenated.css" path="/.../concatenated.css" />
      <css ... />
      <css ... />
   </jsgroup>
 </stylesheet>
 
   If the file "/.../concatenated.css" is readable, WebBuilder will use this file IN PLACE OF all css tags described in the group
   This is very good for performance, but very poor for debugging
   If the file "/.../concatenated.css" is NOT readable, the individual <css > tags will be used, which is better for debugging
   You can generate the file "/.../concatenated.css" with the method CreateDigestCssFiles. 

   SECURITY CONSIDERATIONS: It would be dangerous to use this method from your cgi, because this would mean that Apache is authorized
   to write .JS files - Please use a companion script executed as another user, to generate "/.../concatenated.js"
   

 <javascript>
   <js id="some_id" url="some_url" path="some_file" comment="some comment" />
   ...
 </javascript>
   OPTIONAL - The scripts to introduce. They are sorted by id, the optional comment is used for writing the copyright
              The file attribute is used in two occurrences:
                  1/ If using jsgroup tags (see later)
                  2/ You may specify a directory as url/file attributes, WebBuilder will include ALL .js files found inside the directory
                     It is thus easy to add a js file just be depositing the file in the directory
   
 <javascript>
   <jsgroup id="an_id" url="http://.../concatenated.js" path="/.../concatenated.js" />
      <js ... />
      <js ... />
   </jsgroup>
 </javascript>
   If the file "/.../concatenated.js" is readable, WebBuilder will use this file IN PLACE OF all js tags described in the group
   This is very good for performance, but very poor for debugging
   If the file "/.../concatenated.js" is NOT readable, the individual <js > tags will be used, which is better for debugging
   You can generate the file "/.../concatenated.js" with the method CreateDigestJsFiles. 

   SECURITY CONSIDERATIONS: It would be dangerous to use this method from your cgi, because this would mean that Apache is authorized
   to write .JS files - Please use a companion script executed as another user, to generate "/.../concatenated.js"
   
 NOTE - The rules for controlling the ORDER of the js/css files are as followes:
        1/ Read the css/js tags OUTSIDE of ANY cssgroup/jsgroup IN THE ORDER DEFINED BY THE id OF THE TAGS
        2/ read the cssgroup/jsgroup IN THE ORDER DEFINED BY THE id OF THE TAGS
           2.a/ Concatenate the files in the groupe IN THE ORDER DEFINED BY THE id OF THE TAGS
        3/ Append the AddCss or AddJavascript files
        
        WARNING The msie="" construct is usable ONLY OUTSIDE of any cssgroup (yes, this is a dirty trick)

 <motd id="some_id" path="some_path" param="script" value="^(HOME)?$" />
  OPTIONAL - Message Of The Day - several motd tags may be declared, they are read in the id order
             Concatenate the files and print them just at top of wb_content div
             However, if param=, value= properties are specified, NOTHING IS PRINTED unless the parameter 
             is defined (script in the example above), AND its value matches the value declared (regex match, here it is HOME or nothing)

 <disabled path=some_path" />             
  OPTIONAL - Several diabled tags may be declared, the files are read in the id order
             If at leas one of those files is readable, the Realize method displays the file and immediately returns, 
             so that the application is disabled. The file should explain the reason and the planned duration.

 <header>
   The following tags must be declared inside the header tag:
   
   <title>some title</title>
      The title of the html page, will be repeated on top of page (see the behaviours anyway)

   <description>some description</description>
      The content of the tag description - If not specified, a default value will be sent
      
   <authors>a list of authors</authors>
      The content of the tag Authors - If not specified, a default value will be sent
      
   <keywords>some description</keywords>
      The content of the tag keywords - If not specified, a default value will be sent
      
   <no_cache />
      If specified, a no_cache meta will be sent 
      
   <favicon img_src="some_url" type="image/png" />
      The favicon, not sure it works with MSIE
      
   <logo id="some_id" alt="some text" link="some url" img_src="some other url" />
   <logo id="some_id" html_src="some html text" />
   ...
      The logos to display at top of page. This can be an image, may be with a link, OR some html text (tags accepted)
      They are displayed sorted by the id
      WARNING The id is written in the final html code, so that you can refer to it from a css or a javascript
              So, DO NOT START THE ID WITH A NUMBER (even for sorting), PREFER A LETTER
 </header>
 <full_screen_icon id="ID_FULL_SCREEN" src="some_src" />
      The icon (gif or png) declared here should be used as a full_screen object
      To be used in conjunction with the LipmFullScreen javascript object
      The following js code: new  LipmFullScreen('ID_FULL_SCREEN','HideOnFullScreen') is OK to get a fullscreen behaviour, 
      ie the header1 and header2 of WebBuilder will be hidden
 <main_menu_icons id="some_id" alt="some_text" src="some_src" />
      The icons (gif or png files) declared here will appear in the LEFT part of the header2 line
      This is a good place to put indicators (like leds)
 <main_menu_history id="ID_HISTORY" src="/path/to/icons/dir" />
      A <div> with code useful for the object LipmHistoryUlClass will be written 
      src must point to an icons' DIRECTORY, where you must have deposited 2 images: history_back.png and history_forward.png
      src="" means: don't draw the back/forward arrows
 <main_menu_item id="some_id" function="a_function" label="some label" />
   <sub_menu_item id="some_other_id" function="another_function">A text</sub_menu_item>
   ...
 </main_menu_item>
 <main_menu_item id="some_id" function="external" label="some label" url="some_url" />
      The menu and submenu items
      WARNING for the menu items, The id is written in the final html code, so that you can refer to it from a css or a javascript
              So, DO NOT START THE ID WITH A NUMBER (even for sorting), PREFER A LETTER
              If the special name "external" is specified, the url MUST be specified too: in this case the button is just an external link
              For any other name, do not specify the url, it will be dynamically computed. The fields starting with '__wb_' will be added to the url as parameters

=head1 TODO
 
SetDefaultBehaviours to set the behaviour BEFORE the constructor
 
Possibility to change authors, cache management, etc

Possibility to add some html code between <body> and <div id="wb_content">

=head1 HISTORY

  18-Oct-2012
 Add content legnth management

 22-Sept-2010
 jsgroup, cssgroup, motd tags added
 
 13-June-2010
 New behaviour 'dsp_stacktrace' to display the stack trace (useful only when use_exception is set) 
 
 13-Mar-2009
 New behaviour 'no_robots' to prevent indexing by robots

 17-fev-2009
 Introducing the behaviour do_not_send_header (cf. Realize for the details)
 
 17-fev-2009
 Introducing the SetObject function: if you declare an object, the function declared by SetFunction or by
 the ParamParser is a method of this object
 
 30-jan-2009
 Le parametre $do_not_send_header de Realize peut maintenant prendre la valeur 2 
 afin de supprimer meme le header http: utile pour faire des redirections

 19-mar-2008
 add meta to the xml file
 add SetMimeType to change the mime type
 
 29-Feb-2008
 add some behaviours, substitution table, using exceptions, etc.
 
 07-Oct-2005
 add cookies managment

 12-Oct-2005
 multi css

 04-Dec-2005
 add session managment

 01-Apr-2006
 add SetLocalStyleSheet to add document specific style
 add SetHeaderTitle to modify the main title

 02-Apr-2006
 add "behaviour" to the module
 add url encoding behaviour (warning: __wb_function and __wb_main_menu must be not encoded)
 add GetParameters

 Feb-2007
 add GetParams
 add GetObjectParam
 add AddJavascript and the javascript management
 add InitSession
 add SetFunction
 XHTML 1.0 strict specification
 minor change to :
	__GetMenuItem
	Realize
	__LoadDescription

 March-2007
 add relative path management

 April-2007
 fix bug caused by ForceArray on SetHeaderTitle
 ATTENTION <tag>value</tag> are now forbidden into XML Descriptor except for title !
 use <tag val="value" if you want to add new ones
 TODO try another XML parser than XML::Simple to clarify code.
 Add __PrintPatience
 Add __HidePatience
    both functions deal with a div indicating that calculations are running

=cut

use warnings;
use strict;
use Carp;
use Error qw( :try );
use Cwd 'abs_path';

use CGI qw/:standard/;
use XML::Simple;
use Data::Dumper;
use ParamParser;
use File::Basename;
use LipmError::ParameterException;
use LipmError::IOException;
use Scalar::Util qw( blessed );
use General qw( SafeIsa TRUE FALSE);

#use General;

our %H_DEFBEHAVIOUR;

#eval "use Convert::UU qw(uudecode uuencode);";

#$H_DEFBEHAVIOUR{'encoded_url'}            = ($@ eq "") ? 1 : 0;
$H_DEFBEHAVIOUR{'encoded_url'}            = 0;
$H_DEFBEHAVIOUR{'display_title'}          = 1;
$H_DEFBEHAVIOUR{'use_substitution_table'} = 0;
$H_DEFBEHAVIOUR{'use_exceptions'}         = 0;
$H_DEFBEHAVIOUR{'dsp_stacktrace'}         = 0;
$H_DEFBEHAVIOUR{'use_session'}            = 0;
$H_DEFBEHAVIOUR{'session_autoclean'}      = 0;
$H_DEFBEHAVIOUR{'send_parameters'}        = 0;
$H_DEFBEHAVIOUR{'do_not_send_header'}     = 0;
$H_DEFBEHAVIOUR{'using_yui'}              = 0;
$H_DEFBEHAVIOUR{'no_robots'}              = 0;

# NOTE - For use_session and session_autoclean:
#        If the XML tag tmp is specified, use_session and session_autoclean are automatically set
#        If the XML tag tmp is NOT specified, use_session and session_autoclean are kept unset

# If the behaviour error_is_security_issue is set, an error (mkdir, function not found, etc) will be seen as a security issue
$H_DEFBEHAVIOUR{'error_is_security_issue'} = 0;

use constant PATIENCE_ID         => "wb_patience";
use constant HTTP_ERROR_SECURITY => 880;
use constant OLDEST_SESSION      => 100000;          # a little more than 1 day
use constant REFRESH_TIME        => 0;               # Deactivated by default

#use constant OLDEST_SESSION      => 100;   # a little more than 1 mn, just for debug
use constant PRIVATE => 'private';

our $OLDEST_SESSION = &OLDEST_SESSION;
our $REFRESH_TIME   = &REFRESH_TIME;
our $REFRESH_URL    = undef;

BEGIN
{
    our $VERSION = do {my @r = (q$Rev: 1084 $ =~ /\d+/g); $r[0]};
}

=head2 function New

	Title      :	New
	Usage      :	$o_webbuilder = New WebBuilder($o_param_parser);
                    $o_webbuilder = New WebBuilder($o_param_parser,$o_auth);
                    $o_webbuilder = New WebBuilder($o_param_parser,$o_auth,'%d' => \$path,...);
                    $o_webbuilder = New WebBuilder($o_param,$o_auth,$desc_file, '%d' => \$path,...);
	Prerequiste:	none
	Function   :	Object constructor
	Returns    :	a valid WebBuilder object
	Args       :	$o_param (required), a valid ParamParser object
                    $o_auth  (optional, may be '') nothing or a valid Auth object
                    $desc_file (optional) The xml description file, if specified the __wb_xmlstructure key in $o_param is useless and ignored
                    %H_subst (optional) A substitution table
                    NOTE about the substitution table:
                         -If a substitution table is passed to New, the object automatically goes to the use_substitution_table behaviour
                         -You can insert substitution strings in the property values of the xml file
                         -Each string must be one of %0..%9, %A..%Z, %a,..%z
                         -Each string may appear several times in the xml file
                         -The substitution string may be replaced by:
                              -a variable (pass the ref to a scalar)
                              -the result of a function call (pass the address of the function, $self is passed to the function) 
	Globals    :	none

=cut

sub New
{
    my $pkg     = shift;
    my $o_param = shift;
    my $o_auth  = shift;

    # The default is to take the desc file from the 3rd paramter
    my $desc_file = shift;

    # If the 3rd parameter is %a or so, take the desc file from $o_param
    if (defined $desc_file and $desc_file =~ /^%[a-z0-9]$/i)
    {
        unshift(@_, $desc_file);
        $desc_file = $o_param->Get('__wb_xmlstructure');
    }

    # If the 3rd parameter is not defined, take the desc file from $o_param
    elsif (!defined $desc_file)
    {
        $desc_file = $o_param->Get('__wb_xmlstructure');
    }

    my %h_subst = @_;

    my %__h_behaviour = %H_DEFBEHAVIOUR;

    # If the decode function is not available, we remove the encoded_url behaviour
    if ($o_param->IsEncodeAvailable() == &FALSE)
    {
        delete $H_DEFBEHAVIOUR{encoded_url};
    }

    my $self = {
        __desc_file => $desc_file,
        __mime_type => 'text/html',
        __content_length => undef,
        __charset          => "utf-8",
        __main_menu        => $o_param->Get('__wb_main_menu'),
        __function         => $o_param->Get('__wb_function'),
        __object           => '',
        __session          => $o_param->Get('__wb_session'),
        __tmp_path         => "",
        __tmp_url          => "",
        __error            => 0,
        __message          => "",
        __http_status      => "200",
        __http_message     => "",
        __cgi              => "",
        __url_stylesheet   => [],
        __url_javascript   => [],
        __local_stylesheet => "",
        __h_behaviour      => \%__h_behaviour,
        __o_param          => $o_param,
        __o_auth           => (defined $o_auth) ? $o_auth : "",
        __realizing    => 0,    # 0 = Realize not yet called, 1 = Realize being called, 2 = Realize already called
        __desc         => {},
        __disabled_msg => ''
        };

    $self->{__mime_type} = 'application/json' if ($o_param->Get('__wba_ajax') eq 'j');

    bless($self, $pkg);

    if (keys(%h_subst) != 0)
    {
        $self->SetSubstitutionTable(%h_subst);
    }
    else
    {
        $self->__LoadDescription();
    }

    # TODO - UNIQUEMENT POUR COMPATIBILITE AVEC Auth
    #        A VIRER ASAP
    #        AVRIL 2012 - MIS DANS Realize
    #if (&SafeIsa($o_auth, 'Authentic') == &TRUE)
    #{
        #$self->{__cookie} = $o_auth->GetCookie();
    #}
    #else
    #{
        #$self->{__cookie} = $o_param->Get('__wb_cookie');
    #}


	
	return ($self);
}

=head2 Procedure SetOldestSession

	Title : SetOldestSession
	Usage : $o_webbuilder -> SetOldestSession(20000);
	Args  : oldest session (in sec)
	Globals : none
=cut

sub SetOldestSession
{
    my $self = shift;
    my ($oldest) = @_;
    $OLDEST_SESSION = $oldest;
}

=head2 Procedure SetRefreshTime

	Title : SetRefreshTime
	Usage : $o_webbuilder -> SetRefreshTime(60);
	Args  : refresh time (in sec) : if set to 0, automatic refresh is deactivated
			[optional] redirection  url
	Globals : none
=cut

sub SetRefreshTime
{
    my $self = shift;
    my ($refresh, $redirect) = @_;
    $REFRESH_TIME = $refresh;
    $REFRESH_URL = $redirect if (defined $redirect);
}

=head2 Procedure SetSubstitutionTable

	Title      :	SetSubstitutionTable
	Usage      :	$o_webbuilder -> SetSubstitutionTable('%d' => \$path,...);
	Args       :	%H_subst The substitution table
	                see the NOTE in the constructor
	Globals    :	none
	TODO       :    SI ON FAIT UN AddCss OU AddJavascript AVANT SetSubstitutionTable, ON A TOUT PERDU !!!

=cut

sub SetSubstitutionTable
{
    my $self    = shift;
    my %h_subst = @_;

    if (keys(%h_subst) != 0)
    {
        for my $k (keys %h_subst)
        {
            if ($k !~ /\A%[a-z0-9]\Z/i)
            {
                &Carp::croak('=>You can declare as substitution string ONLY %0..%9, %A..%Z, %a..%z');
            }
        }
        $self->{__substitution_table} = \%h_subst;
        $self->SetBehaviour('use_substitution_table');
        $self->__LoadDescription();
    }
}

=head2 procedure RedirectJs

  Title : RedirectJs
  Usage : $o_webbuilder->RedirectJs('some_url');
  Prerequisite : Should be called from a function called by Realize
  Procedure    : prints some javascript code which will redirect the browser to the url passed by parameter
  WARNING      : 1/ This works ONLY if javascript is enabled on the browser
                 2/ This is the ONLY WAY to generate redirections when the header is already printed
                 3/ Use this ONLY from a function called by Realize - Else, you'll get an error !
  Args         : $url The url to which the redirection must be done

=cut  

sub RedirectJs
{
    my $self = shift;
    my ($url) = @_;

    print <<EOS
<script language="javascript" type="text/javascript">
<!--
window.location.replace("$url");
-->
</script>
EOS
}

=head2 procedure Realize

	Title      :	Realize
	Usage      :    $o_webbuilder->Realize();
                    $o_webbuilder->Realize(1);
                    $o_webbuilder->Realize(2);
	Prerequiste:	the page must have been configured through the XML file and dynamic functions
	Procedure  :	method to print the HTML page:
                       - the session is checked, may be created
                       - html headers and _wb_headers (logos andmenus) are printed
                       - the div with id wb_content is opened
                       - If defined, an external function (set by the __wb_function parameter or by Setfunction) is called
                         NOTE - This function should WRITE THE MAIN HTML DIV, it is passed $self as parameter
                       - close the wb_content div and print the html trailer
	Returns    :	nothing
	Args       :	$do_not_send_header 0 send headers and traile
                                        1 Send http header, but NOT html nor WebBuilder headers - Useful for Ajax responses, file downloads, etc.
                                        2 No not send any header, even http header - Only call the function - Only useful for redirections
                                        Default value : 0 (common case)
                                                        1 if is __wba_ajax is defined
                                                        1 if the behaviour do_not_send_header is set
	Globals    :	none

=cut

sub Realize
{
    my $self = shift;

    my ($do_not_send_header) = (@_);

    if (!defined $do_not_send_header)
    {
        $do_not_send_header =
          ($self->{__o_param}->IsDefined('__wba_ajax') and $self->{__o_param}->Get('__wba_ajax') ne "0") ? 1 : 0;
        $do_not_send_header = 1 if ($self->GetBehaviour('do_not_send_header'));
    }

    # Realize should be ran ONLY ONCE during the object lifetime
    return if ($self->{__realizing} >= 1);
    $self->{__realizing} = 1;

	# Check the authentication parameters
	my $o_authentic = $self -> GetObjectAuth();
	if (defined $o_authentic && $o_authentic->IsAuthenticated() && $o_authentic->GetLogin() eq '')
	{
		$self->SetErrorMessage('There is a problem with you authenticatio (may be the email is bad)<br />Please ask you system administrator');
	}
		
    # Send the http header, unless asked not to do so
    if ($do_not_send_header != 2)
    {
		# Check the cookie
		my $cookie = undef;
		my $o_authentic = $self -> { __o_auth };
		if ( &SafeIsa($o_authentic, 'Authentic') == &TRUE) 
		{
			$cookie = $o_authentic -> GetCookie();
		}
		else
		{
			$cookie = $self -> GetObjectParam() -> Get('__wb_cookie');
		}

        # If the status is not 200, the function SetHttpStatus was called
        # Send the header and return
        if ($self->{__http_status} ne '200')
        {
            my $status = $self->{__http_status} . " " . $self->{__http_message};
            print header(-type => $$self{'__mime_type'}, -charset => $self->{__charset}, -status => $status);
            return;
        }
        
        else
        {
			my %h_prms = ( -type    => $self->{__mime_type}, -charset => $self->{__charset} );
			if ( defined $cookie )
			{
				$h_prms { -cookie } = $cookie;
			}
			if ( defined $self -> { __content_length } )
			{
				$h_prms { -Content_length } = $self->{'__content_length'};
			}
			
			# Send the header
			print header ( %h_prms );
		}
    }

    # Init the session
    if ($self->GetBehaviour('use_session') == 1)
    {

        # __SetDisabledMsg is called by InitSession only if we are starting a new session
        $self->InitSession();
    }
    else
    {

        # If no use_session behaviour, __SetDisabledMsg is called at every request
        $self->__SetDisabledMsg();
    }

    # Unless specified not to do, send the <head>, the logos, the menu...
    if ($do_not_send_header == 0)
    {
        if ($self->{__o_param}->IsDefined('__wba_popup') and $self->{__o_param}->Get('__wba_popup') ne "0")
        {
            $self->__PrintHeader();
            print "<div id=\"wb_popup\">\n";
        }
        else
        {
            $self->__PrintHeader();
            my $dsbld_msg = $self->{__disabled_msg};
            if ($dsbld_msg ne '')
            {
                $self->__PrintDisabled();
                return;
            }
            $self->__PrintSections();
            $self->__PrintPatience();
            print "<div id=\"wb_content\">\n";
            $self->__PrintMotd();
        }
    }

    # Send the error message or call the function
    if ($$self{'__error'} == 1)
    {
        $self->__PrintMessage();
    }
    elsif (defined($$self{'__function'}) && $$self{'__function'} ne "")
    {

        # we have to check whether __function is a name or a ref to a function
        my $ref_fn   = $$self{'__function'};
        my $type_var = ref($ref_fn);
        my $object   = $$self{'__object'};
        my $class    = blessed $object;

        #print "#$ref_fn#$type_var#$object#$class#";
        #warn "#$ref_fn#$type_var#$object#$class#";
        #return;

        # it is a ref: execute the function or the object method
        # TODO : TRAITEMENT D'EERUER !!!
        if ($type_var eq "CODE")
        {

            # SetObject was called: we consider that the function is a method of this object
            if (defined $class)
            {
                &$ref_fn($object, $self);
            }

            # SetObject was not called: simply call the function
            else
            {
                &$ref_fn($self);
            }
        }

        # SetObject was called and __function is the name of a method: call the method
        # through an eval and trap the errors, if any, using the exceptions - or not
        elsif (defined $class and $object->can($ref_fn))
        {
            if ($self->GetBehaviour('use_exceptions') == 1)
            {
                try
                {
                    $object->$ref_fn($self);
                }
                catch Error with
                {
                    my ($o_error) = @_;

                    if ($self->GetBehaviour('dsp_stacktrace') == 1)
                    {
                        my $stack_trace = $o_error->stacktrace();
                        my $msg         = "FUNCTION CALLED BY REALIZE = " . $ref_fn . "\n";
                        $msg .= "IT IS A METHOD OF AN OBJECT= " . ref($object) . "\n";
                        $msg .= $o_error->text . " file=" . $o_error->file . " line=" . $o_error->line . "\n";
                        $self->__PrintMessage("<pre>" . $msg . $stack_trace . "</pre>");
                    }
                    else
                    {
                        $self->__PrintMessage($o_error->text());
                    }
                };
            }
            else
            {
                eval("\$object->$ref_fn(\$self)");
                my $msg = $@;
                if ($msg ne "")
                {
                    $self->__PrintMessage($@);
                }
            }
        }

        # SetObject was not called, or __function is not one of its methods: may be it is just an ordinary function ?
        elsif ($type_var eq "")
        {

            # this sub returns 1 if the function exists and lives in the main namespace, 0 if it does'nt
            sub _fn_exists
            {
                my $fn = shift;
                $fn = 'main::' . $fn if ($fn !~ /^main::/);
                no strict "refs";
                return (*${fn}{CODE}) ? 1 : 0;
            }

            # If the function is unknown, return a security issue message, or at least an error
            if (!&_fn_exists($ref_fn))
            {
                if ($self->GetBehaviour('error_is_security_issue') == 1)
                {
                    $self->__SecurityMessage('Bad function specified');
                }
                else
                {
                    $self->__PrintMessage("ERROR - Bad function specified ($ref_fn)");
                }
            }

            # If the function is known, try to execute it through an eval, may be trapping and displaying errors.
            else
            {
                if ($self->GetBehaviour('use_exceptions') == 1)
                {
                    try
                    {
                        no strict "refs";
                        my $fn = "main::$ref_fn";
                        &$fn($self);
                    }
                    catch Error with
                    {
                        my ($o_error) = @_;
                        $self->__PrintMessage($o_error->text);
                    };
                }
                else
                {
                    eval("&main::$ref_fn(\$self)");
                    my $msg = $@;
                    if ($msg ne "")
                    {
                        $self->__PrintMessage($@);
                    }
                }
            }
        }

        # The ref is something different from code
        else
        {
            if ($self->GetBehaviour('error_is_security_issue') == 1)
            {
                $self->__SecurityMessage('Bad function specified');
            }
            else
            {
                print basename($0) . " - Realize\n!!--$type_var--!!\n";
            }
        }
    }
    else
    {
        $self->__PrintMessage("ERROR - NO FUNCTION SPECIFIED");
    }

    # this object is no more useful - This is important to for codes using object destructors
    $self->{__object} = '';

    # Realize is not completely terminated, but there is no other external function to call
    $self->{__realizing} = 2;

    # If $do_not_send_header is 2, we have finished with Realize
    return if ($do_not_send_header == 2);

    # Send the parameters if the behaviour is set and NOT __wba_index
    # Please note that if the perl module JSON.pm is NOT installed, nothing will be sent
    if (    $self->GetBehaviour('send_parameters') == 1
        and !$self->{__o_param}->IsDefined('__wba_index')
        and $self->GetParams('__wba_ajax') ne "j")
    {
        my $param_json;

        # remove some parameters from $o_param, because they hang the JSON object
        my $o_param = $self->{__o_param};
        my $o_alt_param = New ParamParser('PARAMPARSER', $o_param);
        $o_alt_param->Delete('__wb_url');
        $o_alt_param->Delete('__wb_cookie');
        $o_alt_param->Delete('__wb_xmlstructure');
        my %h_json_keys;
        $o_alt_param->Dump('JSON', \$param_json, '', \%h_json_keys);

        #$o_alt_param->Dump('JSON',\$param_json);
        print "<script type=\"text/javascript\">";
        print "O_PARAM = '$param_json'.evalJSON(true);\n";
        foreach my $k (keys %h_json_keys)
        {
            print "$k = '$h_json_keys{$k}'.evalJSON(true);\n";
        }
        print "</script>";

        # The following should be BETTER because xhmtl correct
        # BUT ca merdouille lamentablement avec IE donc provisoirement on vire les <!-- -->
        # On verra quand protoypejs aura patche son code
        # print <<HTML
        # <script type="text/javascript">
        # <!--
        # O_PARAM = '$param_json'.evalJSON(true);
        # //-->
        # </script>
        # HTML
        # ;
        #}
    }

    # If it was specified not to send header, we do not send the trailer either
    if ($do_not_send_header == 0)
    {
        print "\n</div>\t<!-- end wb_content -->\n";

        # Print the javascript to hide the message, unless the parameter __wba_popup is set
        unless ($self->{__o_param}->IsDefined('__wba_popup') and $self->{__o_param}->Get('__wba_popup') ne "0")
        {
            $self->__HidePatience();
        }

        print "\n</body></html>";
    }

    return;
}

=head2 function GetUrl

	Title      :	GetUrl
	Usage      :	$url = $o_webbuilder->GetUrl($discarded_params);
	Prerequiste:	none
	Function   :	return the url of the script and parameters
                	depending on the behaviour 'encoded_url' the parameters can be encoded or not
	Returns    :	a string,	the url
	Args       :	$discarded_params,	a comma separated list of parameters to be discarded
	Globals    :	none

=cut

sub GetUrl
{
    my ($self, $toremove) = @_;
    $toremove = "" unless defined $toremove;

    $toremove =~ s/,/\|/g;
    $toremove .= "|xmlstructure|__wb_url";
    $toremove =~ s/^\|//;
    $toremove =~ s/\|$//;

    my $url = $$self{'__cgi'} . "?";
    $url .= $self->GetParameters($toremove, "");
    return $url;
}

=head2 function GetParameters

	Title      :	GetParameters
	Usage      :	$params = $o_webbuilder->GetParameters( $unwanted_param, $wanted_param );
	Prerequiste:	none
	Function   :	methods to get parameters in cgi format _encoded or not depending on the behaviour
                	can be more/less than all the parameters of the url
	Returns    :	a string,	the list of parameters in cgi format
	Args       :	$unwanted_param,a string, a comma separated list of parameters to be discarded
                	$wanted_param,	a string, an url formatted list of parameters to add
	Globals    :	none

=cut

sub GetParameters
{
    my ($self, $toremove, $additional_parameters) = @_;

    if (defined($toremove))
    {
        $toremove =~ s/,/\|/g;
    }
    $toremove .= "|xmlstructure|__wb_url";
    $toremove =~ s/^\|//;
    $toremove =~ s/\|$//;

    my @a_param_keys = $$self{'__o_param'}->GetKeys("__wb_");
    my $parameters   = "";
    foreach my $key (sort @a_param_keys)
    {
        next if ($key =~ /$toremove/);
        $parameters .= "$key=" . $$self{'__o_param'}->Get($key) . "&amp;";
    }
    $parameters .= "$additional_parameters" if (defined($additional_parameters));
    $parameters =~ s/\&amp;$//;
    if ($self->GetBehaviour('encoded_url'))
    {
        $parameters = &ParamParser::Encode($parameters);
    }
    return $parameters;
}

=head2 function GetParams

	Title      :	GetParams
	Usage      :	$value = $o_webbuilder->GetParams( $var );
	Prerequiste:	none
	Function   :	accessor for variables of ParamParser object.
	Returns    :	the value of $var in the ParamParser object
	Args       :	$var, a string, the name of the parameter to get
	Globals    :	none

=cut

sub GetParams
{
    my ($self, $param_to_get) = @_;

    return $$self{'__o_param'}->Get($param_to_get);
}

=head2 function IsDefined

	Title      :	IsDefined
	Usage      :	if ($o_webbuilder->IsDefined( $var )){...}
	Prerequiste:	none
	Function   :	checks to know if the parameter is defined
	Returns    :	1 if defined, 0 if not
	Args       :	$var,	a string, the name of the parameter to check
	Globals    :	none

=cut

sub IsDefined
{
    my ($self, $param_to_get) = @_;

    return $$self{'__o_param'}->IsDefined($param_to_get);
}

=head2 function GetObjectParam

	Title      :	GetObjectParam
	Usage      :	$o_param_parser = $o_webbuilder->GetObjectParam();
	Prerequiste:	none
	Function   :	accessor for ParamParser object.
	Returns    :	the ParamParser object
	Args       :	none
	Globals    :	none

=cut

sub GetObjectParam
{
    my ($self) = @_;

    return $$self{'__o_param'};
}

=head2 function GetObjectAuth

	Title      :	GetObjectAuth
	Usage      :	$o_auth = $o_webbuilder->GetObjectAuth();
	Prerequiste:	none
	Function   :	accessor for the Auth object.
	Returns    :	the Auth object or undef if none
	Args       :	none
	Globals    :	none

=cut

sub GetObjectAuth
{
    my ($self) = @_;
    return ($$self{'__o_auth'} ne "") ? $$self{'__o_auth'} : undef;
}

=head2 function GetCgi

 Title    : GetCgi
 Usage    : $cgi_url = $o_webbuilder->GetCgi();
 Function : accessor for the cgi url
 Returns  : return the cgi url, ie the private member __cgi IF INITIALIZED, ie if the tag <cgi > is inside the WebBuilder wml file
            Else, builds the url from the environment variables SERVER_NAME and SCRIPT_NAME
 Args     : none
 Globals  : none

=cut

sub GetCgi
{
    my ($self) = @_;
    if (defined $self->{'__cgi'} and $self->{'__cgi'} ne '')
    {
    
        return $self->{'__cgi'};
    }
    else
    {
    
        my $protocol = 'http';
        $protocol = 'https' if (defined $ENV{HTTPS} and $ENV{HTTPS} eq 'on');
        
        my $server_name = $ENV{SERVER_NAME};
        my $server_port = $ENV{SERVER_PORT};
        my $script_name = $ENV{SCRIPT_NAME};
        
        if ($server_port eq '80')
        {
            $server_port = '';
        }
        else
        {
            $server_port = ':' . $server_port;
        }

        return $protocol . '://' . $server_name . $server_port . $script_name;
    }
}

=head2 function GetMotdFiles,GetDisabledFiles,GetDigestCssFiles,DigestJsFiles

 Title    : GetMotdFiles
 Usage    : my @a_motd = $o_webbuilder->GetMotdFiles();
 Function : Accessor - returns the list of files declared with the motd/disabled/cssgroup/jsgroup tags
 Returns  : An array of file names
 Args     : none
 Globals  : none

=cut

sub GetMotdFiles
{
    my $self = shift;
    return $self->__GetFiles($$self{__desc}->{motd});
}

sub GetDisabledFiles
{
    my $self = shift;
    return $self->__GetFiles($$self{__desc}->{disabled});
}

sub GetDigestCssFiles
{
    my $self = shift;
    return $self->__GetFiles($$self{__desc}->{stylesheet}[0]->{cssgroup});
}

sub GetDigestJsFiles
{
    my $self = shift;
    return $self->__GetFiles($$self{__desc}->{javascript}[0]->{jsgroup});
}

=head2 function __GetFiles

 Title    : __GetFiles
 Usage    : my @a_Files = $o_webbuilder->__GetFiles( 'xml node type' )
 Function : returns the list of files declared with the file attribute of nodes whose type is by parameter
 Returns  : An array of file names
 Args     : A hash containing some nodes, the key is the id attributes of the nodes
 Globals  : none

=cut

sub __GetFiles
{
    my $self = shift;
    my ($rh_tags) = @_;

    my @a_files;
    foreach my $key (sort keys %$rh_tags)
    {
        push @a_files, $rh_tags->{$key}->{path};
    }
    return @a_files;
}

=head2 Procedure ClearSession

	Title      :	ClearSession
	Usage      :	$o_webbuilder->ClearSession();
	Prerequiste:	none
	Procedure  :	(re)initialize the session
	Returns    :	nothing
	Args       :	none
	Globals    :	none

=cut

sub ClearSession
{
    my ($self) = @_;

    $$self{'__session'} = "";
    $$self{'__o_param'}->Set('__wb_session', $$self{'__session'});

    return;
}

=head2 function GetSession

	Title      :	GetSession
	Usage      :	$session_id = $o_webbuilder->GetSession();
	Prerequiste:	none
	Function   :	accessor for the session id
	Returns    :	return the session id
	Args       :	none
	Globals    :	none

=cut

sub GetSession
{
    my ($self) = @_;

    return $$self{'__session'};
}

=head2 function GetSessionUrl

	Title      :	GetSessionUrl
	Usage      :	$session_url = $o_webbuilder->GetSessionUrl();
	Prerequiste:	none
	Function   :	accessor for the url of the session directory
	Returns    :	return the url of the session directory
	Args       :	none
	Globals    :	none

=cut

sub GetSessionUrl
{
    my ($self) = @_;

    if ($$self{'__tmp_url'} ne "" && $$self{'__session'} ne "")
    {
        return $$self{'__tmp_url'} . '/' . $$self{'__session'};
    }
    return undef;
}

=head2 function GetSessionPath

	Title      :	GetSessionPath
	Usage      :	$session_path = $o_webbuilder->GetSessionPath();
	Prerequiste:	none
	Function   :	accessor for the unix path (absolute) of the session directory
	Returns    :	return the (absolute) unix path of the session directory
	Args       :	none
	Globals    :	none

=cut

sub GetSessionPath
{
    my ($self) = @_;

    # if something is not defined just don't compute anything
    if (   (!defined($$self{'__tmp_path'}))
        or ($$self{'__tmp_path'} eq "")
        or (!defined($$self{'__session'}))
        or ($$self{'__session'} eq ""))
    {
        return undef;
    }
    return $$self{'__tmp_path'} . '/' . $$self{'__session'};
}

=head2 function GetPrivatePath

	Title      :	GetPrivatePath
	Usage      :	$priv_path = $o_webbuilder->GetPrivatePath();
	Function   :	Return the path to a subdirectory of the session directory, but protected by .htpasswd 
	Returns    :	The wanted path, undef if does not exist
	Args       :	none
	Globals    :	none

=cut

sub GetPrivatePath
{
    my ($self) = @_;
    my $sess_dir = $self->GetSessionPath();
    return (defined $sess_dir) ? $sess_dir . '/' . &PRIVATE : undef;

    # if something is not defined just don't compute anything
    if (   (!defined($$self{'__tmp_path'}))
        or ($$self{'__tmp_path'} eq "")
        or (!defined($$self{'__session'}))
        or ($$self{'__session'} eq ""))
    {
        return undef;
    }
    return $$self{'__tmp_path'} . '/' . $$self{'__session'};
}

=head2 function GetForm

	Title      :	GetForm
	Usage      :	$html_form = $o_webbuilder->GetForm($cgi,'some_string','input_label','input_name','input_size');
	Prerequiste:	none
	Function   :	Get the form formated html code corresponding to application and function parameters, and let the user to specify additional fields
	Returns    :	the generated html
	Args       :	$cgi (optional)                    The action, if "" the referenced url is used
                    $string_inside_the_form (optional) Some html code to be inserted at the end of the form, but before the last </p></form> tags
                    $label (optional)                  Some label to display before the input field (see under)
                    input_name (optional)              The name of a field to display
                    input_size (optional)              The size of this field
	Globals    :	none

=cut

sub GetForm
{
    my ($self, $cgi, $string_inside_the_form, $label, $input_name, $input_size) = @_;

    my $o_param = $self->GetObjectParam();

    my $str = "<form method=\"post\" action=\"$cgi\" enctype=\"multipart/form-data\" ><p>\n";
    if (defined($input_name))
    {
        $str .= "$label<input name=\"$input_name\"";
        if (defined($input_size))
        {
            $str .= " size=\"$input_size\"";
        }
        if ($o_param->IsDefined($input_name))
        {
            my $val = $o_param->Get($input_name);
            $str .= " value=\"$val\"";
        }
        $str .= " />\n";
    }
    if ($self->GetBehaviour('encoded_url'))
    {
        $str .= "\t\t<input name=\"__wb_url\" type=\"hidden\" value=\"" . $self->GetParameters() . "\" />\n";
    }
    else
    {
        my @a_param_keys = $o_param->GetKeys("__wb_");
        foreach my $key (sort @a_param_keys)
        {
            next if (defined($input_name) && $key eq $input_name);
            $str .= "\t\t<input name=\"$key\" type=\"hidden\" value=\"" . $$self{'__o_param'}->Get($key) . "\" />\n";
        }
    }
    $str .= "$string_inside_the_form\n";
    $str .= "</p></form>\n";
    return $str;
}

=head2 Procedure SetDocumentStyleSheet

	Title      :	SetDocumentStyleSheet
	Usage      :	$o_webbuilder->SetDocumentStyleSheet($style_tag);
	Prerequiste:	none
	Procedure  :	Initialize the internal document stylesheet from the parameter string
	Returns    :	nothing
	Args       :	$style_tag,	a string, an inclusion of css
                	example : $style_tag = '<style type="text/css"> body { background-color:#FFFFCC;} </style>'
	Globals    :	none

=cut

sub SetDocumentStyleSheet
{
    my ($self, $string) = @_;

    $$self{'__local_stylesheet'} = $string;

    return;
}

=head2 Procedure AddJavascript

	Title      :	AddJavascript
	Usage      :	$o_webbuilder->AddJavascript($rh_js_file1 [,$rh_js_file2, ...]);
                 or $o_webbuilder->AddJavascript( {url=>$url1[, comment=>$comment1]} [, {url=>$url2, comment=>$comment2}]);
	Prerequiste:	none
	Procedure  :	Add one extern javascript file
	Returns    :	nothing
	Args       :	$rh_js_file, one or several references to a h table
                	this hash must have 1 key as following
                	( url => "path to the file" [, comment => "any comment for this file, like copyright"])
    Note       : you can use more than one reference and the comment key is optional.
	Globals    :	none

=cut

sub AddJavascript
{
    my ($self, @a_rh_js) = @_;

    my $ra_file_list = $$self{'__url_javascript'};

    push(@$ra_file_list, @a_rh_js);

    return;
}

=head2 Procedure AddCss

	Title       : AddCss
	Usage       : $o_webbuilder->AddCss( $rh_css_files );
			  $o_web_builder->AddCss({ url => "style.css",  comment => "A css file"});
	Procedure   : Add extern css files (different from SetDocumentStyleSheet)
	Returns     : nothing
	Args        : $rh_css_files,	one or several references to a h table.
                       This hash must have 1 key as following
                       (  url => "path to the file" [, comment => "any comment for this file, like copyright"])
                  Optional keys:
                    comment => "any comment for this file, like copyright"
                       msie => "if lt IE 7" (cf. the xml file description)
	Globals    :  none

=cut

sub AddCss
{
    my ($self, @a_rh_css) = @_;

    my $ra_file_list = $$self{'__url_stylesheet'};

    push(@$ra_file_list, @a_rh_css);
    return;
}

=head2 Procedure AddLogo

	Title       : AddLogo
	Usage       : $o_webbuilder->AddLogo({ id => "LOGO_1", url => "logo1.jpeg", link => "http://www.somewher.org", alt => "some project"});
	Prerequisite: beware not to erase a previous file (ie the key id must be uniq, or it will be overwridden)
	Procedure   : Add more logo files (not described in the xml file)
	Returns     : nothing
	Args        : $rh_logo_files,	a reference to a h table.
	                   The key is an id for the logo file
                       The value is another hash ref.
                       This hash must have 3 keys as following
                       ( id => "file identification", img_src => "path to the file", alt => "alternate text" )
                  Optional keys:
                       (link => "some url", weight, height )
	Globals    :  none

=cut

sub AddLogo
{
    my $self = shift;
    my ($rh_logo) = @_;

    my $rh_file_list = $$self{'__desc'}->{header}[0]->{logo};
    if (exists $rh_logo->{'id'})
    {
        my $k = $rh_logo->{'id'};
        delete $rh_file_list->{$k};
        $rh_file_list->{$k} = $rh_logo;
    }

    return;
}

=head2 Procedure SetFunction

   Title      : SetFunction
   Usage      : $o_webbuilder->SetFunction($ref_fn);
                $o_webbuilder->SetFunction($fct_name);
   Prerequiste: none
   Procedure  : If this procedure is called, the function passed by parameter will be called by Realize,
                overwriting the __wb_function parameter
   Returns    : nothing
   Args       : $ref_fn a reference to a function
                $fct_name a function name - If a function name is passed, it must be a function living in the main namespace
                $fct_name a method name - If SetObject is called, the function becomes a method of this object
                none - The function is reset from $o_param, it can be useful if __wb_function was changed since the Webbuilder object was built
   Globals    : none

=cut

sub SetFunction
{
    my $self = shift;
    my ($ref_fn) = @_;

    if (defined $ref_fn)
    {
        $$self{'__function'} = $ref_fn;
    }
    else
    {
        $$self{'__function'} = $$self{'__o_param'}->Get('__wb_function');
    }
    return;
}

=head2 Procedure SetObject

   Title      : SetObject
   Usage      : $o_webbuilder->SetObject($o_obj)
   Procedure  : The function called by realize (see SetFunction, or the constructor) is a method of the object declared by SetObject
                This can be useful, avoiding the use of global variables
                WARNING - Realize CLEARS the private member, it is important if you use a destructor on the object
                However this should not produce any side effect, because Realize should be called only once during the object lifetime
   Returns    : nothing
   Args       : $o_obj An already built object
   Globals    : none

=cut

sub SetObject
{
    my $self = shift;
    my ($o_obj) = @_;

    $$self{'__object'} = $o_obj;
    return;
}

=head2 Procedure SetErrorMessage

    Title       : SetErrorMessage
    Usage       : $o_webbuilder->SetErrorMessage('ERROR - Something ugly happened');
    Prerequisite: none
    Procedure   : If this procedure is called, the external function is ignored, the method __PrintErrorMessage
                  is called by realize, and the message is printed
    Args        : the error message to print
    Globals     : none
    
=cut

sub SetErrorMessage
{
    my $self = shift;
    my ($msg) = @_;

    $self->{__error}   = 1;
    $self->{__message} = $msg;
    $self->__PrintMessage() if ($self->{__realizing} == 1);
}

=head2 Procedure SetHttpStatus

    Title       : SetHttpStatus
    Usage       : $o_webbuilder->SetHttpStatus($http_error,$msg)
    Procedure   : If this procedure is called, the header is sent using an error code and a message as passed by parameters,
                  then the __realizing attribute is set, thus disabling further uses of the Realize method
                  However, if __realizng is already set, SetErrorMessage make nothing, as the html code has already being sent
    NOTE        : the difference between SetHttpStatus and SetErrorMessage is that SetErrorMessage only PRINTS an error instead of the norma http output,
                  but SetHttpStatus returns a REAL http error
    Args        : $status    : The http error, should be one of the codes described here: http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
                               (however no check is performed here)
                  $msg       : The corresponding error message
    Globals     : none
    
=cut

sub SetHttpStatus
{
    my $self = shift;
    my ($status, $msg) = @_;

    # silently ignoring the function if Realize already called
    if ($self->{__realizing} == 1)
    {
        return;
    }

    $self->{'__http_status'}  = $status;
    $self->{'__http_message'} = $msg;
}

=head2 Procedure SetHeaderTitle

	Title      :	SetHeaderTitle
	Usage      :	$o_webbuilder->SetHeaderTitle($title);
	Prerequiste:	none
	Procedure  :	Modify/overwrite the header/title read from the xml file
	Returns    :	nothing
	Args       :	$title,	a string, the new title
	Globals    :	none

=cut

sub SetHeaderTitle
{
    my ($self, $string) = @_;

    $$self{'__desc'}->{header}[0]->{title}[0] = $string;

    return;
}

=head2 Procedure SetBehaviour

	Title      :	SetBehaviour
	Usage      :	$o_webbuilder->SetBehaviour($key);
	Prerequiste:	global Hash $H_DEFBEHAVIOUR must be defined
	Procedure  :	Control the behaviour of the module
	Returns    :	nothing
	Args       :	$key a string (the behaviour to set)
	Globals    :	none

=cut

sub SetBehaviour
{
    my ($self, $key) = @_;

    return unless (&WebBuilder::__ValidBehaviour($key));
    $$self{'__h_behaviour'}{$key} = 1;

    return;
}

=head2 function GetBehaviour

	Title      :	GetBehaviour
	Usage      :	$behaviour_is_set = $o_webbuilder->GetBehaviour($key);
	Prerequiste:	global Hash $H_DEFBEHAVIOUR must be defined
	function   :	accessor for the behaviour
	Returns    :	1 if the behaviour whose name is passed by parameter is set, 0 if not set
	Args       :	$key   a string (the behaviour to get)
	globals    :	none

=cut

sub GetBehaviour
{
    my ($self, $key) = @_;
    return 0 unless (&WebBuilder::__ValidBehaviour($key));
    return $$self{'__h_behaviour'}{$key};
}

=head2 procedure UnsetBehaviour

	Title      :	UnsetBehaviour
	Usage      :	$o_webbuilder->UnsetBehaviour($key);
	Prerequiste:	global Hash $H_DEFBEHAVIOUR must be defined
	Procedure  :	unset the behaviour
	Returns    :	nothing
	Args       :	$key    a string (the behaviour to unset)
	globals    :	none

=cut

sub UnsetBehaviour
{
    my ($self, $key) = @_;

    return unless (&WebBuilder::__ValidBehaviour($key));
    $$self{'__h_behaviour'}{$key} = 0;

    return;
}

=head2 procedure InitSession

	Title      :	InitSession
	Usage      :	$o_web_builder->InitSession();
	Prerequiste:	none
	Procedure  :	initialize the session, unless already initialized
                	Create the session directory, unless already created
	Args       :	none
	globals    :	none

=cut

sub InitSession
{
    my $self = shift;

    # If there is no session specifed it is created
    if (!defined($$self{'__session'}) || $$self{'__session'} eq "")
    {
        my $tmp_dir = $self->{__tmp_path};
        use File::Temp;
        my $tmp_path = &File::Temp::tempdir('WBXXXXXX', CLEANUP => 0, DIR => $tmp_dir);

        # TODO utiliser une option pour definir le mask ou laisser le mask 600 de File::Temp
        chmod(0777, $tmp_path);

        use File::Basename;
        my $session = fileparse($tmp_path);

        $$self{'__session'} = $session;
        $$self{'__o_param'}->Set('__wb_session', $$self{'__session'});

       # Check the existence of a "disabled" file, and if found create 'DISABLED' flag-file inside the session directory
        $self->__SetDisabledMsg();

        #if ($self -> __SetDisabledMsg() == &TRUE)
        #{
        #	my $lock    = $self->GetSessionPath() . '/DISABLED';
        #	my $fh_lock = new IO::File ($lock,'w');
        #	$fh_lock -> close();
        #}
    }

    # on recupere le chemin absolu de session
    my $dirsession = $self->GetSessionPath();

    $self->__Die("ERROR - session directory is not defined", 'parameter', 'missing') if (!defined($dirsession));

    if ($self->GetBehaviour('error_is_security_issue') == 1)
    {

        # The session directory should be called WB12kb3_fd
        $self->__SecurityMessage("Security Issue with some parameter - Good bye")
          if (basename($dirsession) !~ /WB\w+/);    # avant /WB[0-9]+/
    }

    # if the session dir does not exist, we try to create one
    if (!-e $dirsession)
    {
        if ($self->GetBehaviour('error_is_security_issue') == 1)
        {

            # The session directory should be created without any problem
            mkdir $dirsession or $self->__SecurityMessage("Security Issue with some parameter - Good bye");
        }
        else
        {
            mkdir $dirsession or $self->__Die("ERROR - Cannot make the directory", 'io', $dirsession, 'd');
        }
    }

    # now that we are sure that $dirsession exist, we go in it
    chdir($dirsession);

# EXPERIMENTAL - We create a directory called private which will be protected from apache !
#                Et si on faisait un behaviour pour cela ? (actuellement c'est fait systematiquement: plus simple, mais moins performant)
#                ATTENTION IL FAUT QUE LA DIRECTIVE apache AllowOverride Limit soit activee !!!
    {
        mkdir &PRIVATE;
        my $file_htaccess = &PRIVATE . '/.htaccess';
        my $fh_ht_access  = new IO::File ">$file_htaccess"
          or $self->__Die("ERROR - Cannot make the directory", 'io', &PRIVATE, 'd');
        print $fh_ht_access "deny from all\n";
    }

    # The following is executed only if the behaviour session_autoclean is set
    if ($self->GetBehaviour('session_autoclean'))
    {
        my $dirtmp = $$self{'__tmp_path'};
        my $lock   = $dirtmp . "/CLEANING";

        # If the file CLEANING exists and is not too old, nothing to do (the cleaning is being done)
        return if (-e $lock and &__IsTooOld($lock) == 0);

        my $time_stamp_file = $dirtmp . '/WBLIMIT';

        # if the file WBLIMIT does not exist, we create it
        if (!-e $time_stamp_file)
        {
            my $time_stamp_fh = new IO::File(">$time_stamp_file")
              or $self->__Die("ERROR - Cannot open $time_stamp_file for writing - good bye", 'io', 'f');
            print $time_stamp_fh time() . "\n";
            $time_stamp_fh->close();
        }
        else
        {

            # If the time stamp is too old, trigger a cleaning process in the tmp directory
            if (&__IsTooOld($time_stamp_file) == 1)
            {

                # Create a lock file
                my $fh_lock = new IO::File(">$lock")
                  or $self->__Die("ERROR - Cannot open $lock for writing - good bye", 'io', 'f');
                print $fh_lock time . "\n";

                my $script_fh = new IO::File("|batch")
                  or $self->__Die("ERROR - Cannot execute batch for cleaning - good bye", 'io', 'f');

# - FOR DEBUGGING ONLY - my $script_fh   = new IO::File (">/tmp/TOTO") or $self->__Die("ERROR - Cannot execute batch for cleaning - good bye",'io','f');

                my $wd = abs_path('..');
                print $script_fh
                  "find $wd -type d -name WB\\* \\! -newer $time_stamp_file -prune -exec rm -rf {} \\; \n";
                print $script_fh "rm $lock $time_stamp_file\n";

# POUR DEBOGUER
#my $alt_script_fh = new IO::File(">$dirtmp/WBC");
#print $alt_script_fh "cd $dirtmp\nfind . -type d -name WB\\* \\! -newer $time_stamp_file -prune -exec rm -rf {} \\; \n";
#print $alt_script_fh "#rm $lock $time_stamp_file\n";

            }
        }
    }

    # If the file passed by parameter is older than $OLDEST_SESSION, or empty, return 1
    # If the file does not exist or if it is not too old, return 0
    # The time is computed from the number found at top of file
    sub __IsTooOld
    {
        my ($file) = @_;
        my $fh_file = new IO::File($file) or return 0;
        my $time_stamp = <$fh_file>;
        chomp($time_stamp);
        $time_stamp += 0;

        # If the file was empty, we just remove the file and return true
        if ($time_stamp == 0)
        {
            unlink $file;
            return 1;
        }

        if ($time_stamp < time - $OLDEST_SESSION)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}

=head2 procedure __SetDisabledMsg

	Title      :	__SetDisabledMsg
	Usage      :	$self->__SetDisabledMsg();
	Procedure  :	Set the private field __disabled_msg, if it is possible to read the tags <disabled />

=cut

sub __SetDisabledMsg
{
    my $self              = shift;
    my $rh_disabled_files = $$self{__desc}->{disabled};
    my @a_disabled;
    foreach my $key (sort keys %$rh_disabled_files)
    {
        my $rh_disabled = $rh_disabled_files->{$key};
        my $file        = $rh_disabled->{path};
        my $fh_file     = new IO::File($file) or next;
        my @a_f         = <$fh_file>;
        chomp @a_f;
        push @a_disabled, @a_f;
    }
    if (@a_disabled != 0)
    {
        $self->{__disabled_msg} = join('<br />', @a_disabled);
        return &TRUE;
    }
    else
    {
        return &FALSE;
    }
}

=head2 procedure __PrintSections

	Title      :	__PrintSections
	Usage      :	$self->__PrintSections();
	Prerequiste:	none
	Procedure  :	print the elt wb_header2, thus the menu
	Returns    :	nothing
	Args       :	none
	globals    :	none

=cut

sub __PrintSections
{
    my ($self) = @_;
    my $second_level_menu = "";

    my $rh_main_menu  = $$self{'__desc'}->{main_menu_item};
    my $rh_main_icons = $$self{'__desc'}->{main_menu_icons};
    print "<div id=\"wb_header2\" class=\"HideOnFullScreen\" >\n";

    # Display first the icons
    foreach my $key (sort keys %$rh_main_icons)
    {
        print "<img id=\"$key\" src=\"$rh_main_icons->{$key}->{src}\" alt=\"$rh_main_icons->{$key}->{alt}\" />";
    }

    # If main_menu_history specified, it's time to print it
    if (exists $$self{'__desc'}->{main_menu_history})
    {
        my $rh_main_menu_history = $$self{'__desc'}->{main_menu_history};
        foreach my $key (sort keys %$rh_main_menu_history)
        {
            my $src = "";
            $src = $rh_main_menu_history->{$key}->{src} if (exists $rh_main_menu_history->{$key}->{src});
            print "<div id=\"$key\" class=\"HistoryUl\" style=\"display:none\" >";
            if ($src ne "")
            {
                print "<img alt=\"back\" src=\"$src/history_back.png\" />";
            }
            print "<ul><li>&nbsp;</li></ul>";
            if ($src ne "")
            {
                print "<img alt=\"forward\" src=\"$src/history_forward.png\" />";
            }
            print "</div>";
            last;    # Only one history allowed
        }
    }

    # If no item in main menu, don't print <ul></ul>
    if (keys(%$rh_main_menu) != 0)
    {
        print "<ul class=\"wb_main\">\n";

        # i sue this base of string to save space
        my $partial_cgi_start = $$self{'__cgi'} . "?";
        $partial_cgi_start .= $self->GetParameters("main_menu,function", "");

        # Printing the main menu
        foreach my $key (sort {$b cmp $a} keys(%$rh_main_menu))
        {

            #warn ("coucou #${key}#$$rh_main_menu{$key}->{function}#");
            next if ($key eq "");
            next if ($self->__PrivilegeOk($rh_main_menu->{$key}) == &FALSE);
            my $active_li = "";
            my $active_a  = " class=\"wb_main_a_inactive\" ";
            my $url;

            # I complete the cgi url with one more parameter
            my $partial_cgi = $partial_cgi_start;
            $partial_cgi .= "&amp;__wb_main_menu=" . $$rh_main_menu{$key}->{function};

            # Found the active item: mak the item as "active" and prepare the submenu
            if ($$rh_main_menu{$key}->{function} eq $$self{'__main_menu'})
            {
                $active_li = " class=\"wb_main_li_active\" ";
                $active_a  = " class=\"wb_main_a_active\" ";

                # building the submenu (will be printed later)
                my $rh_item = $$rh_main_menu{$key}->{sub_menu_item};
                foreach my $item (sort keys(%$rh_item))
                {
                    next if ($key eq "");
                    next if ($self->__PrivilegeOk($rh_item->{$key}) == &FALSE);

                    my $sub_active_li = "";

                    # Adding the last parameter to the cgi url
                    my $cgi_down = $partial_cgi;
                    $cgi_down .= "&amp;__wb_function=" . $$rh_item{$item}->{function};
                    if ($self->{__function} eq $$rh_item{$item}->{function})
                    {
                        $sub_active_li = " class=\"wb_sub_active\" ";
                    }
                    my $str = $self->__GetMenuItem("menu", $cgi_down, $sub_active_li, "", \$$rh_item{$item});
                    $second_level_menu .= $str;
                }
            }

            # Adding last parameters to the cgi url
            my $cgi_up = $partial_cgi;
            if (defined $$rh_main_menu{$key}->{function})
            {
                $cgi_up .= "&amp;__wb_function=" . $$rh_main_menu{$key}->{function};
            }
            print $self->__GetMenuItem("main_menu", $cgi_up, $active_li, $active_a, \$$rh_main_menu{$key});
        }
        print "</ul>";
    }
    print "</div>\n";

    if ($second_level_menu ne "")
    {
        print "<div class=\"wb_sub\">\n";
        print "<ul>\n";
        print $second_level_menu;
        print "</ul>\n";
        print "</div>\n\n";
    }

    return;
}

=head2 function __PrivilegeOk

 Title     : __PrivilegeOk
 Usage     : if ($self->__PrivilegeOk($rh_item)...
 Function  : return &TRUE if:
                    -there is no $o_authentic object
                    -there is no $rh_item->{privilege}
                    -at least one of the privileges from $rh_item->{privilege} is owned by $o_authentic
 Returns  : 0/1
 Args     :	$rh_item (see (__PrintSections)

=cut

sub __PrivilegeOk
{
    my $self = shift;
    my ($rh_item) = @_;

    my $o_authentic = $self->GetObjectAuth();
    return &TRUE unless (&SafeIsa($o_authentic, 'Authentic'));

    return &TRUE unless defined($rh_item->{role});

    # found an authentic object and a list of roles
    # Return &TRUE if one of the roles cited in $rh_item is found in $o_authentic
    foreach my $priv (split(';', $rh_item->{role}))
    {
        if ($priv =~ s/^!(.+)$/$1/)
        {

            #warn "FALSE $rh_item->{label} $priv " . $o_authentic->HasRole($priv);
            return &TRUE unless ($o_authentic->HasRole($priv));
        }
        else
        {

            #warn "TRUE $rh_item->{label} $priv " . $o_authentic->Hasrole($priv);
            return &TRUE if ($o_authentic->HasRole($priv));
        }
    }
    return &FALSE;
}

=head2 function __GetMenuItem

	Title      :	__GetMenuItem
	Usage      :	$s_menu = $self->__GetMenuItem($type,$cgi,$cgi_only,$active_li,$active_a,$rh_item);
	Prerequiste:	none
	Function   :	return a string containing the html code for a single menu item
	Returns    :	html code
	Args       :	$type,	a string "menu" for non activated link
                	$cgi,	a string containing the url of the cgi with some parameters
                	$active_li,	a string containing qualification for li tag (" class='...'") for a currently activated link
                	$active_a,		a string containing qualification for a tag (" class='...'") for a currently activated link
                	$rh_item,	hash reference of submenu
	globals    :	none

=cut

sub __GetMenuItem
{
    my ($self, $type, $cgi, $active_li, $active_a, $rh_item) = @_;
    my $str = "";
    my $url;

    $url = ($$rh_item->{function} eq "extern") ? $$rh_item->{url} : $cgi;

    if ($$rh_item->{function} =~ /^WBForm/)
    {
        my $tmp = $$self{'__o_param'}->Get("__wb_function");
        $$self{'__o_param'}->Set("__wb_function", $$rh_item->{function});
        my $cgi_to_run = (defined($$rh_item->{url}) && $$rh_item->{url} ne "") ? $$rh_item->{url} : $$self{'__cgi'};
        $str =
            "\t\t<li class=\"wb_li_form\">"
          . $self->GetForm($cgi_to_run, "", $$rh_item->{label}, $$rh_item->{input_name}, $$rh_item->{input_size})
          . "</li>\n";
        $$self{'__o_param'}->Set("__wb_function", $tmp);
    }
    else
    {
        if ($type eq "menu")
        {
            my $content = "";
            $content = $$rh_item->{content} if defined($$rh_item->{content});
            $str = "<li$active_li><a $active_a href=\"$url\">$content</a><br/></li>\n";
        }
        else
        {
            if (defined($$rh_item->{label}))
            {
                $str = "<li$active_li><a $active_a href=\"$url\">" . $$rh_item->{label} . "</a></li>\n";
            }
            elsif (defined($$rh_item->{src}) and defined($$rh_item->{alt}))
            {
                my $src = $$rh_item->{src};
                my $alt = $$rh_item->{alt};

                $str = "<li$active_li><a $active_a href=\"$url\"><img alt=\"$alt\" src=\"$src\" /></a></li>\n";
            }
        }
    }
    return $str;
}

=head2 procedure __PrintHeader

	Title      :	__PrintHeader
	Usage      :	$self->____PrintHeader();
 	Prerequiste:	$self->{'__url_javascript'}->{url} and
	            	$self->{'__url_javascript'}->{code} are mutually exclusive.
	Procedure  :	print out all the HTML header and, unless the parameter __wba_popup is set, the first part of WB header (logos lines)
                 
                    NOTE - A logo may be:
                                  -an image (properties img_src, alt, may be height defined)
                                  -an image with an hypertext link (properties img_src, alt, link defined)
                                  -an html text (property html_src defined. In this case, the embedded html tags MUST be escaped (&lt; instead of <, etc)
	Returns    :	nothing
	Args       :	none
	Globals    :	none

=cut

sub __PrintHeader
{
    my ($self) = @_;

    # [0] has been added to fit with the ForceArray option
    my $title = $$self{'__desc'}->{header}[0]->{title}[0];
    $title = "" unless (defined $title);
    my $favicon        = $$self{'__desc'}->{header}[0]->{favicon}[0];
    my $ra_authors     = $$self{'__desc'}->{header}[0]->{authors};
    my $ra_description = $$self{'__desc'}->{header}[0]->{description};
    my $ra_keywords    = $$self{'__desc'}->{header}[0]->{keywords};

    print <<EOF1
<?xml version="1.0" encoding="$self->{__charset}"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset='$self->{__charset}'" />
    <title>$title</title>
EOF1
      ;

    # The meta description
    if (defined $ra_description)
    {
        foreach my $d (@$ra_description)
        {
            print "    <meta name=\"Description\" content=\"$d\" />\n";
        }
    }
    else
    {
        print
          '    <meta name="Description" content="Tools and databases developed by the LIPM bioinformatics service" />'
          . "\n";
        print
          '    <meta name="Description" content="Outils et bases de donnees developpees par le service bioinformatique du LIPM" />'
          . "\n";
    }

    if (($self->GetBehaviour('no_robots') == 1) or ($ENV{QUERY_STRING} ne ''))
    {
        print '    <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />' . "\n";
    }

    if ($REFRESH_TIME != 0)
    {
        my $redirect = '';
        if (defined $REFRESH_URL)
        {
            $redirect = "; url=$REFRESH_URL";
        }
        print '<meta http-equiv="refresh" content="' . $REFRESH_TIME . $redirect . '">' . "\n";
    }

    # The meta author
    if (defined $ra_authors)
    {
        foreach my $a (@$ra_authors)
        {
            print "    <meta name=\"Author\" content=\"$a\" />\n";
        }
    }
    else
    {
        print '    <meta name="Author" content="Jerome Gouzy, Emmanuel Courcelle, Sebastien Carrere and others" />'
          . "\n";
    }

    # The meta keywords
    if (defined $ra_keywords)
    {
        foreach my $k (@$ra_keywords)
        {
            print "    <meta name=\"Keywords\" content=\"$k\" />\n";
        }
    }
    else
    {
        print
          '    <meta name="Keywords" content="bioinformatique, bioinformatics, bioinfo, INRA, CNRS, LIPM, Toulouse" />'
          . "\n";
    }

    # The meta Cache-Control
    if (exists $$self{'__desc'}->{header}[0]->{no_cache})
    {
        print '    <meta http-equiv="Cache-Control" content="no-cache" />' . "\n";
    }

    # The favicon
    if (defined($favicon))
    {
        my $src_img = $favicon->{'img_src'};
        my $type    = $favicon->{'type'};
        print "<link rel=\"icon\" href=\"$src_img\" type=\"$type\" />";
    }

    # I use a variable to speed the display
    # _usually one print of a big variable is faster than many prints of small variables -Seb-
    my $external_script = "";

    # css
    my $ra_css = $$self{'__url_stylesheet'};

    foreach my $rh_css (@$ra_css)
    {
        my $link = "<link href=\"" . $rh_css->{url} . "\" rel=\"stylesheet\" type=\"text/css\" />";
        if (exists($rh_css->{msie}))
        {
            $external_script .= "<!--[" . $rh_css->{msie} . "]>\n";
            $external_script .= "$link\n";
            $external_script .= "<![endif]-->\n";
        }
        else
        {
            $external_script .= "$link\n";
        }

        #integration of comments mainly used to report copyright
        my $comment = $rh_css->{comment};

        #the comment follows the script tag
        $external_script .= "<!-- $comment -->" if (defined($comment));
    }

    # javascript
    my $ra_javascript = $self->{'__url_javascript'};
    foreach my $rh_js_file (@$ra_javascript)
    {
        $external_script .= "\t<script type=\"text/javascript\"";

        # i admit that if the key exists, it is defined.
        if (exists $$rh_js_file{url} and $$rh_js_file{url} ne '')
        {
            $external_script .= " src=\"" . $$rh_js_file{url} . "\">";
        }
        elsif (exists $$rh_js_file{code} and $$rh_js_file{code} ne '')
        {
            my $tmp = "\t\t//<![CDATA[\n";
            $external_script .= ">\n$tmp" . $$rh_js_file{code} . "\t\t//]]>\n";
        }
        $external_script .= "\t</script>";

        #integration of comments mainly used to report copyright
        my $comment = $rh_js_file->{comment};

        #the comment follows the script tag
        $external_script .= "<!-- $comment -->" if (defined($comment));
        $external_script .= "\n";
    }

    # [0] has been added to fit with the ForceArray option
    my $local_css = $$self{'__local_stylesheet'};

    # using_yui behaviour = body belongs to yui-skin-sam
    my $yui_skin_sam = ($self->GetBehaviour('using_yui') == 1) ? ' class="yui-skin-sam" ' : '';

    print <<EOF
	$local_css
	$external_script
</head>
<body $yui_skin_sam>
EOF
      ;

    # If the parameter wba_popup is set, we've done !
    return if ($self->{__o_param}->IsDefined('__wba_popup') and $self->{__o_param}->Get('__wba_popup') ne "0");

    # Before the first line: the full_screen icon - Inside a div to be validated
    if (exists $$self{'__desc'}->{full_screen_icon})
    {
        my $rh_full_screen_icon = $$self{'__desc'}->{full_screen_icon};
        foreach my $key (sort keys %$rh_full_screen_icon)
        {
            print
              "<div style=\"display:none\" id=\"$key\" ><img src=\"$rh_full_screen_icon->{$key}->{src}\" alt=\"Full Screen\" /></div>";
            last;    # Only one full screen icon allowed
        }
    }

    # the first line
    print "<div id=\"wb_header1\" class=\"HideOnFullScreen\" ><ul>\n";
    my $logo = $$self{'__desc'}->{header}[0]->{logo};
    foreach my $key2 (sort keys(%$logo))
    {
        next if ($key2 eq "");
        if (exists $logo->{$key2}->{img_src})
        {
            my ($a1, $img, $a2) = ('', '', '');
            $a1 = '<a href="' . $logo->{$key2}->{link} . '">' if (exists $logo->{$key2}->{link});
            $img = '<img src="' . $logo->{$key2}->{img_src} . '" class="wb_logo" alt="' . $logo->{$key2}->{alt} . '"';
            $img .= ' height="' . $logo->{$key2}->{height} . '"' if (exists $logo->{$key2}->{height});
            $img .= ' />';
            $a2 = '</a>' if (exists $logo->{$key2}->{link});
            print '<li id="' . $key2 . '">' . $a1 . $img . $a2 . '</li>';
        }
        else
        {
            print '<li id="' . $key2 . '"><div>' . $logo->{$key2}->{html_src} . '</div></li>';
        }
    }
    print "</ul>\n";
    print "<span id=\"wb_title\">$title</span>" if ($self->GetBehaviour('display_title') == 1);
    print "<hr class=\"wb_clear\" /></div>\n";
    return;
}

=head2 procedure __LoadDescription

	Title      :	__LoadDescription
	Usage      :	$self->__LoadDescription();
	Prerequiste:	none
	Procedure  :	parse XML file and fill object attribute with the read information.
	Returns    :	nothing
	Args       :	none
	Globals    :	none

=cut

sub __LoadDescription
{
    my ($self) = @_;

    $$self{'__desc'} = XMLin($$self{'__desc_file'}, ValueAttr => ['id'], ForceArray => 1);

    # Do the substitutions
    if ($self->GetBehaviour('use_substitution_table'))
    {
        $self->__Substitute();
    }

    # [0] has been added to fit with the ForceArray option
    if (defined($$self{'__desc'}->{tmp}[0]->{path}))
    {
        $$self{'__tmp_path'} = $$self{'__desc'}->{tmp}[0]->{path};
        $self->SetBehaviour('use_session');
        $self->SetBehaviour('session_autoclean');
    }
    $$self{'__tmp_url'} = $$self{'__desc'}->{tmp}[0]->{url} if (defined($$self{'__desc'}->{tmp}[0]->{url}));
    my $cgi = $$self{'__desc'}->{cgi}[0]->{url};
    $$self{'__cgi'} = $cgi if (defined($cgi));

    #$$self{'__url_stylesheet'} = $$self{'__desc'}->{stylesheet}[0]->{css};

    # Load the css <css> nodes and the <cssgroup> nodes
    my $ra_css_nodes = $$self{'__url_stylesheet'};
    push(@$ra_css_nodes, $self->__ExtractJsCss($$self{'__desc'}->{stylesheet}[0]->{css}, 'css'));
    $self->__LoadJssCssGroups($$self{'__desc'}->{stylesheet}[0]->{cssgroup}, 'css', $ra_css_nodes);

    # Load the javascript <js> nodes and the <jsgroup> nodes
    my $ra_js_nodes = $$self{'__url_javascript'};
    push @$ra_js_nodes, $self->__ExtractJsCss($$self{'__desc'}->{javascript}[0]->{js}, 'js');
    $self->__LoadJssCssGroups($$self{'__desc'}->{javascript}[0]->{jsgroup}, 'js', $ra_js_nodes);

    # i have to set absolute path here because it seems to be impossible to call it twice
    # unless it is caused by calling it after chdir ? -Seb
    if (defined($$self{'__tmp_path'}) and $$self{'__tmp_path'} ne '' and ($$self{'__tmp_path'} !~ /^\//))
    {
        $$self{'__tmp_path'} = abs_path($$self{'__tmp_path'});
    }

    #debug
    # 	print Dumper($self);

    return;
}

=head2 procedure __LoadJsCssGroups

	Title      : __LoadJsCssGroups
	Usage      : $self->__LoadJsCssGroups($rh_nodes,'js',\@a_nodes)
	procedure  : Starting from a hash, (key=id, values = xml-nodes <jsgroup> or <cssgroup>), sort the nodes with the keys
	             For each node, try to read file from the file attribute
	             If found, push the corresponding node to @a_files_list
	             If not, push the nodes (js or css) which constitue the group
	Args       : $rh_nodes Ref to a hash of nodes (read from the xml file)
	             $ext      Extension (only js or css)
	             $ra_nodes An array of sorted nodes

=cut

sub __LoadJssCssGroups
{
    my $self = shift;
    my ($rh_nodes, $ext, $ra_nodes) = @_;

    # Read the groups, sorted
    foreach my $key (sort keys %$rh_nodes)
    {

        # The digest file exists: we use this file, it will be faster
        my $rh_group    = $rh_nodes->{$key};
        my $digest_file = $rh_group->{path};
        if (-r $digest_file)
        {
            my %h_tmp;
            $h_tmp{url} = $rh_group->{url};
            push @$ra_nodes, \%h_tmp;

            #warn "WEB BUILDER found $key $digest_file";
        }
        else
        {
            push @$ra_nodes, $self->__ExtractJsCss($rh_group->{$ext}, $ext);

            #warn "WEB BUILDER did not find $digest_file";
        }
    }
}

=head2 function __ExtractJsCss

	Title      : __ExtractJsCss
	Usage      : my @a_tmp = $self->__ExtractJsCss($rh_nodes,'js');
	function   : Starting from a hash, (key=id, values = xml-nodes <js> or <css>), sort the nodes with the keys
	             For each node, check the file attribute (if exists)
	             If the file is in fact a directory, create nodes with url/file attributes for every file found inside the directory 
				 Then put the nodes inside an array and return the array
	Returns    : The array
	Args       : $rh_nodes Ref to a hash (read from the xml file)
	             $ext      Extension (only js or css)

=cut

sub __ExtractJsCss
{
    my $self = shift;
    my ($rh_nodes, $ext) = @_;

    # @a_rh_list is a list of reference of hash, the list is SORTED according to the id of the xml tag
    my @a_rh_list;
    foreach my $k (sort keys %$rh_nodes)
    {

        # If it is a directory
        if (exists($rh_nodes->{$k}->{path}) && (-d $rh_nodes->{$k}->{path}))
        {
            my $dir = $rh_nodes->{$k}->{path};
            my $url = $rh_nodes->{$k}->{url};

            #my $id='A';
            foreach my $file (glob "$dir/*.$ext")
            {
                my %h_sub_node;

                #$h_sub_node{id}   = $k . '_' . $id;
                $h_sub_node{url}  = $url . basename($file);
                $h_sub_node{path} = $file;
                push @a_rh_list, \%h_sub_node;

                #$id=chr(ord($id)+1);
            }
        }
        else
        {
            push(@a_rh_list, $rh_nodes->{$k});
        }
    }
    return @a_rh_list;
}

=head2 procedure CreateDigestCssFiles

	Title      : CreateDigestCssFiles
	Usage      : $self->CreateDigestCssFile();
	Procedure  : For all cssgroup tags, create the file whose name is in the attribute file of the tag
	             NOTE - For security reasons, this method should be started from a companion program running with
	                    an identity DIFFERENT from the web server identity (www-data)
	Args       : none

=cut

sub CreateDigestCssFiles
{
    my $self = shift;

    my $rh_cssgroups = $$self{'__desc'}->{stylesheet}[0]->{cssgroup};
    foreach my $key (keys %$rh_cssgroups)
    {
        $self->__CreateDigestFile($rh_cssgroups->{$key}, 'css');
    }
}

=head2 procedure CreateDigestJsFiles

	Title      : CreateDigestJsFiles
	Usage      : $self->CreateDigestJsFile();
	Procedure  : For all jsgroup tags, create the file whose name is in the attribute file of the tag
	             NOTE - For security reasons, this method should be started from a companion program running with
	                    an identity DIFFERENT from the web server identity (www-data)
	Args       : none

=cut

sub CreateDigestJsFiles
{
    my $self = shift;

    my $rh_jsgroups = $$self{'__desc'}->{javascript}[0]->{jsgroup};
    foreach my $key (keys %$rh_jsgroups)
    {
        $self->__CreateDigestFile($rh_jsgroups->{$key}, 'js');
    }
}

=head2 procedure __CreateDigestFile

	Title      : __CreateDigestFile
	Usage      : $self->__CreateDigestFile();
	Procedure  : Starting from a hash representing a jsgroup or a cssgroup, extract the corresponding files
	             and copy those files to the file described by the group (file attribute of the xml node)
	Args       : $rh_nodes A ref to a hash representing a jsgroup/cssgroup
	             $ext      js or css

=cut

sub __CreateDigestFile
{
    my $self = shift;
    my ($rh_group, $ext) = @_;

    return unless defined $rh_group->{path};

    my $digest_file    = $rh_group->{path};
    my $fh_digest_file = new IO::File($digest_file, 'w') || croak("ERROR - Cannot create $digest_file");
    my @a_nodes        = $self->__ExtractJsCss($rh_group->{$ext}, $ext);
    foreach my $rh_node (@a_nodes)
    {
        my $file = $rh_node->{path};
        my $fh_file = new IO::File($file) || croak("ERROR - Cannot open $file");
        print $fh_digest_file "/* FILE $file */\n";
        my $buffer;
        while (my $line = <$fh_file>)
        {
            $buffer .= $line;
        }
        print $fh_digest_file "$buffer\n";
        $fh_file->close();
    }
    $fh_digest_file->close();
}

=head2 function __ValidBehaviour

	Title      :	__ValidBehaviour
	Usage      :	$behaviour_is_valid = &WebBuilder::__ValidBehaviour($behaviour);
	Prerequiste:	global $H_DEFBEHAVIOUR must have been declared, it's a function not $self arg !
	function   :	tell if the $behaviour is known/valid.
                	it is 'class restricted', nothing outside of this class have to use it
	Returns    :	boolean, 1 if $behaviour is known, 0 otherwise
	Args       :	$behaviour,	a string, the behaviour to judge
	globals    :	%H_DEFBEHAVIOUR, a hash of behaviours

=cut

sub __ValidBehaviour
{
    my $key = shift;
    return 1 if (exists $H_DEFBEHAVIOUR{$key});
    return 0;
}

=head2 procedure __Dump

	Title      :	__Dump
	Usage      :	$self->__Dump();
	Prerequiste:	none
	Procedure  :	Print the structure of the layout
	Returns    :	boolean, 1 if $behaviour is known, 0 otherwise
	Args       :	$behaviour,	a string, the behaviour to judge
	Globals    :	%H_DEFBEHAVIOUR, a hash of behaviours

=cut

sub __Dump
{
    my ($self) = @_;

    print Dumper($self);
    return;
}

=head2 procedure SetCharset

 Title      : SetCharset
 Usage      : $self->SetCharset($charset);
 Procedure  : Set the character encoding to the $charset, default used by WB is utf-8
              We check and die if the charset is NOT one of 'utf-8', 'iso-8859-1', 'iso-8859-15'
 Args       : $charset  the wanted character set
              $force (optional, default 0): If 1, no check is performed, let's hope the charset is correct
 Globals   :	none

=cut

sub SetCharset
{
    my $self = shift;
    my ($charset, $force) = @_;
    $force = 0 unless (defined $force);

    if ($force == 0)
    {
        my @a_allowed = ('utf-8', 'iso-8859-1', 'iso-8859-15');
        foreach my $cs (@a_allowed)
        {
            if ($charset eq $cs)
            {
                $self->{__charset} = $charset;
                return;
            }
        }
        $self->__Die("Unrecognized charset $charset", 'parameter', 'value');
    }
    else
    {
        $self->{__charset} = $charset;
        return;
    }
}

=head2 procedure SetMimeType

	Title      :	SetMimeType
	Usage      :	$self->SetMimeType('image/png');
	Prerequiste:	none
	Procedure  :	Set the mime type, default is text/html
	Returns    :	nothing
	Args       :	$mime_type   the wanted mime type 
	Globals    :	none

=cut

sub SetMimeType
{
    my $self = shift;
    my ($mime_type) = @_;

    $self->{__mime_type} = $mime_type;
}

=head2 procedure SetContentLength

 Title		:SetContentLength
 Usage		:$self->SetContentLength(1234);
 Procedure	:Set the content length, default is undef
 Args		:$content_length   the length of the returned message (in octets)
 Globals	:none

=cut

sub SetContentLength
{
	my $self = shift;
	my ($content_length) = @_;
	
	$self->{__content_length} = $content_length;
}

=head2 procedure __PrintMotd

	Title      :	__PrintMotd
	Usage      :	$self->__PrintMotd();
	Prerequiste:	should be called only from Realize()
	Procedure  :	print a div containing the Message Of The Day
	Args       :	none
	Globals    :	none

=cut

sub __PrintMotd
{
    my $self          = shift;
    my $rh_motd_files = $$self{__desc}->{motd};
    my @a_motd;
    foreach my $key (sort keys %$rh_motd_files)
    {
        my $rh_motd = $rh_motd_files->{$key};
        my $file    = $rh_motd->{path};
        if (defined $rh_motd->{param} && defined $rh_motd->{value})
        {
            my $param = $rh_motd->{param};
            my $value = $rh_motd->{value};
            next if ($self->GetParams($param) !~ /$value/);
        }
        my $fh_file = new IO::File($file) or next;
        my @a_f = <$fh_file>;
        chomp @a_f;
        push @a_motd, @a_f;
    }
    if (@a_motd != 0)
    {
        print "<div id=\"wb_motd\" >" . join('<br />', @a_motd) . "</div>\n";
    }
}

=head2 function __PrintDisabled

	Title      : __PrintDisabled
	Usage      : $self->__PrintDisabled()
	Prerequisite: should be called only from Realize()
	Procedure  : print the disabled msg and return 
	Args       : none
	Globals    : none

=cut

sub __PrintDisabled
{
    my $self = shift;
    my $msg;
    $msg = $self->{__disabled_msg};
    $msg .= '<br /><a href="">Try again</a>';
    if ($msg ne '')
    {
        print "<div id=\"wb_header2\" class=\"HideOnFullScreen\" >\n";
        print "<div id=\"wb_disabled\">$msg</div></div></body></html>\n";
    }
}

=head2 procedure __PrintPatience

	Title      :	__PrintPatience
	Usage      :	$self->__PrintPatience();
	Prerequiste:	should be called only from Realize()
	Procedure  :	print a div containing a message and/or an image
	            	that will be hidden, at the end of Realize
	Returns    :	nothing
	Args       :	none
	Globals    :	none

=cut

sub __PrintPatience
{
    my $self = shift;

    my $id = &PATIENCE_ID;
    $| = 1;    # STDOUT must be unbuffered

    my $msg     = $$self{'__desc'}->{patience}[0]->{msg};
    my $img_src = $$self{'__desc'}->{patience}[0]->{img}[0]->{src};
    my $img_alt = $$self{'__desc'}->{patience}[0]->{img}[0]->{alt};

    $msg     = ''         unless defined($msg);
    $img_src = ''         unless defined($img_src);
    $img_alt = 'patience' unless defined($img_alt);

    # no patience message
    return if ($msg . $img_src eq '');

    my $patience = "<div id='$id' style='display:none' >";
    if ($img_src . $img_alt ne '') {$patience .= "<img src='$img_src' alt='$img_alt' />";}
    if ($msg ne '') {$patience .= $msg;}
    $patience .= '</div>';

    print "$patience\n";
    print <<HTML
<script type="text/javascript">
<!--
var obj = document.getElementById("$id");
if (obj != undefined){obj.style.display = "block";};

//-->
</script>
HTML
      ;

    return;
}

=head2 procedure __PrintMessage

	Title      :	__PrintMessage
	Usage      :	$self->__PrintMessage($msg);
                    $self->__PrintMessage();
	Prerequiste:	should be called only from Realize()
	Procedure  :	print an error message
                    1st call the message is read from the parameters
                    2nd call the message is read from the private attribute __message
	Returns    :	nothing
	Args       :	none
	Globals    :	none

=cut

sub __PrintMessage
{
    my $self = shift;
    my ($msg) = (@_);

    $msg = $$self{'__message'} unless (defined($msg));
    print "<div class=\"wb_error\">" . $msg . "</div>";
}

=head2 procedure __HidePatience

	Title      :	__HidePatience
	Usage      :	$self->__HidePatience();
	Prerequiste:	should be called only into Realize()
	            	javascript should be enabled
	Procedure  :	Hide the "patience" div that contains a message and/or an image
	Returns    :	nothing
	Args       :	none
	Globals    :	none

=cut

sub __HidePatience
{
    my $self = shift;

    # 	my () = @_;

    my $id = &PATIENCE_ID;

    print <<HTML
<script type="text/javascript">
<!--
var obj = document.getElementById("$id");
if (obj != undefined){obj.style.display = "none";};

//-->
</script>
HTML
      ;

    return;
}

=head2 procedure __Substitute

	Title      :	__substitute
	Usage      :	$self->__Substitute()
	Prerequiste:	$self->{'__desc'} must be initialized
	Procedure  :	Perform the substitution:
                            -stringify $self->{'__desc'}
                            -substitute what has to be substituted, according to the substitution table
                            -create a new $self->{'__desc'}
	Returns    :	nothing
	Args       :	none
	Globals    :	none

=cut

sub __Substitute
{
    my ($self) = @_;
    return unless (exists $self->{'__substitution_table'});    # If no table, nothing to substitute

    # stringinfy $$self{'__desc'};
    local $Data::Dumper::Purity = 1;
    my $rh_xml = $$self{'__desc'};
    my $rh_xml_s;
    my $txt = Data::Dumper->Dump([$rh_xml], [qw(rh_xml_s)]);
    return if ($txt eq "");                                    # If value not defined, nothing to substitute

    my $rh_sub_table = $self->{'__substitution_table'};
    return unless ($txt =~ /%[a-zA-Z0-9]/);                    # If no %, nothing to substitute

    # perform the substitutions
    foreach my $s (keys(%$rh_sub_table))
    {
        next unless ($txt =~ /$s/);
        my $r = $rh_sub_table->{$s};
        if (defined $r)
        {
            if (ref($r) eq '')                                 # Not a ref at all
            {
                $txt =~ s/$s/$r/g;
            }
            elsif (ref($r) eq 'SCALAR')                        # Substitute if ref to a scalar
            {
                $txt =~ s/$s/$$r/g;
            }
            elsif (ref($r) eq 'CODE')                          # Substitute, calling the sub, if ref to a sub
            {
                my $subst = &$r($self);
                $txt =~ s/$s/$subst/g;                         # N.B. May be several substitutions, but only 1 call
            }
        }
    }

    # eval the substituted txt, creating a new object
    eval $txt;
    $self->{'__desc'} = $rh_xml_s;
    return;
}

=head2 SecurityMessage

  Title     : __SecurityMessage
  Usage     : $self->__SecurityMessage('message')
  Procedure : Send a security message and leave

=cut

sub __SecurityMessage
{
    my ($self, $msg) = @_;
    my $error = &HTTP_ERROR_SECURITY;
    print header(-status => $error);
    print start_html('Security Issue');
    print h3("ERROR " . &HTTP_ERROR_SECURITY . " : The request is not processed because of a security concern: $msg");
    print end_html;
    &Carp::croak("SECURITY ISSUE: Fatal error: > $msg <");
}

=head2 Die

  Title    : __Die
  Usage    : $this->__Die('message','type_of_exception',...)
  Procedure: Read the use_exceptions behaviour, and call croak or throw
  Args     : message                                  The error message
           : type_of_exception  (not used with die)   'io' or 'parameter'
             ...                (not used with die)   will be passed to the exception generated
             
=cut

sub __Die
{
    my $self = shift;
    my (@a_prms) = @_;

    if ($self->GetBehaviour('use_exceptions') == 1)
    {
        my $msg  = shift(@a_prms);
        my $type = shift(@a_prms);
        if ($type eq 'io')
        {

            #warn("coucou " . join(',', @a_prms));
            throw LipmError::IOException(@a_prms, -text => $msg, -user_value => $msg);
        }
        elsif ($type eq 'parameter')
        {
            throw LipmError::ParameterException($type, @a_prms, -text => $msg, -user_value => $msg);
        }
        else
        {
            &Carp::croak("ERROR - Don't know this exception: $type");
        }
    }
    else
    {
        &Carp::croak($a_prms[0]);
    }
}

=pod

=head1 COPYRIGHT NOTICE

This software is governed by the CeCILL license - www.cecill.info

=cut

1;
