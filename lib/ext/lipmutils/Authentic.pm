#
#   emmanuel.courcelle@toulouse.inra.fr
#	Created: Feb, 2010
#



=pod

=head1 NAME

Authentic - An abstract class for authentication-dedicated modules, using Roles Based authentification
	
=head1 SYNOPSIS

DO 	NOT USE directly, use derived classes only

use Authentic::EtcPasswd;
or
use authentic::OpenId;

=cut

package Authentic;
use base qw( LipmObject );
use LipmObject;
use warnings;
use strict;

use Error qw( :try );
use CGI qw/:standard/;
use Apache::Session::File;

use Data::Dumper;
use File::Spec;
use General;

# UTILE SEULEMENT POUR DEBOGGER LES APPLICATIONS AVEC LES MODULESShidLdap OU ShibEtcPasswd !!!
# COMMENTER POUR UTILISER EN PROD !!!
#$ENV { REMOTE_USER } = 'dupont';


use constant SESS_DIR => '/tmp/apache_sessions';
use constant LOCK_DIR => '/tmp/apache_sessions_lock';
use constant COOKIE_NAME => "authentic";
use constant COOKIE_EXPIRES => "+30m";
use constant LOG_TYPE => "L";

=head2 function _Init

 Title   : _Init (the constructor)
 Function: constructor
 Args    : %h_args The arguments (the leading - means optional argument):
           -workspace_parent: path to the parent directory of the workspace
                              If the derived objects do not initialize the workspace, the workspaece is computed from
                              this parameter and the login - see the method Getworkspace
           -cookie_name    : cookie name (def &COOKIE_NAME)
           -cookie_expires : Expiration date of the cookie/session (def +30m)
                             format = +integer[mhd] (minute, hour, days) - ex: +1h +1d +5m
                             WARNING - THE COOKIE IS A SESSION COOKIE, THUS NO EXPIRATION DATE !
                             However, the expiration date of the parameter cookie_expire is in fact the expiration date OF THE SESSION
           -cookie_secure  : default is 1 - When set to default, you MUST use https to read/write the cookie
                             DO NOT CHANGE THE DEFAULT VALUE other than in some demo programs, working on a local server

           -o_logger       : A logger object
            -log_type[L]   : The log type used to trace messages

=cut

sub _Init
{
	my $self = shift;
	my %h_args = (
		-workspace_parent => undef,
        -cookie_name    => &COOKIE_NAME,
        -cookie_expires => &COOKIE_EXPIRES,
        -cookie_secure  => 1,
        -o_logger       => undef,
        -log_type       => &LOG_TYPE,
        @_);
     
	# Init the base class
	$self->SUPER::_Init( %h_args );
	
	# Init private attributes (removing the '-' sign)
    foreach my $prm ( qw( -workspace_parent -cookie_name -cookie_expires -cookie_secure -log_type ) )
    {   
		my $attr = $prm;
		$attr =~ s/^-//;
		$attr = '__' . $attr;
		$self->_Set( $attr, $h_args{ $prm } );
	}
	
	# Init (empty values) other properties
	$self->_Set('_login','');
	$self->_Set('_cookie','');
	$self->_Set('_Authenticated',&FALSE);
	$self->_Set('_ra_roles',[ 0 ]);
	
	return $self;
}

sub _Trace
{
	my $self = shift;
	my ($msg,$stamp) = @_;
	$stamp = '*' unless defined $stamp;
	
	my $log_type = $self->_Get('__log_type');
	#warn "TRACE $msg $stamp $log_type ";
	$self->SUPER::_Trace($msg,$stamp,$log_type);
}
	
=head2 Function _AuthenticateFromSession

 Title	  : _AuthenticateFromSession
 Usage	  : print "Authenticated" if ($self->_AuthenticateFromSession() == &TRUE);
 Prerequisite : There should be a cookie on the client's browser, with the session id to retrieve
 Function : Extract the session id, open the session and retrieve from the session directory the information about the logged user
                
 Return	  : 1 => authentication is ok
 			0 => not authenticated (and no attribute changed)
 
=cut

sub _AuthenticateFromSession
{
	my $self = shift;

	my $cookie_name  = $self->_Get('__cookie_name');
	my $session_id   = cookie($cookie_name);
	
	if (!defined $session_id)
	{
		$self->_Trace("Starting _AuthenticateFromSession NO COOKIE FOUND\n");
		return &FALSE;
	}
	else
	{
		$self->_Trace("Starting _AuthenticateFromSession session=$session_id\n");
	}

	#warn "KOUKOU _AuthenticateFromSession = $session_id";

	my $rvl;
    try
    {
	    my %h_session;
	    tie %h_session, 'Apache::Session::File', $session_id, 
	    {
		  	Directory     => &SESS_DIR,
			LockDirectory => &LOCK_DIR
		};

		# refuse to work with too old sessions
		my $expiration_time = $h_session{'expires'}+0;
		if ( time() > $expiration_time )
		{
			throw Error ( 'Session has expired !' );
		}
		
	    $self->_Set('_common_name', $h_session{'common_name'});
	    $self->_Set('_identity_provider', $h_session{'identity_provider'});
	    $self->_Set('_ra_roles', $h_session{'ra_roles'});
		$self->_Set('_workspace', $h_session{'workspace'});
		$self->_Set('_email', $h_session{'email'});
		$self->_Set('_login', $h_session{'login'});
		$self->_Set('__session_id',$session_id);
		$self->_Set('_Authenticated',&TRUE);
		$self->_Trace("_AuthenticateFromSession: user " . $h_session{'login'} ." authenticated\n");
		$rvl = &TRUE;
	}
	catch Error with
	{
		# We could not tie to the session, so we clear the bad cookie !
		$self->_Set('_Authenticated',&FALSE);
		$self->_Trace("_AuthenticateFromSession: ERROR - could not find session $session_id or session too old\n");

		# clear the cookie
		my $cookie= cookie(
						-name    => $self->_Get('__cookie_name'),
						-value   => "disconnected",
						-expires => '-1d',
						#-domain => 'spe-bioinfo-b'
						#-path    => $self->{'__cookie_path'}
					);
		$self->_Set('_cookie',$cookie);
		$rvl = &FALSE;
	};	
	
	return $rvl;
}

=head2 function IsAuthenticated

=cut

sub IsAuthenticated
{
	my $self = shift;
	return $self->_Get('_Authenticated');
}

=head2 function HasRole

 Title    : HasRole
 Usage    : if ($o_auth->HasRole( 'admin,project1' ) {...};
 Function : Check if we have the role passed by parameter
            The check is done using regex-matching with \b, thus if one of the roles is:
            'admin,project1,demo':
            $o->auth->HasRole('admin,project1')      returns &TRUE
            $o->auth->HasRole('user,project1')       returns &FALSE
            $o->auth->HasRole('admin,project1,demo') returns &TRUE
 Arg      : $role (string): the role to check
 Return   : &TRUE / &FALSE

=cut

sub HasRole
{
    my $self = shift;
    my ($role) = @_;

	my $ra_roles = $self->_Get('_ra_roles');
    my $cnt = grep ( /\b${role}\b/, @$ra_roles );
    return ($cnt==0) ? &FALSE : &TRUE;
}

=head2 function IsPrivilege

 Title    : IsPrivilege
 Usage    : if ($o_auth->IsPrivilege( 'privilege_name' ) {...};
 Function : Check if we have the privilege passed by parameter
            We check ONLY the FIRST cell of the ra_roles array, using 'eq' (NOT regex)
            DO NOT USE FOR NEW CODE
 
 Return   : &TRUE / &FALSE

=cut
sub IsPrivilege
{
	my $self = shift;
	my ($priv) = @_;
	
	my $ra_roles = $self->_Get('_ra_roles');
	return ($priv eq $ra_roles -> [0]);
}


=item Function GetCookie
   Title    : GetCookie
   Usage    : my $o_cgi = $o_authentic->GetCookie();
   Function : Accessor to the $cookie object created
              This function is used by WebBuilder
   Return   : The cgi object
=cut
sub GetCookie
{
	my $self = shift;
	return $self->_Get('_cookie');
}

=item GetLogin,GetRoles,GetEmail,GetWorkspace

	my $login = $o_auth->GetLogin();

=cut
sub GetLogin
{
    my $self = shift;
    return $self->_Get('_login');
}
sub GetRoles
{
    my $self = shift;
    return $self->_Get('_ra_roles');
}
sub GetEmail
{
    my $self = shift;
    return $self->_Get('_email');
}
sub GetCommonName
{
    my $self = shift;
    return $self->_Get('_common_name');
}
sub GetWorkspace
{
    my $self = shift;
    return '' unless $self->IsAuthenticated();
    
    if ( $self->_Get('_workspace') eq '' && defined ($self->_Get('__workspace_parent')) )
    {
		return File::Spec->catfile($self->_Get('__workspace_parent'),$self->_Get('_login'));
	}
	else
	{
		return $self->_Get('_workspace');
	}
}

# NOTE - This makes sense only with Shib* subclasses !
sub GetIdentityProvider
{
	my $self = shift;
	return $self -> _Get('_identity_provider');
}

=head2 Function GetNameOrLogin

 Title	  : GetNameOrLogin
 Accessor : If the common name is set return the commoname, else return the login name
 
=cut
sub GetNameOrLogin
{
	my $self = shift;
	my $name = $self -> GetCommonName();
	$name    = $self -> GetLogin() if ( $name eq "");
	return $name;
}

=head2 Procedure Logout

 Title	  : Logout
 Usage    : $o_auth->Logout();
 Procedure: Destroy the session and clear the authentification attributes
 NOTE     : this method must be called BEFORE sending the http headers, so it must be called
            BEFORE Realize when using with WebBuilder
            See AuthenticDemo for an example
 
=cut
sub Logout
{
	my $self = shift;
	if ( $self->IsAuthenticated() == &TRUE )
	{
		$self->_Trace("User " . $self->_Get('_login') . " Logged out\n");        
		$self->_DeleteSession();
		$self->_Set('_ra_roles',[ 0 ]);
		$self->_Set('_workspace','');
        $self->_Set('_email','');
        $self->_Set('_login','');
        $self->_Set('_Authenticated',&FALSE);
	}
}

=head2 Function _NewSession

 Title	  : _NewSession
 Usage	  : $self->_NewSession();
 Prerequisite : The cookies should be enabled on the client browser
 Function : Create a new session and store the session id inside a cookie
 Args     : The session data
 
=cut
sub _NewSession
{
	my $self = shift;
	my ($login,$common_name,$ra_roles,$workspace,$email,$identity_provider) = @_;
	
	# NOTE - $identity_provider is NOT provided by non-shib subclasses
	$identity_provider = "" unless defined $identity_provider;
	
	if (! -d &SESS_DIR)
	{
		mkdir &SESS_DIR or die "ERROR - Cannot make the directory " . &SESS_DIR;
	}
	if (! -d &LOCK_DIR)
	{
		mkdir &LOCK_DIR or die "ERROR - Cannot make the directory " . &LOCK_DIR;
	}
	
	my %h_session;
	tie %h_session, 'Apache::Session::File', undef,
    { 
		Directory     => &SESS_DIR,
		LockDirectory => &LOCK_DIR
	};
	
	my $cookie_expires  = $self->_Get('__cookie_expires');
	my $session_expires = 0;
	
	if ( $cookie_expires !~ /^[0-9]+[mhd]$/ )
	{
		$cookie_expires = &COOKIE_EXPIRES;
	}
	
	# compute an expiration time for the session
	my ($cookie_expires_nb,$cookie_expires_unit) = ( $cookie_expires =~ /([0-9]+)([mhd])/ );
	my $mult = 1;
	$mult *= 60   if ( $cookie_expires_unit eq 'm' );
	$mult *= 3600 if ( $cookie_expires_unit eq 'h' );
	$mult *= 3600 * 24 if ( $cookie_expires_unit eq 'd' );
	$session_expires = time() + $cookie_expires_nb * $mult;
	
	$h_session{'common_name'}       = $common_name;
	$h_session{'identity_provider'} = $identity_provider;
	$h_session{'ra_roles'}          = $ra_roles;
	$h_session{'workspace'}         = $workspace;
	$h_session{'email'}             = $email;
	$h_session{'login'}             = $login;
	$h_session{'expires'}           = $session_expires;

	my $session_id = $h_session{_session_id};
	$self->_Set('__session_id',$session_id);

	# WARNING - THE -secure FLAG IS SET, SO THAT YOU MUST USE https !!!	
	# You may change this falg with Unsecure BUT DO NOT DO THAT, except for demo
	my $secure = $self->_Get('__cookie_secure');
	my $cookie= cookie(
					-name    => $self->_Get('__cookie_name'),
					-value   => $session_id,
					-secure  => $secure
				);

	$self->_Set('_cookie',$cookie);
	return &TRUE;
}

=head2 Function _DeleteSession

 Title	  : _DeleteSession
 Usage	  : $self->_DeleteSession();
 Prerequisite : The object should be authentified
 Procedure: Destroy the session and the associated cookie
 
=cut
sub _DeleteSession
{
	my $self = shift;
	
	if ($self->IsAuthenticated())
	{
		my $session_id = $self->_Get('__session_id');
		#warn "DELETING SESSION $session_id";
		my %h_session;
		tie %h_session, 'Apache::Session::File', $session_id,
	    { 
			Directory     => &SESS_DIR,
			LockDirectory => &LOCK_DIR
		};

		# Delete the session
		tied(%h_session) -> delete();
	
		# clear the cookie: overwrite the cookie with an out of date (and invalid) cookie
		my $cookie= cookie(
						-name    => $self->_Get('__cookie_name'),
						-value   => "disconnected",
						-expires => "-1d",
						-secure  => 1,
						#-domain => 'spe-bioinfo-b'

						#-path    => $self->{'__cookie_path'}
					);
		$self->_Set('_cookie',$cookie);
	}
}

=head2 Function _LdapExtractPrivilegesFromSsoRoles

 Title	  : _LdapExtractPrivilegesFromSsoRoles
 Usage	  : my $role = $self ->_LdapExtractPrivilegesFromSsoRoles($ssoRole,$application)
 Prerequisite : the privileges should be stored in an ldap server, using a sso schema
                (the schema was downloaded from http://lemonldap-ng.org/welcome/)
 Function : From the value of the ssoRole attribute, return a list of privileges
            Look for ou=roles in the dn found, and concatenate 
                 - All the levels under (the privilege)
                 - 1 level above (the application name)
            If $application if provided, return '' if the application field of the role ('demo' in the example under) is ne $application
            For ssoRoles = "ou=admin,ou=projet1,ou=roles,ou=demo,dc=example,dc=com"
                 return    "admin,projet1,demo"
 Args      : $role => A role 
 Return    : A privilege
 
=cut
sub _LdapExtractPrivilegesFromSsoRoles
{
	my $self = shift;
	my ($role,$application) = @_;
	
	# Remove spaces if any
	$role =~ s/ //g;
	
	my @a_input = split(',',$role);
	my @a_output;
	
	# Find the 'ou=roles' entry
	my $ou_roles=0;
	foreach my $field (@a_input)
	{
		last if $field eq 'ou=roles';
		$ou_roles++
	}
	
	# The n levels under ou=roles
	if ($ou_roles>0)
	{
		my $g=0;
		foreach my $field (@a_input)
		{
			if ($field =~ /^ou=(.+)/)
			{
				push @a_output, $1;
			}
			$g++;
			last if ($ou_roles == $g);
		}
	}
	
	# The 1 level above ou=roles
	my $application_field = "";
	if ($#a_input > $ou_roles)
	{
		for (my $i=$ou_roles + 1; $i <= $#a_input; $i++)
		{
			my $field = $a_input[$i];
			if ($field =~ /^ou=(.+)/)
			{
				$application_field = $1;
				last;
			}
		}
	}
	
	if ( defined($application) && $application ne $application_field )
	{
		return '';
	}
	else
	{
		push @a_output, $application_field;
		return join(',',@a_output);
	}
}

1;
