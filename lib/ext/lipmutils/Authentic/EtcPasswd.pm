#
# 	Sebastien.Carerre@toulouse.inra.fr
# 	Celine.noirot@toulouse.inra.fr
#   emmanuel.courcelle@toulouse.inra.fr
#	Created: March 08, 2006
#   Updated: Feb 08
#

=pod

=head1 NAME

Authentic::EtcPasswd - An object to manage authentification from an /etc/passwd like file

=head1 SYNOPSIS

    my $o_authentic = New Authentic::EtcPasswd (
                          file      => 'some_file.prv',
                          -login    => $login,
                          -password => $passwd
                      );
    
    If -login,-passwd are provided, $o_authentic checks them agains the values found in the .priv file
    It then creates a new session (using Apache::Session::File) and writes a cookie
    If they are NOT provided, $o_authentic tries to access the cookie to find an already opened session
    
=head1 DESCRIPTION

    The .priv file has the following format:
    cnoirot:b4ea23a368b20bc1623e058f392f1fe4:Céline Noirot:1:celine.noirot@inra.toulouse.fr:/www/LeARN_dev/web/tmp:
    
    The fields are separated by ':'
    field 1 (cnoirot) => The user name
    field 2 (b4ea..)  => echo -n $passwd | md5sum
    field 3           => Common Name
    field 4 (1)       => A privilege OR a list of roles, may be numbers (privileges) or ;-separated list of strings. The following are all correct:
                           1
                           2                  => numbers, you may manage privileges by levels
                           user,projet1,demo  => indicating a role (user) in an application context (projet1,demo)
                                                 You may have any number of fields, the separator is the coma (,)              
                                                 strings, you must test for equality or for regex match:
                                                 if ($o_authentic->HasRole('user'))                 ==> return &TRUE if you are 'user' in any project
                                                 if ($o_authentic->HasRole('user','project4,demo')) ==> return &TRUE if you are 'user' in 'project&,demo'
                           user,project1,demo;user,project2,demo => list of roles, the separator is the ;
                                                 CHARACTERS ALLOWED= [a-zA-Z0-9]
                           NOTE - Privilege = 0 IS RESERVED, means: NOT AUTHENTICATED
    field 5 (celi...) => The user's mail address
    field 6 (/www/...)=> A user's "home directory"
    field 7           => not used

=cut

package Authentic::EtcPasswd;
use base qw( Authentic );

use strict;
use warnings;

use IO::File;
use Digest::MD5 qw(md5 md5_hex md5_base64);
#use Data::Dumper;
use Error qw( :try );

use LipmError::IOException;
use General;


=head2 function _Init

 Title		  : _Init (the constructor)
 Usage		  : $o_auth= New EtcPasswd ( %h_args );
 Function	  : constructor
 Args		  : %h_args The arguments (the leading - means optional argument):
                   -login          : Login name
			       -password       : password    If (login,passwd) are specified, we create a new session and a new cookie
			                                     If (login,passwd) are NOT specified, we look for an existing cookie and session
			       file            : path to the passwd file
			       -workspace_parent: path to the parent directory of the workspace (see Authentic)			                          
			       -cookie_name    : cookie name (def &COOKIE_NAME)
			       -cookie_expires : Expiration date of the cookie (def +24h)
			       -o_logger       : A logger object

=cut

sub _Init
{
	my $self = shift;
	my %h_args = (
        -login    => undef,
        -password => undef,
        file      => undef,
        -encrypted_password => &FALSE,
        #-workspace_parent
        #-cookie_name    =>
        #-cookie_expires => 
        #-o_logger       => undef,
        @_);
     
    # Check required parameters
    foreach my $prm ( qw( file ) )
    {   
		die "Parameters missing" unless defined $h_args{ $prm };
	}
	
	# Init the base class
	$self->SUPER::_Init( %h_args);
	
	# Init private attributes (removing the '-' sign)
    foreach my $prm ( qw( -password file -encrypted_password) )
    {   
		my $attr = $prm;
		$attr =~ s/^-//;
		$attr = '__' . $attr;
		$self->_Set( $attr, $h_args{ $prm } );
	}
	
	# Init protected attributes (removing the '-' sign)
    foreach my $prm ( qw( -login ) )
    {   
		my $attr = $prm;
		$attr =~ s/^-//;
		$attr = '_' . $attr;
		$self->_Set( $attr, $h_args{ $prm } );
	}
	
	# Try several authentication methods, the first working is OK
	$self->_AuthenticateFromSession() or $self->__AuthenticateFromLogin(); 

	return $self;
}

=head2 Function __AuthenticateFromLogin

 Title	  : __AuthenticateFromLogin
 Usage	  : print "Authenticated" if ($self->__AuthenticateFromLogin() == &TRUE);
 Prerequisite : The _login and __password attributes exist and are NOT empty
 Function : Parse the password file checking if (login,password) is correct.
            Clear the __password private attribute
            Start a new session and set the cookie
            If OK, set a lot of attributes
                
 Return	  : 1 => authentication is ok
 			0 => not authenticated (and no attribute changed)
 
=cut

sub __AuthenticateFromLogin
{
	my $self = shift;
	
	my $login = $self->_Get('_login');
	my $passwd= $self->_Get('__password');
	my $encrypted_password= $self->_Get('__encrypted_password');

	
	if ($encrypted_password == &FALSE)
	{
		if (defined $passwd && $passwd ne '')
		{
			$passwd = md5_hex($passwd);
		}
	}

    #warn "KOUKOU $login $passwd ". $self->_Get('__password');
    
	# Password or login empty, not authenticated
	if ( (!defined ($login)) || (! defined ($passwd)) || $login eq '' || $passwd eq '')
	{
		$self->_Trace("Starting __AuthenticateFromLogin LOGIN or PASSWORD EMPTY => FALSE\n");
		return &FALSE;
	}

	# Check password from file
	my $file_etc    = $self->_Get('__file');
	my $fh_file_etc = new IO::File($file_etc) or throw LipmError::IOException($file_etc, 'f');
	
	my $fnd_flg = &FALSE;
	my ($common_name, $role, $workspace, $email);
    while (my $line = <$fh_file_etc>)
    {
        chomp($line);

        # blank lines are ignored
        next if ($line eq "");

        my @a_user = split(/:/, $line);

        if ($a_user[0] eq $login && $a_user[1] eq $passwd)
	    {
	        $fnd_flg     = &TRUE;
	        $common_name = $a_user[2];
			$role        = $a_user[3];
			$email       = $a_user[4];
			$workspace   = $a_user[5];
            last;
		}
	};

	# If found, Start a session, and set the attributes if OK
	my @a_roles = split /;/,$role;
	if ( $fnd_flg == &TRUE && $self->_NewSession($login,$common_name,\@a_roles,$workspace,$email) == &TRUE )
	{
		$self->_Trace("__AuthenticateFromLogin: user $login authenticated\n");
		$self->_Set('_common_name',$common_name);
	    $self->_Set('_ra_roles',\@a_roles);
		$self->_Set('_workspace',$workspace);
        $self->_Set('_email',$email);
        $self->_Set('_Authenticated',&TRUE);
        $self->_Set('__password','');	# Do not keep useless password inmemory
        return &TRUE;
	}
	else
	{
		$self->_Trace("__AuthenticateFromLogin: user $login NOT authenticated\n");
		$self->_Set('_common_name','');
		$self->_Set('_ra_roles',[ 0 ]); 
		$self->_Set('_workspace','');
        $self->_Set('_email','');
        $self->_Set('_login','');
        $self->_Set('_Authenticated',&FALSE);
        $self->_Set('__password','');	# Do not keep useless password inmemory
		return &FALSE;
	}
}

1;
