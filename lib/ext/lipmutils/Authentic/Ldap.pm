#
# 	Sebastien.Carerre@toulouse.inra.fr
# 	Celine.noirot@toulouse.inra.fr
#   emmanuel.courcelle@toulouse.inra.fr
#	Created: March 08, 2006
#   Updated: Feb 08
#

=pod

=head1 NAME

Authentic::Ldap - An object to manage authentification from an ldap server

=head1 SYNOPSIS

    my $o_authentic = New Authentic::Ldap (
                          host      => some_server,
                          base      => some_base,
			              -application => The application name (optional)
                          -port     => some_port,
                          -login    => $login,
                          -password => $passwd,
                          -workspace_parent => '/home',
                          -cookie_name      => cookie_name,
                          -cookie_expires   => expiration_date,
                          -o_logger         => $o_logger
                      );
    
    If -login,-passwd are provided, $o_authentic tries to authenticate using the ldap host
    It then creates a new session (using Apache::Session::File) and writes a cookie
    If they are NOT provided, $o_authentic tries to access the cookie to find an already opened session
    If the authentication is OK, it tries to find a valid role in the ldap server
    If -application is provided, ONLY the roles belonging to this applcation are kept
    If none is found, we are considered as NOT AUTHENTICATED
    
=head1 DESCRIPTION

    The ldap directory is searched (using an anonymous connection) with the filter (uid=$login)
    If found, we try to connect to the directory using the returned dn and $passwd, and we retrieve the object 
    Then we keep the information found in the following fields:
         -mail     (the mail address)
         -ssoRoles (the roles)
         -homeDirectory (the workspace) If not found see Authentic for the workspace determination

=cut

package Authentic::Ldap;
use base qw( Authentic );

use strict;
use warnings;

use Net::LDAP;
use IO::File;
use Digest::MD5 qw(md5 md5_hex md5_base64);
#use Data::Dumper;
use Error qw( :try );

use LipmError::IOException;
use General;


=head2 function _Init

 Title		  : _Init (the constructor)
 Usage		  : $o_auth= New EtcPasswd ( %h_args );
 Function	  : constructor
 Args		  : %h_args The arguments (the leading - means optional argument):
                   -login          : Login name
			       -password       : password   If (login,passwd) are specified, we create a new session and a new cookie
			                                    If (login,passwd) are NOT specified, we look for an existing cookie and session
			       host            : The ldap server (ldap.example.com)
			       -port           : The port
			       base            : The base to use in the server (dc=example,dc=com)
			       -host2          : The host used for authorization
			                             - If host2 not specified (undef), host is used (default behaviour)
			                             - If host2='', the authorization step is skipped, meaning that EVERY USER authenticated is AUTHORIZED
			       -base2, -port2  : used only if host2 is not undef and not ""
 		           -application    :  The application name (optional)
			       -workspace_parent: see path to the parent directory of the workspace (see Authentic)	
			       -cookie_name    : cookie name (def &COOKIE_NAME)
			       -cookie_expires : Expiration date of the cookie (def +24h)
			       -o_logger       : A logger object

=cut

sub _Init
{
	my $self = shift;
	my %h_args = (
        -login    => undef,
        -password => undef,
        host      => undef,
        base      => undef,
        -port     => undef,
		-application    => undef,
		-host2    => undef,
		-base2    => undef,
		-port2    => undef,
        #-cookie_name    =>
        #-cookie_expires => 
        #-o_logger       => undef,
        @_);
     
    # Check required parameters
    foreach my $prm ( qw( host base ) )
    {   
		die "Parameters missing" unless defined $h_args{ $prm };
	}
	
	# Init the base class
	$self->SUPER::_Init( %h_args);
	
	# Init private attributes (removing the '-' sign)
    foreach my $prm ( qw( -application -password host base -port -host2 -base2 -port2 ) )
    {   
		my $attr = $prm;
		$attr =~ s/^-//;
		$attr = '__' . $attr;
		$self->_Set( $attr, $h_args{ $prm } );
	}
	
	$self -> _Set( '__host2', $h_args{ host }) if ( ! defined ($h_args{ -host2 }) );
	$self -> _Set( '__base2', $h_args{ base }) if ( ! defined ($h_args{ -base2 }) ); 

	# Init protected attributes (removing the '-' sign)
    foreach my $prm ( qw( -login ) )
    {   
		my $attr = $prm;
		$attr =~ s/^-//;
		$attr = '_' . $attr;
		$self->_Set( $attr, $h_args{ $prm } );
	}
	
	# Try several authentication methods, the first working is OK
	$self->_AuthenticateFromSession() or $self->__AuthenticateFromLogin();

	return $self;
}

=head2 Function __AuthenticateFromLogin

 Title	  : __AuthenticateFromLogin
 Usage	  : print "Authenticated" if ($self->__AuthenticateFromLogin() == &TRUE);
 Prerequisite : The _login and __password attributes exist and are NOT empty
 Function : Try to authenticate using the ldap server
            Clear the __password private attribute
            Start a new session and set the cookie
            If OK, set a lot of attributes

 Return	  : 1 => authentication is ok
 			0 => not authenticated (and no attribute changed)
 
=cut

sub __AuthenticateFromLogin
{
	my $self = shift;
	
	my $login = $self->_Get('_login');
	my $passwd= $self->_Get('__password');

	# Password or login empty, not authenticated
	if ($login eq '' || $passwd eq '')
	{
		$self->_Trace("Starting __AuthenticateFromLogin LOGIN or PASSWORD EMPTY => FALSE\n");
		return &FALSE;
	}
	
	$self->_Trace("Starting __AuthenticateFromLogin login=$login\n");

	my $host = $self->_Get('__host');
	my $base = $self->_Get('__base');
	my $port = $self->_Get('__port');
	
	my (@a_roles,$role,$mail,$workspace);
	my $fnd_flg = &FALSE;
	
	# Create a ldap object
	my %h_opt;
	$h_opt{port} = $port if (defined $port);
	#$h_opt{scheme} = 'ldaps';
	my $ldap = new Net::LDAP ($host,%h_opt) or die "ERROR - Cannot reach host $host ($@)";
	
	# Bind anonymously
	my $mesg = $ldap->bind();
	$self->_Trace("__AuthenticateFromLogin: anonymous bind ldap $host returned " . $mesg->code . " " . $mesg->error . "\n");
	$mesg->code && die $mesg->error;
	
	# Search for the correct uid - we look for the dn and for some useful attributes
	$mesg = $ldap->search(
		base => $base,
		scope=> 'sub',
		filter => "(uid=$login)",
		attrs  => [ qw( dn ssoRoles mail homeDirectory) ]
	);
	
	my $error_uid = $mesg->code;
	$self->_Trace("__AuthenticateFromLogin: search ldap $host (base $base) for $login returned " . $mesg->code . " " . $mesg->error . "\n");

	# There was no error when we asked for the uid, but was there any result ?
	if ($error_uid == &FALSE)
	{
		my @a_all_entries = $mesg->all_entries;
		if (@a_all_entries > 0)
		{
			if (@a_all_entries != 1)
			{
				my $msg = "ERROR - There are " . @a_all_entries . " answers with uid = " .$login;
				$self->_Trace("$msg\n");
				die "$msg";
			}
			my $the_entry = $a_all_entries[0];
			my $dn = $the_entry -> dn();
		

			# Now that we have the dn, let's bind with the dn and the password
			my $mesg1 =  $ldap->bind($dn, password => $passwd);
			$self->_Trace("__AuthenticateFromLogin: $dn bind ldap $host returned " . $mesg1->code . " " . $mesg1->error . "\n");
			#warn "KOUKOU $dn $passwd ". $mesg1->code . " " . $mesg1->error;
			
			# DEBUG ONLY - Skip the authenticated step with forcing this var to 0 !
			my $passwd_not_correct = $mesg1->code;
			#my $passwd_not_correct = 0;
			
			$mesg1 = $ldap->unbind();
			$mesg1->code && die $mesg1->error;

			# If password validated, read attributes returned by the former request
			my $host2 = $self -> _Get ( '__host2' );
			my $base2 = $self -> _Get ( '__base2' );
			my $port2 = $self -> _Get ( '__port2' );

			if ( $passwd_not_correct == &FALSE )
			{
				if ( $host2 eq '' )
				{
					$fnd_flg = &TRUE;
				}
				else
				{
					
					# The mail and workspace is read from the first LDAP server
					$mail         = join(' ', &__ExtractAttributeValues($the_entry,'mail'));
					$workspace    = &__ExtractAttributeValues($the_entry,'homeDirectory');

					if ( $host ne $host2 )
					{
						my %h_opt2;
						$h_opt2{port2} = $port2 if (defined $port2);
						#$h_opt{scheme} = 'ldaps';
						my $ldap2 = new Net::LDAP ($host2,%h_opt2) or die "ERROR - Cannot reach host $host2 ($@)";
						
						# Bind anonymously to the 2nd ldap server
						my $mesg2 = $ldap2->bind();
						$self->_Trace("__AuthenticateFromLogin: anonymous bind ldap $host2 returned " . $mesg2->code . " " . $mesg2->error . "\n");
						$mesg2->code && die $mesg2->error;
	
						# Search for the correct uid - we look for the dn and for some useful attributes
						# NOTE - THE uid IS NOW THE MAIL RETURNED BY THE FIRST ldap SERVER !!!
						$mesg2 = $ldap2->search(
							base => $base2,
							scope=> 'sub',
							filter => "(uid=$mail)",
							attrs  => [ qw( ssoRoles ) ]
						);

						my @a_all_entries2 = $mesg2->all_entries;
						if (@a_all_entries2 > 0)
						{
							if (@a_all_entries2 != 1)
							{
								my $msg = "ERROR - There are " . @a_all_entries2 . " answers with uid = " .$mail;
								$self->_Trace("$msg\n");
								die "$msg";
							}
							$the_entry = $a_all_entries2[0];
						}
						else
						{
							$self->_Trace("__AuthenticateFromLogin: search ldap2 $host2 (base $base2) for $mail returned " . $mesg2->code . " " . $mesg2->error . "\n");
						}
					}

					@a_roles = &__ExtractAttributeValues($the_entry,'ssoRoles');
					my $application = $self-> _Get ( '__application' );
					foreach my $p (@a_roles)
					{
						$p = $self -> _LdapExtractPrivilegesFromSsoRoles($p,$application);
						$fnd_flg      = &TRUE if ( $p ne '');
					}
				}
			}
		}
		else
		{
			$self->_Trace("__AuthenticateFromLogin: no user $login found\n");
		}
	}
	else
	{
		$self->_Trace("__AuthenticateFromLogin: search ldap $host (base $base) for $login returned " . $mesg->code . " " . $mesg->error . "\n");
	}
	
	# If ldap validation OK, Start a session, and set the attributes if OK
	if ( $fnd_flg == &TRUE && $self->_NewSession($login,"",\@a_roles,$workspace,$mail) == &TRUE )
	{
		$self->_Trace("__AuthenticateFromShib: user $login authenticated - roles = " . join ( ';',@a_roles) . "\n");	
	    $self->_Set('_ra_roles',\@a_roles);
		$self->_Set('_workspace',$workspace);
        $self->_Set('_email',$mail);
        $self->_Set('_Authenticated',&TRUE);
        $self->_Set('__password','');	# Do not keep useless password in memory
        return &TRUE;
	}
	else
	{
		$self->_Trace("__AuthenticateFromLogin: user $login not NOT authenticated\n");
		$self->_Set('_ra_roles',[ 0 ]); 
		$self->_Set('_workspace','');
        $self->_Set('_email','');
        $self->_Set('_login','');
        $self->_Set('_Authenticated',&FALSE);
        $self->_Set('__password','');	# Do not keep useless password in memory
		return &FALSE;
	}
	
	# Extract the values for 1 attribute and return the array or the 1st element
	sub __ExtractAttributeValues
	{
		my ($entry,$attr) = @_;
		my @a_rvl;
		foreach my $value ($entry->get_value($attr))
		{
			push (@a_rvl, $value);
		}
		if (wantarray)
		{
			return @a_rvl;
		}
		else
		{
			return (defined $a_rvl[0])?$a_rvl[0]:'';
		}
	}
}		

1;
