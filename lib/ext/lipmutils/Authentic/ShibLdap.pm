#
#   emmanuel.courcelle@toulouse.inra.fr
#

=pod

=head1 NAME

Authentic::ShibLdap - An object to manage authentification from Shiboboleth AND ldap connection for autorization
                      It is similar to Authentic::Ldap, except that the passwords are NOT managed by this module
                      The login is read from the env variable REMOTE_USER if it exists, or HTTP_REMOTE_USER

=head1 SYNOPSIS

    my $o_authentic = New Authentic::ShibEtcLdap (
 			        host            => The ldap server (ldap.example.com)
			        -port           => The port
			        base            => The base to use in the server (dc=example,dc=com)
			        -application    => The application name (optional)
 			        shib_login_url  => The Shibboleth login url
			        shib_logout_url => The Shibboleth logout url
			        target_url      => The url to redirect to after login
			        -return_url     => the url to redirect to after logout (if '', use target_url)
			        -provider_name  => the url of the identity provider (if not specified, use the default wayf service)
			        -shib_login_attr=> The attribute used for login. Default is REMOTE_USER
			        -workspace_parent=> path to the parent directory of the workspace (see Authentic)			                          
			        -cookie_name    => cookie name (def &COOKIE_NAME)
			        -cookie_expires => Expiration date of the session (def +30m)
			                           SEE THE WARNING INSIDE THE Authentic.pm
			        -o_logger       => A logger object
                    );
    
    $authentic tries to authenticate from the session, then from Shibboleth (looking from the env variables),
    at last redirecting to the provider url, if this is given (-provider_id)
    If the authentication is OK, it tries to find a valid role in the ldap server
    If -application is provided, ONLY the roles belonging to this applcation are kept
    If none is found, we are considered as NOT AUTHENTICATED
    
   
=head1 DESCRIPTION

=cut

package Authentic::ShibLdap;
use base qw( Authentic );

use strict;
use warnings;

use IO::File;
use Digest::MD5 qw(md5 md5_hex md5_base64);
#use Data::Dumper;
use Error qw( :try );
use CGI qw/:standard/;

use Net::LDAP;
use LipmError::IOException;
use General;

# The providerIds which will be proposed by the integrated wayf
# They will be proposed in the order of @A_PROVIDER_NAMES, so that you may control the display order
# The Other option will redirect to the standard wayf, with all members - It is recognized by the 'wayf' reserved word
our @A_PROVIDERS = qw( INRA CNRS CIRAD Other... );
our %H_PROVIDERS = (INRA       => 'https://idp.inra.fr/idp/shibboleth',
					CNRS       => 'https://janus.cnrs.fr/idp',
					CIRAD      => 'https://idp.cirad.fr/idp/shibboleth',
					'Other...' => 'wayf' );

# The list of organizations we accept - If the organization is NOT in this list, authentication is refused !
# Organization means: "the right part of the eppn" ex. inra.fr, cnrs.fr, etc.
#
# NOTE - This array is the default value of the constructor parameter -authorized_orga
#        If the list is empty this test is NOT done
# 
our @A_AUTHORIZED_ORGA = qw( inra.fr cnrs.fr cirad.fr ird.fr );

=head2 function _Init

 Title		  : _Init (the constructor)
 Usage		  : $o_auth= New EtcPasswd ( %h_args );
 Function	  : constructor
 Args		  : %h_args The arguments (the leading - means optional argument):
 			        host            => The ldap server (ldap.example.com) - If host='', the authorization step is skipped,
 			                           meaning that EVERY USER authenticated by Shib is AUTHORIZED
			        -port           => The port
			        base            => The base to use in the server (dc=example,dc=com)
			        -application    => The application name (used in the roles discovery)
			        -default_role   => The default role assigned to a user if none is specified in the ldap
 			        shib_login_url  => The Shibboleth login url
			        shib_logout_url => The Shibboleth logout url
			        target_url      => The url to redirect to after login
			        -return_url     => the url to redirect to after logout (if '', use target_url)
			        -provider_name  => the name of the identity provider (if not specified, use the default wayf service)
			                           It must be one of the keys of %H_PROVIDERS, else it is ignored
			        -ra_authorized_orga=> The list of restricted organizations, default \@A_AUTHORIZED_ORGA
			                              If you pass [], no eppn are restricted 
			        -shib_login_attr=> The attribute used for login. Default is REMOTE_USER
			                           Possible values = MAIL, EPPN, ... may be other values (case does not matter)
			                           If NOT REMOTE_USER (default) the REAL attribute used is HTTP_MAIL (in uppercase)
			                           OR mail (in lower case) - See the code of __AuthenticateFromShib for the details
                    -workspace_parent=> path to the parent directory of the workspace (see Authentic)			                          
			        -cookie_name    => cookie name (def &COOKIE_NAME)
			        -cookie_expires => Expiration date of the session
			        -o_logger       => A logger object

=cut

sub _Init
{
	my $self = shift;
	my %h_args = (
		host            => undef,
		-port           => undef,
		-application    => undef,
		-default_role   => undef,
		base            => undef,
        shib_login_url  => '',
        shib_logout_url => '',
        target_url      => '',
        -return_url     => '',
        -provider_name  => '',
        -shib_login_attr=> 'REMOTE_USER',
        -ra_authorized_orga=> \@A_AUTHORIZED_ORGA,
        #-workspace_parent
        #-cookie_name    =>
        #-cookie_expires => 
        #-o_logger       => undef,
        @_);
     
    # Check required parameters
    foreach my $prm ( qw( host base ) )
    {   
		die "Parameters missing" unless defined $h_args{ $prm };
	}
	
	# Init the base class
	$self->SUPER::_Init( %h_args);
	
	# Init private attributes (removing the '-' sign)
    foreach my $prm ( qw( host base -port -application -default_role shib_login_url shib_logout_url target_url -return_url -provider_name -shib_login_attr -ra_authorized_orga ) )
    {   
		my $attr = $prm;
		$attr =~ s/^-//;
		$attr = '__' . $attr;
		$self->_Set( $attr, $h_args{ $prm } );
	}
	if ( $self -> _Get ( '__provider_name' ) ne '' && exists $Authentic::ShibLdap::H_PROVIDERS{ $self -> _Get ( '__provider_name' ) } )
	{
		my $provider_name = $self -> _Get ( '__provider_name' ) ;
		my $provider_id   = $Authentic::ShibLdap::H_PROVIDERS{ $provider_name };
		$self -> _Set ( '__provider_id',$provider_id );
	}
	
	# Try several authentication methods, the first working is OK
	$self->_AuthenticateFromSession() or $self->__AuthenticateFromShib() or $self->__AuthenticateFromProviderId($self->_Get('__provider_id'));

	return $self;
}

=head2 Function GetEppn
 Title    : GetEppn
 Usage    : my $eppn = $self -> GetEppn();
 Function : Returns the 'domain' part of the eppn, identifying the organization (the epst, unversite, etc)
 Return   : The eppn if authentified, undef if not authenticated
 Error    : Throw an error if we are authenticated but the EPPN is empty - SHOULD NOT HAPPEN, see __AuthenticateFromShib

=cut

sub GetEppn
{
	my $self = shift;
	my $eppn = undef;
	if ( $self -> IsAuthenticated() == &TRUE )
	{
		$eppn = $ENV { eppn } if ( defined $ENV { eppn } );
		$eppn = $ENV { HTTP_EPPN } if ( defined $ENV { HTTP_EPPN } );
		if ( defined ( $eppn) && $eppn eq '' )
		{
			throw Error::Simple ( "ERROR - eppn should NOT be blank !" );
		}
	}
	return $eppn;
}

=head2 Function GetOrganization
 Title    : GetOrganization
 Usage    : if ( $o_authentic -> GetOrganization() eq 'inra.fr' ) ...
 Function : Returns the 'domain' part of the eppn, identifying the organization (the epst, unversite, etc)
 Args     : $eppn The eppn: if undef, a call to GetEppn is performed (MUST BE AUTHENTICATED THUS)
 Return   : The organization if found, or undef if not authenticated
 Error    : Throw an error if we are authenticated but the EPPN is empty - SHOULD NOT HAPPEN, see __AuthenticateFromShib

=cut
sub GetOrganization
{
	my $self = shift;
	my ($organization) = @_;
	$organization = $self -> GetEppn() if (!defined $organization);
	
	if ( defined ($organization) && $organization ne '' )
	{
		$organization =~ s/^.+@//;   # Keep only the organization part of the eppn
	}
	return $organization;
}

=head2 Function __AuthenticateFromShib

 Title	  : __AuthenticateFromShib
 Usage	  : print "Authenticated" if ($self->__AuthenticateFromShib() == &TRUE);
 Prerequisite : Shibboleth is configured to authenticate the application
 Function : Look for REMOTE_USER, and if not found for HTTP_REMOTE_USER
            Start a new session and set the cookie
            If OK, set a lot of attributes
                
 Return	  : 1 => authentication is ok
 			0 => not authenticated (and no attribute changed)
 
=cut

sub __AuthenticateFromShib
{
	my $self = shift;
	
	my ($login,$email,$common_name,$identity_provider,$eppn) = ("","","","","");
	
	# The login_attribute is the attribute used to know the login
	# REMOTE_USER is still used to know if we are authentified
	my $login_attribute = $self -> _Get ( '__shib_login_attr' );
	if ( defined $ENV { REMOTE_USER } )
	{
		# Env variables are lower-cased, except if REMOTE_USER specified !
		if ( $login_attribute ne 'REMOTE_USER' )
		{
			$login_attribute = lc($login_attribute);
		}
		$login       = $ENV { $login_attribute };
		$email       = $ENV { mail };
		$common_name= $ENV { cn };
		$identity_provider = $ENV { Shib_Identity_Provider };
		$eppn        = $ENV { eppn } if (exists $ENV{ eppn });

	}
	elsif ( defined $ENV { HTTP_REMOTE_USER } )
	{
		# Env variables are HTTP_ . upper-cased of attribute !
		$login_attribute = 'HTTP_' . uc($login_attribute);
		$login       = $ENV { $login_attribute };
		$email       = $ENV { HTTP_MAIL };
		$common_name= $ENV { HTTP_CN };
		$identity_provider = $ENV { HTTP_SHIB_IDENTITY_PROVIDER };
		$eppn        = $ENV { HTTP_EPPN } if (exists $ENV{ HTTP_EPPN });
	}
    
	# login empty or eppn empty or not authorized ==> not authenticated
	if (!defined($login) || $login eq '')
	{
		$self->_Trace("__AuthenticateFromShib login EMPTY => FALSE\n");
		return &FALSE;
	}
	if ( !defined ($eppn) || $eppn eq '' )
	{
		$self->_Trace("__AuthenticateFromShib eppn EMPTY => FALSE\n");
		return &FALSE;
	}
	else
	{
		my $ra_authorized_orga = $self -> _Get ( '__ra_authorized_orga' );
		my $orga = $self -> GetOrganization($eppn);
		if ( @$ra_authorized_orga > 0 )
		{
			my $found_orga = &FALSE;
			foreach my $o ( @$ra_authorized_orga )
			{
				if ( $o eq $orga )
				{
					$found_orga = &TRUE;
					last;
				}
			}
			if ( $found_orga == &FALSE )
			{
				$self->_Trace("__AuthenticateFromShib orga $orga NOT AUTHORIZED => FALSE\n");
				return &FALSE;
			}
		}
		$self->_Trace("__AuthenticateFromShib login=$login - eppn=$eppn => TRUE\n");
	}

	# authentication OK - Looking for some privileges in ldap
	my (@a_roles,$role,$mail,$workspace);
	my $fnd_flg = &FALSE;
	my $host = $self->_Get('__host');
	
	# host = '', everybody is authorized if identified
	# WARNING ! We DO NOT LOOK FOR ANY workspace, email etc.
	if ( $host eq '' )
	{
		$fnd_flg      = &TRUE;
	}
	
	else
	{
		my $base = $self->_Get('__base');
		my $port = $self->_Get('__port');
		my $application  = $self-> _Get ( '__application' );
		my $default_role = $self-> _Get ( '__default_role' );
		
		# Create a ldap object
		my %h_opt;
		$h_opt{port} = $port if (defined $port);
		#$h_opt{scheme} = 'ldaps';
		my $ldap = new Net::LDAP ($host,%h_opt) or die "ERROR - Cannot reach host $host ($@)";
		
		# Bind anonymously
		my $mesg = $ldap->bind();
		$self->_Trace("__AuthenticateFromShib: anonymous bind ldap returned " . $mesg->code . " " . $mesg->error . "\n");
		$mesg->code && die $mesg->error;
		
		# Search for the correct uid - we look for the dn and for some useful attributes
		$mesg = $ldap->search(
			base => $base,
			scope=> 'sub',
			filter => "(uid=$login)",
			attrs  => [ qw( ssoRoles ) ]
		);
	
		my $error_uid = $mesg->code;
		
		# There was no error when we asked for the uid, but was there any result ?
		if ($error_uid == &FALSE)
		{
			my @a_all_entries = $mesg->all_entries;
			if (@a_all_entries > 0)
			{
				if (@a_all_entries != 1)
				{
					my $msg = "ERROR - There are " . @a_all_entries . " answers with uid = " .$login;
					$self->_Trace("$msg\n");
					die "$msg";
				}
				my $the_entry = $a_all_entries[0];
				my $dn = $the_entry -> dn();
	
				# Read the roles			
				@a_roles = &__ExtractAttributeValues($the_entry,'ssoRoles');
				foreach my $p (@a_roles)
				{
					$p = $self -> _LdapExtractPrivilegesFromSsoRoles($p,$application);	
					#$self->_Trace("KOUKOU2: $p\n") if ( $p ne '' );
					$fnd_flg      = &TRUE if ( $p ne '');	
				}
			}
			else
			{
				$self->_Trace("__AuthenticateFromShib: no user $login found\n");
			}
		}
		else
		{
			$self->_Trace("__AuthenticateFromShib: ldap search returned " . $mesg->code . " " . $mesg->error . "\n" );
		}

		# If no authorization found, look for a default role for this application
		if ( $fnd_flg==&FALSE && $application ne '' && $default_role ne '' )
		{
			my $r = $default_role . ',' . $application;
			push @a_roles, $r;
			$fnd_flg = &TRUE;
			$self->_Trace("__AuthenticateFromShib: using default role $r\n");
		}
	}
			
	# If found, Start a session, and set the attributes if OK
	if ( $fnd_flg == &TRUE && $self->_NewSession($login,$common_name,\@a_roles,$workspace,$email,$identity_provider) == &TRUE )
	{
		$self->_Trace("__AuthenticateFromShib: user $login authenticated - roles = " . join ( ';',@a_roles) . "\n");	
		$self->_Set('_common_name',$common_name);
		$self->_Set('_identity_provider',$identity_provider);
	    $self->_Set('_ra_roles',\@a_roles);
		$self->_Set('_workspace',$workspace);
		$self->_Set('_login',$login);
        $self->_Set('_email',$email);
        $self->_Set('_Authenticated',&TRUE);
        return &TRUE;
	}
	else
	{
		$self->_Trace("__AuthenticateFromShib: user $login NOT authenticated\n");
		$self->_Set('_common_name','');
		$self->_Set('_identity_provider','');
		$self->_Set('_ra_roles',[ 0 ]); 
		$self->_Set('_workspace','');
        $self->_Set('_email','');
        $self->_Set('_login','');
        $self->_Set('_Authenticated',&FALSE);
		return &FALSE;
	}

	# Extract the values for 1 attribute and return the array or the 1st element
	sub __ExtractAttributeValues
	{
		my ($entry,$attr) = @_;
		my @a_rvl;
		foreach my $value ($entry->get_value($attr))
		{
			push (@a_rvl, $value);
		}
		if (wantarray)
		{
			return @a_rvl;
		}
		else
		{
			return (defined $a_rvl[0])?$a_rvl[0]:'';
		}
	}
}

=head2 Function __AuthenticateFromProviderId

 Title	  : __AuthenticateFromLogin
 Usage	  : print "Authenticated" if ($self->__AuthenticateFromProviderId( $provider_url ) == &TRUE);
 Prerequisite : Shibboleth is configured to authenticate the application
 Function : Build a redirect url and redirect the browser to it
 Args     : $provider_url (optional): The url of the wanted provider if authentication wanted
            If $provider_url='wayf', the default wayf is specified
                
 Return	  : 1  if redirected
            0 if no provider_id specified, thus no authentification wanted
 
=cut
sub __AuthenticateFromProviderId
{
	my $self = shift;	
	my ($provider_url) = @_;
	$provider_url ="" unless (defined $provider_url);
	if ( $provider_url eq "" )
	{
		$self->_Trace("Starting __AuthenticateFromProviderId: NO AUTHENTICATION WANTED\n");
		return &FALSE;
	}
	else
	{
		# check the validity of some private variables (better doing now than in _Init, for performance reasons)
		foreach my $prm ( qw( __shib_login_url __target_url) )
		{
			if ( $self -> _Get($prm) eq '' )
			{
				throw Error::Simple ( "ERROR - $prm should NOT be blank !" );
			}
		}
	
		if ( $self->IsAuthenticated() == &FALSE )
		{
			my $shib_url     = $self -> _Get ( '__shib_login_url' );
			my $target_url   = $self -> _Get ( '__target_url' );
			my $redirect_url = $shib_url . '?target=' . $target_url;
			if ( $provider_url ne 'wayf')
			{
				$redirect_url .= '&providerId=' . $provider_url;
			}
	
			$self->_Trace("Starting __AuthenticateFromProviderId: redirecting to $redirect_url\n");
			print redirect($redirect_url);
		}
		return &TRUE;
	}
}

=head2 Function GetLoginForm

	Title    : GetLoginForm
	Usage    : print $o_authentic -> GetLoginForm($o_webbuilder);
	           Should be called from a callback for WebBuilder's Realize function
	Function : Build and return an integrated Wayf form, then redirect to the Shibboleth's lazy session url
	Args     : $o_webbuilder The calling object
	Globals  : %H_PROVIDERS, @A_PROVIDERS (the list of proposed identity providers)

=cut	
sub GetLoginForm
{
	my $self = shift;
    my ($o_webbuilder) = @_;
    
    my $rvl = "";
    if ($self->IsAuthenticated() == &FALSE)
    {
		$rvl .= '<h1>Login using the "Fédération d\'Identités Education-Recherche"</h1>';
		my $str_input = '<p><select name="provider_name">';
		foreach my $prov ( @Authentic::ShibLdap::A_PROVIDERS )
		{
			$str_input .= "<option value=\"$prov\" >$prov</option>";
		}
		$str_input .= '</select></p>';
		$str_input .= '<p><input type="submit" /></p>';
        $rvl .= (defined $o_webbuilder) ? $o_webbuilder->GetForm("", $str_input) : $str_input;
    }
    else
    {
        $rvl .= "You are now connected";
    }
    return $rvl;
}

=head2 Procedure Logout

 Title	  : Logout
 Usage    : $o_auth->Logout($url);
 Procedure: Call the superclass's Logout, terminating the local session
            Then redirect to the Shibboleth's logout url
 Args     : $shib_url The shibboleth logout url
			$return_url The url to be passed to the return= parameter
 
=cut
sub Logout
{
	my $self = shift;
	
	if ( $self->IsAuthenticated() == &TRUE )
	{
		# check the validity of some private variables (better doing now than in _Init, for performance reasons)
		foreach my $prm ( qw( __shib_logout_url __target_url) )
		{
			if ( $self -> _Get($prm) eq '' )
			{
				throw Error::Simple ( "ERROR - $prm should NOT be blank !" );
			}
		}
		my $return_url = ($self->_Get('__return_url') ne '') ? $self->_Get('__return_url') : $self->_Get('__target_url');
		my $shib_url   = $self->_Get('__shib_logout_url');

		$self->SUPER::Logout();
		$shib_url .= "?return=$return_url";
		print redirect($shib_url);
	}
}

1;
