#
# 	olivier.stahl@toulouse.inra.fr
#	Created: May 15, 2007
#	Last Updated: december 03, 2008
#
# Copyright INRA/CNRS

# Emmanuel.Courcelle@toulouse.inra.fr
# Jerome.Gouzy@toulouse.inra.fr
# Thomas.Faraut@toulouse.inra.fr

# This software is a computer program whose purpose is to provide a
# web-based interface for analyzing the different levels of genome
# conservations.

# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".

# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.

# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

package LipmError::IOException;

=pod

=head1 NAME

 LipmError::IOException - a class to define specific exception of Input/Output.

=head1 SYNOPSIS

 throw LipmError::IOException( 'my_file', 'f', -user_msg => $message );
 throw LipmError::IOException( 'my_file', 'file', -text => $dbg_info );
 throw LipmError::IOException( 'my_dir', 'd' );
 throw LipmError::IOException( 'my_dir', 'dir' );
 catch LipmError::IOException with {...}

=head1 DESCRIPTION

 With this class you can describe all type of exception encountered by Input/Output.

=head2 ERROR MESSAGES

 keyword			=>	message

 'default'		=>	"no IO error has been found",
 'exist'			=>	"exists",
 '!exist'		=>	"does not exist",
 '!readable'		=>	"is not readable",
 '!writable'		=>	"can't be written",
 '!executable'	=>	"is not executable",
 'file'			=>	"is a file",
 '!file'			=>	"is not a file",
 'dir'			=> 	"is a directory",
 '!dir'			=>	"is not a directory",
 'pipe'			=>	"is a pipe",
 '!pipe'			=>	"is not a pipe",
 'empty'			=>	"this file is empty"

=head1 SUBROUTINES

 new
 ErrorTypeToString
 TestDir
 TestFile
 TestIO

=cut

use strict;
use warnings;

## this class inherit from LipmError.pm
use base qw(LipmError);

BEGIN
{
	our $VERSION = do {my @r = (q$Rev: 3212 $ =~ /\d+/g); $r[0]};
}

my %h_msg	=	(	'default'	=>	"unknown IO error has been found",
					'exist'		=>	"exists",
					'!exist'	=>	"does not exist",
					'!readable'	=>	"is not readable",
					'!writable'	=>	"can't be written",
					'!executable'=>	"is not executable",
					'file'		=>	"is a file",
					'!file'		=>	"is not a file",
                    '!not_a_filename' => "is not a file name",
					'dir'		=> 	"is a directory",
					'!dir'		=>	"is not a directory",
					'pipe'		=>	"is a pipe",
					'!pipe'		=>	"is not a pipe",
					'empty'		=>	"this file is empty",
					'!createW'	=>	"this file can't be created, you don't have the permission.",
					'!createE'	=>	"this file already exists, you don't have the permission to overwrite it."
				);

##################################################
###########			Constructor		 #############
##################################################
=head2 Function new

 Title        : new
 Usage        : throw LipmError::IOException($io_file [,$test_type])
 Function     : Constructor
 Returns      : an Error object that should be catched
 Args         : $io_file
                $test_type	=> 'f'|'file'  or  'd'|'dir'|'directory'
 Globals      : none

=cut

sub new
{
	my $class = shift;
	my ($io_file,$test_type) = (shift,shift);
	my @args = (@_);        # @_ to propagate other options
	my $keyword = "";
	my $bool_arg_error = 0;

	# Test parameters :
	$bool_arg_error = 1 if (!defined $io_file);
	if (!defined $test_type) {
		$keyword = $class->TestIO($io_file);
	}
	elsif ($test_type =~ /f|file/) {
		$keyword = $class->TestFile($io_file);
	}
	elsif ($test_type =~ /d|dir|directory/) {
		$keyword = $class->TestDir($io_file);
	}
	elsif ($test_type =~ /c|create|creation/) {
		$keyword = $class->TestCreate($io_file);
	}
	else {
		$bool_arg_error = 1;
	}

	# Set stacktrace parameters
# 	local $Error::Depth = $Error::Depth + 1;
# 	local $Error::Debug = 1;

	# Construct the exception object
	my $self;
	if ($bool_arg_error == 1){
		$self = $class->SUPER::new(@args);
	}else {
		$self = $class->SUPER::new(	-handler	=> $io_file,
									-keyword	=> $keyword,
									@args );
		$self->{ _debug } = "$io_file $h_msg{$keyword}\n";
	}

	return $self;
}


##################################################
###########			Function		 #############
##################################################

=head2 Function text

 Title        : text
 Usage        : $o_error->text
 Function     : Useful when this error is generically treated
 Returns      :	some text describing the error
 Args         :	none
 Globals      : none

=cut
sub text
{
    my $self = shift;
    
    my $msg =  "Lipm::IOException - File = " . $self->{-handler} . " keyword = " . $self->{-keyword};
    $msg .= " - " . $self->{-text} if (defined $self->{-text});
    return $msg;
}

=head2 Function ErrorTypeToString

 Title        : ErrorTypeToString
 Usage        : $string = LipmError::IOException -> ErrorTypeToString($keyword);
 Prerequisite : none
 Function     : return the explanation corresponding to an error type
 Returns      :	the string corresponding to the error type explanation
 Args         :	$keyword -> the error type keyword
 Globals      : none

=cut

sub ErrorTypeToString {
	my ($class,$keyword) = (shift,shift) or return;
	my $error_type = $h_msg{$keyword} or return "Unknown error keyword.";

	return $error_type;
}


=head2 Function TestIO

 Title        : TestIO
 Usage        : $error_type = $self -> TestIO($io_file);
 Prerequisite : none
 Function     : test the io_file and return the error type
 Returns      :	the keyword corresponding to the error type
 Args         :	$io_file -> the name of the handler to test
 Globals      : none

=cut

sub TestIO {
	my ($self, $io_file) = (shift, shift);
	my $error_type = 'default';

    if ((!defined $io_file) or (ref $io_file ne '')){
        return $error_type = '!not_a_filename';
    }
	if ( !-e $io_file ){
		return $error_type = '!exist';
	}
	elsif ( !-r $io_file ){
		return $error_type = '!readable';
	}
	elsif ( !-w $io_file ){
		return $error_type = '!writable';
	}

	return $error_type;
}


=head2  TestCreate

 Title        : TestCreate
 Usage        : $error_type = $self -> TestCreate($io_file);
 Prerequisite : none
 Procedure    : test the existence and return an error
 Returns      :	the keyword corresponding to the error type
 Args         :	$io_file -> the name of the handler to test
 Globals      : none

=cut
sub TestCreate {
	my ($self, $io_file) = (shift, shift);
	my $error_type = 'default';

	if ( -e $io_file && !-w $io_file ){
		return $error_type = '!createE';
	}
	elsif ( !-e $io_file && !-w $io_file ){
		return $error_type = '!createW';
	}

	return $error_type;
}


=head2  TestFile

 Title        : TestFile
 Usage        : $error_type = $self -> TestFile($io_file);
 Prerequisite : none
 Function     : test the io_file to return specific file errors
 Returns      :	the keyword corresponding to the error type
 Args         :	$io_file -> the file to test
 Globals      : none

=cut

sub TestFile {
	my ($self, $io_file) = (shift, shift) or return;
	my $error_type = 'default';
	my $primary_test = $self->TestIO($io_file);

	return $primary_test if ( $primary_test eq '!exist' or $primary_test eq '!not_a_filename' );

	if ( -f $io_file ) {
		$error_type = $primary_test unless ( $primary_test eq 'default' );
		$error_type = 'empty'		if ( -z $io_file && $primary_test eq 'default' );
	} else {
		$error_type = '!file';
	}

	return $error_type;
}


=head2 Function TestDir

 Title        : TestDir
 Usage        : $error_type = $self -> TestDir($io_dir);
 Prerequisite : none
 Function     : test the io_dir to return specific directory errors
 Returns      :	the keyword corresponding to the error type
 Args         :	$io_dir -> the directory to test
 Globals      : none

=cut

sub TestDir {
	my ($self, $io_dir) = (shift, shift) or return;
	my $error_type = 'default';
	my $primary_test = $self->TestIO($io_dir);

	return $primary_test if ( $primary_test eq '!exist' or $primary_test eq '!not_a_filename' );

	if ( !-d $io_dir ) {
		$error_type = '!dir';
	}

	return $error_type;
}

1;

__END__

=pod

=head1 AUTHOR

 Olivier Stahl    : olivier.stahl@toulouse.inra.fr
 Sebastien Letort : sebastien.letort@toulouse.inra.fr

=head1 COPYRIGHT NOTICE

This software is governed by the CeCILL license - www.cecill.info

=cut
