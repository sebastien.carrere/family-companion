#
#	sebastien.letort@toulouse.inra.fr
#	Created: May 09, 2007
#	Last Updated: december 03, 2008
#
# Copyright INRA/CNRS

# Emmanuel.Courcelle@toulouse.inra.fr
# Jerome.Gouzy@toulouse.inra.fr
# Thomas.Faraut@toulouse.inra.fr

# This software is a computer program whose purpose is to provide a
# web-based interface for analyzing the different levels of genome
# conservations.

# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".

# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.

# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

package LipmError::ObjectException;

=pod

=head1 NAME

 LipmError::ObjectException - a class to define specific exception of Objects

=head1 SYNOPSIS

 throw LipmError::ObjectException( $class_name, KEYWORD, -text => $msg );
 throw LipmError::ObjectException( $obj, KEYWORD, -text => $msg, -user_msg => $x );
 catch LipmError::ObjectException with {...}

=head1 DESCRIPTION

 With this class you can describe all type of exception encountered by object.

 - try to access to an attribute that doesn't exist
 - an attribute doesn't have a correct value
 - you try to instanciate in a wrong way

=cut

use strict;
use warnings;

## this class inherit from LipmError.pm
use base qw(LipmError);

BEGIN
{
	our $VERSION = do {my @r = (q$Rev: 2399 $ =~ /\d+/g); $r[0]};
}

# closure to make sure that %_h_msg will not be changed by other classes
# 	should i use constant ?
{

	my %_h_msg = ( '!build'     => "object cannot be built.",
	               '!attribute' => "trying to access an undefined attribute",
	               'abstract method' => "trying to use an undefined method.",
	               'abstract class'  => "An interface cannot be instantiated.",
	               'x'          => "'no information transmitted' _can't guess what is wrong."
	             );

	sub _GetMessage
	{
		my ( $keyword ) = @_;
		$keyword = 'x' if (!defined $keyword);

		my $message = $_h_msg{$keyword};

# experimental : mettre dans le tronc si ok
		if( $keyword eq 'abstract method' )
		{
			my( $class, $file, $line, $func ) = caller( 3 );
			$message .= "\nIn file $file, you call $func which is not defined.";
		}
		return $message;
	}
}


=head2 Object new

 Title        : new
 Usage        : my $o_err = new LipmError::ObjectException( $obj|$class_name, KEYWORD );
 Prerequisite : it's better for an error to be catch !
 Object       : Constructor, initialize the object
 Returns      : the object.
 Args         : $obj, any blessed object
                $class, a class name that have a problem with one of this object.
 Globals      : none

=cut

sub new
{
	my $class = shift;
	my ($arg_un, $keyword) = (shift, shift);
	my %h_param = ( -depth     => 0,
	                @_ );

	local $Error::Depth = $Error::Depth + 1 + $h_param{ -depth };
# 	local $Error::Debug = 1;  # Enables storing of stacktrace

	# if arg_un is a scalar, it's the class name
	my $tmp = ref( $arg_un );
	my ( $class_name, $obj ) = ($tmp eq '') ? ($arg_un, undef) : ($tmp, $arg_un);

	# keyword
	my $text = $class_name . ' ' . &_GetMessage($keyword) . "\n";

	my $self = $class->SUPER::new( -object => $obj,	# pas sur que ca serve !
	                               -class => $class_name,
	                               %h_param);
	$self->{ _debug } = $text;


	return $self;
}


=head1 AUTHOR

 Sebastien Letort : sebastien.letort@toulouse.inra.fr
 Olivier Stahl    : olivier.stahl@toulouse.inra.fr

=cut

1;
