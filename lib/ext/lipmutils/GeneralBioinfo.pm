#
# $Id: GeneralBioinfo.pm 1039 2012-11-22 15:48:55Z sallet $
#

# Copyright INRA/CNRS

# Emmanuel.Courcelle@toulouse.inra.fr
# Jerome.Gouzy@toulouse.inra.fr
# Sebastien.Carrere@toulouse.inra.fr
# Erika.Sallet@toulouse.inra.fr
# Ludovic.Cottret@toulouse.inra.fr


# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".

# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.

# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

package GeneralBioinfo;

BEGIN
{
    use Exporter ();
    use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

    $VERSION = do {my @r = (q$Rev: 1039 $ =~ /\d+/g); sprintf "%d." . "%02d" x $#r, @r};

    @ISA = qw(Exporter);
    @EXPORT =
      qw(MinMaxStrand RevSeq CompSeq RevCompSeq ExtractSeq FormatSeq PrintScale CleanSeq Histo ParseM8Line ParseGFF3Line ParseBedLine FastaToHash HashToFasta TabToHash);
    %EXPORT_TAGS = ();    # ex. : TAG => [ qw!name1 name2! ],
    @EXPORT_OK =
      qw(MinMaxStrand RevSeq CompSeq RevCompSeq ExtractSeq FormatSeq PrintScale CleanSeq Histo ParseM8Line ParseGFF3Line ParseBedLine FastaToHash HashToFasta TabToHash);
}

use Carp;
use General;

use constant LINE_LENGTH => 60;    # Default line length for fasta file

=head2 function MinMaxStrand

  Title        : MinMaxStrand
  Usage        : my ($start, $end, $strand) = &MinMaxStrand($pos1, $pos2);
  Prerequisite : $pos1 and $pos2 are integer
  Function     : From a couple of positions on a sequence, return the lower bound, then
                 the upper bound, then the computed strand.
  Returns      : $start: lower bound
                 $end: upper bound
                 $strand: the strand (nomenclature '+/-')
  Args         : $pos1, $pos2: positions on a sequence
  Globals      : none

=cut

sub MinMaxStrand
{
    my ($pos1, $pos2) = @_;

    return ($pos1 <= $pos2) ? ($pos1, $pos2, '+') : ($pos2, $pos1, '-');
}

=head2 function RevSeq

  Title        : RevSeq
  Usage        : my $rev_seq = &RevSeq(\$seq)
  Prerequisite : $seq is a string
  Function     : Reverse the sequence
  Returns      : the reverse sequence
  Args         : $r_seq : reference to a string
  Globals      : none

=cut

sub RevSeq
{
    my ($r_seq) = @_;

    my $rev_seq = reverse($$r_seq);

    return $rev_seq;
}

=head2 function CompSeq

  Title        : CompSeq
  Usage        : my $comp_seq = &CompSeq(\$seq)
  Prerequisite : $seq is a string
  Function     : Complement the sequence
  Returns      : the complemented sequence
  Args         : $r_seq : reference to a string
                 $is_rna :  [Optional] If defined, complement the sequence as RNA
  Globals      : none

=cut

sub CompSeq
{
    my ($r_seq, $is_rna) = @_;

    my $comp_seq = $$r_seq;

    # Case RNA: A --> U
    if (defined $is_rna)
    {
        $comp_seq =~ tr/ATGCUatgcu/UACGAuacga/;
    }
    else
    {

        # Case DNA
        $comp_seq =~ tr/ATGCUatgcu/TACGAtacga/;
    }

    return $comp_seq;
}

=head2 function RevCompSeq

  Title        : RevCompSeq
  Usage        : my $revcomp_seq = &RevCompSeq(\$seq)
  Prerequisite : $seq is a string
  Function     : Reverse complement the sequence
  Returns      : the reversed complemented sequence
  Args         : $r_seq  : reference to a string
                 $is_rna : [Optional] If defined, reverse complement the sequence as RNA
  Globals      : none

=cut

sub RevCompSeq
{
    my ($r_seq, $is_rna) = @_;

    my $revcomp_seq = reverse($$r_seq);

    # Case RNA: A --> U
    if (defined $is_rna)
    {
        $revcomp_seq =~ tr/ATGCUatgcu/UACGAuacga/;
    }
    else
    {

        # Case DNA
        $revcomp_seq =~ tr/ATGCUatgcu/TACGAtacga/;
    }

    return $revcomp_seq;
}

=head2 function ExtractSeq

  Title        : ExtractSeq
  Usage        : my $extracted_seq = &ExtractSeq (\$seq, 10, 100)
                 my $extracted_seq = &ExtractSeq (\$seq, 100, 10)
                 my $extracted_seq = &ExtractSeq (\$seq, 10, 100, '-')
  Prerequisite : $seq is a string. 
                 If $strand is defined, $start has to be lower than $end!
  Function     : Extract the subsequence start-end from $seq. 
                 Reverse complement the sequence if strand is equal to '-' or 
                 if start > end
  Returns      : the subsequence
  Args         : $r_seq  : reference to a string
                 $start  : start position of the region to extract
                 $end    : end position of the region to extract
                 $strand : [Optional] strand of the region to extract
  Globals      : none

=cut

sub ExtractSeq
{
    my ($r_seq, $start, $end, $strand) = @_;

    my $len_seq = length($$r_seq);
    if ($start < 0 || $end < 0 || $start > $len_seq || $end > $len_seq)
    {
        Carp::croak("Error: start ($start) or stop ($end) is out of range [1-$len_seq]\n");
    }
    if (defined($strand) && $start > $end)
    {
        Carp::croak("Error: when strand is defined, start value has to be lower than end value\n");
    }

    my ($min, $max, $computed_strand) = &MinMaxStrand($start, $end);

    my $seq_len = $max - $min + 1;
    my $sub_seq = substr($$r_seq, $min - 1, $seq_len);
    return $sub_seq if (   (defined($strand) && $strand eq '+')
                        || (!defined($strand) && $computed_strand eq '+'));
    return &RevCompSeq(\$sub_seq);
}

=head2 function FormatSeq

  Title        : FormatSeq
  Usage        : my $format_seq = &FormatSeq(\$seq)
  Prerequisite : $seq is a string
  Function     : Format the sequence: write $line_length characters per line.
  Returns      : the formated sequence
  Args         : $r_seq : reference to a string
                 $line_length [Optional] : number of character per line. 
                                           Default value is LINE_LENGTH 
  Globals      : LINE_LENGTH is a constant value

=cut

sub FormatSeq
{
    my ($r_seq, $line_length) = @_;

    $line_length = &LINE_LENGTH if (!defined($line_length));

    my $new_seq = $$r_seq;
    $new_seq =~ s/(\w{$line_length})/$1\n/g;
    chomp($new_seq);

    return $new_seq;
}

=head2 function PrintScale

  Title        : PrintScale
  Usage        : my $scale = &PrintScale(10)
  Prerequisite : none
  Function     : Return a string containing a scale for a sequence which has 
                 a length of $length_line
                 Example
                 ........10........20........30........40........50........60
                          |         |         |         |         |         | 
  Returns      : A string of sequence scale.
  Args         : $length_line : length of the string
  Globals      : none

=cut

sub PrintScale
{
    my ($length_line) = @_;

    my $scale_sup = '';
    my $scale_inf = '';

    for (my $i = 1; $i <= $length_line; $i++)
    {
        if ($i % 10 == 0)
        {

            $scale_sup = substr($scale_sup, 0, -1 * (length($i) - 1));
            $scale_sup .= $i;
            $scale_inf .= '|';
        }
        else
        {
            $scale_sup .= '.';
            $scale_inf .= ' ';
        }
    }

    return $scale_sup . "\n" . $scale_inf;
}

=head2 procedure CleanSeq

  Title        : CleanSeq
  Usage        : CleanSeq(\$seq, "DNA")
                 CleanSeq(\$seq)
  Prerequisite : $seq is a string
  Procedure    : Remove to the sequence the non recognized characters. 
                 If mode == DNA, remove all non DNA nucleotide characters 
                 If mode == RNA, remove all non RNA nucleotide characters  
                 If mode == AA or PROT, remove all non amino acid characters
                 If mode is not defined, remove all non letter characters          
  Args         : $r_seq : reference to a string
                 $mode [Optional] : DNA|RNA|AA|PROT
  Globals      : none

=cut

sub CleanSeq
{
    my ($r_seq, $mode) = @_;

    if (defined($mode))
    {
        if ($mode =~ /DNA/i)
        {
            $$r_seq =~ s/[^ACGTRYMKWSBDHVNacgtrymkwsbdhvn]//g;
        }
        elsif ($mode =~ /RNA/i)
        {
            $$r_seq =~ s/[^ACGURYMKWSBDHVNacgurymkwsbdhvn]//g;
        }
        elsif ($mode =~ /AA|PROT/i)
        {
            $$r_seq =~ s/[^ARNDCQEGHILKMFPSTWYVBJZX*arndcqeghilkmfpstwyvbjzx]//g;
        }
        else
        {
            Carp::croak("Error: mode is $mode. Allowed modes are DNA, RNA, AA (or PROT)\n");
        }
    }
    else
    {
        $$r_seq =~ s/[^A-Za-z]//g;
    }

    return;
}

=head2 function

  Title        : Histo
  Usage        : my @a_classification = &Histo(\@a_val, \@a_class);
                 &Histo(\@a_val, \@a_class, $fh_out);
  Prerequisite : $ra_class and $ra_value contain numeric values
  Function     : Classify values.
                 If needed, create a new class with upper values.
                 Write the result to $fh_out if defined.
  Returns      : An array of strings. The line format is:
                 "Class label\tClass count\tCumulated count"          
  Args         : $ra_value: reference to an array of values to classify 
                 $ra_class: reference to an array of upper values of the intervals.
                 $fh_out [Optional] : Output file handle
  Globals      : none

=cut

sub Histo
{
    my ($ra_value, $ra_class, $fh_out) = @_;

    my @a_sorted_class = sort {$a <=> $b} (@$ra_class);
    my @a_sorted_value = sort {$a <=> $b} (@$ra_value);

    my $class_nb = scalar(@a_sorted_class);    # Number of classes
    my @a_class  = ();                         # Number of element by class
    my $icl      = 0;

    foreach my $value (@a_sorted_value)
    {
        if ($icl < $class_nb)
        {

            # Get the class icl according to the value
            while (defined($a_sorted_class[$icl]) && $value >= $a_sorted_class[$icl])
            {
                $icl++;
            }
        }

        # Inc the number of element in the current class
        $a_class[$icl]++;
    }

    # Add a label to the last class if created
    $a_sorted_class[$icl] = "PLUS" if ($icl == $class_nb);

    my $cumulated_count = 0;
    my $str;
    my @a_result = ();

    for (my $i = 0; $i < scalar(@a_class); $i++)
    {
        $a_class[$i] = 0 if (!defined($a_class[$i]));    # Put 0 if no value in the class
        $cumulated_count += $a_class[$i];

        #  Class    Element count   Cumulated count
        $str = $a_sorted_class[$i] . "\t" . $a_class[$i] . "\t" . $cumulated_count;
        push(@a_result, $str);

        print $fh_out "$str\n" if (defined $fh_out);
    }

    return @a_result;
}

sub HistoFilePattern
{

    # entree : fichier + pattern +fh_out + class
    # return Histo()
}

sub HistoFileColumn
{

    # entree : fichier + num col + fh_out + class
    # return &Histo()
}

=head2 procedure

  Title        : ParseM8Line
  Usage        : &ParseM8Line(\$m8_line, 1, \%h_res);
                 &ParseM8Line(\$m8_line, 2, \%h_res);
  Prerequisite : $query_col value  is 1 or 2
                 $$r_line is a line in the m8 or m9 format (composed of 12 fields)
  Procedure     : Extract from a m8 line these data and fill the hastable with values:
                 pci, len, mm, gap, e-value, score, query, target, qstart, qend, tstart, tend, strand    
  Args         : $r_line    : reference to a string
                 $query_col : column of the query (1 or 2)
                 $rh_result : reference to an hashtable
  Globals      : none

=cut

sub ParseM8Line
{
    my ($r_line, $query_col, $rh_result) = @_;

    # EVSUSMY01EK8A6	MtChr3	100.00	18	0	0	1	18	36123897	36123914	1e-0	18
    # SC: il peut y avoir des espaces notamment dans la derniere colonne (score) pour que tout soit aligne a droite ...
    #     si e-value commence par e , on rajoute 1 devant pour que cela reste numerique
    
    @_ = split("\t", $$r_line);
    $$rh_result{'pci'}     = $_[2];
    $$rh_result{'len'}     = $_[3];
    $$rh_result{'mm'}      = $_[4];
    $$rh_result{'gap'}     = $_[5];
    $$rh_result{'e-value'} = $_[10];
    $$rh_result{'score'}   = $_[11];

    $$rh_result{'score'} =~ s/\s+//g;
    $$rh_result{'e-value'} =~ s/^e/1e/;
    
    if ($query_col == 1)
    {
        $$rh_result{'query'}  = $_[0];
        $$rh_result{'target'} = $_[1];
        ($$rh_result{'qstart'}, $$rh_result{'qend'}) = ($_[6] <= $_[7]) ? ($_[6], $_[7]) : ($_[7], $_[6]);
        ($$rh_result{'tstart'}, $$rh_result{'tend'}) = ($_[8] <= $_[9]) ? ($_[8], $_[9]) : ($_[9], $_[8]);
    }
    else
    {
        $$rh_result{'query'}  = $_[1];
        $$rh_result{'target'} = $_[0];
        ($$rh_result{'tstart'}, $$rh_result{'tend'}) = ($_[6] <= $_[7]) ? ($_[6], $_[7]) : ($_[7], $_[6]) ;
        ($$rh_result{'qstart'}, $$rh_result{'qend'}) = ($_[8] <= $_[9]) ? ($_[8], $_[9]) : ($_[9], $_[8]);
    }
	$$rh_result{'strand'} = ($_[8] <= $_[9] && $_[6] <= $_[7]) ? '+' : '-';

    return;
}

=head2 procedure

  Title        : ParseGFF3Line
  Usage        : &ParseGFF3Line(\$gff3_line, 1, \%h_res);
  Prerequisite : $$gff3_line is a line in the gff3 format (composed of 9 fields)
  Procedure    : Extract from a gff3 line these data and fill the hastable %$rh_hit with the values:
                 seqid, source, type, start, end, len, score, strand, phase, attributes
                 If $mode_full, complete the hashtable with the attributes on the last column.  
  Args         : $r_line    : reference to a string
                 $mode_full : if 1, add to the hashtable the gff3 attributes.
                 $rh_hit    : reference to an hashtable
  Globals      : none

=cut

sub ParseGFF3Line
{
    my ($r_line, $mode_full, $rh_hit) = @_;

    %$rh_hit =
      ();    # meme si c'est couteux il faut l'initialiser a chaque fois car les cles ne sont pas forcement les memes
    return if ($$r_line =~ /^#/);    # apres l'init comme ca c'est vide

    @_ = split("\t", $$r_line);
    $$rh_hit{'seqid'}      = $_[0];
    $$rh_hit{'source'}     = $_[1];
    $$rh_hit{'type'}       = $_[2];
    $$rh_hit{'start'}      = $_[3];
    $$rh_hit{'end'}        = $_[4];
    $$rh_hit{'len'}        = $$rh_hit{'end'} - $$rh_hit{'start'} + 1;
    $$rh_hit{'score'}      = $_[5];
    $$rh_hit{'strand'}     = $_[6];
    $$rh_hit{'phase'}      = $_[7];
    $$rh_hit{'attributes'} = $_[8];
    return if (!$mode_full);

    my $attr = $_[8];
    while ($attr =~ /(\w+)=\s*([^\;]+)/g)
    {
        $$rh_hit{$1} = $2;
        $$rh_hit{$1} =~ s/\s*$//;
    }

    return;
}

=head2 procedure

  Title        : ParseBedLine
  Usage        : &ParseGFF3Line(\$bed_line, $fmt, \%h_res);
  Prerequisite : $bed_line is a line in the bed format ($fmt=1) or in bedpe ($fmt=2)
  Procedure    : Extract from a bed line these data and fill in the hastable %$rh_hit with the values:
                 BED:  chrom, start, end, [name, score, strand]
                 BEDPE: chrom1, start1, end1, chrom2, start2, end2, name, strand1, strand2, misc
				 Warning: as start is 0-based, the returned value of start is bed-start+1
  Args         : $r_line    : reference to a string
                 $fmt       : 1 for bed format, 2 for bedpe format
                 $rh_hit    : reference to an hashtable
  Globals      : none

=cut

sub ParseBedLine
{
    my ($r_l, $pe, $rh_result) = @_;

    if ($pe == 1)
    {
        @_ = split("\t", $$r_l);
        $$rh_result{'chrom'}  = $_[0];
        $$rh_result{'start'}  = $_[1];
		$$rh_result{'start'}++;
        $$rh_result{'end'}    = $_[2];
        $$rh_result{'name'}   = $_[3] || '';
        $$rh_result{'score'}  = $_[4] || '';
        $$rh_result{'strand'} = $_[5] || '';
    }
    elsif ($pe == 2)
    {
        @_ = split("\t", $$r_l, 11);
        $$rh_result{'chrom1'}  = $_[0];
        $$rh_result{'start1'}  = $_[1];
		$$rh_result{'start1'}++;
        $$rh_result{'end1'}    = $_[2];
        $$rh_result{'chrom2'}  = $_[3];
        $$rh_result{'start2'}  = $_[4];
		$$rh_result{'start2'}++;
        $$rh_result{'end2'}    = $_[5];
        $$rh_result{'name'}    = $_[6] || '';
        $$rh_result{'score'}   = $_[7] || '';
        $$rh_result{'strand1'} = $_[8] || '';
        $$rh_result{'strand2'} = $_[9] || '';
        $$rh_result{'misc'}    = $_[10] || '';
    }
    else
    {
        Carp::croak("ERROR: ParseBedLine: unknown specified format, must be 1 for bed or 2 for bedpe\n");
    }
    return;
}

=head2 function FastaToHash

  Title        : FastaToHash
  Usage        : &FastaToHash($stream,\%h_seq, $noseq);
  Prerequisite : sequences are clean
  Function     : Load a fasta stream (file or file handle) in a hash
				 If fasta header contains key=value, build as many keys/values in hash
				 Minimal set of keys will be
				 If $noseq is defined, sequence strings are not kept 
				 $rh_seq->{seqid} = {header => 'complete header', sequence => 'sequence string', len => 'computed len'}
  Returns      : the number of sequences loaded
  Args         : $stream: a file or a file handle
                 \%h_seq: a reference to a hash
                 $noseq : a flag to not load sequences (juste parse header and compute length)
  Globals      : none

=cut

sub FastaToHash
{
    my ($stream, $rh_sequences, $noseq) = @_;
    return 0 if (!defined($stream));

    my $fh_stream = $stream;
    my $open_fh   = 0;
    if (ref($stream) !~ /IO::File/i)
    {
        $fh_stream = &GetStreamIn($stream);
        $open_fh   = 1;
    }

    my $id  = '';
    my $cpt = 0;
    while (my $line = <$fh_stream>)
    {
        chomp($line);
        if ($line =~ /^>(\S+)\s*(.*)/)
        {
            $id = $1;
            my $header = (defined($2)) ? $2 : "";
            $cpt++;

            if (defined($rh_sequences->{$id}->{sequence}))
            {
                Carp::carp("WARNING - sequence $id already loaded\n");
            }
            $rh_sequences->{$id}->{header} = $header;
            while ($header =~ /(\S+)=(\S*)/g)
            {
                $rh_sequences->{$id}->{$1} = $2;
            }
            if ($header =~ /def=(.+)/)
            {
                $rh_sequences->{$id}->{def} = $1;
            }
            $rh_sequences->{$id}->{len} = 0;
        }
        elsif (defined $rh_sequences->{$id})
        {
			if (! defined $noseq)
			{
				$rh_sequences->{$id}->{sequence} .= $line;
				#Le stop final dans les proteines par exemple ne doit pas etre comptabilise
				&CleanSeq(\$line);
			}
            $rh_sequences->{$id}->{len} += length($line);
        }
    }

    $fh_stream->close if ($open_fh == 1);

    return $cpt;
}

=head2 function HashToFasta

  Title        : HashToFasta
  Usage        : &HashToFasta($stream,\%h_seq, $acid);
  Prerequisite : sequences are clean
  Function     : Dump a fasta stream (file or file handle) from a FastaToHash structure (sorted by id)
  Returns      : none
  Args         : $stream: a file or a file handle
               : \%h_seq: a reference to a hash
				 $acid : boolean, if true, force acc=$seqid if $rh_sequence->{seqid}->{acc} is null
  Globals      : none

=cut

sub HashToFasta
{
    my ($stream, $rh_sequences, $acid) = @_;
    return 0 if (!defined($stream));

    my $fh_stream = $stream;
    my $open_fh   = 0;
    if (ref($stream) !~ /IO::File/i)
    {
        $fh_stream = &GetStreamOut($stream);
        $open_fh   = 1;
    }
    foreach my $seqid (sort keys %{$rh_sequences})
    {
        my $header = '';
        if (!defined $rh_sequences->{$seqid}->{sequence})
        {
            Carp::carp("WARNING - No sequence found for $seqid");
            next;
        }
        my $sequence = &FormatSeq(\$rh_sequences->{$seqid}->{sequence});
        foreach my $key (keys %{$rh_sequences->{$seqid}})
        {
            $header .= ' ' . $key . '=' . $rh_sequences->{$seqid}->{$key}
              if ($key !~ /^(leng*t*h*|def|sequence|header|acc)$/);
        }
        my $len = length($rh_sequences->{$seqid}->{sequence});
        my $def = (defined $rh_sequences->{$seqid}->{def}) ? " def=" . $rh_sequences->{$seqid}->{def} : '';

        my $acc =
          (defined($rh_sequences->{$seqid}->{acc}) && ($rh_sequences->{$seqid}->{acc} ne ''))
          ? " acc=" . $rh_sequences->{$seqid}->{acc}
          : "";
        if (defined($acid) && $acc eq '')
        {
            $acc = " acc=$seqid";
        }

        $header = "len=$len" . $acc . $header . "$def";
        print $fh_stream ">$seqid $header\n$sequence\n";
    }

    $fh_stream->close if ($open_fh == 1);

    return;
}

=head2 function TabToHash

  Title        : TabToHash
  Usage        : &TabToHash($stream,\%h_seq);
  Function     : Load a stream (file or file handle) in a hash
  Returns      : none
  Args         : $stream: a file or a file handle
               : \%h_data: a reference to a hash
                 \@a_keys_columns:  list of column numbers (first = 1) used to compose key 
                 \@a_values_columns: list of column numbers (first = 1) used to compose values
                                     default is full line
  Globals      : none

=cut

sub TabToHash
{
    my ($stream, $rh_data, $ra_key_columns, $ra_value_columns) = @_;
    return 0 if (!defined($stream));
    Carp::croak("ERROR - Key columns has to be defined") unless defined $ra_key_columns;

    $ra_key_columns   = [$ra_key_columns]   if (defined($ra_key_columns)   && ref($ra_key_columns)   ne "ARRAY");
    $ra_value_columns = [$ra_value_columns] if (defined($ra_value_columns) && ref($ra_value_columns) ne "ARRAY");

    my $fh_stream = $stream;
    my $open_fh   = 0;
    if (ref($stream) !~ /IO::File/i)
    {
        $fh_stream = &GetStreamIn($stream);
        $open_fh   = 1;
    }

    my $line;
    my $num_fields = undef;
    my @a_fields   = ();
    while ($line = <$fh_stream>)
    {
        next if ($line =~ /^#/);
        chomp($line);
        @a_fields = split("\t", $line, -1);

        $num_fields = scalar @a_fields unless (defined($num_fields));
        if (scalar @a_fields != $num_fields)
        {
            Carp::croak("ERROR - inconsistent number of columns at $line");
        }

        my $key = '';
        foreach my $key_column (@{$ra_key_columns})
        {
            $key .= $a_fields[$key_column - 1] . "\t" if (defined($a_fields[$key_column - 1]));
        }
        chop($key);

        my $value = '';
        if (!defined $ra_value_columns)
        {
            $value = $line;
        }
        else
        {
            foreach my $value_column (@{$ra_value_columns})
            {
                $value .= $a_fields[$value_column - 1] . "\t" if (defined($a_fields[$value_column - 1]));
            }
            chop($value);
        }
        $rh_data->{$key} = $value;
    }

    $fh_stream->close if ($open_fh == 1);

    return;
}

1;

