#
# 	Sebastien.Letort@toulouse.inra.fr
#	Created: April 05, 2007
# 	$Id: General.pm 1372 2014-03-17 07:14:43Z manu $
#

# Copyright INRA/CNRS

# Emmanuel.Courcelle@toulouse.inra.fr
# Jerome.Gouzy@toulouse.inra.fr
# Sebastien.Carrere@toulouse.inra.fr
# Erika.Sallet@toulouse.inra.fr
# Ludovic.Cottret@toulouse.inra.fr

# This software is a computer program whose purpose is to provide a
# web-based interface for analyzing the different levels of genome
# conservations.

# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".

# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.

# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

package General;

BEGIN
{
    use Exporter ();
    use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

    $VERSION = do {my @r = (q$Rev: 1372 $ =~ /\d+/g); sprintf "%d." . "%02d" x $#r, @r};

    @ISA = qw(Exporter);
    @EXPORT =
      qw( &TRUE &FALSE &MAXINT Camelize Capitalize GetStreamIn GetStreamOut IsNotNull Max Min MinMax MinMaxArray SafeIsArray SafeIsHash SafeIsa SetIfNull SetUnlessDefined ShowDebug Verbose TryMain ShowStack UnixSort GetCompressPath GetExePath IsEncodeAvailable Encode Decode Multithreading Exists HashValueDefined MultithreadingBlock );
}

use Data::Dumper;
use Scalar::Util;
use Carp;
use IO::File;
use File::Temp qw/ tempfile tempdir /;
use Error qw( :try );
use Error::Simple;
use LipmError;
use strict;
use threads;
use threads::shared;

# les constantes
use constant TRUE   => 1;
use constant FALSE  => 0;
use constant MAXINT => 4294967296;

# Encode/Decode availability
eval("use Convert::UU qw(uudecode uuencode)");
our $ENCODE_OK = ($@ eq "") ? &TRUE : &FALSE;


#les fonctions generales

=head2 function Capitalize

  Title        : Capitalize
  Usage        : $string = &Capitalize( $str );
  Prerequisite : none
  Function     : puts all first letters of a string to upper case
                 It works also if words are separated by '_'.
  Returns      : $string, $str with upper case on the first letters of every word
  Args         : $str, a string to capitalize.
  Globals      : none

=cut

sub Capitalize
{
    my ($str) = @_;

    my $string = $str;

    # 	$string =~ s/([0-9a-zA-Z]+)/\u$1/g;
    $string =~ s/([[:alnum:]]+)/\u$1/g;

    return $string;
}

=head2 function Camelize

  Title        : Camelize
  Usage        : $string = &Camelize( $str );
  Prerequisite : none
  Function     : puts all first letters of a string to upper case,
                 like Capitalize, removes separators '_', ' ' or '''.
                 Other separators are ignored
  Returns      : $string, a one word string in camelcase style.
  Args         : $str, a string to camelize.
  Globals      : none

  TODO         : est-ce qu'on doit enlever tous les separateurs
                 ou bien seulement ceux entre 2 mots ?
                "salut les gars, ca va ?"
                 => 1) "SalutLesGars,CaVa?"
                 => 2) "SalutLesGars, CaVa ?"

                 Pour le moment c'est le 1)

=cut

sub Camelize
{
    my ($str) = @_;

    my $string = $str;

    # 	$string =~ s/([0-9a-zA-Z]+)/\u$1/g;
    $string =~ s/([[:alnum:]]+)/\u$1/g;
    $string =~ s/[ _']//g;

    return $string;
}

=head2 Procedure TryMain

 Title        : TryMain
 Usage        : 1/ &TryMain();
                2/ &TryMain(1);
                3/ &tryMain(1,\&Main::SomeProcedure());
                4/ &tryMain(\&Main::SomeProcedure(),1);
 Prerequisite : A die, a croak may be thrown
                An error deriving from the Error class may be thrown (see Error.pm package)
 Procedure    : This procedure provides the scripts with a MINIMUM error processing
                Call a function, encapsulating it inside a try/catch construct
				1/ Call the function Main and display minimal message in case of error
				2/ Call the function Main and display the whole stack in case of error
				3/ Same as 2/, but call SomeProcedure instead of Main
				4/ Same as 3/
 Args         : A flag to control the display of the error message:
					defaults to 0, ie a few information on the error
					If 1, defaults the same + the stack
					If -1 defaults only the text, ie the error message.
				A ref to a function, will be called (defaults to Main::Main)
				The order of the arguments is irrelevant

=cut

sub TryMain
{
    my ($stack_flg, $fn_Main) = (undef, undef);

    foreach my $arg (@_)
    {
        if (ref($arg) eq 'CODE')
        {
            $fn_Main = $arg;
        }
        if (ref($arg) eq '')
        {
            $stack_flg = $arg;
        }
    }
    $stack_flg = 0            unless (defined $stack_flg);
    if ($stack_flg > 0)
    {
		$Error::Debug = 1;
		$Error::ShortMsg = 0;
	}
	elsif ($stack_flg==0)
	{
		$Error::Debug = 0;
		$Error::ShortMsg = 0;
	}
	else
	{
		$Error::Debug = 0;
		$Error::ShortMsg = 1;
	}
    $fn_Main   = \&main::Main unless (defined $fn_Main);

    try
    {
        &{$fn_Main}();
        exit 0;
    }
    catch LipmError with
    {
		my ($o_error) = @_;
        if ($Error::Debug == 1)
        {
            print $o_error->stacktrace();
        }
        if ( $Error::ShortMsg==0)
        {
			print "ERROR - FILE  = " . $o_error->file . "\n";
			print "        LINE  = " . $o_error->line . "\n";
			print "        VALUE = " . $o_error->value . "\n";
			print "        TEXT  = " . $o_error->text . "\n";
			print "================\n";
			print $o_error->Explain()."\n";
			print "================\n";
		}
		else
		{
			print $o_error->text . "\n";
		}
		exit 1;
	}	
    catch Error::Simple with
    {
        my ($o_error) = @_;
        if ($Error::Debug == 1)
        {
            print $o_error->stacktrace();
        }
        if ( $Error::ShortMsg==0)
        {
			print "ERROR - FILE  = " . $o_error->file . "\n";
			print "        LINE  = " . $o_error->line . "\n";
			print "        VALUE = " . $o_error->value . "\n";
			print "        TEXT  = " . $o_error->text . "\n";
		}
		else
		{
			print $o_error->text . "\n";
		}
		exit 1;
    };
}

=head2 Procedure ShowStack

 Title        : ShowStack
 Usage        : &ShowStack();
 Prerequisite : try/catch construct being used, probably through TryMain
 Procedure    : Change the Error::Debug global, so that the stack will be displayed in case of error
                Completely equivalent to passing 1 as $stack_flg in TryMain, exwcept that this function may be
                triggered from Main, thus controlled by some environment variable, parameter, etc.
                See GeneralTestTTryMain.pl for a demo
 Args         : None

=cut

sub ShowStack
{
    $Error::Debug = 1;
}

=head2 Procedure Verbose

 Title        : Verbose
 Usage        : &Verbose( $self, $msg );
                or &Verbose( { verbose => &TRUE }, $msg );
 Prerequisite : none
 Procedure    : warn a message if verbose is true
               	or is $param->{ _verbose } is true.
 Args         : first arg, object with _verbose attribute
                or $hash ref with verbose key
                $msg, string, the message to warn

 Note         : deprecated, try Logger attribute in object context

=cut

sub Verbose
{
    my $param = shift;
    my ($msg) = @_;

    my $verbose = &FALSE;
    my $ref     = ref $param;
    if   ($ref eq 'HASH') {$verbose = $$param{verbose};}
    else                  {$verbose = $param->{_verbose};}

    print STDERR "v-->$msg\n" if (defined $verbose and &TRUE == $verbose);

    return;
}

=head2 Procedure ShowDebug

	Title      :	ShowDebug
	Usage      :	ShowDebug( deep=>$deep, arg => $ra_args );	<= a faire
	Prerequiste:	none
	Procedure  :	print onto STDERR the name of the function where the function was called
	        	depending on the deep parameter
	        	with the filename and the line number of the current file.
	        	Then it prints the list of arguments with Dumper.
	Args       :	$deep, an integer representing the deep of the call
	        		must be >=1
	        	@a_args, the list of arguments to print out.
	Error      :    none
	Globals    :    none

	Note       :	I used a hash ref to preserved compatibility with previous usage ShowDebug(@a_args)

=cut

sub ShowDebug
{
    my (@args) = @_;

    my $deep = 1;
    if (ref $args[0] eq 'HASH')
    {
        my $rh_hash = shift @args;
        $deep = $$rh_hash{deep};
    }

    my ($package, $file, $line) = caller();

    my $subroutine = (caller($deep))[3] || $package;
    print STDERR "In $subroutine ($file:$line) :\n", Data::Dumper->Dump([@args]);
}

=head2 Function IsNotNull

	Title      :	IsNotNull
	Usage      :	print "\$var n'est pas vide\n"	if( IsNotNull( $var ) );
	Prerequiste:	only works with scalars (strings)
	Function   :	shortcut to know if a variable is defined and non empty
	Returns    :	&TRUE if var is defined and non empty, &FALSE otherwise
	Args       :	$var, a variable that should contains a string.
	NOTE       :    Was called a long time ago IsDefined !!!
	Error      :    none
	Globals    :    none

=cut

sub IsNotNull
{
    my ($var) = @_;

    return &TRUE if (defined $var and $var ne '');
    return &FALSE;
}

=head2 Procedure SetUnlessDefined,SetIfNull

 Title    : SetUnlessDefined,SetIfNull
 Usage    : SetUnlessDefined( \$var, 'some value' );
            SetIfNull( \$var, 'some value' ); 
 Procedure: Set a value to a variable if it hasn't got any.
                     -if not defined for SetUnlessDefined
                     -if not defined or '' for SetIfNull
 Args     : $r_var, a reference to a scalar variable.
            $val, any value
 Error    : none
 Globals  : none

=cut

sub SetUnlessDefined
{
    my ($r_var, $val) = @_;

    if (ref($r_var) eq 'SCALAR')
    {
        $$r_var = $val unless defined($$r_var);
        return;
    }
    Carp::croak("ERROR - $r_var should be a scalar reference");
}

sub SetIfNull
{
    my ($r_var, $val) = @_;

    if (ref($r_var) eq 'SCALAR')
    {
        $$r_var = $val unless &IsNotNull($$r_var);
        return;
    }
    Carp::croak("ERROR - $r_var should be a scalar reference");
}

=head2 Procedure SafeIsa

 Usage        : $bool = SafeIsa( $var, $class )
 Function     : check that $var is defined,
                that $var is an object and that $var is a $class instance.
 Args         : $var, any scalar to test,
                $class, string, class name.
 Error        : none.

=cut

sub SafeIsa
{
    my ($var, $class) = @_;

    return &TRUE if (defined $var && &Scalar::Util::blessed($var) && $var->isa($class));
    return &FALSE;
}    #SafeIsa

=head2 Procedure SafeIsArray,SafeIsHash

 Usage        : $bool = SafeIsArray( $var )
 Function     : check that $var is defined and is a ref to an array/hash
 Args         : $var, any ref-to-array or ref-to-hash to test
 NOTE         : SafeIsScalar does not make sense, because a ref to a scalar whose value is undef is... a ref to scalar
 Error        : none.

=cut

sub SafeIsArray
{
    my ($var) = @_;

    return &TRUE if (defined $var && ref($var) eq 'ARRAY');
    return &FALSE;
}

sub SafeIsHash
{
    my ($var) = @_;

    return &TRUE if (defined $var && ref($var) eq 'HASH');
    return &FALSE;
}

=head2 Procedure MinMaxArray

 Usage        : my ($min,$max) = &MinMaxArray( \@a_data)
 Prerequisite : @a_data is an array or numbers
 Function     : Retrieves the minimum and maximum values of an array, in one pass
 Args         : $ra_data A ref to an array
 Error        : none.

=cut

sub MinMaxArray
{
    my ($ra_data) = @_;
    Carp::croak("ERROR - Input parameter SHOULD BE an array ref") if (!&SafeIsArray($ra_data));
    Carp::croak("ERROR - the array is empty") if (@$ra_data == 0);

    my ($min, $max) = ($ra_data->[0], $ra_data->[0]);
    foreach my $data (@{$ra_data})
    {
        $min = $data if ($data < $min);
        $max = $data if ($data > $max);
    }
    return ($min, $max);
}

=head2 Procedure MinMax,Min,Max

 Usage        : my ($min,$max) = &MinMax( $x,$y )
 Prerequisite : $x,$y are two numbers
 Function     : returns ( $min,$max) 
 Args         : $x,$y

=cut

sub MinMax
{
    my ($x, $y) = @_;
    return ($x < $y) ? ($x, $y) : ($y, $x);
}

sub Min
{
    my ($x, $y) = @_;
    return ($x < $y) ? $x : $y;
}

sub Max
{
    my ($x, $y) = @_;
    return ($x < $y) ? $y : $x;
}

=head2 Procedure GetStreamIn,GetStreamOut

 Usage        : my $fh_in = &GetStreamIn( $filepath );
                my $fh_out= &GetStreamOut($filepath );
 Prerequisite : The module File::Which is not required, but recommended
 Function     : Opens the (may be compressed) filepath and returns a file handle, opened in read mode
 Args         : the file path
 Returns      : the file handle
 Global       : %H_COMPRESSION_PRGMS (readonly): key = extension, value = binary name
=cut

my %H_COMPRESSION_PRGMS = (xz => 'xz', lzma => 'lzma', gz => 'gzip', bz2 => 'bzip2', Z => 'compress');

sub GetStreamIn
{
    my ($filepath) = @_;
    return *STDIN if ($filepath eq '' || $filepath eq '-' || $filepath eq 'stdin');

    my $exe = &GetCompressPath($filepath);
    my $stream;

    # If ending with |, nothing to do
    if ($filepath =~ /\|\s*$/)
    {
        $stream = $filepath;
    }
    else
    {
        $stream = $exe eq '' ? $filepath : "$exe -cd $filepath |";
    }

    my $fh_in = new IO::File($stream) or Carp::croak("ERROR - Can't read >$stream<");
    return $fh_in;
}

sub GetStreamOut
{
    my ($filepath) = @_;
    return *STDOUT if ($filepath eq '' || $filepath eq '-' || $filepath eq 'stdout');
    my $exe = &GetCompressPath($filepath);
    my $stream;

    # If starting with |, nothing to do
    if ($filepath =~ /^\s*\|/)
    {
        $stream = $filepath;
    }
    else
    {
        $stream = $exe eq '' ? ">$filepath" : "| $exe -c >$filepath";
    }

    my $fh_out = new IO::File($stream) or Carp::croak("ERROR - Can't write >$stream<");
    return $fh_out;
}

=head2 Procedure GetCompressPath

 Usage        : my $exe = &GetCompressPath($filename)
 Function     : returns the path to the compression program or "" if no compression,
				calling GetExePath
 Args         : the file path
 Returns      : path
 Global       : %H_COMPRESSION_PRGMS (readonly): key = extension, value = binary name
=cut

sub GetCompressPath
{
    my ($filepath) = @_;

    my ($ext) = ($filepath =~ /\.(\w+)$/);
    my $exe = (defined($ext) && exists($H_COMPRESSION_PRGMS{$ext})) ? $H_COMPRESSION_PRGMS{$ext} : '';

    return &GetExePath($exe);
}

=head2 Procedure GetExePath

 Usage        : my $exe_path = &GetExePath('some_program')
 Prerequisite : The module File::Which is not required, but recommended
 Function     : returns the complete path to the program some_program if possible
				If a complete path is provided, test if this program is executable
                If the program is not found by the Which module, croak
                If the Which module is not found, returns its argument 
                (no check is done, let's hope the program is in the path)
 Args         : The executable program name
 Returns      : The complete path is possible
 Global       : %H_COMPRESSION_PRGMS (readonly): key = extension, value = binary name
=cut

sub GetExePath
{
    my ($exe) = @_;
    my $exe_path = $exe;

    if ($exe_path ne '')
    {
     	if ($exe_path =~ /^\//) 
	{
		if  (! -x $exe_path)
		{
			Carp::croak("ERROR - Full path to NON executable file $exe_path");
		}
		else
		{
			return $exe_path;
		}
	}
	else
	{
		eval("use File::Which");
		if ($@ eq "")
		{
			$exe_path = which($exe);
			if (!defined $exe_path)
			{
				Carp::croak("ERROR - $exe is NOT in the path, or NOT installed");
			}
		}
		else
		{
			$@ = "";
		}
	}
    }
    return $exe_path;
}

=head2 Procedure UnixSort

 Usage        : &UnixSort(\@a_unsort, \@a_sort)
                &UnixSort($filein, $fileout, ["3", "5n"], \%h_options)
                &UnixSort($streamin, $streamout, "2n")
 Prerequisite : -
 Procedure    : Build unix sort command and execute it.  
 Args         : $in    Input: ref to array or filehandle or filename
                $out   Output: ref to array or filehandle or filename
                $ra_keys string or ref to an array of strings.
                       A key looks like column_number (1-based) optionally followed by a 'r' (reverse) and/or 'n' (numerical)
                       Examples: '2' or '2n' or '3rn' or ['3','2n'] 
                $rh_options Ref to a hash
                            Allowed keys are : 
                            reverse   => 0|1 [default 0] (same as --reverse switch or sort)
                            uniq      => 0|1 [default 0]
                            stable    => 0|1 [default 0] (same as --stable switch)
                            sep       => ascii code [default 9 ie \t]
                            tmpdir    => temporary directory path. By default, the tmp directory is created in the current directory (UnixSortXXXX)
 TODO         : The header options key should be implemented (mais c'est bien chiant)
                Verifier que tout a bien marche (code de sortie de sort)
 Global       : 
 
=cut

#
# unixSort is a wrapper to the other functions
# According to the type of $in/$out, a specific __UnixSort is called
# SCALAR => 'F'    (File name)
# NO SCALR => 'AS' (Ref to Array of Stream)
#

sub UnixSort
{
    my ($in, $out, $ra_keys, $rh_options) = @_;

	my $is_user_tmpdir = &TRUE;
	if ( !exists $rh_options->{tmpdir})
	{
		$rh_options->{tmpdir} = tempdir( 'UnixSortXXXXX' );
		$is_user_tmpdir = &FALSE;
	}
	
    my $function = '__UnixSort';
    $function .= (ref(\$in)  eq 'SCALAR') ? 'F' : 'AS';    # UnixSortF or UnixSortAS
    $function .= '2';
    $function .= (ref(\$out) eq 'SCALAR') ? 'F' : 'AS';    # UnixSort.2F or UnixSort.2AS

	# JG: lorsque $rh_options est vide en arrivant il faut reinitialiser @_
	@_ = ($in, $out, $ra_keys, $rh_options);

    eval "$function (\@_)";
    if ($@ ne "")
    {
        croak $@;
    }


	if ($is_user_tmpdir == &FALSE)
	{
		my $tmpdir = $rh_options->{tmpdir};
		my @a_rm_list = glob("$tmpdir/*");
		#use Data::Dumper;
		#print Dumper(\@a_rm_list);
		unlink(@a_rm_list);
		rmdir $tmpdir;
	}
    return;
}

sub __UnixSortAS2F
{
    my ($in, $out, $ra_keys, $rh_options) = @_;

    my $out_exe_path = &GetCompressPath($out);    # "/bin/gzip"
    if ($out_exe_path ne "")
    {
        $out_exe_path .= " -c > $out ";           # Ex "/bin/gzip -c outfile.gz "
    }
    my $cmd = &GetExePath("sort") . ' ' . &__GetSortOptions($ra_keys, $rh_options);
    if ($out_exe_path eq '')
    {
        $cmd .= " > $out ";
    }
    else
    {
        $cmd .= " | $out_exe_path";
    }

    #my $f_out = new IO::File ( "| $cmd" ) or die "ERROR - Cannot write to | $cmd";
    my $f_out = &GetStreamOut("| $cmd");

    # Is it a array or a stream ?
    if (ref $in eq 'ARRAY')
    {
        foreach my $l (@$in)
        {
            chomp $l;
            print $f_out "$l\n";
        }
    }
    else
    {
        while (my $l = <$in>)
        {
            chomp $l;
            print $f_out "$l\n";
        }
    }
    $f_out->close();

    return;
}

sub __UnixSortF2AS
{
    my ($in, $out, $ra_keys, $rh_options) = @_;

    my $in_exe_path = &GetCompressPath($in);    # "/bin/gzip"
    if ($in_exe_path ne "")
    {
        $in_exe_path .= " -cd $in "             # Ex "/bin/gzip -cd infile.gz"
    }
    my $cmd = &GetExePath("sort") . ' ' . &__GetSortOptions($ra_keys, $rh_options);
    if ($in_exe_path eq '')
    {
        $cmd .= " $in ";
    }
    else
    {
        $cmd = "$in_exe_path | $cmd";
    }

    my $f_in = &GetStreamIn("$cmd |");

    # Is it a array or a stream ?
    if (ref $out eq 'ARRAY')
    {
        @$out = ();    # Clearing the output array
        while (my $l = <$f_in>)
        {
            chomp $l;
            push @$out, $l;
        }
    }
    else
    {
        while (my $l = <$f_in>)
        {
            chomp $l;
            print $out "$l\n";
        }
    }

    $f_in->close();

    return;
}

sub __UnixSortAS2AS
{
    my ($in, $out, $ra_keys, $rh_options) = @_;

    my ($fh, $tmp);
	
	(undef, $tmp) = tempfile('UnixSortXXXXX', DIR => $rh_options->{tmpdir}, OPEN => 0);
    
    &__UnixSortAS2F($in, $tmp, $ra_keys, $rh_options);
    my $f_tmp = &GetStreamIn($tmp);
    if (ref $out eq "ARRAY")
    {
        @$out = ();    # Clearing the output array
        while (my $l = <$f_tmp>)
        {
            chomp $l;
            push @$out, $l;
        }
    }
    else
    {
        while (my $l = <$f_tmp>)
        {
            chomp $l;
            print $out "$l\n";
        }
    }
    $f_tmp->close();
	unlink $tmp;

    return;
}

sub __UnixSortF2F
{
    my ($in, $out, $ra_keys, $rh_options) = @_;

    # Only input and output files
    my $in_exe_path = &GetCompressPath($in);    # "/bin/gzip"
    if ($in_exe_path ne "")
    {
        $in_exe_path .= " -cd $in "             # Ex "/bin/gzip -cd infile.gz"
    }

    my $out_exe_path = &GetCompressPath($out);    # "/bin/gzip"
    if ($out_exe_path ne "")
    {
        $out_exe_path .= " -c > $out ";           # Ex "/bin/gzip -c outfile.gz "
    }

    my $cmd = &GetExePath("sort") . ' ' . &__GetSortOptions($ra_keys, $rh_options);

    if ($in_exe_path eq '')
    {
        $cmd .= " $in ";
    }
    else
    {
        $cmd = "$in_exe_path | $cmd";
    }
    if ($out_exe_path eq '')
    {
        $cmd .= " > $out ";
    }
    else
    {
        $cmd .= " | $out_exe_path";
    }

    system $cmd;

    return;

}

=head2 Function __GetSortOptions

 Usage        : Called from Unixsort
 Function     : Build the options string for UnixSort
 
=cut

sub __GetSortOptions
{
    my ($ra_keys, $rh_options) = @_;

    if (!defined $ra_keys)
    {
        $ra_keys = [];
    }
    elsif (ref($ra_keys) ne "ARRAY")
    {
        $ra_keys = [$ra_keys];
    }
    my $options = "  --compress-program=gzip ";
    foreach my $key (@$ra_keys)
    {
        if ($key !~ /[0-9]+[rn]*/)
        {
            croak "ERROR ! Invalid key specification $key";
        }
        $options .= " -k$key,$key ";
    }
    $options =~ s/[a-z]*,/,/g;    # Before " -k2n,2n " After " -k2,2n "

    if (exists $rh_options->{'reverse'} && $rh_options->{'reverse'} == &TRUE)
    {
        $options .= " -r ";
    }
    if (exists $rh_options->{'uniq'} && $rh_options->{'uniq'} == &TRUE)
    {
        $options .= " -u ";
    }
    if (exists $rh_options->{'stable'} && $rh_options->{'stable'} == &TRUE)
    {
        $options .= " -s ";
    }

	$options .= " --temporary-directory=" . $rh_options->{'tmpdir'};
	
    my $sep = 9;    # ascii code for character \t
    if (exists $rh_options->{'sep'})
    {
        $sep = $rh_options->{'sep'};
    }
    $options .= ' -t "' . chr($sep) . '"';

    return $options;
}



=head2 Procedure Multithreading

 Usage        : &Multithreading(\&SliceAnalysis,$nb_cpus,\@a_inputs,\@a_results,@a_params);
				&Multithreading(\&SliceAnalysis,$nb_cpus,\%h_inputs,\%h_results,@a_params);

sub SliceAnalysis
{
my ($i,$ra_tasks,$ra_inputs,$sa_results,@a_param) = @_;
	
	my ($param1,$param2) = @a_param;
	my $task = $i;
	$$sa_results[$i] = "";
}
 
sub SliceAnalysis
{
my ($i,$ra_tasks,$rh_inputs,$sh_results,@a_param) = @_;

	my ($param1,$param2) = @a_param;
	my $task = $$ra_tasks[$i];
	$$sh_results{$$ra_tasks[$i]} = "";
}


see General.t for examples

 Prerequisite : use threads; use threads::shared;
 Procedure    : Run SliceAnalysis in parallel
 Args         : \&SliceAnalysis                 : ref to to the fonction that will analyze the slice
                $nb_cpus                        : number of threads (integer)
				\@a_inputs, \%h_inputs          : reference to an ARRAY/HASH that contains data to be analyzed
				\@a_results, \%h_results, undef : shared data structure (my @a_results :shared = () ; my %h_results :shared=() );
				@a_param                        : list of parameters, can be complex parameters

 Global       : none 

 
=cut


my $MULTITHREADING_BEHAVIOUR = "JOB";

sub Multithreading
{
my($r_fct,$nb_cpus,$r_inputs,$r_sharedresults,@a_params)=@_;

	$nb_cpus = &Max(1,$nb_cpus);

	if ( ! defined($r_inputs) )
	{
		print STDERR "Multithreading warning: nothing to do\n";
	}

	my %h_threads = ();
	my @a_tasks=();
	if ( ref($r_inputs) eq 'HASH' )
	{
		@a_tasks = sort keys(%$r_inputs);
	}
	elsif ( ref($r_inputs) eq 'ARRAY')
	{
		@a_tasks = @{$r_inputs};
	}
	else
	{
		Carp::croak("MultiThreading error: unmanaged TODO data type\n");
	}
	$nb_cpus = &Min(scalar(@a_tasks),$nb_cpus);
	my $step   = int(scalar(@a_tasks)/$nb_cpus);
	$step++ if ( int(scalar(@a_tasks)/$nb_cpus) != scalar(@a_tasks)/$nb_cpus );
	for (my $i=0;$i<scalar(@a_tasks);$i+=$step)
	{
		my $imax = &Min(scalar(@a_tasks)-1,$i+$step-1);
		if ( $MULTITHREADING_BEHAVIOUR eq "JOB" )
		{
			if ( $nb_cpus > 1 )
			{
				$h_threads{$i} = threads->create(\&__MultithreadingSlice,$r_fct,$i,$imax,\@a_tasks,$r_inputs,$r_sharedresults,@a_params);
			}
			else
			{
				 &__MultithreadingSlice($r_fct,$i,$imax,\@a_tasks,$r_inputs,$r_sharedresults,@a_params);
			}
		}
		elsif (  $MULTITHREADING_BEHAVIOUR eq "BLOCK" )
		{
			if ( $nb_cpus > 1 )
			{
				$h_threads{$i} = threads->create(\&__MultithreadingSliceBlock,$r_fct,$i,$imax,\@a_tasks,$r_inputs,$r_sharedresults,@a_params);
			}
			else
			{
				 &__MultithreadingSliceBlock($r_fct,$i,$imax,\@a_tasks,$r_inputs,$r_sharedresults,@a_params);
			}
		}
		else
		{
			Carp::croak("Unknown MULTITHREADING_BEHAVIOUR value (must be JOB or BLOCK)\n");
		}
	}
	for (my $i=0;$i<scalar(@a_tasks);$i+=$step)
	{
		next if ( $nb_cpus <= 1 );
		$h_threads{$i}->join();
		if ( my $err = $h_threads{$i}->error() )
		{
			Carp::croak("Multithreading: thread error >$err<\n");
		}
	}
}

sub __MultithreadingSlice
{
	my ($r_fct,$imin,$imax,$ra_tasks,$r_inputs,$r_sharedresults,@a_params) = @_;
	
	for (my $i=$imin; $i<=$imax; $i++)
	{
		&$r_fct($i,$ra_tasks,$r_inputs,$r_sharedresults,@a_params);
	}
}

=head2 Procedure MultithreadingBlock

 Usage        : &MultithreadingBlock(\&SliceAnalysisBlock,$nb_cpus,\@a_inputs,\@a_results,@a_params);
				&MultithreadingBlock(\&SliceAnalysisBlock,$nb_cpus,\%h_inputs,\%h_results,@a_params);
 Procedure    : idem as Multithreading
				The standard behaviour of Multithreading is one function call per job. but here the same
				fonction instance manages all jobs of the slice (from imin to <=imax).
                This behaviour allows specific data structure to be shared inside the same block of jobs.
				Warning: depending on what is done in the fonction, it may conduct to non reproductible results
				(results depend indirectly on nb_cpu)

sub SliceAnalysisBlock
{
my ($imin,$imax,$ra_tasks,$ra_inputs,$sa_results,@a_param) = @_;

}

=cut


sub MultithreadingBlock
{
my($r_fct,$nb_cpus,$r_inputs,$r_sharedresults,@a_params)=@_;

	$MULTITHREADING_BEHAVIOUR = "BLOCK";

	&Multithreading($r_fct,$nb_cpus,$r_inputs,$r_sharedresults,@a_params);
	
	$MULTITHREADING_BEHAVIOUR = "JOB";
}


sub __MultithreadingSliceBlock
{
	my ($r_fct,$imin,$imax,$ra_tasks,$r_inputs,$r_sharedresults,@a_params) = @_;
	
	&$r_fct($imin,$imax,$ra_tasks,$r_inputs,$r_sharedresults,@a_params);
}

#
# ENCODE/DECODE
#

=head2 procedure IsEncodeAvailable

    Title     :	IsEncodeAvailable
    Usage     :	my $encode_status = IsEncodeAvailable();
    Function  : return 1 if Encode/Decode can be used, 0 if not
    Args      :	none

=cut

sub IsEncodeAvailable
{

	return $ENCODE_OK;
}


=head2 function Encode

	Title      :	Encode
	Usage      :	&General::Encode($parameters);
	Prerequiste:	uuencode must be installed
	Function   :	Encode a $param if required
                    THIS FUNCTION IS NOT A METHOD
	Returns    :	$params, encoded or not
	Args       :	$param,	a string, generally an url formatted parameters
	globals    :	none

=cut

sub Encode
{
    my ($parameters) = @_;

	#
	#tester la longueur de $parameters
	#
    my $str = uuencode($parameters);
    $str =~ s/^begin 644 uuencode.uu\s//o;
    $str =~ s/`\send\s$//o;
    $str =~ s/\=/code1/go;
    $str =~ s/\&/code2/go;
    $str =~ s/\?/code3/go;
    $str =~ s/\n/code4/go;
    $str =~ s/\"/code5/go;
    $str =~ s/ /code6/go;
    $str =~ s/;/code7/go;
    $str =~ s/([^A-Za-z0-9])/sprintf("%%%02X", ord($1))/seg;
    $str =~ s/%/code8/go;
    $str = reverse($str);
    return '' if ($str eq '');
	return "$str";
}


=head2 function Decode

	Title      :	Decode
	Usage      :	$query_string = &General::Decode();
	Prerequiste:	uudecode must be installed
	Function   :	Decode the $ENV{'QUERY_STRING'} if needed
                    THIS FUNCTION IS NOT A METHOD
	Returns    :	the $ENV{'QUERY_STRING'} decoded
	Args       :	none
	globals    :	$ENV{QUERY_STRING} modified

=cut

sub Decode
{
	my $encoded_substr = shift;
	return '' unless (defined $encoded_substr);

    $encoded_substr = reverse($encoded_substr);
    $encoded_substr =~ s/code1/=/go;
    $encoded_substr =~ s/code2/\&/go;
    $encoded_substr =~ s/code3/\?/go;
    $encoded_substr =~ s/code4/\n/go;
    $encoded_substr =~ s/code5/\"/go;
    $encoded_substr =~ s/code6/ /go;
    $encoded_substr =~ s/code7/;/go;
    $encoded_substr =~ s/code8/%/go;
    $encoded_substr =~ s/\%([A-Fa-f0-9]{2})/pack('C', hex($1))/seg;

    $encoded_substr = "begin 644 uuencode.uu\n$encoded_substr\`\nend\n";

    my $str = uudecode($encoded_substr);
    return $str;
}

=head2 function Exists

	Title      : Exists	
	Usage      : &Exists(\%h_hash,$i,$j,$k)	
	Prerequiste: non	
	Function   : Tests the existence of keys without side effect (exists($h{1}{2} DEFINES $h{1})
	Returns    : returns &TRUE if the key exists in the hash, &FALSE otherwise 
	Args       : \%h_hash:  reference to the multidimentional hash 
	             $i,$j,$k : keys in multidimentional hash
	globals    : none

=cut

sub Exists
{
my($rh_,@a_keys) = @_;

	foreach my $k (@a_keys)
	{
		return &FALSE if ( ! exists($$rh_{$k}) );
		$rh_ = $$rh_{$k};
	}
	return &TRUE;
}

=head2 function HashValueDefined

	Title      : HashValueDefined
	Usage      : &HashValueDefined(\%h_hash,$i,$j,$k)	
	Prerequiste: non	
	Function   : Tests the definition of keys without side effect (defined($h{1}{2} DEFINES $h{1})
	Returns    : returns &TRUE if the key exists in the hash, &FALSE otherwise 
	Args       : \%h_hash:  reference to the multidimentional hash 
	             $i, $j, $k : keys in multidimentional hash
	globals    : none

=cut

sub HashValueDefined
{
my($rh_,@a_keys) = @_;

	foreach my $k (@a_keys)
	{
		return &FALSE if ( ! defined($$rh_{$k}) );
		$rh_ = $$rh_{$k};
	}
	return &TRUE;
}


1;    # package General

