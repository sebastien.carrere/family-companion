#
# $Id: Runner.pm 606 2011-01-07 16:59:23Z manu $
#

package Runner;

# Copyright Notice

# Copyright I.N.R.A. - C.N.R.S. 

# emmanuel.courcelle@toulouse.inra.fr
# jerome.gouzy@toulouse.inra.fr

# This software is a perl module whose purpose is to help you writing 
# your own scripts

# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# http://www.cecill.info" 

# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 

# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

=pod

=head1 NAME

Runner - Run an external program through fork, with timeout and other signal management

=head1 NOT AN OBJECT

This package is NOT an object oriented package

=cut

use strict;
use warnings;
use Carp;
use IO::File;
use Text::ParseWords;

BEGIN
{
    use Exporter ();
    use vars qw($VERSION @ISA @EXPORT);

    our $VERSION = do {my @r = (q$Rev: 606 $ =~ /\d+/g); $r[0]};
    @ISA         = qw(Exporter);
    @EXPORT      = qw( &RunExt &RunExtNoWait &RunExtTimed );
    $SIG{'TERM'} = sub {};          # Ignoring the SIGINT
    $SIG{'ALRM'} = \&__KillProcess;
}

our $RUN_PID   = 0;     # Only one process ran by RunExt
our @RUN_PID   = ();    # Many processes ran by RunExtNoWait
our $KILLED_BY = '';

#
# EXPORTED functions
#

=head2 RunExt

 my ($status, $killed) = RunExt(-cmd     => $cmd,
                                -stdin   => $stdin,
                                -stdout  => $stdout,
                                -stderr  => $sterr,
                                -timeout => $timeout,
                                -snippet => 1,
                                -dryrun  => 1,
                                -debug   => 1);

 PARAMETERS:

   -cmd     = The external command whith its parameters.
   -stdin   = The standard input, if redirected   (a file name, or anything you can pass to open)
   -stdout  = The standard output, if redirected  (a file name, or anything you can pass to open - You MUST specify > or >> in front of the name)
   -stderr  = The standard error, if redirected   (a file name, or anything you can pass to open - You MUST specify > or >> in front of the name)
   -timeout = The timeout in seconds. No timeout if -1
   -dryrun  = The command is NOT EXECUTED, it is only printed to stderr
   -debug   = The command is printed to stderr, THEN executed
   -log     = Should be a file handle opened for write. The command is printed to this file handle, before execution
              You can pass undef, in this case the switch is simply ignored
   -snippet = The command is a little shell snippet, it is executed through a call to /bin/sh
              WARNING - With the -snippet switch, PLEASE AVOID THE SIMPLE QUOTES (') IN YOUR COMMAND !!!
   -nice    = The command is called by nice
                  --> If value = 0, ignore (same as not specifying -nice)
                  --> If value = 1, use the MAX nice value allowed (19)
                  --> For any other value use this as the nice value


 RETURNED VALUES:
   $status  ==> the return value of the child
   $timeout ==> 0 if OK
                -1 if the child was killed by timeout
                -2 if the child was killed by TERM (ctrl-C, qdel, ...)

 The cmd is executed, it may be killed if a TERM signal or an ALMR is received. 
 The child is then killed with a TERM, followed by a KILL signal: it should return in any circumstance

=cut

sub RunExt
{
    my %args = @_;
    
    my @a_allowed = qw( -cmd -stdin -stdout -stderr -timeout -dryrun -debug -snippet -nice -log);
    croak("ERROR - Runner.pm internal error (RUN_PID $RUN_PID)") unless ($RUN_PID == 0);
    croak("ERROR - The cmd to run is NOT specified")             unless (exists($args{'-cmd'}));
    foreach my $key (keys %args)
    {
		croak ("ERROR - Forbidden key: $key") unless (grep /^$key$/, @a_allowed);
	}

    if ($KILLED_BY eq 'TERM')
    {
        return (0, -2);    # a TERM signal was received JUST BEFORE calling RunExt
    }
    else
    {
        $KILLED_BY = '';
    }

	# If the -snippet arg is specified, call /bin/sh to execute the comman
	$args{'-cmd'} = "/bin/sh -c '" . $args{'-cmd'} . "'" if ( exists $args{'-snippet'} );

	# If a nice was specified, it is time to use
	if ( exists $args{-nice} && $args{-nice} != 0)
	{
		my $nice_val = ($args{-nice} == 1) ? 19 : $args{-nice} == 1;
		$args{'-cmd'} = "nice -n $nice_val " . $args{'-cmd'};
	}

    my @tmp;
    push(@tmp,$args{'-cmd'});
    my @arguments = &shellwords(@tmp);
    # Program a timeout, if necessary
    if (exists $args{'-timeout'})
    {
        my $timeout = $args{'-timeout'} + 0;
        if ($timeout > 0)
        {
            alarm($args{'-timeout'});
        }
    }
    if ( exists $args{'-dryrun'} || exists $args{'-debug'} || exists $args{'-log'} )
    {
		my $cmd_to_print = join( ' ', @arguments );
		$cmd_to_print .= ' ' . $args{ '-stdin' }  if ( exists $args{ '-stdin' } );
		$cmd_to_print .= ' ' . $args{ '-stdout' } if ( exists $args{ '-stdout'} );
		$cmd_to_print .= ' 2' . $args{ '-stderr' } if ( exists $args{ '-stderr'} );
		if ( exists $args{'-dryrun'} || exists $args{'-debug'} )
		{
			warn "$cmd_to_print\n";
			warn "TIMEOUT " . $args{ '-timeout' } . "\n" if ( exists $args{ '-timeout' } );
		}
		if ( defined $args{'-log'} )
		{
			my $fh_log = $args{'-log'};
			print $fh_log "$cmd_to_print\n";
		}
		return (0,0) if exists $args{'-dryrun'};
	}
		
    $RUN_PID = fork();

    if ($RUN_PID == 0)
    {                      # T H E   C H I L D
        my $path = shift(@arguments);
        if (exists $args{'-stdin'})
        {
            close(STDIN);
            my $stdin = $args{'-stdin'};
            open(STDIN, $stdin) or die "CANNOT OPEN $stdin";
        }
        if (exists $args{'-stdout'})
        {
            close(STDOUT);
            my $stdout = $args{'-stdout'};
            open(STDOUT, $stdout) or die "CANNOT OPEN $stdout";
        }
        if (exists $args{'-stderr'})
        {
            close(STDERR);
            my $stderr = $args{'-stderr'};
            open(STDERR, $stderr) or die "CANNOT OPEN $stderr";
        }
        exec($path, @arguments);    # should NOT return from here
        exit 127;                   # If returns, something bad happened
    }

    my $rvl = waitpid($RUN_PID,0);    # T H E   P A R E N T
    
    my $sts = $? >> 8;
    $RUN_PID = 0;

    if ($KILLED_BY eq 'ALRM')
    {          # The child was killed by a timeout
        return (0, -1);
    }
    elsif ($KILLED_BY eq 'TERM')
    {          # The child was killed by a TERM signal (qdel)
        return (0, -2);
    }
    else
    {          # The child returned, with or without an error
        return ($sts, 0);
    }
}

=head2 function RunExtTimed

	Title      :	RunExtTimed
	Usage      :	my ( $status, $killed) = RunExtTimed( %hash );
	Prerequiste:	cf RunExt for more details.
	function   :	Call the RunExt function with its cmd prefixed with a time command
	                to print out the duration of the process.
	Args       :	-timefile => $string, the filename where to write duration
	            	-append => $bool, 1 if append, 0 to erase the file.
	Error      :    none
	Globals    :    none

=cut

sub RunExtTimed
{
	my %h_args = (	-append => 1,
					@_ );

	my $format = '%C => %U sec';	# print time used in sec. cf man time to know more
	#my $cmd = "/usr/bin/time -f '$format' ";
	my $cmd = "/usr/bin/time";
	&Carp::croak ( "ERROR - Before calling RunExtTimed, you should install the program \"gnu time\"") if ( ! -x $cmd );
	
	$cmd .= " -f '$format' ";

	#append ?
	$cmd .= "--append "	if( 1 == $h_args{ -append } );

	# output file ? default is STDERR
	my $out = $h_args{ -timefile };
	$cmd .= "-o $out "	if( defined $out );

	# call for RunExt
	#	the last -cmd will erase previous -cmd from @_
	return &RunExt( @_, -cmd => $cmd .$h_args{ -cmd } );
}

=head2 RunExtNoWait

 my $pid = RunExtNoWait(-cmd     => $cmd,
                        -stdin   => $stdin,
                        -stdout  => $stdout,
                        -stderr  => $sterr,
                        -snippet => 1,
                        -nice    => 1,
                        -dryrun  => 1,
                        -debug   => 1,
						-log     => 1);
                        );

 PARAMETERS:

   -cmd     = The external command whith its parameters.
   -stdin   = The standard input, if redirected   (a file name, or anything you can pass to open)
   -stdout  = The standard output, if redirected  (a file name, or anything you can pass to open)
   -stderr  = The standard error, if redirected   (a file name, or anything you can pass to open)
   -snippet = The command is a little shell snippet, it is executed through a call to /bin/sh
   -dryrun  = The command is NOT EXECUTED, it is only printed to stderr
   -debug   = The command is printed to stderr, THEN executed
   -nice    = The command is called by nice
                  --> If value = 0, ignore (same as not specifying -nice)
                  --> If value = 1, use the MAX nice value allowed (19)
                  --> For any other value use this as the nice value

 RETURNED VALUE:
   $pid ==> the process Id of the child

 The cmd is executed, but the sub returns without waiting for the child's death.
 The children started may be killed if a TERM signal or an ALARM signal are received.
 The child is then killed with a TERM signal. NO KILL signal is sent to the child,
 terminating the child in a graceful way. If I don't have to wait, I am less violent...

=cut
sub RunExtNoWait
{
    my %args = @_;
    my @a_allowed = qw( -cmd -stdin -stdout -stderr -snippet -nice -dryrun -debug -log);
    croak("The cmd to run is NOT specified") unless (exists($args{'-cmd'}));
    foreach my $key (keys %args)
    {
		croak ("ERROR - Forbidden key: $key") unless (grep /^$key$/, @a_allowed);
	}

    if ($KILLED_BY eq 'TERM')
    {
        return 0;    # a TERM signal was received JUST BEFORE calling RunExtNoWait
    }
    else
    {
        $KILLED_BY = '';
    }

	# If the -snippet arg is specified, call /bin/sh to execute the comman
	$args{'-cmd'} = "/bin/sh -c '" . $args{'-cmd'} . "'" if ( exists $args{'-snippet'} );

	# If a nice was specified, it is time to use
	if ( exists $args{-nice} && $args{-nice} != 0)
	{
		my $nice_val = ($args{-nice} == 1) ? 19 : $args{-nice} == 1;
		$args{'-cmd'} = "nice -n $nice_val " . $args{'-cmd'};
	}

	my @tmp;
    push(@tmp,$args{'-cmd'});
    my @arguments = &shellwords(@tmp);

	# Compute the command as a text if necessary
	my $cmd_to_print = '';
	if ( exists $args{'-dryrun'} || exists $args{'-debug'} || exists $args{'-log'} )
    {
		$cmd_to_print = join( ' ', @arguments );
		$cmd_to_print .= ' ' . $args{ '-stdin' }  if ( exists $args{ '-stdin' } );
		$cmd_to_print .= ' ' . $args{ '-stdout' } if ( exists $args{ '-stdout'} );
		$cmd_to_print .= ' 2' . $args{ '-stderr' } if ( exists $args{ '-stderr'} );
		
		if ( defined $args{'-log'} )
		{
			my $fh_log = $args{'-log'};
			print $fh_log "$cmd_to_print\n";
		}

		# If dryrun, print the command and return 0 (illegal PID number)
		if (exists $args{'-dryrun'})
		{
			warn "$cmd_to_print\n";
			return 0;
		}
	}

    my $run_pid = fork();

    if ($run_pid == 0)
    {                # T H E   C H I L D
        my $path = shift(@arguments);
        if (exists $args{'-stdin'})
        {
            close(STDIN);
            my $stdin = $args{'-stdin'};
            open(STDIN, $stdin) or die "CANNOT OPEN $stdin";
        }
        if (exists $args{'-stdout'})
        {
            close(STDOUT);
            my $stdout = $args{'-stdout'};
            open(STDOUT, $stdout) or die "CANNOT OPEN $stdout";
        }
        if (exists $args{'-stderr'})
        {
            close(STDERR);
            my $stderr = $args{'-stderr'};
            open(STDERR, $stderr) or die "CANNOT OPEN $stderr";
        }
        exec($path, @arguments);
        exit 127;
    }

	# If debug, print the command and the pid
	if ( exists $args{'-debug'} )
    {
		warn "$cmd_to_print - PID=$run_pid\n";
	}

    push(@RUN_PID, $run_pid);

    return $run_pid;

}

#
# Title: __KillProcess
# Usage: kill the process whose PID is in $RUN_PID.
#        Send him a TERM signal, then a KILL signal
#        kill the processes whose PID are in @RUN_PID, and remove them from @RUN_PID
#        Send them ONLY a TERM signal (less violence...).
#        Called by the ALRM and the TERM signal handlers
#

sub __KillProcess
{
    my $signame = shift;
    if ($RUN_PID != 0)
    {
        kill 'TERM', $RUN_PID;
        kill 'KILL', $RUN_PID;

        #	warn "$$ killed $RUN_PID";
    }
    while (my $pid = shift(@RUN_PID))
    {
        kill 'TERM', $pid;

        #  	warn "$$ killed $pid";
    }
    $KILLED_BY = $signame;
}

1;

