require 'common.pl';
require 'WebApp/authentication.pl';
require 'WebApp/account.pl';
require 'WebApp/upload.pl';
require 'WebApp/analysis.pl';
require 'WebApp/download.pl';
require 'WebApp/ws.pl';

sub __GetUserId
{
    my $ro_webbuilder = shift;
    my $o_authentic   = $$ro_webbuilder->GetObjectAuth();

    my $userid = lc($o_authentic->GetEmail());
    $userid = undef if ($userid eq '');
    return $userid;
}

=head2 function Cat

 Title        : Cat
 Usage        : my $content = &Cat($file)
 Prerequisite : none
 Function     : Get any file content
 Returns      : File content
 Args         : File path
 Globals      : none
	
=cut

sub Cat
{
    my $file    = shift;
    my $content = '';
    my $fh      = &GetStreamIn($file);
    while (my $line = <$fh>)
    {
        $content .= $line;
    }
    $fh->close;
    return $content;
}

1;
