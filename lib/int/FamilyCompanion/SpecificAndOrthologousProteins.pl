
=head2 function  ExtractSpecificAndOrthologousProteins

 Title        : ExtractSpecificProteins
 Usage        : &ExtractSpecificProteins($rh_homologygroups,$rh_species,$rh_specific_proteins, $outdir)
 Prerequisite : none
 Function     : 
 Returns      : 
 Args         : 
 Globals      : none

=cut

sub ExtractSpecificAndOrthologousProteins
{
    my ($rh_homologygroups, $rh_species,$rh_specific, $outdir) = @_;


	&Log("INFO - ExtractSpecificAndOrthologousProteins");
	
    my %h_inparalogs = ();


    my %h_orthologous = ();

	my @a_orthologous_groups = ();
    foreach my $groupid (sort {$a cmp $b} keys %{$rh_homologygroups})
    {
        if (scalar(keys %{$rh_homologygroups->{$groupid}}) == 1)
        {
            &Debug("INFO - $groupid is a group of inparalog only proteins");
            &__ExtracSequenceFromHomologyGroupsGroupId($rh_homologygroups, $rh_species, $groupid, \%h_inparalogs);

        }
        else
        {
            if (scalar(keys %{$rh_homologygroups->{$groupid}}) == scalar(keys %{$rh_species}))
            {
                if (&IsOrthologousGroup($rh_homologygroups, $groupid) == &TRUE)
                {
					push (@a_orthologous_groups, $rh_homologygroups->{$groupid});
                    &Debug("INFO - $groupid is a group of orthologous proteins");
                    &__ExtracSequenceFromHomologyGroupsGroupId($rh_homologygroups, $rh_species, $groupid, \%h_orthologous);
                }
            }
        }
    }

	my $rh_aligned_coreproteome = &GetOrthologousAlignments(\@a_orthologous_groups,\%h_orthologous);
	
	
    
    &PrintSpecificProteinsFiles($outdir, $rh_species, $rh_specific, \%h_inparalogs);
    &PrintOrthologousProteinsFiles($outdir, $rh_species, \%h_orthologous,$rh_aligned_coreproteome);

    return;
}

sub GetOrthologousAlignments
{
	my ($ra_orthologous_groups,$rh_orthologous) = @_;
	my %h_aligned_coreproteome = ();
	foreach my $rh_group  (@{$ra_orthologous_groups})
	{
		my ($fh_tmp, $tempfile) = tempfile(UNLINK => 1, DIR => $O_CONF->Get('tmpdir'));
		foreach my $code (keys %{$rh_group})
		{
			my $seqid = $rh_group->{$code}->[0];
			print $fh_tmp ">$code\n" . $rh_orthologous->{$code}->{$seqid}->{'sequence'} . "\n";
		}
		$fh_tmp->close;
		
		__Align($tempfile);
		__CleanAlignment($tempfile);
		
		if (-e "$tempfile.aln.clean")
		{
			my %h_alignment = ();
			&FastaToHash("$tempfile.aln.clean",\%h_alignment);
		
			unlink "$tempfile";
			unlink "$tempfile.aln";
			unlink "$tempfile.aln.clean";
		
			
			foreach my $code (sort keys %{$rh_group})
			{
				$h_aligned_coreproteome{$code} = {} unless defined  $h_aligned_coreproteome{$code};
				$h_aligned_coreproteome{$code}->{'sequence'} = '' unless defined  $h_aligned_coreproteome{$code}->{'sequence'};
				$h_aligned_coreproteome{$code}->{'sequence'} .= $h_alignment{$code}->{'sequence'};
			}
		}
	}
	return \%h_aligned_coreproteome;
}


sub __Align
{
	my $infile = shift;
	my $cmd = "$MAFFT --thread $THREADS --globalpair --quiet $infile > $infile.aln";
	&System($cmd);
	my %h_alignment = ();
	return;
}

sub __CleanAlignment
{
	my $infile = shift;
	my $cmd = "$TRIMAL -gt 0.9 -keepheader -in $infile.aln -out $infile.aln.clean";
	&System($cmd);
	return;
}

sub IsOrthologousGroup
{
    my ($rh_homologygroups, $groupid) = @_;

    my $is_orthologous = &TRUE;
    foreach my $code (keys %{$rh_homologygroups->{$groupid}})
    {
        if (scalar(@{$rh_homologygroups->{$groupid}->{$code}}) > 1)
        {
            $is_orthologous = &FALSE;
            last;
        }
    }
    return $is_orthologous;
}

sub PrintSpecificProteinsFiles
{
    my ($outdir, $rh_species, $rh_specific, $rh_inparalogs) = @_;
    mkdir "$outdir/specific_proteins";
    my $rh_description = {'specific_proteins' => 'Extraction of Specific protein sequences and annotation'};
    
    my $fh_readme = &GetStreamOut("$outdir/specific_proteins/README");
    print $fh_readme <<END;
    Splitted in two subsets : 
		- specifics "inparalogs" : proteins belonging to a mono-species homology group
		- singlecopy specific proteins: proteins present in one and only one copy in only one species
END
    $fh_readme->close;
    &__PrintSpecificProteinsFilesByType("$outdir/specific_proteins", $rh_species, $rh_specific,   'singlecopy',$rh_description);
    &__PrintSpecificProteinsFilesByType("$outdir/specific_proteins", $rh_species, $rh_inparalogs, 'inparalogs',$rh_description);
    push(
         @A_OUTPUT_DATA,
         {
          'directory' => 'specific_proteins',
          'description' => $rh_description
         }
         );


    return;
}

sub PrintOrthologousProteinsFiles
{
    my ($outdir, $rh_species, $rh_orthologous, $rh_aligned_coreproteome) = @_;
    mkdir "$outdir/orthologous_proteins";

	my $rh_description = {'orthologous_proteins' => 'Extraction of "orthologous" protein sequences and annotation'};
    
    &__PrintSpecificProteinsFilesByType("$outdir/orthologous_proteins", $rh_species, $rh_orthologous, 'orthologous',$rh_description);

	push(
         @A_OUTPUT_DATA,
         {
          'directory' => 'orthologous_proteins',
          'description' => $rh_description
         }
         );

    my $fh_readme = &GetStreamOut("$outdir/orthologous_proteins/README");
    print $fh_readme <<END;
Orthologuous proteins belongs to homology groups containing one and only one protein sequence copy for each of the species
Number of taxa = Number of genes = Number of input species
END
    $fh_readme->close;

	
    mkdir "$outdir/core_proteome";

    push(
         @A_OUTPUT_DATA,
         {
          'directory' => 'core_proteome',
          'description' =>{ 'core_proteome' => 'Core proteome pseudomolecule', 'core_proteome/aligned.core.proteome.fasta' => 'Alignment of the core proteome'}
         }
         );

    $fh_readme = &GetStreamOut("$outdir/core_proteome/README");
    print $fh_readme <<END;

+ aligned.core.proteome.fasta: 
	Alignment of the core proteome: each orthologous family has been individually aligned (using MAFFT software) and trimmed using trimal to remove all positions in the alignment with gaps in 10% or more of the sequences.
	Then all aligned sequence is concatenated per species.
	
	Usefull to build species tree.
END
    $fh_readme->close;

	#~ my $fh_coreproteome = &GetStreamOut("$outdir/core_proteome/pseudosequences.core.proteome.fasta");
    my $fh_aligned_coreproteome = &GetStreamOut("$outdir/core_proteome/aligned.core.proteome.fasta");
    
    foreach my $code (keys %{$rh_aligned_coreproteome})
    {
        #~ my $seq = $h_coreproteome{$code};
        #~ $seq = &FormatSeq(\$seq);
        #~ print $fh_coreproteome ">$code\n$seq\n";
        
        my $ali = $rh_aligned_coreproteome->{$code}->{'sequence'};
        print $fh_aligned_coreproteome ">$code\n$ali\n";
    }
    #~ $fh_coreproteome->close;
    $fh_aligned_coreproteome->close;
    return;
}

sub __PrintSpecificProteinsFilesByType
{
    my ($outdir, $rh_species, $rh_sequences, $suffix, $rh_description) = @_;

    foreach my $code (keys %{$rh_species})
    {
        mkdir "$outdir/$code";
        $rh_description->{basename ($outdir) . "/$code"} = $rh_species->{$code}->{'title'} . " $suffix proteins analysis";
        mkdir "$outdir/$code/fasta_files";

		$rh_description->{basename ($outdir) . "/$code/fasta_files/$code.$suffix.fasta"} = $rh_species->{$code}->{'title'} . " $suffix sequences";
		$rh_description->{basename ($outdir) . "/$code/$code.$suffix.quality.xls"} = $rh_species->{$code}->{'title'} . " $suffix table";

        my $fh_fasta   = &GetStreamOut("$outdir/$code/fasta_files/$code.$suffix.fasta");
        my $fh_quality = &GetStreamOut(">$outdir/$code/$code.$suffix.quality.xls");
        print $fh_quality "#seqid\tgroupid\tlength\t%of undetermined AA (X or *)\n";

        foreach my $seqid (keys %{$rh_sequences->{$code}})
        {
            my $sequence   = $rh_sequences->{$code}->{$seqid}->{'sequence'};
            my $annotation = $rh_sequences->{$code}->{$seqid}->{'annotation'};
            my $groupid    = 'null';
            $groupid = $rh_sequences->{$code}->{$seqid}->{'groupid'}
              if (defined $rh_sequences->{$code}->{$seqid}->{'groupid'});
            my $len          = length $sequence;

            if ($len < 1)
            {
				&Kill("Empty sequence - Check consistency between Homologous groups protein accessions and fasta IDs");
			}
            my $undetermined = 0;
            while ($sequence =~ /X|\*/g)
            {
                $undetermined++;
            }
            $undetermined_percentage = $undetermined / $len;
            $sequence                = &FormatSeq(\$sequence);

            print $fh_fasta ">$seqid len=$len groupid=$groupid def=$annotation\n$sequence\n";
            print $fh_quality "$seqid\t$groupid\t" . $len . "\t" . $undetermined_percentage . "\n";

        }
        &BuildIprscanDBTables($rh_sequences->{$code}, $code, $outdir, "specific.$suffix",$rh_species, $rh_description);

        $fh_fasta->close;
        $fh_quality->close;
    }

    return;

}

sub __ExtracSequenceFromHomologyGroupsGroupId
{
    my ($rh_homologygroups, $rh_species, $groupid, $rh_result) = @_;

    foreach my $code (keys %{$rh_homologygroups->{$groupid}})
    {
        $rh_result->{$code} = {} unless defined $rh_result->{$code};
        foreach my $seqid (@{$rh_homologygroups->{$groupid}->{$code}})
        {
            if (!defined $rh_species->{$code}->{'sequences'}->{$seqid})
            {
                &Log("WARNING - $groupid - $seqid not defined in $code sequence file");
            }
            else
            {
                $rh_result->{$code}->{$seqid} = $rh_species->{$code}->{'sequences'}->{$seqid};
                $rh_result->{$code}->{$seqid}->{'groupid'} = $groupid;
            }
        }
    }
    return;

}

1;
