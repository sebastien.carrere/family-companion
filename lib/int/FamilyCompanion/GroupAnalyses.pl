
=head2 function ListGroupAndUniqueForAllProteomes

 Title        : ListGroupAndUniqueForAllProteomes
 Usage        : &ListGroupAndUniqueForAllProteomes($rh_homologygroups,$rh_species,$rh_specific_proteins,$o_param->Get('outdir'))
 Prerequisite : none
 Function     : Find for each proteome, the list of all group with a protein of this proteome + unique protein
 Returns      : none
 Args         : hash ref of homology groups, hash ref of species, hash ref of specific proteins, outdir
 Globals      : none

=cut

sub ListGroupAndUniqueForAllProteomes
{
    my ($rh_homologygroups, $rh_species, $rh_specific, $outdir, $owner) = @_;
    my %h_list_group_by_code;
   
	&Log("INFO - ListGroupAndUniqueForAllProteomes");

    foreach my $code (keys %{$rh_species})
    {
        $h_list_group_by_code{$code} = [];
    }

    foreach my $groupid (keys %{$rh_homologygroups})
    {
        foreach my $code (keys %{$rh_homologygroups->{$groupid}})
        {
            push (@{$h_list_group_by_code{$code}}, $groupid);
        }
    }

    foreach my $code (keys %{$rh_specific})
    {
        my $nb = scalar(keys %{$rh_specific->{$code}});
        &Debug("INFO - $code contains $nb unique specific protein");
        foreach my $seqid (keys %{$rh_specific->{$code}})
        {
            push (@{$h_list_group_by_code{$code}}, "$code|$seqid");
        }
    }

    &PrintListGroupProteome($outdir, \%h_list_group_by_code, $owner);

    return;
}

=head3 function PrintListGroupProteome

 Title        : PrintListGroupProteome
 Usage        : &PrintListGroupProteome($outdir,\%h_list_group_by_code)
 Prerequisite : none
 Function     : Print for each proteome, a list of all group with a protein of this proteome + unique protein
 Returns      : none
 Args         : outdir, hash of group list
 Globals      : none

=cut

sub PrintListGroupProteome
{
    my ($outdir, $rh_listgroupbycode, $owner) = @_;
    my $subdirectory = "venn_diagrams";
    my $subdirectory_description =
      'Lists of all groups with a protein of this proteome + unique protein by proteome';
    mkdir "$outdir/$subdirectory";
    mkdir "$outdir/$subdirectory/lists";

    push (@A_OUTPUT_DATA, {'directory' => $subdirectory, 'description' => {$subdirectory => $subdirectory_description}});
    foreach my $code (keys %{$rh_listgroupbycode})
    {
        my $fh_liste = &GetStreamOut("$outdir/$subdirectory/lists/$code.list.txt");
        foreach my $group (@{$rh_listgroupbycode->{$code}})
        {
            print $fh_liste "$group\n";
        }
        $fh_liste->close;
    }
    
    my $fh_readme = &GetStreamOut("$outdir/$subdirectory/README");
    print $fh_readme <<END;
Generate Venn diagrams that show shared and specific families proteins of selected proteomes.
On these Venn diagrams :
- intersection between proteomes contain number of homology groups shared by these proteomes.
- number of specific elements of a proteome is the sum of :
    * specific inparalogs groups of this proteome
    * specific singlecopy protein of this proteome.
Sub directory lists contain, for each proteome, the list of each group that contain a protein of this proteome and specific singlecopy protein of this proteome.

END
	$fh_readme->close;

    &CreateJsonVenn($rh_listgroupbycode, "$outdir/$subdirectory", "Homology Groups content analysis", $owner);

    return;
}

1;
