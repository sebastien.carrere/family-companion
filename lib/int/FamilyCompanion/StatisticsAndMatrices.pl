#~ use GD::Graph::bars;

=head2 function StatisticsAndMatrices

 Title        : StatisticsAndMatrices
 Usage        : &StatisticsAndMatrices($rh_homologygroups,$rh_analysis_params->{'proteomes'},$o_param->Get('outdir'));
 Prerequisite : none
 Function     : Generate descriptive statistics, an abundance matrix, a presence/absence matrix (2 graphs, 4 tabular files)
 Returns      : none
 Args         : hash ref of homologygroups, proteomes hash ref of analysis parameters, outdir
 Globals      : none

=cut

sub StatisticsAndMatrices
{
    my ($rh_homologygroups, $rh_proteome, $outdir) = @_;

    &Log("INFO - StatisticsAndMatrices");

    my (@a_proteome_name, %h_nbprot_by_grp, %h_nbtaxa_by_grp);
    my (%h_matrice_quanti, %h_matrice_binary);
    my %h_graph_prot_att = (
                            x_label => 'Homology groups',
                            y_label => 'Number of proteins',
                            title   => 'Homology Groups Vs. Number of proteins'
                            );
    my %h_graph_taxa_att = (
                            x_label => 'Homology groups',
                            y_label => 'Number of taxa',
                            title   => 'Homology Groups Vs. Number of taxa'
                            );

    my $nb_grpomcl  = scalar(keys(%{$rh_homologygroups}));
    my $nb_proteome = scalar(keys(%{$rh_proteome}));
    push(@a_proteome_name, sort(keys(%{$rh_proteome})));

    foreach my $grp (sort __sort_by_by_group_number (keys(%{$rh_homologygroups})))
    {
        for (my $x = 0; $x < $nb_proteome; $x++)
        {
            if (exists($$rh_homologygroups{$grp}{$a_proteome_name[$x]}))
            {
                # descritive_stats
                $h_nbprot_by_grp{$grp} += scalar(@{$$rh_homologygroups{$grp}{$a_proteome_name[$x]}});
                $h_nbtaxa_by_grp{$grp}++;

                # matrices
                $h_matrice_quanti{$grp}{$a_proteome_name[$x]} += scalar(@{$$rh_homologygroups{$grp}{$a_proteome_name[$x]}});
                $h_matrice_binary{$grp}{$a_proteome_name[$x]} = 1;
            }
            else
            {
                # matrices
                $h_matrice_quanti{$grp}{$a_proteome_name[$x]} = 0;
                $h_matrice_binary{$grp}{$a_proteome_name[$x]} = 0;
            }
        }
    }

    # print
    mkdir "$outdir/statistics_matrices";
    mkdir "$outdir/statistics_matrices/xls_files/";

    push(
         @A_OUTPUT_DATA,
         {
          'directory' => 'statistics_matrices',
          'description' =>
          {
			    'statistics_matrices' => ' Phylogenetics matrices',
				'statistics_matrices/BinaryMatrix.txt' => 'Presence/Absence matrix',
				'statistics_matrices/AbundancyMatrix.txt.html' => 'Abundance heatmap',
				'statistics_matrices/BinaryMatrix.txt.html' => 'Presence/Absence heatmap',
				'statistics_matrices/AbundancyMatrix.txt' => 'Abundance matrix',
				'statistics_matrices/Number_Of_Proteins_by_homology_group.classes.html' => 'Proteins by homology group barplot',
				'statistics_matrices/Number_Of_Taxa_by_homology_group.classes.html' => 'Taxa by homology group barplot'
			  }
         }
         );


    # Print tabular stat used to draw graphs
    my $rh_count_class_nbprot_by_grp = &PrintDescriptiveStatisticsFiles($outdir, 'Number_Of_Proteins_by_homology_group',
                                                                        \%h_nbprot_by_grp, $nb_grpomcl, $nb_proteome);
    my $rh_count_class_nbtaxa_by_grp = &PrintDescriptiveStatisticsFiles($outdir, 'Number_Of_Taxa_by_homology_group',
                                                                        \%h_nbtaxa_by_grp, $nb_grpomcl, $nb_proteome);

    &__BuildClassCountJS($rh_count_class_nbprot_by_grp,
                         'Number_Of_Proteins_by_homology_group',
                         'Nb of proteins',
                         'Nb of groups', $outdir);
    &__BuildClassCountJS($rh_count_class_nbtaxa_by_grp, 'Number_Of_Taxa_by_homology_group',
                         'Nb of taxa', 'Nb of groups', $outdir);

    # Print matrices
    &PrintPhylogeneticsMatricesFiles("$outdir/statistics_matrices/AbundancyMatrix.txt",        \%h_matrice_quanti);
    my $convert_cmd = $O_CONF->Get('portal_install_dir') . '/bin/int/convert_matrixToheatmap.pl'
						. " --type=abundancy --infile=$outdir/statistics_matrices/AbundancyMatrix.txt";
	&System($convert_cmd);
	
    &PrintPhylogeneticsMatricesFiles("$outdir/statistics_matrices/BinaryMatrix.txt", \%h_matrice_binary);
	$convert_cmd = $O_CONF->Get('portal_install_dir') . '/bin/int/convert_matrixToheatmap.pl'
						. " --type=binary --infile=$outdir/statistics_matrices/BinaryMatrix.txt";
	&System($convert_cmd);
	return;
}

sub __BuildClassCountJS
{
    my ($rh_classes, $prefix, $xaxis, $yaxis, $outdir) = @_;

    
    my $fh_out_xls = &GetStreamOut("$outdir/statistics_matrices/xls_files/$prefix.classes.count.xls");
    print $fh_out_xls "##$prefix\n";
    print $fh_out_xls "#Class\tcount\n";

    my $rh_data =  {'title' => $prefix, 'data' => [], 'path' => "xls_files/$prefix.xls", 'xaxis' => $xaxis, 'yaxis' => $yaxis};

	my @a_files;
	my @a_titles;
	my @a_yaxis;
	my @a_startcolumn;

    foreach my $class (sort {$a <=> $b} keys %{$rh_classes})
    {
        print $fh_out_xls "$class\t" . $rh_classes->{$class} . "\n";
        
    }
    $fh_out_xls->close;

	push (@a_files, "xls_files/$prefix.classes.count.xls");
	push (@a_titles, "$prefix");
	push (@a_yaxis, $yaxis);
	push (@a_startcolumn, 1);
    
    my $root_url = __GetLocalUrl();

	my $file_list = join("','", @a_files);
	my $title_list = join("','", @a_titles);
	my $yaxis_list = join("','", @a_yaxis);
	my $startcolumn_list = join(",", @a_startcolumn);


    my $fh_html_template = &GetStreamIn("$FindBin::RealBin/../../web/templates/hist/index.html");
    my $fh_html_out      = &GetStreamOut("$outdir/statistics_matrices/$prefix.classes.html");
	while (my $line = <$fh_html_template>)
	{
		$line =~ s/%ROOT_URL%/$root_url/g;
		$line =~ s/%TITLE%/$prefix/g;
		$line =~ s/%TITLE_LIST%/$title_list/g;
		$line =~ s/%YAXIS_LIST%/$yaxis_list/g;
		$line =~ s/%FILE_LIST%/$file_list/g;
		$line =~ s/%STARTCOLUMN_LIST%/$startcolumn_list/g;
		print $fh_html_out $line;
	}
		
    $fh_html_out->close;
    $fh_html_template->close;

    return;
}

=head2 function PrintDescriptiveStatisticsGraph

 Title        : PrintDescriptiveStatisticsGraph
 Usage        : &PrintDescriptiveStatisticsGraph($path_outfile,$rh_att,$rh_data)
 Prerequisite : none
 Function     : Draw and save a graph (png)
 Returns      : none
 Args         : param1 => complete path of output file , 
                param2 => hash ref of attributs to draw a barplot, 
                param3 => hash ref with key = homology group and value = number of something in omclgrp
 Globals      : none
 Notes        : * $o_graph->set_attr()
                  ___________________ ___________________________________________________________
                |                   | 
                | set_text_clr      | set the color of the text
                | set_title_size    | set the font that will be used for the title of the chart
                |___________________|___________________________________________________________
                
                * $o_graph->set(options used to draw the bars plot)
                 ___________________ ___________________________________________________________
                |                   | 
                | x_label           | label of horizontal axis
                | y_label           | label of vertical axis
                | title             | title of graph
                | y_max_value       | maximum value displayed on the y axis
                | y_label_skip      | default:1 => print every x_label_skip.th number under 
                |                   | the tick on the y axis 
                | x_labels_vertical | default:0 => X axis labels will be printd vertically
                | bar_spacing       | number of pixels to leave open betwwen bars
                | shadow_depth      | depth of a shadow
                | transparent       | transparent background default:1
                | fgclr             | color used for foreground color
                | bgclr             | color used for background color
                | dclrs             | color of bar sets 
                | accentclr         | color used for bar, area and pie outlines
                | shadowclr         | color used for  shadow; only for bars
                |___________________|___________________________________________________________      


sub PrintDescriptiveStatisticsGraph
{
	my ($outdir,$rh_att,$rh_data) = @_;
	
	my (@a_data);
	foreach my $cle ( sort { $$rh_data{$b} <=> $$rh_data{$a} || $a cmp $b } keys %{$rh_data} )
	{
		push( @{$a_data[0]},$cle );
		push( @{$a_data[1]},$$rh_data{$cle} );
	}
	
	# nex obj graph
	my $o_graph = GD::Graph::bars->new(800, 800);	
	
	# attr text
	$$rh_att{'x_label'} = 'Abscisses' if( !exists( $$rh_att{'x_label'} ) );
	$$rh_att{'y_label'} = 'Ordonnees' if( !exists( $$rh_att{'y_label'} ) );
	$$rh_att{'title'}   = 'Abscisses Vs. Ordonnees' if( !exists( $$rh_att{'title'} ) );
	# attr numeric
	$$rh_att{'y_max_value'}       = undef if( !exists( $$rh_att{'y_max_value'} ) );
	$$rh_att{'y_label_skip'}      = 2     if( !exists( $$rh_att{'y_label_skip'} ) );	
	$$rh_att{'x_labels_vertical'} = 1     if( !exists( $$rh_att{'x_labels_vertical'} ) );
	$$rh_att{'bar_spacing'}       = 2     if( !exists( $$rh_att{'bar_spacing'} ) );
	$$rh_att{'shadow_depth'}      = 0     if( !exists( $$rh_att{'shadow_depth'} ) );
	$$rh_att{'transparent'}       = 0     if( !exists( $$rh_att{'transparent'} ) );
    # attr color
	$$rh_att{'textclr'}   = 'black' if( !exists( $$rh_att{'textclr'} ) );			
	$$rh_att{'fgclr'}     = 'black' if( !exists( $$rh_att{'fgclr'} ) );		
	$$rh_att{'bgclr'}     = 'white' if( !exists( $$rh_att{'bgclr'} ) );	
	$$rh_att{'dclrs'}     = 'white' if( !exists( $$rh_att{'dclrs'} ) );
	$$rh_att{'accentclr'} = 'black' if( !exists( $$rh_att{'accentclr'} ) );
	$$rh_att{'shadowclr'} = 'grey'  if( !exists( $$rh_att{'shadowclr'} ) );
	# set_attr
	$$rh_att{'set_text_clr'} = 'black'  if( !exists( $$rh_att{'set_text_clr'} ) );
	$$rh_att{'set_font'} = '/usr/share/texmf/fonts/opentype/public/lm/lmmonocaps10-regular.otf' if( !exists( $$rh_att{'set_font'} ) );
	$$rh_att{'set_title_size'}  = 14 if( !exists( $$rh_att{'set_title_size'} ) );

	# graph obj
	$o_graph->set_text_clr('black');
	$o_graph->set_title_font($$rh_att{'set_font'}, $$rh_att{'set_title_size'});
	$o_graph->set( 
		x_label				=> $$rh_att{'x_label'},
		y_label				=> $$rh_att{'y_label'},
		title				=> $$rh_att{'title'},
		y_max_value			=> $$rh_att{'y_max_value'},
		y_label_skip		=> $$rh_att{'y_label_skip'},
		x_labels_vertical	=> $$rh_att{'x_labels_vertical'},
		bar_spacing			=> $$rh_att{'bar_spacing'},
		shadow_depth		=> $$rh_att{'shadow_depth'},
		transparent			=> $$rh_att{'transparent'},
		fgclr				=> $$rh_att{'fgclr'},
		bgclr				=> $$rh_att{'bgclr'},
		dclrs				=> [$$rh_att{'dclrs'}],
		accentclr			=> $$rh_att{'accentclr'},
		shadowclr			=> $$rh_att{'shadowclr'}
	  ) or die $o_graph->error;

	my $gd = $o_graph->plot(\@a_data) or die $o_graph->error;
    
	open(IMG, '>' . $outdir ) or die $!;
    binmode IMG;
    print IMG $gd->png;	
}

=cut

=head2 function PrintDescriptiveStatisticsFiles

 Title        : PrintDescriptiveStatisticsFiles
 Usage        : &PrintDescriptiveStatisticsFiles($path_outfile,\%h_nbprot_by_grp,$nb_grpomcl,$nb_proteome);
 Prerequisite : none
 Function     : Print tabular file (used to draw graph) order by 'number of' and into 'number of' order by 'omclgrp' 
 Returns      : none
 Args         : param1 => complete path of output file ,
                param2 => hash ref with key = homology group and value = number of something in omclgrp
                param3 => total number of homology group
                param4 => total number of proteome
 Globals      : none

=cut

sub PrintDescriptiveStatisticsFiles
{
    my ($outdir, $prefix, $rh_data, $ngroup, $nproteome) = @_;

    my $fh_stat   = &GetStreamOut("$outdir/statistics_matrices/xls_files/$prefix.xls");
    my %h_classes = ();
    print $fh_stat join("\t", '##Number of homology group', $ngroup) . "\n";
    print $fh_stat join("\t", '##Number of Proteome',       $nproteome) . "\n\n";
    print $fh_stat join("\t",   '#GROUP',                    'Number') . "\n";
    foreach my $k (sort __sort_by_by_group_number keys %{$rh_data})
    {
		$h_classes{$$rh_data{$k}} = 0 if (!defined($h_classes{$$rh_data{$k}}));
        $h_classes{$$rh_data{$k}}++;
        print $fh_stat join("\t", $k, $$rh_data{$k}) . "\n";
    }
    $fh_stat->close();
    return \%h_classes;
}

=head2 function PrintPhylogeneticsMatricesFiles

 Title        : PrintPhylogeneticsMatricesFiles
 Usage        : &PrintPhylogeneticsMatricesFiles($path_outfile,$rh_matrice);
 Prerequisite : none
 Function     : Print tabular file
 Returns      : none
 Args         : param1 => outdir,
                param2 => hash ref, keys : {homology group}->{proteome} , value = number of in-paralogs into pronteome into homology group 
 Globals      : none

=cut

sub PrintPhylogeneticsMatricesFiles
{
    my ($outfile, $rh_data) = @_;

    my (@a_title, @a_row);
    push(@{$a_title[0]}, "GROUP");

    my $ct = 0;
    foreach my $grp (sort __sort_by_by_group_number keys %{$rh_data})
    {
        push(@{$a_row[$ct]}, $grp);
        foreach my $p (sort {$a cmp $b} keys %{$rh_data->{$grp}})
        {
			push(@{$a_title[0]}, $p) if ($ct == 0);
            push(@{$a_row[$ct]}, $$rh_data{$grp}{$p});
        }
        $ct++;
    }
    my ($ra_matrice) = [@a_title, @a_row];

    my $fh_matrice = &GetStreamOut("$outfile");
    foreach my $row (@{$ra_matrice})
    {
        print $fh_matrice join("\t", @{$row}) . "\n";
    }
    $fh_matrice->close();
	return;
}

1;
