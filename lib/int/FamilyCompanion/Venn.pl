
=head2 procedure

 Title        : CreateJsonVenn
 Usage        : 	my $rh_sets = {};
					$rh_sets->{"set1"} = ["id1,id2"];
					$rh_sets->{"set2"} = ["id2,id3,id4"];
					$rh_sets->{"set3"} = ["id1,id2,id5,id6"];
					$rh_sets->{"set4"} = ["id3,id1"];
					&CreateJsonVenn($rh_sets, $outdir, $title);
 Prerequisite : General, JSON, File::Path, the jvenn libray installed
 Function     : creates a json for jvenn from a set of ids
 Returns      : nothing
 Args         : 1. a hashref containing arrays of strings containing the ids separated by commas 
 				2. a directory path to save the json and venn.html
 				3. a title for the html page
 Globals      : none

=cut 

sub CreateJsonVenn
{

    my ($rh_sets, $outdir, $title, $owner) = @_;

	my @a_series = ();

	my $analysisid = basename($OUTDIR);
	my $rooturl = __GetPublicUrl();
	
    foreach my $name (sort keys %{$rh_sets})
    {
		
        my $rh_serie = {};
        $rh_serie->{"name"} = $name;
		$rh_serie->{"data"} = $rh_sets->{$name} ;
		
        push(@a_series, $rh_serie);

    }

    my $json_series = to_json(\@a_series, {pretty => 1, canonical => 1});

    if (!-d $outdir)
    {
        make_path($outdir);
    }

    my $json_file = "$outdir/series.json";
    my $fh_output = &GetStreamOut($json_file);
    print $fh_output "$json_series";
    $fh_output->close();
    &MinifyJson($json_file);


	my $o_pp = New ParamParser("$FindBin::RealBin/../../site/cfg/site.cfg");
	my $root_url = __GetLocalUrl();

	my $fh_html_template = &GetStreamIn("$FindBin::RealBin/../../web/templates/venn/index.html");
	my $fh_html_out = &GetStreamOut("$outdir/venn.html");
	while (my $line = <$fh_html_template>)
	{
		$line =~ s/%ROOT_URL%/$root_url/g;
		$line =~ s/%TITLE%/$title/g;
		$line =~ s/%ANALYSISID%/$analysisid/g;
		$line =~ s/%OWNER%/$owner/g;
		
		my $readme_html = '';
		if (-e "$outdir/README")
		{
			 $readme_html = '<textarea readonly id="readme" style="width: 100%;" wrap="off" rows="6">' . &Cat("$outdir/README") . '</textarea>';
		}
		$line =~ s/%README%/$readme_html/g;
		print $fh_html_out $line;
	}
	$fh_html_out->close;
	$fh_html_template->close;
		
	return;
}

1;
