

=head2 procedure BuildSummaryFile

 Title        : BuildSummaryFile
 Usage        : &BuildSummaryFile($outdir)
 Prerequisite : none
 Function     : Build a summary file of the analysis output directory
 Returns      : none
 Args         : Analysis parameters, Output directory
 Globals      : @A_OUTPUT_DATA
	
=cut

sub BuildSummaryFile
{
    my ($rh_analysis_params, $outdir) = @_;

	
	my @a_dir_tree = ();
    foreach my $rh_output_data (@A_OUTPUT_DATA)
    {

		my $rh_dir_tree = __TreeJson($outdir,$rh_output_data);
		push (@a_dir_tree, $rh_dir_tree);
        #~ $rh_output_data->{'status'}  = $status;
        #~ $rh_output_data->{'message'} = $message;
        #~ $rh_output_data->{'files'}   = \@a_files;

    }
    

    my %h_data = ('analysis' => $rh_analysis_params, 'results' => { 'root' =>  {'expanded' => &TRUE, 'children' => \@a_dir_tree} });
    my $fh_out = &GetStreamOut("$outdir/results.json");
    print $fh_out to_json(\%h_data, {pretty => 1});
    $fh_out->close;

    $fh_out = &GetStreamOut("$outdir/success");
    print $fh_out "Finished : " . `date "+%Y%m%d %H:%M:%S"`;
    $fh_out->close;

    $fh_out = &GetStreamOut("$outdir/title");
    print $fh_out $rh_analysis_params->{'title'};
    $fh_out->close;

	return;
}

=pod

JE PENSE QU'IL Y A BCP PLUS SIMPLE !!!

=cut


sub __TreeJson
{
	my ($outdir,$rh_output_data)  = @_;
	
	my %h_file_icons = ('fasta' => 'icon-application_view_list', 'xls' => 'icon-page_excel', 'tsv' => 'icon-page_excel', 'html' => 'icon-xhtml', 'classes.html' => 'icon-chart_bar' , 'count.html' => 'icon-chart_bar', 'percentage.html' => 'icon-chart_bar');
    my $name = $rh_output_data->{'directory'};
    my $rh_description = $rh_output_data->{'description'};
	
	my $cmd = "cd $outdir; find $name | grep -v -E 'error\|success\|README\|.js'";
	&Log("CMD - $cmd");
    my @a_raw_files = `$cmd`;
    chomp @a_raw_files;
    my @a_files = ();
    
    foreach my $file (@a_raw_files)
    {
		if (-d "$outdir/$file")
		{
			#Pour tri profoondeur d'abord et differencier fichiers et rep de meme niveau
			$file .= '/.';
		}
		push(@a_files, $file);
	}
    my $directory = "$outdir/$name";
    
    my $rh_dirtree = {};
    my $rh_last = {};
    my $last_upper = undef;
    my %h_upper_dir = ();
    foreach my $file (sort {scalar (split ('/', $a)) <=> scalar (split ('/', $b))} @a_files)
    {
		#~ print STDERR "FILE:$file\n";
		#~ print STDERR "GREP:" . scalar (split ('/', $file)) . "\n" ;
		$file =~ s,/\.$,,g;
		my $file_description = '';
		$file_description = $rh_description->{$file};	
		
		my %h_tree = ('name' => basename ($file), 'status' => 'success', 'description' => $file_description, 'message' => '' , 'link' => '', 'children' => []);
		
		my $upper_dir = dirname($file);
		
		if (-d "$outdir/$file")
		{
			$h_tree{'message'}  = &Cat("$outdir/$file/success") if (-e "$outdir/$file/success");
			$h_tree{'message'}  .= &Cat("$outdir/$file/README") if (-e "$outdir/$file/README");
			if (-e "$outdir/$file/error")
			{
				$h_tree{'status'}  = 'error';
				$h_tree{'message'}  = &Cat("$outdir/$file/error")	
			}

			if ($upper_dir ne '.')
			{
				push (@{$h_upper_dir{$upper_dir}->{'children'}}, \%h_tree);
			}
			$h_upper_dir{$file} = \%h_tree unless defined ($h_upper_dir{$file});
			
		}
		else
		{
			
				delete $h_tree{'children'};
				$h_tree{'leaf'} = &TRUE;
				$h_tree{'link'} = $file;
				
				foreach my $ext (sort {length $a <=> length $b} (keys %h_file_icons))
				{
					$h_tree{'iconCls'} = $h_file_icons{$ext} if ($file =~ /\.$ext$/);
				}
				
				push (@{$h_upper_dir{$upper_dir}->{'children'}}, \%h_tree);
		}
	}

	
	return $h_upper_dir{$name};

}


=head2 procedure GetAnalysisSummary

 Title        : GetAnalysisSummary
 Usage        : &GetAnalysisSummary($rh_species, $rh_count_proteins_in_group_by_code,$rh_count_inparalogs_by_code,$rh_count_specific_inparalogs_by_code,$rh_specific_proteins,$outdir);
 Prerequisite : none
 Function     : Build a summary file of the raw analysis count
 Returns      : none
 Args         : hash refs of raw counts (mainly from CheckProteomeCodesConsistency and GetSpecificProteins)
 Globals      : @A_OUTPUT_DATA
	
=cut

sub GetAnalysisSummary
{
	my ($rh_species, $rh_count_proteins_in_group_by_code,$rh_count_inparalogs_by_code,$rh_count_specific_inparalogs_by_code,$rh_specific_proteins,$outdir) = @_;
	
	unshift (@A_OUTPUT_DATA, {'directory' => 'summary',
							'description' => {
									'summary' => 'Analysis summary',
									'summary/summary.html' => 'Global counts and percentage barplot',
									'summary/summary.count.xls' => 'Global counts',
									'summary/summary.percent.xls' => 'Global percentages'
									}
								});
	
	mkdir "$outdir/summary";
	my $fh_out_count = &GetStreamOut("$outdir/summary/summary.count.xls");
	my $fh_out_percent = &GetStreamOut("$outdir/summary/summary.percent.xls");
	my @a_species = keys %{$rh_species};
	my @a_perc_headers =  ('PROTEINS IN GROUP','INPARALOGS','INPARALOGS SPECIFIC PROTEINS','SINGLE COPY SPECIFIC PROTEINS');
	my @a_count_headers =  @a_perc_headers;
	unshift(@a_count_headers, 'NB OF PROTEINS');
	
	print $fh_out_count "#CODE\tTITLE\t" . join ("\t", @a_count_headers) . "\n";
	print $fh_out_percent "#CODE\tTITLE\t" . join ("\t", @a_perc_headers) . "\n";
	
	
	foreach my $species (sort @a_species)
	{
		my $nb_protein = $rh_species->{$species}->{'size'};
		my $title = $rh_species->{$species}->{'title'};
		
		
		my $nb_protein_in_group = 0;
		my $nb_inparalogs = 0;
		my $nb_specific_inparalogs = 0; 
		my $nb_specific = 0;
		
		$nb_protein_in_group = $rh_count_proteins_in_group_by_code->{$species} if (defined $rh_count_proteins_in_group_by_code->{$species});
		$nb_inparalogs = $rh_count_inparalogs_by_code->{$species} if (defined  $rh_count_inparalogs_by_code->{$species});
		$nb_specific_inparalogs = $rh_count_specific_inparalogs_by_code->{$species} if (defined $rh_count_specific_inparalogs_by_code->{$species});
		$nb_specific = scalar (keys %{$rh_specific_proteins->{$species}}) if (defined $rh_specific_proteins->{$species});
		
		
		my $perc_protein_in_group = 100 * $nb_protein_in_group / $nb_protein;
		my $perc_inparalogs = 100 * $nb_inparalogs / $nb_protein;
		my $perc_specific_inparalogs = 100 * $nb_specific_inparalogs / $nb_protein;
		my $perc_specific_specific = 100 * $nb_specific / $nb_protein;
		
		
		print $fh_out_count "$species\t$title\t$nb_protein\t$nb_protein_in_group\t$nb_inparalogs\t$nb_specific_inparalogs\t$nb_specific\n";
		print $fh_out_percent "$species\t$title\t$perc_protein_in_group\t$perc_inparalogs\t$perc_specific_inparalogs\t$perc_specific_specific\n";
	}
	$fh_out_count->close;
	$fh_out_percent->close;
	
	
	
	my $o_pp = New ParamParser("$FindBin::RealBin/../../site/cfg/site.cfg");
	my $root_url = __GetLocalUrl();

	my $fh_html_template = &GetStreamIn("$FindBin::RealBin/../../web/templates/barplots/index.html");
	my $fh_html_out = &GetStreamOut("$outdir/summary/summary.html");

	my $file_list = join("','", ('summary.count.xls','summary.percent.xls'));
	my $title_list = join("','", ('Count of every kind of datasets for all species','Percentage of every kind of datasets for all species'));
	my $yaxis_list = join("','", ('Count','Percentage'));
	my $startcolumn_list = join(",", (2,2));
	
	while (my $line = <$fh_html_template>)
	{
		$line =~ s/%ROOT_URL%/$root_url/g;
		$line =~ s/%TITLE%/Summary/g;
		$line =~ s/%TITLE_LIST%/$title_list/g;
		$line =~ s/%YAXIS_LIST%/$yaxis_list/g;
		$line =~ s/%FILE_LIST%/$file_list/g;
		$line =~ s/%STARTCOLUMN_LIST%/$startcolumn_list/g;
		print $fh_html_out $line;
	}
	$fh_html_out->close;
	$fh_html_template->close;
	
	
	return;
    
	
}


sub CopyInputDataIntoOutdir
{
    my ($rh_analysis_params, $rh_species, $outdir) = @_;

	mkdir "$outdir/input_data";
    mkdir "$outdir/input_data/proteomes";

    my $rh_description_input = {
						  'input_data' => 'Input proteomes and InterPro analyses',
						  'input_data/proteomes' => 'Input proteome fasta files'
						  };
    
	&Log("INFO - Copy input data");
    my $rh_proteomes = $rh_analysis_params->{'proteomes'};
    foreach my $code (keys %{$rh_proteomes})
    {
        &Debug("INFO - Copy $code proteome");
        my $proteome_file = $rh_proteomes->{$code}->{'fasta'};
        copy($proteome_file, "$outdir/input_data/proteomes/$code.fasta");
        $rh_proteomes->{$code}->{'fasta'} = "./input_data/proteomes/$code.fasta";

        if (defined $rh_proteomes->{$code}->{'iprscan'})
        {
			$rh_description->{'input_data/iprscan'} = 'InterPro raw data';
			$rh_description->{"input_data/iprscan/$code"} = $rh_species->{$code}->{'title'} . ' InterPro raw data';
            mkdir "$outdir/input_data/iprscan" unless -d ("$outdir/input_data/iprscan");
            mkdir "$outdir/input_data/iprscan/$code" unless -d ("$outdir/input_data/iprscan/$code");
            
            &Debug("INFO - Copy $code iprscan");
            my $iprscan_file = $rh_proteomes->{$code}->{'iprscan'};
            copy($iprscan_file, "$outdir/input_data/iprscan/$code/$code.iprscan");
            $rh_proteomes->{$code}->{'iprscan'} = "./input_data/iprscan/$code/$code.iprscan";
            my $rh_sequences = $rh_species->{$code}->{'sequences'};
            &BuildIprscanDBTables($rh_sequences, $code, "$outdir/input_data/iprscan/", "proteome",$rh_species, $rh_description_input);
        }
    }

    
    mkdir "$outdir/homologygroups_output/" unless -d "$outdir/homologygroups_output/";
    copy("$WORKDIR/homologygroups.out", "$outdir/homologygroups_output/");
    $rh_analysis_params->{'homologygroups'}->{'outfile'} = "./homologygroups_output/homologygroups.out";
	my $rh_description_homology = {
    					  'homologygroups_output' => 'Homology groups',
						  'homologygroups_output/homologygroups.out' => 'Homology groups raw file',
						  'homologygroups_output/group_annotations.xls' => 'Restructured homology group file with functionnal annotations'};
	
	unshift (@A_OUTPUT_DATA, {'directory' => 'homologygroups_output', 'description' => $rh_description_homology});
	unshift (@A_OUTPUT_DATA, {'directory' => 'input_data', 'description' => $rh_description_input});
	
	

    return;

}


=head3 function __DeleteSequencesFromGroupId

 Title        : __DeleteSequencesFromGroupId
 Usage        : &__DeleteSequencesFromGroupId($outdir,\%ListGroupByCode)
 Prerequisite : none
 Function     : Keep only specific protein for each proteome
 Returns      : none
 Args         : hash ref of homologygroups groups, 
 Globals      : none

=cut

sub __DeleteSequencesFromGroupId
{
    my ($rh_homologygroups, $rh_specific, $groupid) = @_;
    foreach my $code (keys %{$rh_homologygroups->{$groupid}})
    {
        foreach my $seqid (@{$rh_homologygroups->{$groupid}->{$code}})
        {
            if (!defined $rh_specific->{$code}->{$seqid})
            {
                &Log("WARNING - __DeleteSequencesFromGroupId - $seqid not defined in $code sequence file");
            }
            else
            {
                delete $rh_specific->{$code}->{$seqid};
            }
        }
    }
    return;
}

=head3 function GetSpecificProteins

 Title        : GetSpecificProteins
 Usage        : $rh_specific = &GetSpecificProteins($rh_homologygroups, $rh_species)
 Prerequisite : none
 Function     : Keep only specific protein for each proteome (proteins not in any homologygroups group, excluding inparalogs)
 Returns      : hash ref of specific proteins / species
 Args         : hash ref of homologygroups groups, hash ref of proteomes
 Globals      : none

=cut

sub GetSpecificProteins
{
    my ($rh_homologygroups, $rh_species) = @_;
    &Log("INFO - Get Specific proteins i.e. not belonging to any homology group");
    my %h_specific = ();
    foreach my $code (keys %{$rh_species})
    {
        $h_specific{$code} = Storable::dclone($rh_species->{$code}->{'sequences'});
    }

	foreach my $groupid (keys %{$rh_homologygroups})
	{
		&__DeleteSequencesFromGroupId($rh_homologygroups, \%h_specific, $groupid);
    }
    return \%h_specific;
}


sub __sort_by_by_group_number
{
    my $supa = $a;
    my $supb = $b;

    $supa =~ s/^\D+//;
    $supb =~ s/^\D+//;
        
    return $supa <=> $supb;
}

1;
