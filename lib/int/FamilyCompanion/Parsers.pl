
=head2 function  ParseIprscan

 Title        : ParseIprscan
 Usage        : my $rh_iprscan = &ParseIprscan($iprscan_file)
 Prerequisite : none
 Function     : Parse tabulatted InterProScan results to keep informative data (accessions, definitions, terms)
 Returns      : Hash ref of iprscan results
 Args         : iprscan tabulatted result file
 Globals      : none

=cut

sub ParseIprscan
{
    my $iprscan_file = shift;
    my $rh_go_mapping_for_ipr5 = shift();
    &Log("INFO - ParseIprscan");
    my %h_iprscan    = ();
    
    my @a_firstlines    = `grep --max-count=500 IPR $iprscan_file`;
    chomp @a_firstlines;
    
    my $version = 4;
    
    foreach my $firstline (@a_firstlines)
    {
		$version = &__GuessIprscanVersion($firstline);
		last if ($version == 5);
	}
		
    &Debug("INFO - IprScan version $version for $iprscan_file");

    if ($version == 4)
    {
        &__ParseIprScanResultV4($iprscan_file, \%h_iprscan);
    }
    else
    {
		if (scalar (keys  %{$rh_go_mapping_for_ipr5}) == 0 )
		{
			&ParseGeneOntologyOBO($rh_go_mapping_for_ipr5);
		}
        &__ParseIprScanResultV5($iprscan_file, \%h_iprscan,$rh_go_mapping_for_ipr5);
    }
    return \%h_iprscan;

}

=head2 function __GuessIprscanVersion

 Title        : __GuessIprscanVersion
 Usage        : my $version = &__GuessIprscanVersion($firstline)
 Prerequisite : none
 Function     : Try to determine the InterProScan software version used to generate data basend on the number of columns and the presence of "|"
 Returns      : Major version (4 or 5)
 Args         : First line of the iprscan file with an IPR domain
 Globals      : none

=cut

sub __GuessIprscanVersion
{
    my $line = shift();

    my $version = 4;
    my @a_fields = split(/\t/, $line);
    if ((scalar @a_fields > 14) or ($line =~ /\|GO/))
    {
		$version = 5;
    }

    return $version;
}

=head2 procedure __ParseIprScanResultV4

 Title        : __ParseIprScanResultV4
 Usage        : &__ParseIprScanResultV4($iprscan_file,$rh_iprscan)
 Prerequisite : none
 Function     : Parse tabulatted InterProScan v4 results to keep informative data (accessions, definitions, terms)
 Returns      : none
 Args         : iprscan tabulatted result file, hash ref of iprscan results
 Globals      : none

	
=cut

sub __ParseIprScanResultV4
{
    my ($iprscan_result, $rh_iprscan) = @_;
    my $lign;

    my $fh_ipr = &GetStreamIn($iprscan_result);
    while ($lign = <$fh_ipr>)
    {

        chomp($lign);
        my @a_line = split(/\t/, $lign, 14);

        my $seqid = $a_line[0];
        $rh_iprscan->{$seqid} = {} unless (defined $rh_iprscan->{$seqid});

        my $dbname = $a_line[3];
        my $dbacc  = $a_line[4];
        my $dbdesc = $a_line[5];
        $rh_iprscan->{$seqid}->{$dbname} = {} unless defined $rh_iprscan->{$seqid}->{$dbname};
        $rh_iprscan->{$seqid}->{$dbname}->{$dbacc} = $dbdesc;

        my $ipracc  = $a_line[11];
        my $iprdesc = $a_line[12];
        $rh_iprscan->{$seqid}->{'INTERPRO'} = {} unless defined $rh_iprscan->{$seqid}->{'INTERPRO'};
        $rh_iprscan->{$seqid}->{'INTERPRO'}->{$ipracc} = $iprdesc if ($ipracc ne 'NULL');

        my $raw_goacc = $a_line[13];
        $rh_iprscan->{$seqid}->{'GO'} = {} unless defined $rh_iprscan->{$seqid}->{'GO'};
        my $regexp = 'GO:\d+';
        
        my @a_terms = split (/\(($regexp)\)/, $a_line[13]);

        for (my $i = 0; $i < scalar @a_terms; $i = $i+2)
        {
			my $term = $a_terms[$i+1];
			my $desc = $a_terms[$i];
			$desc =~ s/Biological Process: /P:/;
			$desc =~ s/Molecular Function: /F:/;
			$desc =~ s/Cellular Component: /C:/;
			
			$desc =~ s/\s+$//;
			$desc =~ s/^[\s,]+//;
			$rh_iprscan->{$seqid}->{'GO'}->{$term} = $desc;
		}
        
    }
    $fh_ipr->close;
    return;
}

=head2 procedure __ParseIprScanResultV5

 Title        : __ParseIprScanResultV5
 Usage        : &__ParseIprScanResultV5($iprscan_file,$rh_iprscan)
 Prerequisite : none
 Function     : Parse tabulatted InterProScan v5 results to keep informative data (accessions, definitions, terms)
 Returns      : none
 Args         : iprscan tabulatted result file, hash ref of iprscan results
 Globals      : none

	
=cut

sub __ParseIprScanResultV5
{
    my ($iprscan_result, $rh_iprscan,$rh_go_mapping_for_ipr5) = @_;
    my $lign;

    my $fh_ipr = &GetStreamIn($iprscan_result);
    while ($lign = <$fh_ipr>)
    {

        chomp($lign);
        my @a_line = split(/\t/, $lign, 15);
        my $seqid = $a_line[0];
        $rh_iprscan->{$seqid} = {} unless (defined $rh_iprscan->{$seqid});

        my $dbname = $a_line[3];
        my $dbacc  = $a_line[4];
        my $dbdesc = $a_line[5];
        $rh_iprscan->{$seqid}->{$dbname} = {} unless defined $rh_iprscan->{$seqid}->{$dbname};
        $rh_iprscan->{$seqid}->{$dbname}->{$dbacc} = $dbdesc;

        my $ipracc  = $a_line[11];
        my $iprdesc = $a_line[12];
        $rh_iprscan->{$seqid}->{'INTERPRO'} = {} unless defined $rh_iprscan->{$seqid}->{'INTERPRO'};
        $rh_iprscan->{$seqid}->{'INTERPRO'}->{$ipracc} = $iprdesc if ($ipracc ne 'NULL' and $ipracc ne '');

        $rh_iprscan->{$seqid}->{'GO'} = {} unless defined $rh_iprscan->{$seqid}->{'GO'};

        my @a_gos = split('\|', $a_line[13]);
        foreach my $go (@a_gos)
        {
            $rh_iprscan->{$seqid}->{'GO'}->{$go} = $rh_go_mapping_for_ipr5->{$go};
        }

        my @a_pathways = split('\|', $a_line[14]);
        foreach my $raw_pathway (@a_pathways)
        {
            my ($dbname, $dbacc) = ($raw_pathway =~ /^(\w+):\s+(.+)/);
            $rh_iprscan->{$seqid}->{$dbname} = {} unless defined $rh_iprscan->{$seqid}->{$dbname};
            $rh_iprscan->{$seqid}->{$dbname}->{$dbacc} = 'null';
        }

    }
    $fh_ipr->close;
    return;
}

sub ParseGeneOntologyOBO
{
	my $rh_go_mapping_for_ipr5 = shift;
    &Log("INFO - Get GO OBO File");
    
    
    my $cache_file = $O_CONF->Get('portal_install_dir') . '/site/tmp/cache.go.obo';
    if (! -e "$cache_file.lock")
    {
		my $cmd = "wget --tries=10 -q -O $cache_file http://purl.obolibrary.org/obo/go/go-basic.obo";
		if (-e $cache_file)
		{
			my $now = time;
			my $datecache = (stat($cache_file))[9];
		
			if (($now - $datecache) > 86400)
			{
				&System("touch $cache_file.lock");
				&System($cmd);
				&System("rm -f $cache_file.lock");
			}
		}
		else
		{
			&System($cmd);
		}
	}
    
    my %h_code = ('biological_process' => 'P', 'molecular_function' => 'F', 'cellular_component' => 'C');

	&Log("INFO - Parse GO OBO File");

	my $fh_obo = &GetStreamIn($cache_file);
    $/ = "[Term]\n";

    while (my $block = <$fh_obo>)
    {
        my %h_term = ();
        chomp $block;

        my @a_lines = split("\n", $block);
        foreach my $line (@a_lines)
        {
            my ($key, $value) = ($line =~ /^(\w+):\s+"?([^"]+)"?/);
            if ($key eq 'namespace')
            {
				$value = $h_code{$value};
			}
            $h_term{$key} = $value;
        }
        
        $rh_go_mapping_for_ipr5->{$h_term{'id'}} =  $h_term{'namespace'} . ':' . $h_term{'name'};
    }
    $fh_obo->close;
    $/ = "\n";
    return;

}




=head2 function ParseProteomes

 Title        : ParseProteomes
 Usage        : my $rh_species = &ParseProteomes($rh_proteomes)
 Prerequisite : none
 Function     : Parse proteome information from analysis confirguration file 
					- title, code, sequence, annotation and iprscan data if provided
 Returns      : hash ref of species (including sequences)
 Args         : hash ref of proteomes (referencing fasta files and iprscan files)
 Globals      : none
	
=cut

sub ParseProteomes
{
    my ($rh_proteomes) = @_;

    my %h_species = ();
    my %h_seqidspeciescode = ();

	&Log("INFO - ParseProteomes");

	my %h_go_mapping_for_ipr5 = ();
    foreach my $code (keys %{$rh_proteomes})
    {
        &Debug("INFO - Parse $code proteome");
        $h_species{$code}                = {};
        $h_species{$code}->{'title'}     = $rh_proteomes->{$code}->{'title'};
        $h_species{$code}->{'size'}      = 0;
        $h_species{$code}->{'sequences'} = {};

        my $rh_iprscan = {};
        if (defined $rh_proteomes->{$code}->{'iprscan'})
        {
            &Debug("INFO - Parse $code iprscan data");
            $rh_iprscan = &ParseIprscan($rh_proteomes->{$code}->{'iprscan'}, \%h_go_mapping_for_ipr5);
        }

        my $proteome_file = $rh_proteomes->{$code}->{'fasta'};
        my @a_duplicates = @{&InSpeciesProteomeDuplicateId($proteome_file)};
        if (scalar @a_duplicates > 0)
        {
			&Kill("Non unique sequence id in $code proteome:" . join (", ", @a_duplicates));
		}
        my %h_fasta       = ();
        
        &FastaToHash($proteome_file, \%h_fasta);
        $h_species{$code}->{'size'} = scalar(keys %h_fasta);
		
        foreach my $seqid (sort keys %h_fasta)
        {
			#CAS ORTHOMCL V2: LE CODE EST AUSSI DANS LE HEADER FASTA
			my $clean_seqid = $seqid;
			$clean_seqid =~ s/^$code\|// if ($clean_seqid =~ /^$code\|/);
			
            $h_species{$code}->{'sequences'}->{$clean_seqid}                 = {};
            $h_species{$code}->{'sequences'}->{$clean_seqid}->{'sequence'}   = $h_fasta{$seqid}->{'sequence'};
            $h_species{$code}->{'sequences'}->{$clean_seqid}->{'annotation'} = $h_fasta{$seqid}->{'header'};
            $h_species{$code}->{'sequences'}->{$clean_seqid}->{'iprscan'}    = {};
            $h_species{$code}->{'sequences'}->{$clean_seqid}->{'iprscan'}    = $rh_iprscan->{$seqid}  if (defined $rh_iprscan->{$seqid});
            $h_species{$code}->{'sequences'}->{$clean_seqid}->{'iprscan'}    = $rh_iprscan->{$clean_seqid}  if (defined $rh_iprscan->{$clean_seqid});


			if (defined $h_seqidspeciescode{$clean_seqid})
			{
				&Kill("Non unique sequence id: $clean_seqid already found in $code proteome");
			}
			$h_seqidspeciescode{$clean_seqid} = $code;

        }

    }
    return (\%h_species, \%h_seqidspeciescode);
}

sub InSpeciesProteomeDuplicateId
{
	my $fasta = shift;
	my @a_ids = `grep '^>' $fasta | cut -f1 -d ' ' | sort | uniq -c`;
	chomp @a_ids;
	my @a_duplicates = ();
	foreach my $id_count (@a_ids)
	{
		$id_count =~ s/^\s+//;
		my ($count,$id) = split (/\s+/,$id_count);
		if ($count > 1)
		{
			push (@a_duplicates, $id_count);
		}
	}
	return \@a_duplicates;
	
}


=head2 function ParseHomologyGroupsResults

 Title        : ParseHomologyGroupsResults
 Usage        : my $rh_homologygroups = &ParseHomologyGroupsResults($outfile, $version, $rh_species)
 Prerequisite : none
 Function     : Parse homologygroups outfile 
 Returns      : hash ref of homologygroups
 Args         : homologygroups outfile , homology tool version, hash ref of species
 Globals      : none
	
=cut

sub ParseHomologyGroupsResults
{
    my ($outfile, $version, $rh_seqidspeciescode) = @_;

	&Log("INFO - ParseHomologyGroupsResults $outfile");
    my %h_homologygroups = ();
    
    
    my $fh_in = &GetStreamIn($outfile);

    while (my $homologygroups_grp_line = <$fh_in>)
    {
        chomp $homologygroups_grp_line;
        &__ParseHomologyGroupsLine($homologygroups_grp_line, $version, \%h_homologygroups, $rh_seqidspeciescode);
    }

    $fh_in->close;
    return \%h_homologygroups;
}

=head2 procedure __ParseHomologyGroupsLine

 Title        : __ParseHomologyGroupsLine
 Usage        : &__ParseHomologyGroupsLine($line,$version,$rh_homologygroups,$rh_seqidspeciescode)
 Prerequisite : none
 Function     : Parse homology groups line to get sequence accessions per species and per group
 Returns      : none
 Args         : homologygroups line , homologygroups version, hash ref of homologygroups groups, hash ref of SequenceID -> species
 Globals      : none
	
=cut

sub __ParseHomologyGroupsLine
{
    my ($line, $version, $rh_homologygroups, $rh_seqidspeciescode) = @_;
    if ($version =~ /^1\./)
    {
        #ORTHOMCL4(3 genes,2 taxa):	 Hs4885503(human) YKL129c(yeast) YMR109w(yeast)
        #SC20150709:allVSallNoRepet1000(468 genes, 13 taxa): EPS32122(Penox1) estExt_fgenesh1_pg.C_4_t20369(Penla1) fgenesh1_pm.1_#_642(Penbr2)
        my ($groupid, $genes, $taxa, $raw_members) = ($line =~ /^(\w+\d+)\((\d+) genes,(\d+) taxa\):\s+(.+)/);

        $rh_homologygroups->{$groupid} = {};

        my @a_rawmembers = split(' ', $raw_members);

        foreach my $item (@a_rawmembers)
        {
            my ($acc, $code) = ($item =~ /(\S+)\(([^\)]+)\)/);
            &Kill("Parse OrthoMCL v1 line : $acc is not found in Id Seq of the proteomes.") unless (defined $rh_seqidspeciescode->{$acc});
            $rh_homologygroups->{$groupid}->{$code} = [] unless defined($rh_homologygroups->{$groupid}->{$code});
            push(@{$rh_homologygroups->{$groupid}->{$code}}, $acc);
        }
    }
    elsif ($version =~ /^2\./)
    {
        #GRP1214: S288|S000004856 S288|S000005843 EK12|P75757 S288|S000002613
        my ($groupid, $raw_members) = ($line =~ /^(\S+\d+):\s+(.+)/);
        &Kill("Group IDs are duplicated in your homologygroups output file - eg: $groupid")
          if (defined $rh_homologygroups->{$groupid});
        $rh_homologygroups->{$groupid} = {};

        my @a_rawmembers = split(' ', $raw_members);

        foreach my $item (@a_rawmembers)
        {
            my ($code, $acc) = ($item =~ /([^\|]+)\|(\S+)/);
            &Kill("Parse OrthoMCL v2 line : $acc is not found in Id Seq of the proteomes.") unless (defined $rh_seqidspeciescode->{$acc});
            $rh_homologygroups->{$groupid}->{$code} = [] unless defined($rh_homologygroups->{$groupid}->{$code});
            push(@{$rh_homologygroups->{$groupid}->{$code}}, $acc);
        }
    }
    elsif ($version =~ /^sy\./)
    {
        #Cluster205 (taxa: 2, genes: 2)  gene:NZ_AJJO01000256.1.10 gene:XaaCFBP6367064.10
        my ($groupid, $raw_members) = ($line =~ /^(Cluster\d+)\s+\(taxa:\s+\d+,\s+genes:\s+\d+\)\s+(.+)/);
        &Kill("Group IDs are duplicated in your homologygroups output file - eg: $groupid")
          if (defined $rh_homologygroups->{$groupid});

        my @a_rawmembers = split(' ', $raw_members);
        if (scalar(@a_rawmembers) > 1)
        {
            $rh_homologygroups->{$groupid} = {};
            foreach my $item (@a_rawmembers)
            {
                my ($acc) = ($item =~ /gene:(\S+)/);
                if (defined $rh_seqidspeciescode->{$acc})
                {
                    $rh_homologygroups->{$groupid}->{$rh_seqidspeciescode->{$acc}} = [] unless defined($rh_homologygroups->{$groupid}->{$rh_seqidspeciescode->{$acc}});
                    push(@{$rh_homologygroups->{$groupid}->{$rh_seqidspeciescode->{$acc}}}, $acc);
                }
                else
                {
                    &Kill("Parse Synergy line : $acc is not found in Id Seq of the proteomes.")
                }
            }
        }
    }
    elsif ($version =~ /^of\./)
    {
        #OG002975: NC_002488.866 AWYH01000035.1.39 AXBS01000039.1.58
        my ($groupid, $raw_members) = ($line =~ /^(OG\d+):\s+(.+)/);
        &Kill("Group IDs are duplicated in your homologygroups output file - eg: $groupid")
          if (defined $rh_homologygroups->{$groupid});

        my @a_rawmembers = split(' ', $raw_members);

              
        if (scalar(@a_rawmembers) > 1)
        {
            $rh_homologygroups->{$groupid} = {};
            foreach my $item (@a_rawmembers)
            {
				#GESTION DES ID FASTA AVEC CODES ESPECES
				if ($item  =~ /([^\|]+)\|(\S+)/)
				{
					my ($code, $acc) = ($item =~ /([^\|]+)\|(\S+)/);
					$rh_homologygroups->{$groupid}->{$code} = [] unless defined($rh_homologygroups->{$groupid}->{$code});
					push(@{$rh_homologygroups->{$groupid}->{$code}}, $acc);
				}
				else
				{

					if (defined $rh_seqidspeciescode->{$item})
					{
						$rh_homologygroups->{$groupid}->{$rh_seqidspeciescode->{$item}} = [] unless defined($rh_homologygroups->{$groupid}->{$rh_seqidspeciescode->{$item}});
						push(@{$rh_homologygroups->{$groupid}->{$rh_seqidspeciescode->{$item}}}, $item);
					}
					else
					{
						&Kill("Parse OrthoFinder line : $item is not found in Id Seq of the proteomes.")
					}
				}
            }
        }
    }
    else
    {
        &Kill("Homology groups version $version parsing is not implemented");
    }
    return;

}

=head2 procedure BuildIprscanDBTables

 Title        : BuildIprscanDBTables
 Usage        : &BuildIprscanDBTables($rh_sequences,$code,$outdir)
 Prerequisite : none
 Function     : Build a summary file for each InterPro DB and a count file for each accession to draw histograms
 Returns      : none
 Args         : $rh_sequences,$code,$outdir,$suffix
 Globals      : none
	
=cut

sub BuildIprscanDBTables
{
    my ($rh_sequences, $code, $outdir, $suffix, $rh_species, $rh_description) = @_;
    
    mkdir $outdir unless -d $outdir;
    
    
    my $species_title = $rh_species->{$code}->{'title'};
    
    mkdir "$outdir/$code" unless -d "$outdir/$code";
    mkdir "$outdir/$code/interprodb_xls_files" unless -d "$outdir/$code/interprodb_xls_files";


	$rh_description->{basename ($outdir) . "/$code/interprodb_xls_files"} = "$species_title InterPro database term counts";
	$fh_readme = &GetStreamOut("$outdir/$code/interprodb_xls_files/README");
    print $fh_readme <<END;
Per organism and per InterPro database term counts.
END
    $fh_readme->close;


    my %h_count = ();
	my %h_total_count_per_dbname = ();
    foreach my $seqid (keys %{$rh_sequences})
    {
        foreach my $dbname (keys %{$rh_sequences->{$seqid}->{'iprscan'}})
        {
			$h_total_count_per_dbname{$dbname} = 0 unless (defined $h_total_count_per_dbname{$dbname});
            $h_count{$dbname} = {} unless (defined $h_count{$dbname});
            $rh_description->{basename ($outdir) . "/00.summary_interprodb_xls"} = "Tabulatted files with all organisms terms count for each InterPro database";
            
            if (! -d "$outdir/00.summary_interprodb_xls")
            {
				mkdir "$outdir/00.summary_interprodb_xls";
				my $fh_readme = &GetStreamOut("$outdir/00.summary_interprodb_xls/README");
				print $fh_readme <<END;
Tabulatted files with all organisms terms count for each InterPro database.
END
				$fh_readme->close;
			}
    
            $rh_description->{basename ($outdir) . "/00.summary_interprodb_xls/$dbname.iprscan.$suffix.xls"} = "$dbname terms belonging to $suffix proteins";
            my $fh_out = &GetStreamOut(">$outdir/00.summary_interprodb_xls/$dbname.iprscan.$suffix.xls");
            foreach my $dbacc (keys %{$rh_sequences->{$seqid}->{'iprscan'}->{$dbname}})
            {
                $dbdesc = $rh_sequences->{$seqid}->{'iprscan'}->{$dbname}->{$dbacc};
                print $fh_out "$seqid\t$code\t" . $dbacc . "\t" . $dbdesc . "\n";
                $h_count{$dbname}->{$dbacc} = {'count' => 0, 'desc' => $dbdesc} unless defined $h_count{$dbname}->{$dbacc};
                $h_count{$dbname}->{$dbacc}->{'count'}++;
                $h_total_count_per_dbname{$dbname}++;
            }
            $fh_out->close;
        }
    }

    foreach my $dbname (keys %h_count)
    {
        my $rh_local = $h_count{$dbname};
        $rh_description->{basename ($outdir) . "/$code/interprodb_xls_files/$code.$dbname.iprscan.$suffix.count.xls"} = "$species_title $dbname terms belonging to $suffix protein";
        my $fh_out   = &GetStreamOut(">$outdir/$code/interprodb_xls_files/$code.$dbname.iprscan.$suffix.count.xls");
        if (-z "$outdir/$code/interprodb_xls_files/$code.$dbname.iprscan.$suffix.count.xls")
        {
				print $fh_out "#TERM\tDESCRIPTION\tHITS\tRATIO\n";
		}
        foreach my $dbacc (sort {$rh_local->{$b}->{'count'} <=> $rh_local->{$a}->{'count'}} keys %{$rh_local})
        {
			my $amount = $rh_local->{$dbacc}->{'count'} / $h_total_count_per_dbname{$dbname};
            print $fh_out "$dbacc\t" . $rh_local->{$dbacc}->{'desc'}  . "\t" . $rh_local->{$dbacc}->{'count'} . "\t" . $amount . "\n";
        }
        $fh_out->close;
    }
    
    #C'EST TROP GROS POUR LE JS - TROP DE TERMES EN ORDONNEE
    #if ($suffix ne 'proteome')
    #{
#		&__BuildIprscanDBJSdata(\%h_count,$code, $outdir, $suffix);
	#}
    return;
}

sub __BuildIprscanDBJSdata
{
	my ($rh_count,$code, $outdir, $suffix) = @_;
	
	if (scalar (keys %{$rh_count}) > 0)
	{
		my @a_files;
		my @a_titles;
		my @a_yaxis;
		my @a_startcolumn;
	
		foreach my $dbname (sort keys %{$rh_count})
		{
			push (@a_files, "interprodb_xls_files/$code.$dbname.iprscan.$suffix.count.xls");
			push (@a_titles, "$code $dbname $suffix");
			push (@a_yaxis, 'Count');
			push (@a_startcolumn, 2);
		}
		
		my $root_url = __GetLocalUrl();

		my $file_list = join("','", @a_files);
		my $title_list = join("','", @a_titles);
		my $yaxis_list = join("','", @a_yaxis);
		my $startcolumn_list = join(",", @a_startcolumn);
	
		my $fh_html_template = &GetStreamIn("$FindBin::RealBin/../../web/templates/barplots/index.html");
		my $fh_html_out = &GetStreamOut("$outdir/$code/$code.iprscan.$suffix.count.html");

		while (my $line = <$fh_html_template>)
		{
			$line =~ s/%ROOT_URL%/$root_url/g;
			$line =~ s/%TITLE%/$code $suffix InterPro database hits count/g;
			$line =~ s/%TITLE_LIST%/$title_list/g;
			$line =~ s/%YAXIS_LIST%/$yaxis_list/g;
			$line =~ s/%FILE_LIST%/$file_list/g;
			$line =~ s/%STARTCOLUMN_LIST%/$startcolumn_list/g;
			print $fh_html_out $line;
		}
		
		$fh_html_out->close;
		$fh_html_template->close;
		
	}
    
    
    return;
        
        
	
}

=head2 procedure CheckProteomeCodesConsistency

 Title        : CheckProteomeCodesConsistency
 Usage        : my ($rh_count_proteins_in_group_by_code,$rh_count_inparalogs_by_code,$rh_count_specific_inparalogs_by_code) = &CheckProteomeCodesConsistency($rh_species,$rh_homologygroups)
 Prerequisite : none
 Function     : Check if the same codes are present in loaded proteomes and homologygroups outfile and then build data summaries
 Returns      : hash ref for the count of proteins belonging to a group, inparalogs, specific inparalogs and single copy specific proteins
 Args         : $rh_sequences,$code,$outdir,$suffix
 Globals      : none
	
=cut

sub CheckProteomeCodesConsistency
{
	my ($rh_species,$rh_homologygroups) = @_;
	
	my @a_proteome_codes = sort (keys %{$rh_species});
	my %h_homologygroups_proteome_codes = ();
	my %h_count_inparalogs_specific  = ();
	my %h_count_inparalogs  = ();
	foreach my $groupid (keys %{$rh_homologygroups})
	{
		my $count_species = 0;
		my $last_code;
		foreach my $code (keys %{$rh_homologygroups->{$groupid}})
		{
			$last_code = $code;
			$count_species++;
			$h_homologygroups_proteome_codes{$code} = 0 unless defined ($h_homologygroups_proteome_codes{$code});
			my $nb_proteins = scalar (@{$rh_homologygroups->{$groupid}->{$code}});
			$h_homologygroups_proteome_codes{$code} += $nb_proteins;
			
			if ($nb_proteins > 1)
			{
				$h_count_inparalogs{$code} = 0 unless defined ($h_count_inparalogs{$code});
				$h_count_inparalogs{$code} += $nb_proteins;
				
			}
			
		}
		
		if ($count_species == 1)
		{
			$h_count_inparalogs_specific{$last_code} = 0 unless defined ($h_count_inparalogs_specific{$last_code});
			$h_count_inparalogs_specific{$last_code} += scalar (@{$rh_homologygroups->{$groupid}->{$last_code}});
		}
	}
	my @a_homologygroups_proteome_codes = sort (keys %h_homologygroups_proteome_codes);
	
	if (scalar @a_proteome_codes != scalar @a_homologygroups_proteome_codes)
	{
		&Kill('Not the same number of proteomes in homologygroups file : loaded proteomes="' . join (',', @a_proteome_codes) . '" vs homologygroups_proteomes="' . join (',',@a_homologygroups_proteome_codes) . '"') ;
	}
	else
	{
		my @a_errors = ();
		
		for (my $i = 0; $i < scalar  @a_proteome_codes; $i++)
		{
			if ($a_proteome_codes[$i] ne $a_homologygroups_proteome_codes[$i])
			{
				push (@a_errors, 'proteome="' . $a_proteome_codes[$i] . '" vs homologygroups_proteome="' . $a_homologygroups_proteome_codes[$i] . '"');
			}
		}
		
		if (scalar @a_errors != 0)
		{
			&Kill('Proteome codes inconsistency : ' . join (', ', @a_errors) ) ;
		}
		
	}
	

	
	return (\%h_homologygroups_proteome_codes,\%h_count_inparalogs, \%h_count_inparalogs_specific) ;
}


sub CreateGroupAnnotation
{
	my ($rh_homologygroups, $rh_species, $outdir) = @_;
	
	
	my %h_group_annotation = ();
	my @a_dbs = ();
    foreach my $groupid (keys %{$rh_homologygroups})
	{
		$h_group_annotation{$groupid} = {};
		my $num_species = scalar keys %{$rh_homologygroups->{$groupid}};
		$h_group_annotation{$groupid}->{'num_species'} =  $num_species;
		$h_group_annotation{$groupid}->{'species'} = {};
		
		my $num_prot = 0;
		my %h_annotations = ();
		my %h_iprscan = ();
		foreach my $code (keys %{$rh_homologygroups->{$groupid}})
		{
			$h_group_annotation{$groupid}->{'species'}->{$code} = [];
			$num_prot += scalar @{$rh_homologygroups->{$groupid}->{$code}};
			
			foreach my $seqid (@{$rh_homologygroups->{$groupid}->{$code}})
			{
				push (@{$h_group_annotation{$groupid}->{'species'}->{$code}}, $seqid);
				$h_annotations{$rh_species->{$code}->{'sequences'}->{$seqid}->{'annotation'}} = 1;
				
				if (defined $rh_species->{$code}->{'sequences'}->{$seqid}->{'iprscan'})
				{
					foreach my $db (keys %{$rh_species->{$code}->{'sequences'}->{$seqid}->{'iprscan'}})
					{
						push (@a_dbs, $db) if (! grep (/^$db$/,@a_dbs));
						$h_iprscan{$db} = {} unless defined $h_iprscan{$db};
						foreach my $dbacc (keys %{$rh_species->{$code}->{'sequences'}->{$seqid}->{'iprscan'}->{$db}})
						{
							$h_iprscan{$db}->{$dbacc} = $rh_species->{$code}->{'sequences'}->{$seqid}->{'iprscan'}->{$db}->{$dbacc};
						}
					}
				}
			}
		}
		$h_group_annotation{$groupid}->{'num_prot'} =  $num_prot;
		$h_group_annotation{$groupid}->{'iprscan'} =  \%h_iprscan;
		$h_group_annotation{$groupid}->{'annotation'} =  join (';' , keys %h_annotations);
	}
	
	mkdir "$outdir/homologygroups_output/" unless -d "$outdir/homologygroups_output/";
   
	my $fh_readme = &GetStreamOut("$outdir/homologygroups_output/README");
	print $fh_readme <<END;
Homology groups raw output file and Excel file with all group annotation and composition.
Usefull when you get homology groups from Venn comparison and you want to know the function of the proteins belonging to these groups.
END
   
	my $fh_out = &GetStreamOut("$outdir/homologygroups_output/group_annotations.xls");
	print $fh_out "#GROUP\tNUMBER OF SPECIES\tNUMBER OF PROTEINS\tPROTEINS FROM " . join ("\tPROTEINS FROM ", sort keys %{$rh_species}) . "\tPROTEIN ANNOTATIONS\t" . join ("\t", sort @a_dbs) . "\n";
	foreach my $groupid (sort __sort_by_by_group_number (keys %h_group_annotation))
	{
		print $fh_out $groupid 
						. "\t" . $h_group_annotation{$groupid}->{'num_species'} 
						. "\t" . $h_group_annotation{$groupid}->{'num_prot'};
						
		
		foreach my $species ( sort keys %{$rh_species} )
		{
			my $acc = '';
			$acc = join (',', @{$h_group_annotation{$groupid}->{'species'}->{$species}}) if (defined $h_group_annotation{$groupid}->{'species'}->{$species});
			print $fh_out "\t" . $acc;
		}
		
		print $fh_out "\t" . $h_group_annotation{$groupid}->{'annotation'};
		foreach my $db (sort @a_dbs)
		{
			print $fh_out "\t";
			foreach my $dbacc (keys %{$h_group_annotation{$groupid}->{'iprscan'}->{$db}})
			{
				my $dbdesc = $h_group_annotation{$groupid}->{'iprscan'}->{$db}->{$dbacc};
				$dbdesc =~ s/\.$//;
				$dbdesc =~ s/\s*null\s*//;
				print $fh_out $dbacc;
				print $fh_out ':' . $dbdesc if ($dbdesc ne '');
				print $fh_out '; ';
			}
		}
		print $fh_out "\n";
	}
	$fh_out->close;
    return;
}


1;
