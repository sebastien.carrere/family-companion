
sub GetAuthentication
{
    my $o_wb   = shift;
    my $o_auth = $o_wb->GetObjectAuth();

    my %h_param =
      ('email' => undef, 'login' => undef, 'success' => &TRUE, 'roles' => undef, 'cn' => undef, 'eppn' => undef);
    if (&OFFLINE_MODE == &TRUE)
    {
        $h_param{'email'} = $O_CONF->Get('admin_mail');
    }
    elsif ($o_auth->IsAuthenticated() == &TRUE)
    {
        $h_param{'email'} = $o_auth->GetEmail();
        $h_param{'login'} = $o_auth->GetLogin();
        $h_param{'roles'} = $o_auth->GetRoles();
        $h_param{'cn'}    = $o_auth->GetCommonName();
        if ($O_CONF->Get('authentication_mode') eq 'shibboleth')
        {
            $h_param{'eppn'} = $o_auth->GetEppn();
        }
        $h_param{'email'} = 'guest@guest.fc' if ($O_CONF->Get('mode') eq 'local');
        $h_param{'success'} = &TRUE;
    }
    print encode_json(\%h_param);
    return;
}

sub __Authentication
{
    my ($o_param, $appli_url) = @_;

    my $o_authentic;
    my $provider_name = $o_param->Get('provider_name');

    my $cookie_name = md5_hex($O_CONF->Get('authentic_cookie_name'));

    my $authentication_mode = $O_CONF->Get('authentication_mode');

    if ($authentication_mode eq 'shibboleth')
    {
		#LES AUTHOTIZATIONS LDAP NE SONT PAS OBLIGATOIRES
        #if ($O_CONF->Get('authentic_ldap_host') eq '' or $O_CONF->Get('authentic_ldap_base') eq '')
        #{
        #    &Die(
        #         '1- LDAP configuration is missing (authentic_ldap_host,authentic_ldap_base) - Contact '
        #           . $O_CONF->Get('admin_mail'),
        #         'early'
        #         );
        #}

        if ($O_CONF->Get('authentic_shibboleth_login') eq '' or $O_CONF->Get('authentic_shibboleth_logout') eq '')
        {
            &Die(
                 'Shibboleth configuration is missing (authentic_shibboleth_login,authentic_shibboleth_logout) - Contact '
                   . $O_CONF->Get('admin_mail'),
                 'early'
                 );
        }

        $o_authentic = New Authentic::ShibLdap(
                                               host                => $O_CONF->Get('authentic_ldap_host'),
                                               base                => $O_CONF->Get('authentic_ldap_base'),
                                               -application        => 'family-companion',
                                               -default_role       => 'user',
                                               -ra_authorized_orga => [],
                                               shib_login_url      => $O_CONF->Get('authentic_shibboleth_login'),
                                               shib_logout_url     => $O_CONF->Get('authentic_shibboleth_logout'),
                                               target_url          => $appli_url,
                                               -cookie_name        => $cookie_name,
                                               -cookie_expires     => '+1d',
                                               -provider_name      => $provider_name,
                                               -cookie_secure      => $O_CONF->Get('authentic_cookie_secure')
                                               );
    }
    elsif ($authentication_mode eq 'file')
    {
        if (!-s $O_CONF->Get('authentic_file') && ($o_param->Get('__wb_function') ne 'SetAdminAccount'))
        {
			my $admin_account_url = __GetLocalUrl() . '/cgi/index.cgi?__wb_function=SetAdminAccount';
			my $button = '<a href="'.$admin_account_url.'"><button type="button" class="btn btn-danger">Create Admin Account First !</button></a><br><br>';
			my %h_param = (
                        'success' => 0,
                        'message' => $button
                        );
                        
			print "Content-type:application/json\n\n";
			print to_json(\%h_param);
			exit;
            #~ &Die('Authentic file empty - Contact ' . $O_CONF->Get('admin_mail') . ' to create at least one user', 'early');
            #~ __Error('<a href=""><button>Your connexion is lost - Please login again</button></a>',   \$o_param);
			#~ &Die(to_json(\%h_param), 'early');
        }
        else
        {
            if ($o_param->IsDefined('fc_login'))
            {
                $o_authentic = New Authentic::EtcPasswd(
                                                        file            => $O_CONF->Get('authentic_file'),
                                                        -login          => $o_param->Get('fc_login'),
                                                        -password       => $o_param->Get('fc_password'),
                                                        -cookie_name    => $cookie_name,
                                                        -cookie_expires => '+1d',
                                                        -cookie_secure  => $O_CONF->Get('authentic_cookie_secure')
                                                        );
            }
            else
            {
                $o_authentic = New Authentic::EtcPasswd(
                                                        file            => $O_CONF->Get('authentic_file'),
                                                        -cookie_name    => $cookie_name,
                                                        -cookie_expires => '+1d',
                                                        -cookie_secure  => $O_CONF->Get('authentic_cookie_secure')
                                                        );
            }
        }
    }
    elsif ($authentication_mode eq 'ldap')
    {
        if ($O_CONF->Get('authentic_ldap_host') eq '' or $O_CONF->Get('authentic_ldap_base'))
        {
            &Die(
                 '2- LDAP configuration is missing (authentic_ldap_host,authentic_ldap_base) - Contact '
                   . $O_CONF->Get('admin_mail'),
                 'early'
                 );
        }
        else
        {
            $O_CONF->SetUnlessDefined('authentic_ldap_port', 389);
            $o_authentic = New Authentic::Ldap(
                                               host            => $O_CONF->Get('authentic_ldap_host'),
                                               base            => $O_CONF->Get('authentic_ldap_base'),
                                               -port           => $O_CONF->Get('authentic_ldap_port'),
                                               -application    => 'family-companion',
                                               -login          => $o_param->Get('fc_login'),
                                               -password       => $o_param->Get('fc_password'),
                                               -cookie_name    => $cookie_name,
                                               -cookie_expires => '+1d',
                                               -cookie_secure  => $O_CONF->Get('authentic_cookie_secure')
                                               );
        }
    }
    else
    {
        &Die('Unknown Authentication Protocol:' . $authentication_mode . '- Contact ' . $O_CONF->Get('admin_mail'),
             'early');
    }
    return $o_authentic;
}

sub Logout
{
    my $o_webbuilder = shift;

    my $o_authentic = $o_webbuilder->GetObjectAuth();
    $o_authentic->Logout();
    return;
}

sub Login
{
    my $o_webbuilder = shift;

    my $o_param     = $o_webbuilder->GetObjectParam;
    my $o_authentic = $o_webbuilder->GetObjectAuth();

    my $authentication_mode = $O_CONF->Get('authentication_mode');

    print <<END;
<html>
	<head>
	<link href="//maxcdn.bootstrapcdn.com/bootswatch/3.3.0/spacelab/bootstrap.min.css" rel="stylesheet">
</head>
<body>
END

    if ($o_authentic->IsAuthenticated() == &FALSE)
    {
        print <<END;
		<div class="container-fluid alert-success">
END
    }

    if ($authentication_mode eq 'file')
    {
        if ($o_authentic->IsAuthenticated() == &FALSE)
        {
            my $url = __GetLocalUrl() . '/cgi/proxy/index.cgi';
            print <<END;
<iframe width="100%" height="315" src="$url" frameborder="0" allowfullscreen></iframe>
END
        }
        else
        {
            &Home($o_webbuilder);
        }
    }
    elsif ($authentication_mode eq 'ldap')
    {
        if ($o_authentic->IsAuthenticated() == &FALSE)
        {
            if ($o_param->IsDefined('fc_login'))
            {
                print "<b>Bad login or passwd</b><br /> Please try again";
            }

            my $str_input = '</p><table><tr><td>Login</td><td><input type="text" name="fc_login" /></td></tr>';
            $str_input .= '<tr><td>Password</td><td><input type="password" name="fc_password" /></td></tr></table>';
            $str_input .=
              '<p><input name="launch_form" type="submit" value="Login" /><input type="reset" value="Reset" />';
            print $o_webbuilder->GetForm("", $str_input);
        }
        else
        {
            &Home($o_webbuilder);
        }
    }
    elsif ($authentication_mode eq 'shibboleth')
    {
        print $o_authentic->GetLoginForm($o_webbuilder);
    }
    else
    {
        print '<div class="container-fluid alert-warning">Unknown authentication mode</div>';
    }

    if ($o_authentic->IsAuthenticated() == &FALSE)
    {
        print <<END;
</div>
END
    }
    return;
}

sub CreateUserDir
{
    my ($userdir) = @_;
    mkdir $userdir unless -d $userdir;
    mkdir "$userdir/.share" unless -d "$userdir/.share";

    my $fh_htaccess = &GetStreamOut("$userdir/.share/.htaccess");
    print $fh_htaccess <<END;
deny from all
END
    $fh_htaccess->close;

    return;

}

sub CreateContactFile
{

    my ($userdir, $email) = @_;

    if (!-s ("$userdir/contact.txt"))
    {
        my $fh_contact = &GetStreamOut("$userdir/contact.txt");
        print $fh_contact $email;
        $fh_contact->close;
    }
    return;
}

sub AddHtaccess
{
    my ($userdir, $directory, $portalname, $email) = @_;

    my $remote_user_env_name = $O_CONF->Get("remote_user_env_name");

    if (-d $directory)
    {
        my $fh_out = &GetStreamOut("$directory/.htaccess");
        print $fh_out <<END;
<If "%{ENV:$remote_user_env_name} !~ /^$email/i">
AuthType Basic
AuthName "Password Required"
AuthBasicProvider file
AuthUserFile "$userdir/.share/$portalname.password"
Require valid-user
</If>
END
    }
    else
    {
        Carp::croak("ERROR - directory $directory not found");
    }
    return;
}

sub HtpasswdUpdate
{
    my ($userdir, $portalname, $login, $password) = @_;

    my $file = "$userdir/.share/$portalname.password";

    if (!-e $file)
    {
        &System("touch $file");
    }

    my $o_htpasswsd = new Apache::Htpasswd($file);
    $o_htpasswsd->htpasswd($login, $password);
    return;
}

sub HtpasswdRevoke
{
    my ($userdir, $portalname, $login) = @_;

    my $file = "$userdir/.share/$portalname.password";
    Carp::croak("ERROR - password file $portalname.password not found") unless -e $file;
    my $o_htpasswsd = new Apache::Htpasswd($file);
    $o_htpasswsd->htDelete($login, $password);
    return;
}

sub __GetUserId
{
    my $o_authentic = shift();

    my $userid;

    if (&OFFLINE_MODE == &TRUE)
    {
        $userid = lc($O_CONF->Get('admin_mail'));
    }
    elsif ($o_authentic->IsAuthenticated())
    {
        $userid = lc($o_authentic->GetEmail());
        $userid = undef if ($userid eq '');
    }
    return $userid;
}

sub __GetUserRootDir
{
    my $userid      = shift;
    my $datarootdir = $O_CONF->Get('datarootdir');
    my $userdir     = $datarootdir . '/' . md5_hex(lc($userid));
    return $userdir;
}

sub __GetUserHttpDir
{
    my $userid      = shift;
    my $datahttpdir = $O_CONF->Get('datahttpdir');
    my $userdir     = $datahttpdir . '/' . md5_hex(lc($userid));
    return $userdir;
}

sub ProtectDataDir
{
    my ($userdir, $portalname) = @_;
    my $email = &GetEmail($userdir);
    &AddHtaccess($userdir, "$userdir/browsers/$portalname/", $portalname, $email);
    &AddHtaccess($userdir, "$userdir/blastdb/$portalname/",  $portalname, $email);

    if ($O_CONF->Get('authentication_mode') eq 'file')
    {
        my $htpasswd_file      = $O_CONF->Get('authentic_htpasswd_file');
        my $o_htpasswsd        = new Apache::Htpasswd($htpasswd_file);
        my $encrypted_password = $o_htpasswsd->fetchPass(lc($email));
        my $fh_out             = &GetStreamOut("$userdir/.share/$portalname.password");
        print $fh_out lc($email) . ':' . $encrypted_password . "\n";
        $fh_out->close();
    }

    return;
}

sub Home
{
    print "<div class='alert alert-success'>You are logged in as " . $ENV{$O_CONF->Get('remote_user_env_name')};
    print "<a href='"
      . __GetLocalUrl()
      . "' target='_parent'><button class='button'>Access to your analyses</button></a>";
    print "</div>";
}

sub SetAdminAccount
{
	my $o_wb = shift;
	my $o_webparam = $o_wb->GetObjectParam();
	
	
	if ($o_webparam->IsDefined('admin_account'))
	{
		
		if (IsEmailValid($o_webparam->Get('admin_mail')))
		{
			SetAdminAccountSend(\$o_webparam);
		}
		else
		{
			&Die('ERROR - admin_mail is not correct-Try again ', 'early');
		}
	}
	else
	{
		SetAdminAccountForm(\$o_webparam);
	}
	return;
	
}

sub SetAdminAccountSend
{
	my ($ro_webparam) = @_;
	my $admin_mail = $$ro_webparam->Get('admin_mail');
	my $admin_fullname = $$ro_webparam->Get('admin_fullname');
	my $admin_password = $$ro_webparam->Get('admin_password');
	my $captcha = $$ro_webparam->Get('captcha');
	my $captcha_valid = $$ro_webparam->Get('captcha_valid');
	
	my $captcha_md5 = md5_hex($captcha);

	
	if ($captcha_md5 ne $captcha_valid)
	{
		&EarlyMessage("<div class='alert alert-danger'>INVALID KEY</div>");
		SetAdminAccountForm($ro_webparam);
	}
	else
	{
		if (! IsEmailValid($admin_mail))
		{
			&EarlyMessage("<div class='alert alert-danger'>EMAIL ADDRESS IS NOT VALID</div>");
			&SetAdminAccountForm($ro_webparam);
		}
		else
		{
			my $install_dir = $FindBin::RealBin . '/..';
			my $cmd = $install_dir. '/bin/int/admin_addUser.pl --sendmail';
			$cmd .= ' --email="' . $admin_mail .'" ';
			$cmd .= ' --password="' . $admin_password .'" ';
			$cmd .= ' --common_name="' . $admin_fullname . '" ';
			$cmd .= ' --outdir=' . $install_dir .'/etc';
			
			if (! -d $install_dir .'/etc')
			{
				&Die("ERROR - $install_dir/etc  does not exist", 'early');  
			}
			else
			{
				my ($fh_err,$err) = File::Temp::tempfile(UNLINK => 1, DIR => $O_CONF->Get('tmpdir'));
				my $return = system ("($cmd > $err) 2> $err");
				if ($return ne 0)
				{
					&Die("ERROR " .`cat $err`, 'early');
				}
				else
				{
					#Update .cfg
					my $main_cfg = "$FindBin::RealBin/../site/cfg/site.cfg";
					my $o_pp = New ParamParser($main_cfg);
					$o_pp->Set('admin_mail', $admin_mail);
					$o_pp->Dump($main_cfg);
					
					if (-e "$FindBin::RealBin/../data/cfg/local.cfg")
					{
						my $local_cfg = "$FindBin::RealBin/../data/cfg/local.cfg";
						my $o_pp = New ParamParser($local_cfg);
						$o_pp->Set('admin_mail', $admin_mail);
						$o_pp->Dump($local_cfg);
					}
					my $url = __GetLocalUrl();
					my $button = '<a href="'.$url.'"><button type="button" class="btn btn-success">Access to Family Companion</button></a><br><br>';
			
					EarlyMessage("<div class='alert alert-info'>Admin Account created</div>$button");
				}

			}
		}
	}

}

	
	

sub SetAdminAccountForm
{
	my ($ro_webparam) = @_;

	my $captcha = __GenerateKey();
	my $md5_captcha = md5_hex($captcha);

	my $admin_mail_part  = '<input type="email" class="form-control" id="admin_mail"  name="admin_mail" placeholder="Email" required>';

	if ( $O_CONF->IsDefined('admin_mail') && IsEmailValid( $O_CONF->Get('admin_mail') ))
	{
		$admin_mail_part  = '<input type="email" class="form-control" id="admin_mail"  name="admin_mail" value="'.$O_CONF->Get('admin_mail').'" readonly>';
	}
	
	my $url = __GetLocalUrl(). '/cgi/index.cgi';
	my $form =  <<END;
<div class="container-fluid">
<div class="alert alert-warning">Please configure Admin Account before</div>
<form method="POST" action="$url">
  <div class="form-group">
    <label for="admin_fullname">Full name</label>
    <input type="text" class="form-control" id="admin_fullname" name="admin_fullname"  placeholder="Your Name" required>
  </div>
  <div class="form-group">
    <label for="admin_mail">Admin Email address</label>
    $admin_mail_part
  </div>
  <div class="form-group">
    <label for="admin_password">Type your password (min. 8 characters)</label>
    <input type="password" class="form-control" id="admin_password" name="admin_password"  placeholder="Your Name" pattern=".{8,}" required>
  </div>
  <div class="form-group alert alert-warning">
	<label for="captcha">Validation key: <i>$captcha</i></label>
	<input type="text" class="form-control" required name="captcha" id="captcha" placeholder="report key"/>
	<input type="hidden" name="captcha_valid" value="$md5_captcha"/>
  </div>
  <input type="hidden" name="admin_account" value="1"/>
  <input type="hidden" name="__wb_function" value="SetAdminAccount"/>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
<div>
END

	EarlyMessage($form);
	return;
}

sub EarlyMessage
{
	my $msg       = shift;
	my $http_root = __GetLocalUrl();

	print <<END;
<html lang="en"><head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
        <meta charset="utf-8">
        <title>myGenomeBrowser -Config Error</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="$http_root/web/css/portal.css">
    </head>
    <body>
END

    print STDOUT $msg;
    print <<END;
                </body></html>
END
 
}

1;
