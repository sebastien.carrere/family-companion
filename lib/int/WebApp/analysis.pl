
# 
# Lance l'analyse a partir du json envoye par le client
#
sub LaunchAnalysis
{

    my $o_webbuilder = shift;

    my $o_param = $o_webbuilder->GetObjectParam;

    my $o_cgi = new CGI;

    my $o_authentic = $o_webbuilder->GetObjectAuth;
    my $userid      = &__GetUserId($o_authentic);

    my %h_param = (
                   'success' => 0,
                   'message' => 'Unknown error'
                   );

    if (defined $userid)
    {

        my $userdir = __GetStoreDir() . '/' . md5_hex($userid);

        if (!-d $userdir)
        {
            $O_CONF->SetUnlessDefined('admin_mail', 'sebastien.carrere@toulouse.inra.fr');
            my $email   = $O_CONF->Get('admin_mail');
            my $subject = '[Family-Companion] Creation de compte - ' . $userid;
            my $body    = 'User: ' . $userid . "\n";
            $body .= 'Server: ' . __GetPublicUrl() . "\n";
            $body .= 'Store: ' . $userdir . "\n";
            &CreateUserDir($userdir);
            &SendMail($email, $subject, $body);
        }

        my $json_str = $o_param->Get("json");

        #my $rh_document = from_json($json_str);

        my $outdir = tempdir(CLEANUP => 0, DIR => $userdir);

        #print STDERR "DEBUG $outdir :: $json_str\n";

        my $fh_json = &GetStreamOut("$outdir/analysis.json");
        print $fh_json $json_str;
        $fh_json->close;

        #print STDERR "DEBUG $outdir/analysis.json\n";

        my $cmd =
          "$FindBin::RealBin/../bin/int/bbric_family-companion.pl --verbose --outdir=$outdir --analysis_cfg=$outdir/analysis.json";
        my $ret = system("(nohup $cmd > $outdir/log ) 2> $outdir/error&");

		

        #print STDERR "[DEBUG] cmd : $cmd \n";

        if ($? == -1)
        {
            %h_param = (
                        'success' => 0,
                        'message' => "failed to execute: $!"
                        );

        }
        elsif ($ret != 0)
        {
            %h_param = (
                        'success' => 0,
                        'message' => "failed to execute:<pre>" . &Cat("$outdir/error" . '</pre>')
                        );
        }
        else
        {
            %h_param = (
                        'success' => 1,
                        'message' => "analysis launched"
                        );

        }

        #unlink ("$outdir/error") if (-z "$outdir/error");

    }
    else
    {
		my $login_url = __GetRootURI() . '/login';
        $o_param->Set('hyper_next', $login_url);
		$o_param->Set('login', "false");
        __Error('<a href="' . $login_url . '"><button>Your connexion is lost - Please login again</button></a>', \$o_param);

    }

    print to_json(\%h_param);
    return;
}

#
# Liste les analyses à partir du repertoire de l'utilisateur
#
sub ListAnalyses
{

    #print STDERR "DEBUG ListAnalysis\n";

    my $o_webbuilder = shift;

    my $o_param     = $o_webbuilder->GetObjectParam;
    my $o_authentic = $o_webbuilder->GetObjectAuth;

    my $publicid = $o_param->Get('view');
    my $owner    = lc($o_param->Get('owner'));

    my $o_cgi = new CGI;

    my $userid = &__GetUserId($o_authentic);

    if (($owner ne '') && ($publicid ne ''))
    {
        $userid = 'public' if ($userid ne $owner);
    }

    my %h_param = (
                   'success' => 0,
                   'message' => 'Unknown error'
                   );

    if (defined $userid)
    {

        $h_param{"success"} = 1;
        $h_param{"message"} = "List the user's analyses";

        $h_param{"analyses"} = [];

        #POUR NE PAS DONNER ACCES AU REPERTOIRE data/store directement
        #on fait un lien symbolique du userdr dans son repertoire de session

        my $store_dir = __GetStoreDir();

        my $tmp_dir = $o_webbuilder->GetSessionPath();
        my $tmp_url = $o_webbuilder->GetSessionUrl();

        my $md5     = md5_hex($userid);
        my $out_dir = $store_dir . "/" . $md5;
        if ($userid eq 'public')
        {
            $out_dir = $store_dir . "/" . $userid;
            my $public_analyse_link = $out_dir . '/' . md5_hex($owner) . '.' . $publicid;
            if (-e $public_analyse_link)
            {
                mkdir "$tmp_dir/$md5";
                symlink($public_analyse_link, "$tmp_dir/$md5/$publicid");
            }
            else
            {
                __Error("This analysis is not public - Try login in before", \$o_param);
            }
        }
        else
        {
            symlink($out_dir, "$tmp_dir/$md5");
        }
        $out_dir = "$tmp_dir/$md5";

        #print STDERR "[DEBUG] repository user : $out_dir\n";

        my $out_url = $tmp_url . "/" . $md5;

        # liste les repertoires dans le repertoire de l'utilisateur
        foreach my $subdir (glob "$out_dir/*")
        {
            if (-d $subdir)
            {

                my $analysis = basename($subdir);
                if ($o_param->IsDefined('view'))
                {
                    my $view_analysis = $o_param->Get('view');
                    next if ($view_analysis ne $analysis);
                }
                my $rh_desc = {};

                $rh_desc->{"public"} = &FALSE;
                my $public_analyse_link = $store_dir . "/public/$md5." . $analysis;
                $rh_desc->{"public"} = &TRUE if (-e $public_analyse_link or $userid eq 'public');

                $rh_desc->{"status"} = "";
                $rh_desc->{"owner"}  = "$userid";
                $rh_desc->{"owner"}  = "$owner" if ($owner ne '');
                $rh_desc->{"title"}  = "";
                $rh_desc->{"url"}    = "";

                # Si un fichier error existe, c'est lui qui prime pour definir le status
                # ensuite le fichier success puis le fichier log
                # Il faut que l'un des trois fichiers soient presents pour que l'analyse soit listee

                unlink("$subdir/error") if (-e "$subdir/error" && -z "$subdir/error" && !-e "$subdir/.pid");

                # checke si un fichier success, log, ou error existe

                foreach my $file (glob "$subdir/*")
                {

                    $rh_desc->{"url"} = $out_url . "/" . basename($subdir);

                    if (!-d $file)
                    {

                        my $fileName = basename($file);

                        if ($fileName eq "log")
                        {

                            # l'analyse est en cours
                            # le fichier contient les logs
                            #print STDERR "DEBUG - $file - " .$rh_desc->{"status"}. "\n";

                            if ($rh_desc->{"status"} ne "success" && $rh_desc->{"status"} ne "error")
                            {
                                $rh_desc->{"log"}    = &Cat($file);
                                $rh_desc->{"status"} = "log";
                            }

                        }
                        elsif ($fileName eq "error" && -s $file)
                        {

                            # l'analyse s'est terminee par une erreur
                            # le fichier contient ce qu'il y avait dans le fichier error
                            my $error_msg = &Cat($file);

                            $rh_desc->{"log"}    = $error_msg;
                            $rh_desc->{"status"} = "log";

                            if ($error_msg =~ /error/i)
                            {
                                $rh_desc->{"status"} = "error";
                            }
                        }
                        elsif ($fileName eq "success")
                        {
                            # l'analyse s'est terminee par un succes
                            # le fichier contient ce qu'il y avait dans le fichier log

                            if ($rh_desc->{"status"} ne "error")
                            {
                                $rh_desc->{"log"}    = &Cat($file);
                                $rh_desc->{"status"} = "success";
                            }
                        }
                        elsif ($fileName eq "analysis.json")
                        {
                            my $rh_document = from_json(&Cat($file));
                            if (defined $rh_document->{"title"})
                            {
                                $rh_desc->{"title"} = $rh_document->{"title"};
                                $rh_desc->{"date"}  = $rh_document->{"date"};
                            }

                        }

                    }
                }
                if ($rh_desc->{"status"} ne "" && $rh_desc->{"title"} ne "")
                {
                    push(@{$h_param{"analyses"}}, $rh_desc);
                }
            }
        }
    }
    else
    {
        my $login_url = __GetRootURI() . '/login';
        $o_param->Set('hyper_next', $login_url);
        $o_param->Set('login', "false");
        __Error('<a href="' . $login_url . '"><button>Your connexion is lost - Please login again</button></a>', \$o_param);
    }

    print encode_json(\%h_param);
    return;
}

sub Publish
{
    my $o_webbuilder = shift;
    my $analysis_id  = $o_webbuilder->GetParams('publish');
    my $o_authentic  = $o_webbuilder->GetObjectAuth;
    my $userid       = &__GetUserId($o_authentic);

    my %h_message = ();
    $h_message{'success'} = &FALSE;
    $h_message{'message'} = "You must be authenticated";

    if ($analysis_id eq '')
    {
        $h_message{'message'} = "You must provide an analysis id";
    }
    elsif (defined $userid)
    {

        my $store_dir = __GetStoreDir();

        my $md5                 = md5_hex($userid);
        my $analysis_dir        = $store_dir . "/" . $md5 . "/" . $analysis_id;
        my $public_analyse_link = $store_dir . '/public/' . "$md5.$analysis_id";
        if (!-d $analysis_dir)
        {
            $h_message{'message'} = "Analysis $analysis_id  NOT FOUND in your repository or in published analyses";
        }
        elsif (-e $public_analyse_link)
        {
            unlink($public_analyse_link);
            $h_message{'success'} = &TRUE;
            $h_message{'message'} = "$analysis_id is no more shared";
        }
        else
        {
            mkdir $store_dir . '/public' unless (-d $store_dir . '/public');
            symlink($analysis_dir, $public_analyse_link);
            $h_message{'success'} = &TRUE;
            my $root_url = __GetPublicUrl();
            $h_message{'message'} = "$root_url?view=$analysis_id&owner=$userid";
        }

    }
    print encode_json(\%h_message);
    return;

}

sub Delete
{
    my $o_webbuilder = shift;
    my $analysis_id  = $o_webbuilder->GetParams('delete');
    my $o_authentic  = $o_webbuilder->GetObjectAuth;
    my $userid       = &__GetUserId($o_authentic);

    my %h_message = ();
    $h_message{'success'} = &FALSE;
    $h_message{'message'} = "You must be authenticated";

    if ($analysis_id eq '')
    {
        $h_message{'message'} = "You must provide an analysis id";
    }
    elsif (defined $userid)
    {

        my $store_dir           = __GetStoreDir();
        my $md5                 = md5_hex($userid);
        my $public_analyse_link = $store_dir . '/public/' . "$md5.$analysis_id";

        my $analysis_dir = $store_dir . "/" . $md5 . "/" . $analysis_id;
        
        print STDERR "$analysis_dir\n";
        
        if(-e "$analysis_dir/.pid") 
        {
        	my $fh_process_file = &GetStreamIn("$analysis_dir/.pid");
        
        	
        	my $pid = <$fh_process_file>;
        	$fh_process_file->close;
        	
        	chomp $pid;
        	
        	if($pid ne "" && $pid > 0)
        	{
				my @a_pids = ($pid);
        		my $proc_table=Proc::ProcessTable->new();
				foreach my $proc (@{$proc_table->table()})
				{
					if ($proc->ppid == $pid)
					{
						push (@a_pids, $proc->pid);
					}
				}
				killfam 'KILL', @a_pids;
        	}
        
        
        	
        }
        
        if (!-d $analysis_dir)
        {
            $h_message{'message'} = "Analysis $analysis_id  NOT FOUND";
        }
        else
        {
            mkdir $store_dir . '/deleted' unless (-d $store_dir . '/deleted');
            my $deleted_analysis_dir = $store_dir . '/deleted/' . $md5 . '.' . $analysis_id;
            
            print STDERR "Mv to $deleted_analysis_dir\n";
            
            &System(
                "mv $analysis_dir $deleted_analysis_dir; cd $store_dir/deleted; tar cfz $md5.$analysis_id.tgz $md5.$analysis_id && rm -rf $md5.$analysis_id"
                );

            unlink($public_analyse_link) if (-e $public_analyse_link);
            $h_message{'success'} = &TRUE;
            $h_message{'message'} = "Analysis deleted";
        }

    }
    print to_json(\%h_message);
    return;

}

1;

