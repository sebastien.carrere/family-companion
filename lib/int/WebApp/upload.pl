sub Upload
{

    my $o_webbuilder = shift;

    my $o_param     = $o_webbuilder->GetObjectParam;
    my $o_authentic = $o_webbuilder->GetObjectAuth;
    my $userid      = &__GetUserId($o_authentic);

    my $o_cgi = new CGI;

    my %h_param = (
                   'success' => 0,
                   'message' => 'Problem while uploading file'
                   );

    if (defined $userid)
    {

        if (!defined $o_cgi->param("dataFile"))
        {

            $h_param{"message"} = "No file specified";

            print to_json(\%h_param);

            return;
        }

        my $original_file_name = $o_cgi->param("dataFile");

        my $fh_original = $o_cgi->upload("dataFile");

        if (!defined $fh_original)
        {
            $h_param{"message"} = "Error while copying file";
            print to_json(\%h_param);
            return;
        }

        my $original_path = $o_cgi->tmpFileName($fh_original);

        my $tmp_dir       = $o_webbuilder->GetSessionPath();
        my $uploaded_file = $tmp_dir . "/" . basename($original_path);

        #print STDERR "DEBUG-SEB:$original_path, $uploaded_file\n";

        #~ print STDERR "DEBUG-SEB:" , Dumper \$o_param, "\n";
        File::Copy::move($original_path, $uploaded_file);

        my @a_tmp = split(/\./, $original_file_name);
        my $code = $a_tmp[0];

        $code =~ s/\s/_/g;
        if ($o_param->IsDefined('pathHomologyGroups'))
        {
            %h_param = (
                        'success'            => 1,
                        'message'            => 'file uploaded',
                        'path'               => $uploaded_file,
                        'original_file_name' => "$original_file_name"
                        );
        }
        else
        {
            %h_param = (
                        'success'            => 1,
                        'message'            => 'file uploaded',
                        'path'               => $uploaded_file,
                        'code'               => $code,
                        'original_file_name' => "$original_file_name"
                        );
        }
    }
    else
    {
        my $login_url = __GetRootURI() . '/login';
        $o_param->Set('hyper_next', $login_url);
		$o_param->Set('login', "false");
        __Error('<a href="' . $login_url . '"><button>Your connexion is lost - Please login again</button></a>', \$o_param);

    }

    #~ print STDERR "DEBUG-SEB:" , Dumper \%h_param, "\n";
    print to_json(\%h_param);
    return;

}

sub ZipUpload
{

    my $o_webbuilder = shift;
    my $o_param      = $o_webbuilder->GetObjectParam;
    my $o_authentic  = $o_webbuilder->GetObjectAuth;
    my $userid       = &__GetUserId($o_authentic);

    my $o_cgi = new CGI;

    my %h_param = (
                   'success' => 0,
                   'message' => 'Problem while uploading file'
                   );

    if (defined $userid)
    {

        if (!defined $o_cgi->param("dataFile"))
        {
            $h_param{"message"} = "No file specified";
            print to_json(\%h_param);
            return;
        }

        my $original_file_name = $o_cgi->param("dataFile");
        my $fh_original        = $o_cgi->upload("dataFile");
        if (!defined $fh_original)
        {
            $h_param{"message"} = "Error while copying file";
            print to_json(\%h_param);
            return;
        }

        my $original_path = $o_cgi->tmpFileName($fh_original);

        my $tmp_dir       = $o_webbuilder->GetSessionPath();
        my $uploaded_file = $tmp_dir . "/" . basename($original_path);

        File::Copy::move($original_path, $uploaded_file);
        my $cmd = "mkdir $tmp_dir/unzip; cd $tmp_dir/unzip; unzip -q $uploaded_file";

        #print STDERR "[DEBUG] CMD - $cmd\n";
        &System("$cmd");
        my $zip_root     = __FindZipRoot($tmp_dir);
        my @a_zipcontent = `ls $zip_root/*`;
        chomp @a_zipcontent;
        my $rh_param = &__ValidZipContent(\@a_zipcontent, "$zip_root/");
        %h_param = %{$rh_param};

        $h_param{'zipdata'}->{'title'} = basename($original_path) if (!defined $h_param{'zipdata'}->{'title'});

    }
    else
    {
        my $login_url = __GetRootURI() . '/login';

        my $message = '<a href="' . $login_url . '"><button>Your connexion is lost - Please login again</button></a>';
        %h_param = (
                    'success' => 0,
                    'message' => $message
                    );
    }

    print encode_json(\%h_param);
    return;
}

sub __FindZipRoot
{
    my $tmp_dir   = shift;
    my $cmd       = "find $tmp_dir -name '*.fa*' | head -1";
    my $fastafile = `$cmd`;
    chomp $fastafile;
    if ($fastafile eq '')
    {
        return undef;
    }
    else
    {
        return dirname($fastafile);
    }
}

sub __ValidZipContent
{
    my $ra_zipcontent = shift;
    my $dirname       = shift;

    #print STDERR "[DEBUG] __ValidZipContent - $dirname\n";
    my @a_fastafiles = grep (/(\w+)\.fas?t?a?$/, @{$ra_zipcontent});
    my @a_iprscan    = grep (/(\w+)\.iprscan$/,  @{$ra_zipcontent});
    my @a_desc       = grep (/(\w+)\.txt$/,      @{$ra_zipcontent});

	my $iprscan_provided = &FALSE;
	$iprscan_provided = &TRUE if (scalar(@a_iprscan) > 0);
    my %h_param = (
                   'success' => 0,
                   'message' => 'Problem while uploading file'
                   );

    if (scalar(@a_fastafiles) == 0)
    {
        $h_param{'message'} = 'ERROR - No fasta file found in your zip file';
        return \%h_param;
    }

    if ($iprscan_provided == &TRUE && scalar(@a_iprscan) < scalar(@a_fastafiles))
    {
        $h_param{'message'} = 'ERROR - Not same number of fasta files and iprscan files  in your zip file';
        return \%h_param;
    }

    my @a_references = ();
    if (-e "$dirname/reference.txt")
    {
        @a_references = split('\n', &Cat("$dirname/reference.txt"));
    }

    my %h_data = ();
    $h_data{'proteomes'} = {};
    foreach my $file (@a_fastafiles)
    {
        #my $dirname = dirname ($file);
        my ($code) = ($file =~ /([^\/]+)\.fas?t?a?$/);
        $h_data{'proteomes'}->{$code} = {'fasta' => $file};
        if ($iprscan_provided == &TRUE && !-e "$dirname/$code.iprscan")
        {
            $h_param{'message'} =
              "ERROR - Iprscan file not found for $code in your zip file; Found: " . to_json(\@a_iprscan);
            return \%h_param;
        }
        elsif ($iprscan_provided == &TRUE)
        {
            $h_data{'proteomes'}->{$code}->{'iprscan'} = "$dirname/$code.iprscan";
        }

        if (-e "$dirname/$code.txt")
        {
            my $title = &Cat("$dirname/$code.txt");
            chomp $title;
            $h_data{'proteomes'}->{$code}->{'title'} = $title;
        }
        $h_data{'proteomes'}->{$code}->{'reference'} = &FALSE;
        $h_data{'proteomes'}->{$code}->{'reference'} = &TRUE if (grep (/^$code$/, @a_references));

    }

    #Gestion des orthomcl precalcules
    $h_data{"homologygroups"} = {};

    if (-e "$dirname/homologygroups.out")
    {
        $h_data{"homologygroups"}->{'outfile'} = "$dirname/homologygroups.out";
    }

    #Gestion des parametres orthomcl
    if (-e "$dirname/homologygroups.txt")
    {
        my $o_pportho = New ParamParser("$dirname/homologygroups.txt");
        $o_pportho->Dump('HASH', $h_data{"homologygroups"});
    }

    if (-e "$dirname/title.txt")
    {
        my $title = &Cat("$dirname/title.txt");
        chomp $title;
        $h_data{'title'} = $title;
    }
    if (-e "$dirname/description.txt")
    {
        my $description = &Cat("$dirname/description.txt");
        chomp $description;
        $h_data{'description'} = $description;
    }

    $h_param{'success'} = 1;
    $h_param{'message'} = scalar(keys %{$h_data{'proteomes'}}) . ' proteome found in your zip file';
    $h_param{'zipdata'} = \%h_data;

    return \%h_param;

}

1;
