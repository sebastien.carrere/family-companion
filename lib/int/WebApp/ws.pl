
my $DBH = undef;

my %H_EXTENSIONS = ('table' => 'tsv', 'list' => 'txt', 'sequences' => 'fasta', 'alignment' => 'aln', 'matrix' => 'tsv');

my %H_TABLES = (
    'homologyGroup' => {'field_names' => ['id', 'accession', 'genes',       'taxa']},
    'protein'       => {'field_names' => ['id', 'accession', 'proteome_id', 'homologyGroup_id', 'annotation']},
    'proteome' => {'field_names' => ['id', 'code', 'organism', 'strain', 'release', 'description', 'sequence_number']},
    'homologyGroup_proteome_association' => {'field_names' => ['homologyGroup_id', 'proteome_id']}
    );

my %H_FIELD_DESC = (
    'accession' => {'pretty_name' => 'Accession',     'desc' => 'Protein accession or locus_tag', 'type' => 'string'},
    'proteome'  => {'pretty_name' => 'Proteome code', 'desc' => 'Proteome short name',            'type' => 'string'},
    'code'      => {'pretty_name' => 'Proteome code', 'desc' => 'Proteome short name',            'type' => 'string'},
    'strain'    => {'pretty_name' => 'Strain',        'desc' => 'Proteome organism strain',       'type' => 'string'},
    'organism'  => {'pretty_name' => 'Organism',      'desc' => 'Proteome organism longname',     'type' => 'string'},
    'release'   => {'pretty_name' => 'Release',       'desc' => 'Proteome annotation release',    'type' => 'string'},
    'description' => {'pretty_name' => 'Proteome description', 'desc' => 'Proteome information', 'type' => 'string'},
    'sequence_number' => {'pretty_name' => 'Size',      'desc' => 'Proteome sequence number',   'type' => 'integer'},
    'homologyGroup'   => {'pretty_name' => 'Group',     'desc' => 'Protein family group name',  'type' => 'string'},
    'proteomes'       => {'pretty_name' => 'Proteomes', 'desc' => 'List of proteomes in group', 'type' => 'array'},
    'proteins'        => {'pretty_name' => 'Proteins',  'desc' => 'List of proteins in group',  'type' => 'array'},
    'taxa'       => {'pretty_name' => 'Taxa Number', 'desc' => 'Number of proteomes in group', 'type' => 'integer'},
    'genes'      => {'pretty_name' => 'Gene Number', 'desc' => 'Number of proteins in group',  'type' => 'integer'},
    'annotation' => {'pretty_name' => 'Annotation',  'desc' => 'Functionnal annotation',       'type' => 'array'}
    );

my %H_HYPERMEDIA_DESC = (
                         'abundancy_matrix' => 'Get abundancy matrix',
                         'binary_matrix'    => 'Get binary matrix',
                         'next'             => 'Go to next page',
                         'previous'         => 'Go to previous page',
                         'self'             => 'Current page',
                         'rawdata'          => 'Get raw data file',
                         'alignment'        => 'Get sequence alignment (Fasta format)',
                         'consensus'        => 'Get sequence alignment consensus (Fasta format)',
                         'tree'             => 'Get sequence alignment tree (Newick format)',
                         'proteins'         => 'Get table of proteins',
                         'sequences'        => 'Get sequence file (Fasta format)',
                         'groups'           => 'Go to the groups table',
                         'inchlib'          => 'InCHlib clustering file'
                         );

my %H_SQL_MASKS = ();

sub __GetSqlTemplates
{
    my $template_dir = $O_CONF->Get('portal_install_dir') . '/site/sql_templates';
    my @a_files      = `find $template_dir -name "*.sql"`;
    chomp @a_files;
    foreach my $file (@a_files)
    {
        my ($method, $resource, $mode) = ($file =~ /(\w+)\/(\w+)\/(\w+)\.sql$/);
        $H_SQL_MASKS{$method} = {} unless (defined $H_SQL_MASKS{$method});
        $H_SQL_MASKS{$method}->{$resource} = {} unless (defined $H_SQL_MASKS{$method}->{$resource});
        my @a_filecontent = `cat $file`;
        chomp @a_filecontent;
        $H_SQL_MASKS{$method}->{$resource}->{$mode} = join(' ', @a_filecontent);
    }
    return;
}

sub WebService
{

    my $o_webbuilder = shift;

    __GetSqlTemplates();
    my $o_param     = $o_webbuilder->GetObjectParam;
    my $o_authentic = $o_webbuilder->GetObjectAuth;

    my $publicid = $o_param->Get('view');
    my $owner    = undef;
    ($owner, my @a_trash) = split(/\s+/, lc(URI::Encode::uri_decode($o_param->Get('owner'))))
      if ($o_param->IsDefined('owner'));

    if ($o_param->IsDefined('blastmethod'))
    {
        $o_param->Set('method',   'search');
        $o_param->Set('resource', 'blast');
    }

    my $analysis_id = $o_param->Get('analysis');
    my $method      = $o_param->Get('method');
    my $resource    = $o_param->Get('resource');

    my $userid = &__GetUserId($o_authentic);

    $userid = 'public' if ($owner ne '' && $owner ne $userid);
    $o_param->Set('userid', $userid);

    my %h_param = ('success' => 'false', 'message' => 'Unknown error');

    if (defined $userid)
    {
        $o_param->Set('userhash', md5_hex($userid));
        $o_param->Set('userhash', md5_hex($owner)) if (defined $owner);

        WSRouter(\$o_param);
    }
    else
    {
        my $login_url = __GetRootURI() . '/login';
        $o_param->Set('hyper_next', $login_url);
        $o_param->Set('login',      "false");
        __Error('<a href="' . $login_url . '"><button>Your connexion is lost - Please login again</button></a>',
                \$o_param);
    }
    return;
}

sub WSRouter
{
    my $ro_param = shift;

    $$ro_param->Set('profiler', []);
    if ($$ro_param->Get('method') eq 'api')
    {
        WSAPI($ro_param);
    }
    elsif ($$ro_param->Get('method') eq 'status')
    {
        WSRouterStatus($ro_param);
    }
    elsif ($$ro_param->Get('method') eq 'get')
    {
        WSRouterGet($ro_param);
    }
    elsif ($$ro_param->Get('method') eq 'search')
    {
        WSRouterSearch($ro_param);
    }
    else
    {
        __Error($$ro_param->Get('method') . ' method unknown', $ro_param);
    }
    return;
}

=pod

	API: status/
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/status/exists
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/status/remove
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/status/build
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/status/info
	
=cut

sub WSRouterStatus
{
    my $ro_param = shift;
    if ($$ro_param->Get('resource') eq 'exists')
    {
        __WSStatusExists($ro_param);
    }
    elsif ($$ro_param->Get('resource') eq 'build')
    {
        __Error("You are not allowed to perform this operation", $ro_param) if ($$ro_param->Get('userid') eq 'public');
        __WSStatusBuild($ro_param);
    }
    elsif ($$ro_param->Get('resource') eq 'info')
    {
        __WSStatusInfo($ro_param);
    }
    elsif ($$ro_param->Get('resource') eq 'remove')
    {
        __Error("You are not allowed to perform this operation", $ro_param) if ($$ro_param->Get('userid') eq 'public');
        __WSStatusRemove($ro_param);
    }
    elsif ($$ro_param->Get('resource') eq 'clear')
    {
        __Error("You are not allowed to perform this operation", $ro_param) if ($$ro_param->Get('userid') eq 'public');
        __WSStatusClear($ro_param);
    }
    else
    {
        __Error($$ro_param->Get('resource') . ' resource unknown', $ro_param);
    }
    return;
}

sub __WSStatusExists
{
    my $ro_param = shift;
    my $exists   = 'false';
    my $message  = 'This db does not exist';
    my $build_query_builder;

    if (__AssertDBExists($ro_param))
    {
        $exists              = 'true';
        $build_query_builder = 'done';
        $message             = 'You can query this db';
        $$ro_param->Set('hyper_next', __GetRootURI() . '/' . $$ro_param->Get('analysis') . '/status/info');
    }

    my $lock_file  = __GetUserDir($ro_param) . '/' . $$ro_param->Get('analysis') . '/' . 'db.lock';
    my $error_file = __GetUserDir($ro_param) . '/' . $$ro_param->Get('analysis') . '/' . 'db.build.err';

    if ($exists eq 'false')
    {
        if (!-e $lock_file)
        {
            $message = `cat $error_file`;
            chomp $message;
            $build_query_builder = 'error';
        }
        else
        {
            $message             = 'Query builder is running';
            $build_query_builder = 'building';
        }
    }
    __Return({'exists' => $exists, 'build_query_builder' => $build_query_builder}, $message, $ro_param);

    return;
}

sub __WSStatusRemove
{
    my $ro_param = shift;
    if (!__AssertDBExists($ro_param))
    {
        __Error("Analysis database does not exist", $ro_param);
    }
    else
    {

        unlink(__GetDBFile($ro_param));
        unlink(__GetUserDir($ro_param) . '/' . $$ro_param->Get('analysis') . '/' . 'db.lock');
        my $rmblast_cmd = 'rm -rf ' . __GetUserDir($ro_param) . '/' . $$ro_param->Get('analysis') . '/blastdb';
        system($rmblast_cmd);
        __ClearCache($ro_param);

        __Return({}, 'Analysis database removed', $ro_param);
    }
    return;
}

sub __WSStatusClear
{
    my $ro_param = shift;
    if (!__AssertDBExists($ro_param))
    {
        __Error("Analysis database does not exist", $ro_param);
    }
    else
    {
        __ClearCache($ro_param);
        __Return({}, 'Analysis cache cleared', $ro_param);
    }
    return;
}

sub __ClearCache
{
    my $ro_param = shift;
    my ($cache_dir, $cache_url) = __GetCacheDir($ro_param);
    my $rmcache_cmd = 'rm -rf ' . $cache_dir;
    system($rmcache_cmd);
    return;
}

sub __WSStatusInfo
{
    my $ro_param = shift;

    if (!__AssertDBExists($ro_param))
    {
        __Error("Analysis database does not exist", $ro_param);
    }
    else
    {
        __DBConnect($ro_param);

        my $statement = $H_SQL_MASKS{'status'}->{'info'}->{'proteins'};

        my %h_info = ();
        my ($ra_list, $ra_fields) =
          __DBExecute($H_SQL_MASKS{'status'}->{'info'}->{'proteins'}, ['count'], 'list', $ro_param);
        $h_info{'proteins'} = $ra_list->[0];
        ($ra_list, $ra_fields) =
          __DBExecute($H_SQL_MASKS{'status'}->{'info'}->{'proteomes'}, ['count'], 'list', $ro_param);
        $h_info{'proteomes'} = $ra_list->[0];
        ($ra_list, $ra_fields) =
          __DBExecute($H_SQL_MASKS{'status'}->{'info'}->{'groups'}, ['count'], 'list', $ro_param);
        $h_info{'groups'} = $ra_list->[0];

        __Return({'stats' => \%h_info}, 'General statistics', $ro_param);

    }
    return;
}

sub __WSStatusBuild
{
    my ($ro_param) = @_;

    #TESTER SI LOCK
    # if LOCK : {build : false, message : running}
    #Lancer la commande
    if (__AssertDBExists($ro_param))
    {
        __Error("Analysis database already exists", $ro_param);
    }
    else
    {

        my ($dir) = (__GetDBFile($ro_param) =~ /(.+)\/familyCompanion\.db$/);
        __Error("Analysis does not exist", $ro_param) unless (-d $dir);
        __Error("Analysis too old - homologygroups_output/group_annotations.xls mandatory file does not exist",
                $ro_param)
          unless (-e "$dir/homologygroups_output/group_annotations.xls");

        if (-e "$dir/db.lock")
        {
            __Return({'build' => 'false', 'build_query_builder' => 'building'},
                     'Build already launched - Please wait and check your mailbox', $ro_param);
        }
        else
        {
            if (-s "$dir/db.build.killed")
            {
                __Return({'build' => "false", 'build_query_builder' => 'error'},
                         'Build error - ' . `cat $dir/db.build.killed`, $ro_param);
            }
            else
            {
                my ($cmd, $status, $killed) = &RunBuildQueryBuilder($dir);

                $$ro_param->Set('debug', $cmd);

                __Return({'build' => "$status:$killed" . ' @ ' . time, 'build_query_builder' => 'building'},
                         'Build launch - Check your mailbox' . `cat $dir/db.build.err`, $ro_param);
            }
        }
    }
    return;
}

sub WSRouterSearch
{
    my $ro_param = shift;
    if (!__AssertDBExists($ro_param))
    {
        __Error('Analysis not found', $ro_param);
    }
    else
    {
        __DBConnect($ro_param);

        if ($$ro_param->Get('resource') eq 'blast')
        {
            &__WSBlast($ro_param);
        }
    }
    return;
}

sub WSRouterGet
{
    my $ro_param = shift;

    if (!__AssertDBExists($ro_param))
    {
        __Error('Analysis not found', $ro_param);
    }
    else
    {
        __DBConnect($ro_param);

        if ($$ro_param->Get('resource') eq 'proteomes')
        {
            __WSGetProteomes($ro_param);
        }
        elsif ($$ro_param->Get('resource') eq 'proteins')
        {
            __WSGetProteins($ro_param);
        }
        elsif ($$ro_param->Get('resource') eq 'groups')
        {
            __WSGetGroups($ro_param);
        }
        elsif ($$ro_param->Get('resource') eq 'matrix')
        {
            __WSGetMatrix($ro_param);
        }
        elsif ($$ro_param->Get('resource') eq 'sequences')
        {
            __WSGetSequences($ro_param);
        }
        elsif ($$ro_param->Get('resource') eq 'alignment')
        {
            __WSGetAlignment($ro_param);
        }
        else
        {
            __Error($$ro_param->Get('resource') . ' resource unknown', $ro_param);
        }
    }
    return;
}

=pod

	API: get/proteomes
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/proteomes
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/proteomes?field=code
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/proteomes?field=description
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/proteomes?group=testOk78

=cut

sub __WSGetProteomes
{

    my ($ro_param) = @_;

    my $statement = $H_SQL_MASKS{'get'}->{'proteomes'}->{'all'};
    my $returns;

    if ($$ro_param->IsDefined('group'))
    {
        my $group = uc($$ro_param->Get('group'));
        $group =~ s/,/","/g;
        $statement = $H_SQL_MASKS{'get'}->{'proteomes'}->{'from_group'};
        $statement =~ s/#MASK_GROUP#/$group/g;
    }

    my $table     = 'proteome';
    my $ra_fields = __GetAllFields($table);
    if ($$ro_param->IsDefined('field'))
    {
        my $field = $$ro_param->Get('field');
        $ra_fields = [$table . '.' . $field];
        $returns   = 'list';
    }
    else
    {
        $returns = 'table';
    }

    my $mask_field = join(',', @{$ra_fields});
    $statement =~ s/#MASK_FIELD#/$mask_field/g;

    my $ra_list = [];
    ($ra_list, $ra_fields) = __DBExecute($statement, $ra_fields, $returns, $ro_param);
    my $ra_links = [];
    if ($returns eq 'table')
    {
        my %h_rels = (
                      'groups' => ['code', 'inproteome'],
                      'matrix' => ['code', 'inproteome']
                      );

        $ra_links = __AddTableHypermediaLinks($ra_list, \%h_rels, $ro_param);
    }

    $ra_fields = __GetReturnedFields($ra_fields, $ra_list);
    __AddFieldsMetadata($ra_fields);
    __Return({$returns => [sort @{$ra_list}], 'fields' => $ra_fields, 'links' => $ra_links},
             scalar @{$ra_list} . " items", $ro_param);

    return;

}

=pod

	API: get/proteins
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/proteins
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/proteins?accession=tr
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/proteins?annotation=kinase
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/proteins?annotation=kinase&field=accession
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/proteins?annotation=kinase&field=annotation&proteome=Bacsu,Xbbric
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/proteins?annotation=IPR009082
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/proteins?group=testOk98,testOk97
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/proteins?group=testOk98,testOk97&field=annotation
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/proteins?group=testOk98,testOk97&proteome=Xbbric
=cut

sub __GetAllFields
{
    my $table    = shift;
    my @a_fields = ();
    foreach my $field (@{$H_TABLES{$table}->{'field_names'}})
    {
        push(@a_fields, $table . '.' . $field);
    }
    return \@a_fields;
}

sub __WSGetProteins
{
    my ($ro_param) = @_;

    push(@{$$ro_param->Get('profiler')}, {"start" => time});
    my $returns;

    my $statement = $H_SQL_MASKS{'get'}->{'proteins'}->{'filter'};

    if ($$ro_param->IsDefined('proteome') && !$$ro_param->IsDefined('group'))
    {
        my $proteome = uc($$ro_param->Get('proteome'));
        $proteome =~ s/,/","/g;
        $statement = $H_SQL_MASKS{'get'}->{'proteins'}->{'from_proteome'};
        $statement =~ s/#MASK_PROTEOME#/$proteome/g;
    }
    elsif ($$ro_param->IsDefined('group'))
    {
        my $group = uc($$ro_param->Get('group'));
        $group =~ s/,/","/g;
        $statement = $H_SQL_MASKS{'get'}->{'proteins'}->{'from_group'};
        if ($$ro_param->IsDefined('proteome'))
        {
            my $proteome = uc($$ro_param->Get('proteome'));
            $proteome =~ s/,/","/g;
            $statement = $H_SQL_MASKS{'get'}->{'proteins'}->{'from_group_and_proteome'};
            $statement =~ s/#MASK_PROTEOME#/$proteome/g;
        }
        $statement =~ s/#MASK_GROUP#/$group/g;
    }

    my $table     = 'protein';
    my $ra_fields = __GetAllFields($table);
    if ($$ro_param->IsDefined('field'))
    {
        my $field = $$ro_param->Get('field');
        $ra_fields = [$table . '.' . $field];
        $returns   = 'list';
    }
    else
    {
        $returns = 'table';
    }

    my $mask_field = join(',', @{$ra_fields});
    $statement =~ s/#MASK_FIELD#/$mask_field/g;

    my $filter;
    $filter = 'accession'  if ($$ro_param->IsDefined('accession'));
    $filter = 'annotation' if ($$ro_param->IsDefined('annotation'));
    my $filter_value = uri_unescape($$ro_param->Get($filter));
    $filter_value =~ s/\*/%/g;
    if ($filter ne 'accession')
    {
        $filter_value = '%' . $filter_value . '%';
    }
    else
    {
        #Pour rechercher des accessions sans preciser le Namespace
        $filter_value = '%' . $filter_value;
    }
    $statement =~ s/#MASK_FILTER#/$filter/;
    $statement =~ s/#MASK_FILTER_VALUE#/$filter_value/;

    my $ra_list = [];
    ($ra_list, $ra_fields) = __DBExecute($statement, $ra_fields, $returns, $ro_param);
    push(@{$$ro_param->Get('profiler')}, {"end_sql" => time});
    my $ra_links = [];
    if ($returns eq 'table')
    {
        my %h_rels = (
                      'groups'    => ['homologyGroup', 'accession'],
                      'sequences' => ['accession',     'accession']
                      );

        $ra_links = __AddTableHypermediaLinks($ra_list, \%h_rels, $ro_param);
    }
    push(@{$$ro_param->Get('profiler')}, {"end_hypermedia" => time});

    $ra_fields = __GetReturnedFields($ra_fields, $ra_list);
    __AddFieldsMetadata($ra_fields);
    push(@{$$ro_param->Get('profiler')}, {"end___AddFieldsMetadata" => time});
    if (grep ('annotation', @{$ra_list}))
    {
        __JsonifyAnnotation($ra_list);
    }

    push(@{$$ro_param->Get('profiler')}, {"end__JsonifyAnnotation" => time});
    __Return({$returns => $ra_list, 'fields' => $ra_fields, 'links' => $ra_links},
             scalar @{$ra_list} . " items", $ro_param);

    return;

}

sub __WSGetSequences
{
    my ($ro_param) = @_;

    my $ra_list = [];
    if ($$ro_param->IsDefined('accession'))
    {
        $ra_list = __GetProteinInfoFromAccessions($ro_param);
    }
    else
    {
        $ra_list = __GetProteinInfoFromGroup($ro_param);
    }

    my ($cache_dir, $cache_url) = __GetCacheDir($ro_param);
    my $statement_hash =
      md5_hex(  $$ro_param->Get('method')
              . $$ro_param->Get('resource')
              . $$ro_param->Get('accession')
              . join(',', sort split(',', $$ro_param->Get('group')))
              . join(',', sort split(',', $$ro_param->Get('proteome')))
              . $$ro_param->Get('analysis'));
    my $sequence_file = $cache_dir . '/' . $statement_hash . '.' . $H_EXTENSIONS{'sequences'};
    my $sequence_url  = $cache_url . '/' . $statement_hash . '.' . $H_EXTENSIONS{'sequences'};

    my %h_sequence_data = ();
    if (!-s $sequence_file)
    {
        __ExtractSequenceFromProteinInfo($ra_list, $sequence_file, \%h_sequence_data, $ro_param);
    }
    else
    {
        &FastaToHash($sequence_file, \%h_sequence_data);
    }
    $$ro_param->Set('hyper_rawdata', $sequence_url);
    $$ro_param->Set('totalCount',    scalar keys %h_sequence_data);

    $ra_list = [values %h_sequence_data];
    my %h_rels = (
                  'groups'   => ['protein_accession', 'protein'],
                  'proteins' => ['protein_accession', 'accession']
                  );

    my $ra_links = __AddTableHypermediaLinks($ra_list, \%h_rels, $ro_param);

    __Return({'sequences' => $ra_list, 'links' => $ra_links}, "Fasta file", $ro_param);
}

sub __WSGetAlignment
{
    my ($ro_param) = @_;

    my $ra_list = __GetProteinInfoFromGroup($ro_param);

    my ($cache_dir, $cache_url) = __GetCacheDir($ro_param);
    $$ro_param->SetUnlessDefined('algo', 'globalpair');

    my $algo = $$ro_param->Get('algo');
    my @a_allowed_algos = ('globalpair', 'localpair', 'genafpair');
    if (!grep (/$algo/, @a_allowed_algos))
    {
        __Error("$algo is not a valid alignment type: " . join(',', @a_allowed_algos), $ro_param);
    }

    my $statement_hash =
      md5_hex(  $$ro_param->Get('method')
              . $$ro_param->Get('resource')
              . join(',', sort split(',', $$ro_param->Get('group')))
              . join(',', sort split(',', $$ro_param->Get('proteome')))
              . $$ro_param->Get('analysis')
              . $algo);

    my $sequence_file  = $cache_dir . '/' . $statement_hash . '.' . $H_EXTENSIONS{'sequences'};
    my $sequence_url   = $cache_url . '/' . $statement_hash . '.' . $H_EXTENSIONS{'sequences'};
    my $alignment_file = $cache_dir . '/' . $statement_hash . '.' . $H_EXTENSIONS{'alignment'};
    my $alignment_url  = $cache_url . '/' . $statement_hash . '.' . $H_EXTENSIONS{'alignment'};

    if (!-s $alignment_file)
    {
        if (!-s $sequence_file)
        {
            __ExtractSequenceFromProteinInfo($ra_list, $sequence_file, {}, $ro_param);
        }

        my $num_seq = `grep -c '^>' $sequence_file`;
        chomp $num_seq;

        if ($num_seq < 2)
        {
            __Error("Less than 2 sequences found - Can not align", $ro_param);
        }
        else
        {

            my $alncmd =
                'mafft --auto --'
              . $algo
              . ' --quiet --reorder '
              . $sequence_file
              . ' | cut -d " " -f1 > '
              . $alignment_file;
            $$ro_param->Set('debug', $alncmd);
            system $alncmd;
        }
    }

    my $tree_file = $alignment_file . '.tree';
    my $tree_url  = $alignment_url . '.tree';

    if (!-s $tree_file)
    {
        my $fasttree = __GetFasttreeBin();

        if ($fasttree =~ /fasttree/i)
        {
            my $treecmd = $fasttree . ' -quiet -nopr -fastest ' . $alignment_file . ' > ' . $tree_file;
            $$ro_param->Set('debug', $treecmd);
            system $treecmd;
        }
        else
        {
            __Error("FastTree binary not found - Please contact " . $$ro_param->Get('admin_mail'), $ro_param);
        }

    }

    my $alistat_file   = $alignment_file . '.alistat';
    my $consensus_file = $alignment_file . '.alistat-consensus.fasta';
    my $consensus_url  = $alignment_url . '.alistat-consensus.fasta';

    if (!-s $alistat_file)
    {
        my $alistatcmd = "alistat --consensus $consensus_file $alignment_file > $alistat_file";
        $$ro_param->Set('debug', $alistatcmd);
        system $alistatcmd;
        my $group = $$ro_param->Get('group');
        $group =~ s/\W+/_/g;
        system("sed -i 's/>consensus/>$group\|consensus/' $consensus_file");

    }

    my $alistat = `cat $alistat_file`;
    chomp $alistat;

    $$ro_param->Set('hyper_rawdata',   $alignment_url);
    $$ro_param->Set('hyper_sequences', $sequence_url);
    $$ro_param->Set('hyper_consensus', $consensus_url);
    $$ro_param->Set('hyper_tree',      $alignment_url . '.tree');
    $$ro_param->Set('totalCount',      1);

    __Return({'alignment' => $alistat}, "Aligned Fasta file", $ro_param);
}

sub __GetFasttreeBin
{
    my @a_binnames = ('fasttree', 'FastTree');
    foreach my $binname (@a_binnames)
    {
        my $cmd  = "which $binname";
        my $test = `$cmd`;
        chomp $test;
        if ($test ne '')
        {
            return $binname;
        }
    }

    return &FALSE;

}

sub __ExtractSequenceFromProteinInfo
{

    my ($ra_list, $sequence_file, $rh_sequence_data, $ro_param) = @_;

    my %h_sequence_data = ();
    my $accession_file  = "$sequence_file.acc";
    my $fh_acc          = &GetStreamOut($accession_file);
    foreach my $rh_tuple (@{$ra_list})
    {

        delete $rh_tuple->{'id'};
        my $accession = $rh_tuple->{'protein_accession'};
        print $fh_acc $accession . "\n";
        $rh_sequence_data->{$accession} = $rh_tuple;
    }

    $fh_acc->close;

    my $blastdb = __GetUserDir($ro_param) . $$ro_param->Get('analysis') . '/blastdb/_ALL';

#Je passe par le fasta car s'il y a des namespace dans le fasta original (gi|) le outfmt %a crashe parfois : FASTA-style ID LCL|PIPE|TEST5G54280 has too many parts
	my $blast_bindir = $O_CONF->Get('blast_bindir');
    my $cmd = $blast_bindir . '/' 
      .  'blastdbcmd'
      . ' -entry_batch '
      . $accession_file . ' -db '
      . $blastdb
      . ' -outfmt "%f" | sed "s/lcl|//" > '
      . $accession_file
      . '.fasta';
    $$ro_param->Set('debug', $cmd);
    &System($cmd);

    my %h_fasta_to_hash = ();
    &FastaToHash($accession_file . '.fasta', \%h_fasta_to_hash);

    my @a_to_found = keys %{$rh_sequence_data};
    my @a_found    = ();
    my $fh_seq     = &GetStreamOut($sequence_file);
    foreach my $accession (keys %h_fasta_to_hash)
    {
        my $sequence = $h_fasta_to_hash{$accession}->{'sequence'};
        my $len      = $h_fasta_to_hash{$accession}->{'len'};
        my $product  = $h_fasta_to_hash{$accession}->{'header'};

        push(@a_found, $accession);
        print $fh_seq '>' . $rh_sequence_data->{$accession}->{'proteome'} . '_' . $accession;
        foreach my $key (sort keys %{$rh_sequence_data->{$accession}})
        {
            next if ($key =~ /sequence|def/);
            print $fh_seq ' ' . $key . '=' . $rh_sequence_data->{$accession}->{$key};
        }
        $rh_sequence_data->{$accession}->{'len'}      = $len;
        $rh_sequence_data->{$accession}->{'sequence'} = $sequence;
        $rh_sequence_data->{$accession}->{'def'}      = $product;

        print $fh_seq ' len=' . $len;
        print $fh_seq ' ' . $product . "\n";
        print $fh_seq &FormatSeq(\$sequence) . "\n";

    }
    $fh_seq->close;
    my @a_missing_accessions = Array::Utils::array_minus(@a_to_found, @a_found);
    if (scalar @a_missing_accessions > 0)
    {
        unlink $sequence_file;
        __Error("Accessions not found: " . join(',', @a_missing_accessions), $ro_param);
    }

    unlink $accession_file . '.fasta';
    unlink $accession_file;

    return;

}

sub __GetProteinInfoFromAccessions
{
    my ($ro_param) = @_;
    my $returns = 'table';
    my $statement;

    if ($$ro_param->IsDefined('accession'))
    {
        my $accession = uc($$ro_param->Get('accession'));
        $accession =~ s/,/","/g;
        $statement = $H_SQL_MASKS{'get'}->{'sequences'}->{'from_accession'};
        $statement =~ s/#MASK_ACCESSION#/$accession/g;
    }
    else
    {
        __Error("accession must be specified to extract sequence", $ro_param);
    }

    my ($ra_list, $ra_fields) = __DBExecute($statement, [], $returns, $ro_param);
    return $ra_list;

}

sub __GetProteinInfoFromGroup
{
    my ($ro_param) = @_;
    my $returns = 'table';
    my $statement;

    #
    # Extraire les sequences d'un groupe
    # Extraire les sequences d'une espece et d'un groupe
    # Extraire toutes les sequences d'une espece qui sont au moins dans un groupe
    #

    if ($$ro_param->IsDefined('group') && $$ro_param->IsDefined('proteome'))
    {
        my $group = uc($$ro_param->Get('group'));
        __Error("Alignment is available only for one group at a time", $ro_param)
          if ($group =~ /,/ && $$ro_param->Get('resource') eq 'alignment');
        my $proteome = uc($$ro_param->Get('proteome'));
        $group =~ s/,/","/g;
        $proteome =~ s/,/","/g;
        $statement = $H_SQL_MASKS{'get'}->{'sequences'}->{'from_group_and_proteome'};
        $statement =~ s/#MASK_GROUP#/$group/g;
        $statement =~ s/#MASK_PROTEOME#/$proteome/g;
    }
    elsif ($$ro_param->IsDefined('group'))
    {
        my $group = uc($$ro_param->Get('group'));
        __Error("Alignment is available only for one group at a time", $ro_param)
          if ($group =~ /,/ && $$ro_param->Get('resource') eq 'alignment');
        $group =~ s/,/","/g;
        $statement = $H_SQL_MASKS{'get'}->{'sequences'}->{'from_group'};
        $statement =~ s/#MASK_GROUP#/$group/g;
    }
    else
    {
        __Error("group must be specified to extract sequence", $ro_param);
    }

    my ($ra_list, $ra_fields) = __DBExecute($statement, [], $returns, $ro_param);
    return $ra_list;

}

sub __GetInOutGroupIds
{
    my $ro_param       = shift;
    my %h_ingroups_ids = ();
    my @a_inproteomes  = split(',', $$ro_param->Get('inproteome'));

    my $ra_inproteomes_ids  = __GetProteomeIds($$ro_param->Get('inproteome'),  $ro_param);
    my $ra_outproteomes_ids = __GetProteomeIds($$ro_param->Get('outproteome'), $ro_param);

    if ($$ro_param->Get('inproteome') eq '_ALL')
    {
        __Error("outproteome MUST BE DEFINED", $ro_param) unless ($$ro_param->IsDefined('outproteome'));
        @a_inproteomes = @{__GetNotInProteomeCodes($$ro_param->Get('outproteome'), $ro_param)};
    }

    my @a_outproteomes = split(',', $$ro_param->Get('outproteome'));

    my @a_ingroup_ids  = ();
    my @a_outgroup_ids = (0);

	foreach my $inproteome_id (@{$ra_inproteomes_ids})
    {
	    foreach my $ingroup_id (@{__GetGroupIdsFromProteomeId($inproteome_id, $ro_param)})
        {
            $h_ingroups_ids{$ingroup_id} = 0 unless defined $h_ingroups_ids{$ingroup_id};
            $h_ingroups_ids{$ingroup_id}++;
        }
    }

	foreach my $ingroup_id (keys %h_ingroups_ids)
    {
        push(@a_ingroup_ids, $ingroup_id) if ($h_ingroups_ids{$ingroup_id} == scalar(@a_inproteomes));
    }
	    
    foreach my $outproteome_id (@{$ra_outproteomes_ids})
    {
        push(@a_outgroup_ids, @{__GetGroupIdsFromProteomeId($outproteome_id, $ro_param)});
    }
    return (\@a_ingroup_ids, \@a_outgroup_ids);

}

=pod

	API: get/groups
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/groups?accession=testOk97,testOk78
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/groups?annotation=GO:0005737&field=genes
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/groups?inproteome=Ecoli,Xbbric
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/groups?inproteome=Ecoli,Xbbric&outproteome=Bacsu
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/groups?inproteome=Ecoli,Xbbric&outproteome=Bacsu&annotation=kinase
	
	
=cut

sub __WSGetGroups
{
    my ($ro_param) = @_;

    my $returns;

    my $statement = $H_SQL_MASKS{'get'}->{'groups'}->{'filter'};

    if ($$ro_param->IsDefined('accession'))
    {
        my $accession = uc($$ro_param->Get('accession'));
        $accession =~ s/,/","/g;
        $statement = $H_SQL_MASKS{'get'}->{'groups'}->{'accession'};
        $statement =~ s/#MASK_ACCESSION#/$accession/g;
    }
    elsif ($$ro_param->IsDefined('protein'))
    {
        my $protein = uc($$ro_param->Get('protein'));
        $protein =~ s/,/","/g;
        $statement = $H_SQL_MASKS{'get'}->{'groups'}->{'from_protein'};
        $statement =~ s/#MASK_PROTEIN#/$protein/g;
    }
    elsif ($$ro_param->IsDefined('inproteome') || $$ro_param->IsDefined('outproteome'))
    {
        my ($ra_ingroup_ids, $ra_outgroup_ids) = __GetInOutGroupIds($ro_param);

        if (scalar @{$ra_ingroup_ids} == 0)
        {
            $statement = $H_SQL_MASKS{'get'}->{'groups'}->{'not_from_ids'};
            my $not_keep = join(',', sort @{$ra_outgroup_ids});
            $statement =~ s/#MASK_IDS#/$not_keep/g;
        }
        else
        {
            my @a_keep_groups = Array::Utils::array_minus(@{$ra_ingroup_ids}, @{$ra_outgroup_ids});
            $statement = $H_SQL_MASKS{'get'}->{'groups'}->{'from_ids'};
            my $keep = join(',', sort @a_keep_groups);
            $statement =~ s/#MASK_IDS#/$keep/g;
        }

        #MASK_FILTER_STATEMENT#
        my $filter_statement = '';
        if ($$ro_param->IsDefined('annotation'))
        {
            $filter_statement = ' AND homologyGroup.#MASK_FILTER# LIKE "%#MASK_FILTER_VALUE#%"';
        }
        $statement =~ s/#MASK_FILTER_STATEMENT#/$filter_statement/;
    }

    my $table     = 'homologyGroup';
    my $ra_fields = __GetAllFields($table);

    if ($$ro_param->IsDefined('field'))
    {
        my $field = $$ro_param->Get('field');
        $ra_fields = [$table . '.' . $field];
        $returns   = 'list';
    }
    else
    {
        $returns = 'table';
    }

    my $mask_field = join(',', @{$ra_fields});
    $statement =~ s/#MASK_FIELD#/$mask_field/g;

    my $filter;
    $filter = 'accession'  if ($$ro_param->IsDefined('accession'));
    $filter = 'annotation' if ($$ro_param->IsDefined('annotation'));

    my $filter_value = uri_unescape($$ro_param->Get($filter));
    $statement =~ s/#MASK_FILTER#/$filter/;
    $statement =~ s/#MASK_FILTER_VALUE#/$filter_value/;

    my $ra_list = [];
    ($ra_list, $ra_fields) = __DBExecute($statement, $ra_fields, $returns, $ro_param);

    my $ra_links = [];
    if ($returns eq 'table')
    {
        my %h_rels = (
                      'sequences' => ['accession' => 'group'],
                      'alignment' => ['accession' => 'group'],
                      'proteins'  => ['accession' => 'group']
                      );

        $ra_links = __AddTableHypermediaLinks($ra_list, \%h_rels, $ro_param);
    }

    $ra_fields = __GetReturnedFields($ra_fields, $ra_list);
    __AddFieldsMetadata($ra_fields);

    if (grep ('annotation', @{$ra_list}))
    {
        __JsonifyAnnotation($ra_list);
    }

    __Return({$returns => $ra_list, 'fields' => $ra_fields, 'links' => $ra_links},
             scalar @{$ra_list} . " items", $ro_param);

    return;

}

sub __GetReturnedFields
{
    my $ra_fields = shift();
    my $ra_list   = shift();
    $ra_fields = [sort keys %{$ra_list->[0]}] if (scalar @{$ra_fields} == 0);

    my @a_tmp = ();
    foreach my $field (sort @{$ra_fields})
    {
        next if ($field =~ /^id|\.id$|_id$|^links$/);
        $field =~ s/^\w+\.//;
        push(@a_tmp, $field);
    }
    return \@a_tmp;

}

sub __AddFieldsMetadata
{
    my $ra_fields = shift;

    for (my $i = 0; $i < scalar @{$ra_fields}; $i++)
    {
        my $field = $ra_fields->[$i];
        my $rh_field = {'pretty_name' => ucfirst($field), 'desc' => ucfirst($field)};
        $rh_field = $H_FIELD_DESC{$field} if (defined $H_FIELD_DESC{$field});
        $rh_field->{'name'} = $field;
        $ra_fields->[$i] = $rh_field;
    }
    return;
}

sub __AddTableHypermediaLinks
{
    my ($ra_list, $rh_rels, $ro_param) = @_;
    my $root_uri = __GetRootURI() . '/' . $$ro_param->Get('analysis') . '/get/';
    my @a_array_item_fields = ('proteomes', 'proteins');

    #ra_list a changé !!!
    foreach my $rh_item (@{$ra_list})
    {
        #LISTE = ARRAY EN JSON
        delete $rh_item->{'header'};
        foreach my $array_field (@a_array_item_fields)
        {
            if (defined $rh_item->{$array_field})
            {
                $rh_item->{$array_field} = [split(',', $rh_item->{$array_field})];
            }
        }
        $rh_item->{'links'} = [];
        foreach my $rel (sort keys %{$rh_rels})
        {
            my $url = $root_uri . $rel . '?' . $rh_rels->{$rel}->[1] . '=' . $rh_item->{$rh_rels->{$rel}->[0]};
            $url .= '&owner=' . $$ro_param->Get('owner') if ($$ro_param->IsDefined('owner'));
            push(@{$rh_item->{'links'}}, {'rel' => $rel, 'href' => $url, 'desc' => $H_HYPERMEDIA_DESC{$rel}});
        }
    }

    my @a_links = ();
    foreach my $rel (sort keys %{$rh_rels})
    {
        push(@a_links, {'rel' => $rel, 'desc' => $H_HYPERMEDIA_DESC{$rel}});
    }
    return \@a_links;

}

sub __JsonifyAnnotation
{
    my ($ra_list, $rh_rels, $ro_param) = @_;
    foreach my $rh_item (@{$ra_list})
    {
        my $json = $rh_item->{'annotation'};
        eval {$rh_item->{'annotation'} = decode_json($json)};
    }
}

=pod


	API: get/matrix
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/matrix?proteome=Ecoli,Xbbric
		#On ne veut que les 2 colonnes Ecoli et Xbbric pour les groupes ayant au moins 1 proteine de l'un des deux proteomes
	
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/matrix?inproteome=Ecoli,Xbbric
		#On veut toutes les colonnes pour les groupes ayant au moins 1 proteine de chacun des 2 proteomes
		
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/matrix?inproteome=Ecoli,Xbbric&outproteome=Bacsu
		#On veut toutes les colonnes pour les groupes ayant au moins 1 proteine de chacun des 2 proteomes et aucune proteine de Bacsu
	
	@ https://lipm-hal.toulouse.inra.fr/family-companion/ws/NPoqvcSoPE/get/matrix?inproteome=Ecoli,Xbbric&outproteome=Bacsu&limit=10&type=binary
	
	
=cut

sub __WSGetMatrix
{
    my ($ro_param) = @_;

    my $statement;
    
	my @a_keep_groups = ();
	my ($ra_ingroup_ids, $ra_outgroup_ids) = ([],[]);
    if ($$ro_param->IsDefined('proteome'))
    {
        my $proteome = uc($$ro_param->Get('proteome'));
        $proteome =~ s/,/","/g;
        $statement = $H_SQL_MASKS{'get'}->{'matrix'}->{'abundance_from_proteome'};
        $statement = $H_SQL_MASKS{'get'}->{'matrix'}->{'binary_from_proteome'} if ($$ro_param->Get('type') eq 'binary');
        $statement =~ s/#MASK_PROTEOME#/$proteome/g;
    }
    elsif ($$ro_param->IsDefined('inproteome') || $$ro_param->IsDefined('outproteome'))
    {
        ($ra_ingroup_ids, $ra_outgroup_ids) = __GetInOutGroupIds($ro_param);

        if (scalar @{$ra_ingroup_ids} == 0)
        {
            $statement = $H_SQL_MASKS{'get'}->{'matrix'}->{'abundance_not_from_ids'};
            $statement = $H_SQL_MASKS{'get'}->{'matrix'}->{'binary_not_from_ids'} if ($$ro_param->Get('type') eq 'binary');
            my $not_keep = join(',', sort @{$ra_outgroup_ids});
            $statement =~ s/#MASK_IDS#/$not_keep/g;
        }
        else
        {
            @a_keep_groups = Array::Utils::array_minus(@{$ra_ingroup_ids}, @{$ra_outgroup_ids});
            $statement = $H_SQL_MASKS{'get'}->{'matrix'}->{'abundance_from_ids'};
            $statement = $H_SQL_MASKS{'get'}->{'matrix'}->{'binary_from_ids'} if ($$ro_param->Get('type') eq 'binary');

            my $keep = join(',', sort @a_keep_groups);
            $statement =~ s/#MASK_IDS#/$keep/g;
        }

    }

    #MASK_FILTER_STATEMENT#
    if ($$ro_param->IsDefined('annotation') || $$ro_param->IsDefined('accession'))
    {
		my $ra_filter_ids = [];
    	$ra_filter_ids = __GetGroupIdsFromFilter('annotation', $$ro_param->Get('annotation'), $ro_param) if ($$ro_param->IsDefined('annotation'));
		$ra_filter_ids = __GetGroupIdsFromFilter('accession', $$ro_param->Get('accession'), $ro_param) if ($$ro_param->IsDefined('accession'));
	
		if (scalar $ra_filter_ids > 0)
		{
			if (scalar @{$ra_ingroup_ids} == 0)
			{
				@{$ra_ingroup_ids} = @{$ra_filter_ids};
			}
			if (scalar @{$ra_ingroup_ids} > 0)
			{
				@a_keep_groups = Array::Utils::intersect(@{$ra_ingroup_ids}, @{$ra_filter_ids});
			}
			@a_keep_groups = Array::Utils::array_minus(@{$ra_ingroup_ids}, @{$ra_outgroup_ids});
			$statement = $H_SQL_MASKS{'get'}->{'matrix'}->{'abundance_from_ids'};
            $statement = $H_SQL_MASKS{'get'}->{'matrix'}->{'binary_from_ids'} if ($$ro_param->Get('type') eq 'binary');

            my $keep = join(',', sort @a_keep_groups);
            $statement =~ s/#MASK_IDS#/$keep/g;
		}
    }
    my $returns = 'matrix';

    my $table     = 'homologyGroup';
    my $ra_fields = __GetAllFields($table);
    if ($$ro_param->IsDefined('field'))
    {
        my $field = $$ro_param->Get('field');
        $ra_fields = [$table . '.' . $field];
    }

    my $mask_field = join(',', @{$ra_fields});
    $statement =~ s/#MASK_FIELD#/$mask_field/g;

    my $ra_list = [];
    ($ra_list, $ra_fields) = __DBExecute($statement, $ra_fields, $returns, $ro_param);

    $ra_fields = [grep {$_ !~ /^id|\.id$|_id$|^links$/} sort keys %{$ra_list->[0]}] if (scalar @{$ra_fields} == 0);
    __AddFieldsMetadata($ra_fields);
    __Return({$returns => $ra_list, 'fields' => $ra_fields}, scalar @{$ra_list} . " items", $ro_param);
}

sub __GetNotInProteomeCodes
{
    my ($notinproteome, $ro_param) = @_;
    $notinproteome =~ s/,/","/g;
    $notinproteome = uc($notinproteome);
    my $statement = $H_SQL_MASKS{'get'}->{'proteomes'}->{'code_not_in'};
    $statement =~ s/#MASK_PROTEOME#/$notinproteome/g;
    my $field = 'code';
    $statement =~ s/#MASK_FIELD#/$field/g;

    my $ra_fields = ['code'];
    my $returns   = 'list';
    my $ra_list   = [];
    ($ra_list, $ra_fields) = __DBExecute($statement, $ra_fields, $returns, $ro_param);
    return $ra_list;

}

sub __GetProteomeIds
{
    my ($proteome_codes, $ro_param) = @_;
    $proteome_codes =~ s/,/","/g;
    $proteome_codes = uc($proteome_codes);
    my $statement = $H_SQL_MASKS{'get'}->{'proteomes'}->{'code_in'};
    $statement =~ s/#MASK_PROTEOME#/$proteome_codes/g;
    my $field = 'id';
    $statement =~ s/#MASK_FIELD#/$field/g;

    my $ra_fields = ['id'];
    my $returns   = 'list';
    my $ra_list   = [];
    if ($proteome_codes ne '')
    {
        ($ra_list, $ra_fields) = __DBExecute($statement, $ra_fields, $returns, $ro_param);
    }
    return $ra_list;

}

sub __GetGroupIdsFromProteomeId
{
    my ($proteome_id, $ro_param) = @_;
    my $statement = $H_SQL_MASKS{'get'}->{'groups'}->{'groupids_from_single_proteomeid'};
    $statement =~ s/#MASK_PROTEOME_ID#/$proteome_id/g;

    my $ra_fields = ['id'];
    my $returns   = 'list';
    my $ra_list   = [];

    if ($proteome_id =~ /^\d+$/)
    {
        ($ra_list, $ra_fields) = __DBExecute($statement, $ra_fields, $returns, $ro_param);
    }
    return $ra_list;

}

sub __GetGroupIdsFromFilter
{
	my ($filter, $filter_value, $ro_param) = @_;
	my $statement = $H_SQL_MASKS{'get'}->{'groups'}->{'groupids_from_filter'};
	$statement =~ s/#MASK_FILTER#/$filter/;
    $statement =~ s/#MASK_FILTER_VALUE#/$filter_value/;
    my $ra_fields = ['id'];
    my $returns   = 'list';
    my $ra_list   = [];
    ($ra_list, $ra_fields) = __DBExecute($statement, $ra_fields, $returns, $ro_param);
    return $ra_list;
}


sub __GetGroupIdFromSingleProteome
{
    my ($proteome, $ro_param) = @_;
    $proteome = uc($proteome);
    my $statement = $H_SQL_MASKS{'get'}->{'groups'}->{'groupids_from_single_proteome'};
    $statement =~ s/#MASK_PROTEOME#/$proteome/g;
    my $field = 'homologyGroup.id';
    $statement =~ s/#MASK_FIELD#/$field/g;

    my $filter_statement = '';
    if ($$ro_param->IsDefined('annotation'))
    {
        $statement = $H_SQL_MASKS{'get'}->{'groups'}->{'from_single_proteome'};
        $statement =~ s/#MASK_PROTEOME#/$proteome/g;
        $field = 'homologyGroup.id';
        $statement =~ s/#MASK_FIELD#/$field/g;
        $filter_statement = ' AND homologyGroup.#MASK_FILTER# LIKE "%#MASK_FILTER_VALUE#%"';
    }
    $statement =~ s/#MASK_FILTER_STATEMENT#/$filter_statement/;

    my $filter;
    if ($statement =~ /#MASK_FILTER#/)
    {
        $filter = 'accession'  if ($$ro_param->IsDefined('accession'));
        $filter = 'annotation' if ($$ro_param->IsDefined('annotation'));
        my $filter_value = $$ro_param->Get($filter);
        $statement =~ s/#MASK_FILTER#/$filter/;
        $statement =~ s/#MASK_FILTER_VALUE#/$filter_value/;
    }
    my $ra_fields = ['id'];
    my $returns   = 'list';
    my $ra_list   = [];
    ($ra_list, $ra_fields) = __DBExecute($statement, $ra_fields, $returns, $ro_param);
    return $ra_list;

}

sub __DBExecute
{
    my ($statement, $ra_fields, $returns, $ro_param) = @_;

    my $limit  = '';
    my $offset = '';

    my $ra_list = [];
    ($ra_list, $ra_fields) = __BuildRawdataFile($statement, $ra_fields, $returns, $ro_param);
    push(@{$$ro_param->Get('profiler')}, {"end_rawdata" => time});
    $$ro_param->Set('sql', []) unless $$ro_param->IsDefined('sql');
    if ($$ro_param->IsDefined('limit'))
    {
        $$ro_param->Set('start', 0) unless ($$ro_param->IsDefined('start'));
        $offset = ' OFFSET ' . $$ro_param->Get('start');
        $limit  = ' LIMIT ' . $$ro_param->Get('limit');
        $statement =~ s/#MASK_LIMIT#/$limit/;
        $statement =~ s/#MASK_OFFSET#/$offset/;

        __DBSanitize(\$statement);

        push(@{$$ro_param->Get('sql')}, $statement);

        my $sth = __DBPrepareStatement($statement, {}, $ro_param);
        $sth->execute();
        $ra_fields = $sth->{'NAME'};    #  if (scalar @{$ra_fields} == 0);

        my $ra_tuples = $sth->fetchall_arrayref();
        if ($returns eq 'list')
        {
			$ra_list = [];
            foreach my $ra_values (@{$ra_tuples})
            {
                push(@{$ra_list}, $ra_values->[0]);
            }
        }
        elsif ($returns eq 'table')
        {
            #REECRIRE AVEC LE FETCHARRAYREF
            $ra_list = [values %{$sth->fetchall_hashref('id')}];

            #recuperer l'indice de ID ou _ID)
            foreach my $ra_values (@{$ra_tuples})
            {
                my %h_tmp = ();
                for (my $i = 0; $i < scalar @{$ra_fields}; $i++)
                {
                    my ($clean_field) = ($ra_fields->[$i] =~ /(\w+)$/);
                    $h_tmp{$clean_field} = $ra_values->[$i];
                }
                push(@{$ra_list}, \%h_tmp);
            }

        }
        elsif ($returns eq 'matrix')
        {
            $ra_list = __ConvertToMatrix($sth, undef);
        }

    }
    return ($ra_list, $ra_fields);
}

sub __GetGroupAnnotationFiles
{
    my ($dir)   = @_;
    my $cmd     = 'find ' . $dir . ' -name "*.tsv" -exec grep -H "^#accession" {} \; | cut -d ":" -f1';
    my @a_files = `$cmd`;

    chomp @a_files;
    return \@a_files;
}

sub __ConvertToInchlibData
{
    my ($infile, $ro_param) = @_;

    #my $cmd = 'sed -E \'s/\t/,/g;s/^#ROW/id/;s/^GROUP/id/;\' ' . $infile . ' > ' . $infile . '.inchlib.csv;';
    my $cmd = 'sed -E \'s/\t/,/g;s/^#ROW/id/;\' ' . $infile . ' > ' . $infile . '.inchlib.csv;';
    my $dt  = 'numeric';
    $dt = 'binary' if ($$ro_param->Get('type') eq 'binary');
	my $input_lines = `grep -c -v ^# $infile` ;
	chomp $input_lines;
	
	
	my $compress_threshold = $O_CONF->Get('inchlib_clust_compress_threshold');
	my $compress_mode =&FALSE;
	if ($input_lines > $compress_threshold)
	{
		$compress_mode = &TRUE;
	}

    #~ $cmd .= $O_CONF->Get('inchlib_clust') . ' -c ' . $compress_threshold .' -a both -dh -dd , -dt ' . $dt . ' -o ' . "$infile.inchlib.json ";
    $cmd .= $O_CONF->Get('inchlib_clust') . ' -c ' . $compress_threshold .' -cv mean -mcv mean -a both -dh -dd , -dt ' . $dt . ' -o ' . "$infile.inchlib.json ";

    my ($cache_dir, $cache_url) = __GetCacheDir($ro_param);
    my $ra_group_annotation_files = __GetGroupAnnotationFiles($cache_dir);

    if (scalar @{$ra_group_annotation_files} > 0 && scalar @{$ra_group_annotation_files} < $compress_threshold)
    {
        my $adapt_metadata_cmd = 'cat ' . join(' ', @{$ra_group_annotation_files}) . ' | sort -u > ' . "$infile.inchlib.metadata.raw";
        if (!-e "$infile.inchlib.metadata")
        {

            system($adapt_metadata_cmd);
            my @a_groups = `grep -v ^# $infile | cut -f1`;
            chomp @a_groups;
            $$ro_param->Set('debug', \@a_groups);
            my %h_groups = ();
            foreach my $group (@a_groups)
            {
                $h_groups{$group} = {};
            }

            my $fh_raw            = &GetStreamIn("$infile.inchlib.metadata.raw");
            my @a_fields          = ();
            my %h_array_separator = ('annotation' => ';', 'proteins' => ',', 'proteomes' => ',');
            while (my $line = <$fh_raw>)
            {
                chomp $line;
                $line =~ s/^#//;
                if (scalar @a_fields == 0)
                {
                    @a_fields = split("\t", $line);
                }
                else
                {
                    my @a_values = split("\t", $line);
                    $$ro_param->Set('debug', $line);
                    next unless defined $h_groups{$a_values[0]};
                    for (my $i = 0; $i < scalar @a_fields; $i++)
                    {
                        my $value = $a_values[$i];
                        if (defined $h_array_separator{$a_fields[$i]})
                        {
                            $value = [split($h_array_separator{$a_fields[$i]}, $value)];
                        }
                        $h_groups{$a_values[0]}->{$a_fields[$i]} = $value;
                    }
                }
            }
            $fh_raw->close;
            unlink("$infile.inchlib.metadata.raw");

            my $fh_inchclust_metadata = &GetStreamOut("$infile.inchlib.metadata");
            print $fh_inchclust_metadata "id,proteins,species\n";
            foreach my $group (keys %h_groups)
            {
                print $fh_inchclust_metadata $group . ','
                  . $h_groups{$group}->{'genes'} . ','
                  . $h_groups{$group}->{'taxa'} . "\n";
            }
            $fh_inchclust_metadata->close();

            my $fh_inchclust_onmouseovermetadata = &GetStreamOut("$infile.inchlib.onmouseover.metadata");
            print $fh_inchclust_onmouseovermetadata encode_json(\%h_groups);
            $fh_inchclust_onmouseovermetadata->close();

            if (-e "$infile.inchlib.metadata")
            {
                $cmd .= ' -mh -md , -m ' . "$infile.inchlib.metadata";
            }

        }
    }

    $cmd .= " $infile.inchlib.csv";

    system("($cmd > $infile.inchlib.stdout ) 2> $infile.inchlib.stderr");

    if (-s "$infile.inchlib.json")
    {
		my $compress_info = "\"compression\" : { \"mode\" : $compress_mode, \"input_lines\" : $input_lines, \"threshold\" : $compress_threshold }";
        my $add_success_cmd = 'sed -i \'s/^{/{"success" : "true",'.$compress_info.',/\' ' . "$infile.inchlib.json";
        system $add_success_cmd;
    }

    if (-s "$infile.inchlib.onmouseover.metadata")
    {
        my $cmd = 'sed -i \'s/^\}$/,/\' ' . "$infile.inchlib.json;";
        $cmd .= 'echo \'"onmouseover":\' >> ' . "$infile.inchlib.json;";
        $cmd .= "cat $infile.inchlib.onmouseover.metadata >> $infile.inchlib.json;";
        $cmd .= 'echo \'}\' >> ' . "$infile.inchlib.json";
        system $cmd;
        unlink("$infile.inchlib.onmouseover.metadata");

    }
    unlink("$infile.inchlib.metadata");
    unlink("$infile.inchlib.csv");

	system ("mv $infile.inchlib.json $infile.inchlib.json.raw ; cat $infile.inchlib.json.raw | json_xs -f json -t json > $infile.inchlib.json");

    return $compress_mode;
}

sub __ConvertToMatrix
{
    my ($sth, $fh_out) = @_;

    my $ra_list   = [];
    my @a_headers = ();
    my %h_headers = ();
    
    my %h_data    = ();
    my @a_lines   = ();
    my $ra_fields = $sth->{'NAME'};

	my %h_indices = ();
	for (my $i = 0; $i < scalar (@{$ra_fields}); $i++)
	{
		$h_indices{$ra_fields->[$i]} = $i;
	}
	#~ my $fh_debug = &GetStreamOut('/tmp/debug');
	#~ print $fh_debug "#### RA_TUPLES ####\n\n";
	foreach my $ra_tuple (@{$sth->fetchall_arrayref()})
    {
		#~ print $fh_debug Dumper $ra_tuple;
		#~ print $fh_debug "##\n\n";
		
		$h_data{$ra_tuple->[$h_indices{'line'}]} = {};
        my @a_columns = split(',', $ra_tuple->[$h_indices{'column_array'}]);
        push(@a_lines, $ra_tuple->[$h_indices{'line'}]);
        foreach my $column (@a_columns)
        {
            if (! defined $h_headers{$column})
            {
				push(@a_headers, $column) ;
				$h_headers{$column} = 1;
			}
            $h_data{$ra_tuple->[$h_indices{'line'}]}->{$column} = 0 unless defined $h_data{$ra_tuple->[$h_indices{'line'}]}->{$column};
            $h_data{$ra_tuple->[$h_indices{'line'}]}->{$column}++;
        }
    }
	#~ print $fh_debug "#### h_data ####\n\n";
    #~ print $fh_debug Dumper \%h_data;
	#~ print $fh_debug "##\n\n";
		

	#ORDRE COLONNES !!
    print $fh_out "#ROW\t" . join("\t", sort @a_headers) . "\n" if (defined $fh_out);

    foreach my $line (sort @a_lines)
    {
        my @a_values = ();
        foreach my $header (sort @a_headers)
        {
            $h_data{$line}->{$header} = 0 unless defined $h_data{$line}->{$header};
            push(@a_values, $h_data{$line}->{$header});
            $h_data{$line}->{'ROW'} = $line;

        }
        push(@{$ra_list}, $h_data{$line});
        print $fh_out "$line\t" . join("\t", @a_values) . "\n" if (defined $fh_out);
    }
	#~ $fh_debug->close;
    #inchlib_clust
    return $ra_list;
}

sub __BuildRawdataFile
{
    my ($statement, $ra_fields, $returns, $ro_param) = @_;
    $statement =~ s/#MASK_LIMIT#//;
    $statement =~ s/#MASK_OFFSET#//;

    __DBSanitize(\$statement);
    $$ro_param->Set('sql', []) unless $$ro_param->IsDefined('sql');
    push(@{$$ro_param->Get('sql')}, $statement);

    my $statement_hash = md5_hex($statement . $$ro_param->Get('analysis'));
    my ($cache_dir, $cache_url) = __GetCacheDir($ro_param);
    my $cache_file    = $cache_dir . '/' . $statement_hash . '.' . $H_EXTENSIONS{$returns};
    my $cache_fileurl = $cache_url . '/' . $statement_hash . '.' . $H_EXTENSIONS{$returns};

    my $ra_list = [];
    if (!-e $cache_file || &NOCACHE)
    {
        $$ro_param->Set('debug', $statement);

        my $fh_out = &GetStreamOut($cache_file);
        my $sth = __DBPrepareStatement($statement, {}, $ro_param);
        $sth->execute();

        $ra_fields = $sth->{'NAME'};    #  if (scalar @{$ra_fields} == 0);
                                        #~ __Error($ra_fields,$ro_param);
        push(@{$$ro_param->Get('profiler')}, {"end_sql_raw" => time});

        if ($returns eq 'list')
        {
            my $ra_tuples = $sth->fetchall_arrayref();
            if (scalar @{$ra_tuples} == 0)
            {
                print $fh_out '#no_hit' . "\n";
            }
            else
            {
                print $fh_out '#' . $ra_fields->[0] . "\n";

                foreach my $ra_values (@{$ra_tuples})
                {
                    print $fh_out $ra_values->[0] . "\n";
                    push(@{$ra_list}, $ra_values->[0]);
                }

            }

        }
        elsif ($returns eq 'table')
        {
            push(@{$$ro_param->Get('profiler')}, {"start_table" => time});

            my $ra_tuples = $sth->fetchall_arrayref();

            push(@{$$ro_param->Get('profiler')}, {"fetchall_arrayref" => time});

            if (scalar @{$ra_tuples} == 0)
            {
                print $fh_out '#no_hit' . "\n";
            }
            else
            {
                my %h_ignore_fields = ();
                my @a_headers       = ();
                for (my $i = 0; $i < scalar @{$ra_fields}; $i++)
                {
                    if ($ra_fields->[$i] =~ /^id|\.id$|_id$/)
                    {
                        $h_ignore_fields{$i} = 1;
                    }
                    else
                    {
                        push(@a_headers, $ra_fields->[$i]);
                    }
                }

                #recuperer l'indice de ID ou _ID)
                print $fh_out '#' . join("\t", @a_headers) . "\n";
                my @a_tsv_values = ();
                foreach my $ra_values (@{$ra_tuples})
                {
                    @a_tsv_values = ();
                    my %h_tmp = ();

                    for (my $i = 0; $i < scalar @{$ra_fields}; $i++)
                    {
                        my ($clean_field) = ($ra_fields->[$i] =~ /(\w+)$/);
                        $h_tmp{$clean_field} = $ra_values->[$i];

                        my $value = $ra_values->[$i];
                        if (   defined $H_FIELD_DESC{$header}
                            && defined $H_FIELD_DESC{$header}->{'type'}
                            && $H_FIELD_DESC{$header}->{'type'} eq 'array'
                            && $value =~ /\[".+"\]/)
                        {
                            eval {$value = join(';', @{decode_json($value)})};
                        }
                        push(@a_tsv_values, $value) if (!defined $h_ignore_fields{$i});
                    }
                    push(@{$ra_list}, \%h_tmp);
                    print $fh_out join("\t", @a_tsv_values) . "\n";
                }

                #~ __Error($ra_list,$ro_param);

            }
        }
        elsif ($returns eq 'matrix')
        {
            $ra_list = __ConvertToMatrix($sth, $fh_out);
            if (scalar @{$ra_list} > 1)
            {
                __ConvertToInchlibData($cache_file, $ro_param);
            }
        }
        $fh_out->close;

    }
    else
    {
        $$ro_param->Set('debug', 'From cache file');

        if (!$$ro_param->IsDefined('limit'))
        {
            my $fh_in = &GetStreamIn($cache_file);
            if ($returns eq 'list')
            {
                while (my $line = <$fh_in>)
                {
                    chomp $line;
                    next if ($line =~ /#/);
                    push(@{$ra_list}, $line);
                }
            }
            elsif ($returns =~ /table|matrix/)
            {
                my @a_headers = ();
                while (my $line = <$fh_in>)
                {
                    chomp $line;
                    if ($line =~ /#/)
                    {
                        $line =~ s/#//;
                        @a_headers = split("\t", $line);
                    }
                    else
                    {
                        my @a_values = split("\t", $line);
                        my %h_local  = ();
                        my $i        = 0;
                        foreach my $header (@a_headers)
                        {
                            $h_local{$header} = $a_values[$i];
                            $i++;
                        }
                        push(@{$ra_list}, \%h_local);
                    }
                }
            }

            $fh_in->close;
        }

    }

    my $totalCount = `wc -l $cache_file`;
    chomp $totalCount;
    $totalCount--;

    $$ro_param->Set('totalCount',    $totalCount);
    $$ro_param->Set('hyper_rawdata', $cache_fileurl);

    if (-s "$cache_file.inchlib.json")
    {
        $$ro_param->Set('hyper_inchlib', "$cache_fileurl.inchlib.json");
    }

    return ($ra_list, $ra_fields);

}

sub __GetCacheDir
{
    my $ro_param  = shift;
    my $cache_dir = $O_CONF->Get('portal_install_dir') . '/site/tmp/Cache/';
    mkdir $cache_dir unless -d $cache_dir;
    $cache_dir .= $$ro_param->Get('userhash');
    mkdir $cache_dir unless -d $cache_dir;

    $cache_dir .= '/' . $$ro_param->Get('analysis');
    mkdir $cache_dir unless -d $cache_dir;

    my $cache_url =
      __GetLocalUrl() . '/site/tmp/Cache/' . $$ro_param->Get('userhash') . '/' . $$ro_param->Get('analysis');
    return ($cache_dir, $cache_url);
}

sub __DBSanitize
{
    my $r_statement = shift;
    $$r_statement =~ s/;/_/g;
    $$r_statement =~ s/DROP/_/ig;
}

sub __GetStoreDir
{
    return $O_CONF->Get('datarootdir');
}

sub __GetUserDir
{
    my $ro_param = shift;

    my $userdir = __GetStoreDir() . '/' . $$ro_param->Get('userhash') . '/';

    if ($$ro_param->Get('userid') eq 'public')
    {
        $userdir = __GetStoreDir() . '/public/' . $$ro_param->Get('userhash') . '.';

    }

    return $userdir;
}

sub __AssertDBExists
{
    my $ro_param = shift;
    my $return   = &FALSE;
    $return = &TRUE if (-e __GetDBFile($ro_param));
    return $return;
}

sub __GetDBFile
{
    my $ro_param = shift;
    my $db_file  = __GetUserDir($ro_param) . $$ro_param->Get('analysis') . '/familyCompanion.db';

    #~ __Error($db_file,$ro_param);
    return $db_file;
}

sub __GetCurrentURI
{
    my $ro_param = shift;

    my $uri =
        __GetRootURI() . '/'
      . $$ro_param->Get('analysis') . '/'
      . $$ro_param->Get('method') . '/'
      . $$ro_param->Get('resource');

    my @a_params;
    foreach my $param ($$ro_param->GetKeys())
    {
        next if ($param =~ /^(__wb|user\w+|analysis|method|resource|debug|hyper_|totalCount|sql)/);
        push(@a_params, "$param=" . $$ro_param->Get($param));
    }
    $uri .= '?' . join('&', sort @a_params);
    return $uri;

}

sub __GetRootURI
{
    return __GetLocalUrl() . '/ws';
}

sub __Error
{
    my ($message, $ro_param) = @_;

    my $rh_data = {'success' => 'false', 'message' => $message, 'info' => {}, 'login' => $$ro_param->Get('login')};

    $rh_data->{'metadata'}->{'debug'} = $$ro_param->Get('debug') if (&DEBUG == &TRUE);
    $rh_data->{'metadata'}->{'sql'}   = $$ro_param->Get('sql')   if (&DEBUG == &TRUE);
    $rh_data->{'metadata'}->{'userid'}   = $$ro_param->Get('userid');
    $rh_data->{'metadata'}->{'analysis'} = $$ro_param->Get('analysis');
    $rh_data->{'metadata'}->{'method'}   = $$ro_param->Get('method');
    $rh_data->{'metadata'}->{'resource'} = $$ro_param->Get('resource');

    #API HYPERMEDIA
    $rh_data->{'metadata'}->{'links'} = [
                                         {
                                          'href' => __GetCurrentURI($ro_param),
                                          'rel'  => 'self',
                                          'desc' => $H_HYPERMEDIA_DESC{'self'}
                                         },
                                         {
                                          'href' => __GetRootURI() . '/api',
                                          'rel'  => 'api',
                                          'desc' => 'API description'
                                         }
                                         ];
    $$ro_param->SelectNameSpace('hyper_');
    foreach my $key ($$ro_param->GetKeys())
    {
        my $desc = $key;
        $desc = $H_HYPERMEDIA_DESC{$key} if defined $H_HYPERMEDIA_DESC{$key};
        push(@{$rh_data->{'metadata'}->{'links'}}, {'rel' => $key, 'href' => $$ro_param->Get($key), 'desc' => $desc});
    }
    $$ro_param->SelectNameSpace('');

    $DBH->disconnect if (defined $DBH);
    my $o_json = new JSON;
    $o_json->canonical();
    print $o_json->encode($rh_data);
    exit 0;
}

sub __Return
{
    my ($rh_data, $message, $ro_param) = @_;

    $rh_data->{'returns'}  = [keys %{$rh_data}];
    $rh_data->{'metadata'} = {};

    $rh_data->{'metadata'}->{'debug'}    = $$ro_param->Get('debug')    if (&DEBUG == &TRUE);
    $rh_data->{'metadata'}->{'sql'}      = $$ro_param->Get('sql')      if (&DEBUG == &TRUE);
    $rh_data->{'metadata'}->{'profiler'} = $$ro_param->Get('profiler') if (&DEBUG == &TRUE);

    $rh_data->{'success'} = 'true';
    $rh_data->{'totalCount'} = $$ro_param->Get('totalCount') if ($$ro_param->IsDefined('totalCount'));

    $rh_data->{'metadata'}->{'userid'}   = $$ro_param->Get('userid');
    $rh_data->{'metadata'}->{'analysis'} = $$ro_param->Get('analysis');
    $rh_data->{'metadata'}->{'method'}   = $$ro_param->Get('method');
    $rh_data->{'metadata'}->{'resource'} = $$ro_param->Get('resource');

    my $current_uri = __GetCurrentURI($ro_param);

    if ($$ro_param->IsDefined('limit'))
    {
        my $limit = $$ro_param->Get('limit');

        my $next_offset = 0;
        $next_offset = $$ro_param->Get('start') if ($$ro_param->IsDefined('start'));
        $next_offset += $limit;

        if ($next_offset < $$ro_param->Get('totalCount'))
        {
            $$ro_param->Set('start',      $next_offset);
            $$ro_param->Set('hyper_next', __GetCurrentURI($ro_param));
        }

        if ($next_offset >= ($limit * 2))
        {
            my $previous_offset = $next_offset - (2 * $limit);
            $$ro_param->Set('start',          $previous_offset);
            $$ro_param->Set('hyper_previous', __GetCurrentURI($ro_param));
        }

    }

    #API HYPERMEDIA
    $rh_data->{'metadata'}->{'links'} = [
                                         {
                                          'href' => $current_uri,
                                          'rel'  => 'self',
                                          'desc' => $H_HYPERMEDIA_DESC{'self'}
                                         },
                                         {
                                          'href' => __GetRootURI() . '/api',
                                          'rel'  => 'api',
                                          'desc' => 'API description'
                                         }
                                         ];

    if ($$ro_param->Get('resource') eq 'groups')
    {
        __AddMatrixHypermediaLinks($current_uri, 'groups', $rh_data) if ($rh_data->{'totalCount'} > 1);
    }

    $$ro_param->SelectNameSpace('hyper_');
    foreach my $key ($$ro_param->GetKeys())
    {
        my $desc = $key;
        $desc = $H_HYPERMEDIA_DESC{$key} if defined $H_HYPERMEDIA_DESC{$key};
        push(@{$rh_data->{'metadata'}->{'links'}}, {'rel' => $key, 'href' => $$ro_param->Get($key), 'desc' => $desc});
    }
    $$ro_param->SelectNameSpace('');

    $rh_data->{'message'} = $message;
    $DBH->disconnect if (defined $DBH);

    my $o_json = new JSON;
    $o_json->canonical();
    print $o_json->encode($rh_data);
    exit 0;
}

sub __AddMatrixHypermediaLinks
{
    my ($current_uri, $from_resource, $rh_data) = @_;
    my $matrix_uri = $current_uri;
    $matrix_uri =~ s,/$from_resource,/matrix,;

    #SC: on veut tout afficher ou juste ce qui est visible a l'ecran ?
    $matrix_uri =~ s,start=\d+,,;
    $matrix_uri =~ s,limit=\d+,,;
    my $abundancy_matrix_uri = $matrix_uri . '&type=abundancy';
    my $binary_matrix_uri    = $matrix_uri . '&type=binary';
    push(
         @{$rh_data->{'metadata'}->{'links'}},
         {
          'href' => $abundancy_matrix_uri,
          'rel'  => 'abundancy_matrix',
          'desc' => $H_HYPERMEDIA_DESC{'abundancy_matrix'}
         }
         );
    push(
         @{$rh_data->{'metadata'}->{'links'}},
         {'href' => $binary_matrix_uri, 'rel' => 'binary_matrix', 'desc' => $H_HYPERMEDIA_DESC{'binary_matrix'}}
         );

    return;
}

sub __WSBlast
{
    my $ro_param = shift;

    my $query       = $$ro_param->Get('blastquery');
    my $analysis    = $$ro_param->Get('analysis');
    my $blastmethod = $$ro_param->Get('blastmethod');
    my $owner       = $$ro_param->Get('owner');

    my @a_allowed_blastmethods = ('blastp', 'blastx');
    if (!grep (/$blastmethod/, @a_allowed_blastmethods))
    {
        __Error("$blastmethod is not a valid alignment method: " . join(',', @a_allowed_blastmethods), $ro_param);
    }

    my $evalue = $$ro_param->Get('blastevalue');

    my ($fh_query, $query_file) = tempfile(UNLINK => 1, DIR => $O_CONF->Get('tmpdir'));
    print $fh_query uri_unescape($query);
    $fh_query->close;

    my $blastdb = __GetUserDir($ro_param) . $analysis . '/blastdb/_ALL';

    my ($cache_dir, $cache_url) = __GetCacheDir($ro_param);
    my $filehash   = md5_hex($analysis . $query . $evalue . $blastmethod);
    my $cache_file = $cache_dir . '/' . $filehash . '.blast';

    my $cmd = "cat $cache_file";
    $$ro_param->Set('debug', 'From cache');
    if (!-e $cache_file)
    {
		my $blast_bindir = $O_CONF->Get('blast_bindir');
		my $cmd = $blast_bindir . '/' 
          .  $blastmethod
          . ' -query '
          . $query_file . ' -db '
          . $blastdb
          . ' -evalue '
          . $evalue
          . ' -outfmt "6 qseqid sseqid pident evalue bitscore" '
          . ' -out '
          . $cache_file;
        $$ro_param->Set('debug', $cmd);
        system $cmd;
    }

    my @a_results = `$cmd`;
    chomp @a_results;

    my %h_results         = ();
    my @a_display_results = ();
    foreach my $result_line (@a_results)
    {
        my ($qseqid, $sseqid, $pident, $evalue, $bitscore) = split(/\s+/, $result_line);
        next if defined($h_results{"$qseqid:$sseqid"});
        $h_results{"$qseqid:$sseqid"} = 1;

        push(
             @a_display_results,
             {
              'query'     => $qseqid,
              'accession' => $sseqid,
              'pident'    => $pident,
              'evalue'    => $evalue,
              'bitscore'  => $bitscore
             }
             );
    }

    $$ro_param->Set('totalCount', scalar @a_display_results);

    my $ra_list = \@a_display_results;

    #ICI FAIRE LA PAGINATION SUR LE TABLEAU
    if ($$ro_param->IsDefined('limit'))
    {
        $$ro_param->Set('start', 0) unless ($$ro_param->IsDefined('start'));
        my $offset    = $$ro_param->Get('start');
        my $limit     = $$ro_param->Get('limit');
        my $end_index = $offset + $limit - 1;
        $end_index = $#a_display_results if ($end_index > $#a_display_results);
        $ra_list = [@a_display_results[$offset .. $end_index]];
    }

    my %h_rels = (
                  'groups'    => ['accession', 'protein'],
                  'sequences' => ['accession', 'accession']
                  );

    my $ra_links = __AddTableHypermediaLinks($ra_list, \%h_rels, $ro_param);

    my @a_blast_fields = (
                          {
                           'name'        => 'query',
                           'pretty_name' => 'Query',
                           'desc'        => 'Query id',
                           'type'        => 'string'
                          },
                          {
                           'name'        => 'accession',
                           'pretty_name' => 'Accession',
                           'desc'        => 'Protein accession or locus_tag',
                           'type'        => 'string'
                          },
                          {
                           'name'        => 'pident',
                           'pretty_name' => '% Identity',
                           'desc'        => 'Percentage of identical matches',
                           'type'        => 'float'
                          },
                          {
                           'name'        => 'evalue',
                           'pretty_name' => 'E-value',
                           'desc'        => 'Expect value',
                           'type'        => 'float'
                          },
                          {
                           'name'        => 'bitscore',
                           'pretty_name' => 'Bit score',
                           'desc'        => 'Bit score',
                           'type'        => 'float'
                          }
                          );

    __Return({'table' => $ra_list, 'fields' => \@a_blast_fields, 'links' => $ra_links},
             "$blastmethod returns " . scalar @a_results . " hits", $ro_param);

}

sub __DBConnect
{
    my $ro_param = shift;
    $DBH = DBI->connect("dbi:SQLite:dbname=" . __GetDBFile($ro_param), "", "") or __Error(DBI::errstr, $ro_param);
    return;
}

sub __DBPrepareStatement
{
    my ($statement, $rh_attributes, $ro_param) = @_;
    my $sth = $DBH->prepare($statement, $rh_attributes) or __Error($DBH->errstr, $ro_param);
    return $sth;
}

sub WSAPI
{
    my $root_uri = __GetRootURI() . '/{{ANALYSIS}}';
    my %h_api = (
        'syntax'  => $root_uri . '/{{METHOD}}/{{RESOURCE}}?[KEY=VALUE PARAMS]',
        'methods' => {
            'search' => {
                         'desc'      => 'Search analysis data',
                         'resources' => [
                                         {
                                          'blast' => {
                                                  'http'   => 'POST',
                                                  'desc'   => 'Search protein similarities',
                                                  'params' => {
                                                      'access' => [{'ownner' => 'email', 'analysis' => '{{ANALYSIS}}'}],
                                                      'filter' => [
                                                                   {'blastevalue' => 'float'},
                                                                   {'blastmethod' => ['blastp', 'blastx']},
                                                                   {'blastquery'  => 'string'}
                                                        ],
                                                      'output' => []
                                                    },
                                                  'syntax'   => $root_uri . '/search/blast',
                                                  'examples' => []
                                            }
                                         }
                           ]
              },
            'status' => {
                         'resource' => [
                                        {
                                         'exists' => {
                                                      'http'   => 'GET',
                                                      'desc'   => 'Test query db existence',
                                                      'params' => {'access' => ['ownner']},
                                                      'syntax' => $root_uri . '/status/exists'
                                           }
                                        },
                                        {
                                         'remove' => {
                                                      'http'   => 'GET',
                                                      'desc'   => 'Remove query db',
                                                      'params' => {},
                                                      'syntax' => $root_uri . '/status/remove'
                                           }
                                        },
                                        {
                                         'build' => {
                                                     'http'   => 'GET',
                                                     'desc'   => 'Build query db',
                                                     'params' => {},
                                                     'syntax' => $root_uri . '/status/build'
                                           }
                                        },
                                        {
                                         'clear' => {
                                                     'http'   => 'GET',
                                                     'desc'   => 'Clear db cache files',
                                                     'params' => {},
                                                     'syntax' => $root_uri . '/status/clear'
                                           }
                                        },
                                        {
                                         'info' => {
                                                    'http'   => 'GET',
                                                    'desc'   => 'Display general statistics about db',
                                                    'params' => {'access' => ['ownner']},
                                                    'syntax' => $root_uri . '/status/info'
                                           }
                                        },
                           ],
                         'desc' => 'Admin tools'
              },
            'get' => {
                'desc'      => 'Extract analysis data',
                'resources' => [
                    {
                     'proteins' => {
                           'http'   => 'GET',
                           'desc'   => 'Get proteins information',
                           'params' => {
                                    'access' => [{'ownner' => 'email'}],
                                    'filter' =>
                                      [{'accession' => 'string'}, {'annotation' => 'string'}, {'proteome' => 'string'}],
                                    'output' => [
                                                 {'field' => ['accession', 'annotation']},
                                                 {'limit' => 'integer'},
                                                 {'start' => 'integer'}
                                      ]
                             },
                           'syntax'   => $root_uri . '/get/proteins?[KEY=VALUE PARAMS]',
                           'examples' => [
                               {
                                'desc'   => 'Get table of proteins with annotation word KEYWORD',
                                'syntax' => $root_uri . '/get/proteins?annotation={{KEYWORD}}'
                               },
                               {
                                'desc' => 'Get table of proteins with annotation word KEYWORD within proteome PROTEOME',
                                'syntax' => $root_uri . '/get/proteins?annotation={{KEYWORD}}&proteome={{PROTEOME}}'
                               },
                               {
                                'desc'   => 'Get list of protein accessions with annotation word KEYWORD',
                                'syntax' => $root_uri . '/get/proteins?annotation={{KEYWORD}}&field=accession'
                               },
                               {
                                'desc'   => 'Get table NUMBER first proteins with annotation word KEYWORD',
                                'syntax' => $root_uri . '/get/proteins?annotation={{KEYWORD}}&limit={{NUMBER}}'
                               }
                             ]
                       }
                    },
                    {
                     'proteomes' => {
                         'http'   => 'GET',
                         'desc'   => 'Get proteomes information',
                         'params' => {
                             'access' => [{'ownner' => 'email'}],
                             'filter' => [{'group'  => 'string'}],
                             'output' => [
                                 {
                                  'field' => ['code', 'description', 'organism', 'release', 'strain', 'sequence_number']
                                 },
                                 {'limit' => 'integer'},
                                 {'start' => 'integer'}
                               ]
                           },
                         'syntax'   => $root_uri . '/get/proteomes?[KEY=VALUE PARAMS]',
                         'examples' => [
                                        {
                                         'desc'   => 'Get table of proteomes presents in group GROUP1',
                                         'syntax' => $root_uri . '/get/proteomes?group={{GROUP1}}'
                                        },
                                        {
                                         'desc'   => 'Get table of proteomes used for analysis',
                                         'syntax' => $root_uri . '/get/proteomes'
                                        },
                                        {
                                         'desc'   => 'Get list of proteome codes used for analysis',
                                         'syntax' => $root_uri . '/get/proteomes?field=code'
                                        },
                                        {
                                         'desc'   => 'Get list of proteome codes presents in group GROUP1 and GROUP2',
                                         'syntax' => $root_uri
                                           . '/get/proteomes?field=code&group={{GROUP1}},{{GROUP2}}'
                                        },
                           ]
                       }
                    },
                    {
                     'matrix' => {
                             'http'   => 'GET',
                             'desc'   => 'Get abundancy/binary matrix from groups',
                             'params' => {
                                 'access' => [{'ownner' => 'email'}],
                                 'filter' =>
                                   [{'proteome' => 'string'}, {'outproteome' => 'string'}, {'outproteome' => 'string'}],
                                 'output' =>
                                   [{'limit' => 'integer'}, {'start' => 'integer'}, {'type' => ['binary', 'abundancy']}]
                               },
                             'syntax'   => $root_uri . '/get/matrix?[KEY=VALUE PARAMS]',
                             'examples' => []
                       }
                    },
                    {
                     'groups' => {
                                'http'   => 'GET',
                                'desc'   => 'Get homologuous gene groups',
                                'params' => {
                                    'access' => [{'ownner' => 'email'}],
                                    'filter' => [],
                                    'output' => [
                                                 {'field' => ['accession', 'annotation', 'proteomes', 'genes', 'taxa']},
                                                 {'limit' => 'integer'},
                                                 {'start' => 'integer'}
                                      ]
                                  },
                                'syntax'   => $root_uri . '/get/groups?[KEY=VALUE PARAMS]',
                                'examples' => []
                       }
                    },
                    {
                     'sequences' => {
                           'http'   => 'GET',
                           'desc'   => 'Get protein sequences ',
                           'params' => {
                               'access' => [{'ownner'    => 'email'}],
                               'filter' => [{'accession' => 'string'}, {'group' => 'string'}, {'proteome' => 'string'}],
                               'output' => [{'limit' => 'integer'}, {'start' => 'integer'}]
                             },
                           'syntax'   => $root_uri . '/get/sequences?[KEY=VALUE PARAMS]',
                           'examples' => []
                       }
                    },
                    {
                     'alignment' => {
                         'http'   => 'GET',
                         'desc'   => 'Get sequence alignment from groups',
                         'params' => {
                                      'access' => [{'ownner' => 'email'}],
                                      'filter' => [{'group'  => 'string'}, {'proteome' => 'string'}],
                                      'output' => [{'algo'   => ['globalpair', 'localpair', 'genafpair']}]
                           },
                         'syntax'   => $root_uri . '/get/alignment?[KEY=VALUE PARAMS]',
                         'examples' => [
                             {
                              'desc' => 'Get global alignment (Needleman-Wunsch) of proteins presents in group GROUP1',
                              'syntax' => $root_uri . '/get/alignment?group={{GROUP1}}'
                             },
                             {
                              'desc' =>
                                'Get local alignment (Smith-Waterman) with affine gap costs of proteins presents in group GROUP1',
                              'syntax' => $root_uri . '/get/alignment?group={{GROUP1}}&algo=localpair'
                             },
                             {
                              'desc' =>
                                'Get  local alignment with generalized affine gap costs (Altschul) of proteins presents in group GROUP1',
                              'syntax' => $root_uri . '/get/alignment?group={{GROUP1}}&algo=genafpair'
                             },
                             {
                              'desc' =>
                                'Get global alignment (Needleman-Wunsch) of PROTEOME1 proteins presents in group GROUP1',
                              'syntax' => $root_uri . '/get/alignment?group={{GROUP1}}&proteome={{PROTEOME1}}'
                             }
                           ]
                       }
                    }
                  ]
              }
          }
          );

    my $o_json = new JSON;
    $o_json->canonical();
    print $o_json->encode(\%h_api);
    exit 0;
}

1;

