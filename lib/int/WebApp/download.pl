sub Download
{

    my $path = shift;

    my $o_pp    = New ParamParser("$FindBin::RealBin/../site/cfg/site.cfg");
    my $tmp_dir = $o_pp->Get('portal_install_dir') . '/site/tmp/';

    if (!-d "$tmp_dir/$path")
    {
        print STDOUT "Content-Type:text/plain\n\n";
        print STDOUT "ERROR - ANALYSIS NOT FOUND";
        return;
    }
    else
    {
        $path = $tmp_dir . '/' . $path;
        my $basename = basename($path);
        my $dirname  = dirname($path);
        unless (-e "$dirname/$basename.zip")
        {
            my $cmd = "cd $dirname;zip -q -r $basename.zip $basename";

            my $ret = system("$cmd");

            if (($? == -1) or ($ret != 0))
            {
                print STDOUT "Content-Type:text/plain\n\n";
                print STDOUT "ERROR ZIP $cmd - $!";
                return;
            }

        }
        my $zipcontent = &Cat("$dirname/$basename.zip");
        print STDOUT <<EOF;
Content-Type:application/x-download
Content-Disposition:attachment;filename=$basename.zip

$zipcontent
EOF
    }
    return;

}

1;
