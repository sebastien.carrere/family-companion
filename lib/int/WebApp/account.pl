
sub AskAccount
{
    my $o_webbuilder = shift;
    my $o_webparam   = $o_webbuilder->GetObjectParam();
    print <<END;
<html>
	<head>
	<link href="../web/css/bootstrap.css" rel="stylesheet">
</head>
<body>
<h1 class="alert alert-success"><img src="../web/resources/images/fc.png" width="100">Account creation</h1>
END
    if ($o_webparam->Get('account_email') eq '')
    {
        &AskAccountForm($o_webparam);
    }
    else
    {
        &AskAccountSend($o_webparam);
    }

    print '<body></html>';
    return;

}

sub AskAccountForm
{
    my ($o_webparam) = @_;

	if ($O_CONF->Get('mode') eq 'local')
	{
		 print <<END;
<div class="container-fluid">
<div class="alert alert-danger">Account creation is not available for local install</div>
<div class="alert alert-success">Use guest/guest credentials to log into family-companion</div>
</div>
END
		return;
	}
    my $captcha     = __GenerateKey();
    my $md5_captcha = md5_hex($captcha);

    my $url = __GetLocalUrl() . '/cgi/index.cgi';
    print <<END;
<div class="container-fluid">
<form method="POST" action="$url">
  <div class="form-group">
    <label for="account_email">Email address</label>
    <input type="email" class="form-control" id="account_email"  name="account_email" placeholder="Email" required>
  </div>
  <div class="form-group">
    <label for="account_fullname">Full name</label>
    <input type="text" class="form-control" id="account_fullname" name="account_fullname"  placeholder="Your Name" required>
  </div>
  <div class="form-group">
    <label for="account_institution">Institution</label>
    <input type="text" class="form-control" id="account_institution" name="account_institution"  placeholder="Your Employer" required>
  </div>
  <div class="form-group alert alert-warning">
	<label for="captcha">Validation key: <i>$captcha</i></label>
	<input type="text" class="form-control" required name="captcha" id="captcha" placeholder="report key"/>
	<input type="hidden" name="captcha_valid" value="$md5_captcha"/>
  </div>
  <div class="form-group">
    <label for="account_message">Message</label>
    <textarea class="form-control" name="account_message" id="account_message" rows="10" cols="30" placeholder="Supplementary information for Family-Companion admin"></textarea>
  </div>
  <input type="hidden" name="account" value="1"/>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
<div>
END
    return;
}

sub AskAccountSend
{
    my ($o_webparam) = @_;

    my $admin_mail          = $O_CONF->Get('admin_mail');
    my $account_email       = $o_webparam->Get('account_email');
    my $account_fullname    = $o_webparam->Get('account_fullname');
    my $account_institution = $o_webparam->Get('account_institution');
    my $account_message     = $o_webparam->Get('account_message');
    my $captcha             = $o_webparam->Get('captcha');
    my $captcha_valid       = $o_webparam->Get('captcha_valid');

    my $captcha_md5 = md5_hex($captcha);

    if ($captcha_md5 ne $captcha_valid)
    {
        print "<div class='alert alert-danger'>INVALID KEY</div>";
        &AskAccountForm($o_webparam);
    }
    else
    {
        my $url = __GetPublicUrl();
        $url .=
            '/cgi/index.cgi?create_account=1&account_email='
          . $account_email
          . '&account_fullname='
          . uri_escape($account_fullname)
          . '&account_institution='
          . uri_escape($account_institution);
        my $create_message =
          'To valid this user, click here after being authneticated with your admin account: ' . "\n" . $url;

        $create_message = 'Family-companion: ' . __GetPublicUrl() if ($O_CONF->Get('authentication_mode') ne 'file');

        if (!IsEmailValid($admin_mail))
        {
            print "<div class='alert alert-danger'>CAN NOT SEND ACCOUNT QUERY</div>";
            &AskAccountForm($o_webparam);
        }
        else
        {
            my $body = <<END;
Email: $account_email
FullName: $account_fullname
Institution: $account_institution
Message: $account_message

$create_message

END

            &SendMail($admin_mail, '[Family-Companion] New Account request', $body);
            print "<div class='alert alert-info'>Account query sent - Must be validated</div>";
        }
    }
}

sub CreateAccount
{
    my $o_webbuilder = shift;
    my $o_webparam   = $o_webbuilder->GetObjectParam();
    my $o_authentic  = $o_webbuilder->GetObjectAuth;
    my $userid       = &__GetUserId($o_authentic);

    #VALIDER USER (admin_email = userid)
    my @a_admin_email = split(/,/, lc($O_CONF->Get('admin_mail')));
    my $login_url = __GetRootURI() . '/login';
    &Die('<a href="' . $login_url . '"><button>Your connexion is lost - Please login again</button></a>')  if ($userid eq '');
    &Die("ERROR - $userid is not part of this instance administrators - You do not have access to this page") if (!grep (/^$userid$/i, @a_admin_email));

    my $admin_mail          = $O_CONF->Get('admin_mail');
    my $account_email       = $o_webparam->Get('account_email');
    my $account_fullname    = $o_webparam->Get('account_fullname');
    my $account_institution = $o_webparam->Get('account_institution');

    my $password = __GenerateKey();

    my $install_dir = $FindBin::RealBin . '/..';
    my $cmd         = $install_dir . '/bin/int/admin_addUser.pl --sendmail';
    $cmd .= ' --email="' . $account_email . '" ';
    $cmd .= ' --password="' . $password . '" ';
    $cmd .= ' --common_name="' . $account_fullname . ',' . $account_institution . '" ';
    $cmd .= ' --outdir=' . $install_dir . '/etc';

    if (!-d $install_dir . '/etc')
    {
        &Die("ERROR - $install_dir/etc  does not exist");
    }
    else
    {
        my ($fh_err, $err) = File::Temp::tempfile(UNLINK => 1, DIR => $O_CONF->Get('tmpdir'));
        my $return = system("($cmd > $err) 2> $err");
        if ($return ne 0)
        {
            print "<div class='alert alert-danger'> " . `cat $err` . "</div>";
        }
        else
        {
            print "<div class='alert alert-info'>Account created</div>";
        }

    }
    return;
}

1;
