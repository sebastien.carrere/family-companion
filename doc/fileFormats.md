<!DOCTYPE html>
<html>

<h1 id="familycompanion-file-formats">Family-Companion File Formats</h1>

<p><p><div class="toc">
<ul>
<li><a href="#orthologous-group-file">ORTHOLOGOUS GROUP FILE</a></p>

<pre><code>&lt;ul&gt;
    &lt;li&gt;&lt;a href=&quot;#orthomcl-v1&quot;&gt;OrthoMCL v1&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href=&quot;#orthomcl-v2&quot;&gt;OrthoMCL v2&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href=&quot;#orthofinder&quot;&gt;OrthoFinder&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href=&quot;#synergy&quot;&gt;Synergy&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
</code></pre>

<p></li>
<li><a href="#proteomes">PROTEOMES</a></li>
<li><a href="#interpro-annotation">INTERPRO ANNOTATION</a></li>
<li><a href="#archive-file">ARCHIVE FILE</a></li>
</ul></p>

:warning: <b>To prevent any strange behaviour (depending of the tool you use to compute homology groups), please remove any non alpha-numeric character (especially ":" or "|") from your fasta sequence IDs when it is possible</b>

<h1 id="orthologous-group-file">ORTHOLOGOUS GROUP FILE</h1>

<h2 id="orthomcl-v1">OrthoMCL v1</h2>

<ul>
<li>Tool: <a href="http://orthomcl.org/common/downloads/software/unsupported/v1.4/">http://orthomcl.org/common/downloads/software/unsupported/v1.4/</a></li>
<li>Default file name: all_orthomcl.out</li>
<li>File format:</li>
</ul>

<pre><code>ORTHMCL$GROUPIDa (taxa: $NUMBEROFTAXAINGROUPa, genes: $NUMBEROFGENESINGROUPa) $SEQIDa($TAXONa) $SEQIDb($TAXONa) $SEQIDc($TAXONb)
ORTHMCL$GROUPIDb (taxa: $NUMBEROFTAXAINGROUPb, genes: $NUMBEROFGENESINGROUPb) $SEQIDd($TAXONa) $SEQIDe($TAXONc)
</code></pre>

<h2 id="orthomcl-v-2">OrthoMCL v 2.*</h2>

<ul>
<li>Tool: <a href="http://orthomcl.org/common/downloads/software/v2.0/">http://orthomcl.org/common/downloads/software/v2.0/</a></li>
<li>Default file name: groups.txt</li>
<li>File format:</li>
</ul>

<pre><code>$YOURPREFIX$GROUPIDa: $TAXONa|$SEQIDa $TAXONa|$SEQIDb $TAXONb|$SEQIDc
$YOURPREFIX$GROUPIDb: $TAXONa|$SEQIDc $TAXONc|$SEQIDd
</code></pre>


<strong> :warning: USE RAW FASTA FILES NOT THE ONE MODIFIED TO FIT ORTHOMCL SPECS (the ones before you ran "run orthomclAdjustFasta (or your own simple script) to generate protein fasta files in the required format.")</strong>
<h2 id="orthofinder">OrthoFinder</h2>

<ul>
<li>Tool: <a href="https://github.com/davidemms/OrthoFinder">https://github.com/davidemms/OrthoFinder</a></li>
<li>Default file name: OrthologousGroups.txt</li>
<li>File Format:</li>
</ul>

<pre><code>OG$GROUPIDa: $SEQIDa $SEQIDb $SEQIDc
OG$GROUPIDb: $SEQIDd $SEQIDe
</code></pre>

:warning: <b>If your fasta SEQIDs contain ":" characters, OrthoFinder will automatically substitute them with "_"</b> !

<h2 id="synergy">Synergy</h2>

<ul>
<li>Tool:<a href="https://dx.doi.org/10.1093/bioinformatics/btm193">https://dx.doi.org/10.1093/bioinformatics/btm193</a></li>
<li>Default file name: final_clusters.txt</li>
<li>File format:</li>
</ul>

<pre><code> Cluster$GROUPIDa (taxa: $NUMBEROFTAXAINGROUPa, genes: $NUMBEROFGENESINGROUPa) $SEQIDa $SEQIDb $SEQIDc
    Cluster$GROUPIDb (taxa: $NUMBEROFTAXAINGROUPb, genes: $NUMBEROFGENESINGROUPb) $SEQIDd $SEQIDe
</code></pre>

<h1 id="proteomes">PROTEOMES</h1>

<p>Proteomes must be uploaded in Fasta format:</p>

<pre><code>&gt;$SEQIDa some product definition
MRTEIESLWVFALASKFNIYMQQHFASLLVAIAITWFTITIVFWSTPGGPAWGKYFFTRRFISLDYNRKY
KNLIPGPRGFPLVGSMSLRSSHVAHQRIASVAEMSNAKRLMAFSLGDTKVVVTCHPAVAKEILNSSVFAD
RPVDETAYGLMFNRAMGFAPNGTYWRTLRRLGSNHLFNPKQIKQSEDQRRVIATQMVNAFARNPKSACAV
RDLLKTASLCNMMGLVFGREYELESNNNLESECLKGLVEEGYDLLGTLNWTDHLPWLAGLDFQQIRFRCS
QLVPKVNLLLSRIIHEQRAATGNFLDMLLSLQGSEKLSESDMVAVLWEMIFRGTDTVAVLVEWVLARIVM
HPKVQLTVHDELDRVVGRSRTVDESDLPSLTYLTAMIKEVLRLHPPGPLLSWARLSITDTSVDGYHVPAG
TTAMVNMWAIARDPHVWEDPLEFKPERFVAKEGEAEFSVFGSDLRLAPFGSGKRVCPGKNLGLTTVSFWV
ATLLHEFEWLPSVEANPPDLSEVLRLSCEMACPLIVNVSSRRKIM
&gt;$SEQIDb other product definition
MILVLASLFAVLILNVLLWRWLKASACKAQRLPPGPPRLPILGNLLQLGPLPHRDLASLCDKYGPLVYLR
LGNVDAITTNDPDTIREILLRQDDVFSSRPKTLAAVHLAYGCGDVALAPMGPHWKRMRRICMEHLLTTKR
LESFTTQRAEEARYLIRDVFKRSETGKPINLKEVLGAFSMNNVTRMLLGKQFFGPGSLVSPKEAQEFLHI
THKLFWLLGVIYLGDYLPFWRWVDPSGCEKEMRDVEKRVDEFHTKIIDEHRRAKLEDEDKNGDMDFVDVL
LSLPGENGKAHMEDVEIKALIQDMIAAATDTSAVTNEWAMAEAIKQPRVMRKIQEELDNVVGSNRMVDES
DLVHLNYLRCVVRETFRMHPAGPFLIPHESVRATTINGYYIPAKTRVFINTHGLGRNTKIWDDVEDFRPE
RHWPVEGSGRVEISHGPDFKILPFSAGKRKCPGAPLGVTMVLMALARLFHCFEWSSPGNIDTVEVYGMTM
PKAKPLRAIAKPRLAAHLYT
</code></pre>

<p>Sequence IDs must be unique in a proteome.</p>

<h1 id="interpro-annotation">INTERPRO ANNOTATION</h1>

<p>Use Tab-separated values format (TSV)</p>

<p>Documentation can be found <a href="https://github.com/ebi-pf-team/interproscan/wiki/InterProScan5OutputFormats#tab-separated-values-format-tsv">here</a></p>

<p>First column must match proteome Fasta IDs.</p>

<pre><code>$SEQIDa  14086411a2cdf1c4cba63020e1622579    3418    Pfam    PF09103 BRCA2, oligonucleotide/oligosaccharide-binding, domain 1    2670    2799    7.9E-43 T   15-03-2013
$SEQIDa  14086411a2cdf1c4cba63020e1622579    3418    ProSiteProfiles PS50138 BRCA2 repeat profile.   1002    1036    0.0 T   18-03-2013  IPR002093   BRCA2 repeat    GO:0005515|GO:0006302
$SEQIDa  14086411a2cdf1c4cba63020e1622579    3418    Gene3D  G3DSA:2.40.50.140       2966    3051    3.1E-52 T   15-03-2013
</code></pre>

<h1 id="archive-file">ARCHIVE FILE</h1>

<p>In order to speed up input settings, you can directly upload a complete ZIP file whit the following content :</p>

<pre><code>|--title.txt
|--description.txt
|--reference.txt
|--homologygroups.out
|--homologygroups.txt
|--$TAXONa.txt
|--$TAXONa.fa
|--$TAXONa.iprscan
|--$TAXONb.fa
|--$TAXONb.fa
`--$TAXONb.iprscan
</code></pre>

<p>:information_source:  Filenames and extensions are controlled, so use the same to avoid errors</p>

<p>Here is a short description of the content  of each file:</p>

<table>
<thead>
<tr>
<th>File</th>
<th>Description</th>
<th>Mandatory ?</th>
</tr>
</thead>

<tbody>
<tr>
<td>title.txt</td>
<td>Analysis title</td>
<td>No</td>
</tr>

<tr>
<td>description.txt</td>
<td>Analysis complete dscription</td>
<td>No</td>
</tr>

<tr>
<td>homologygroups.out</td>
<td>OrthoMCL/Synergy/OrthoFinder matrix file</td>
<td>No</td>
</tr>

<tr>
<td>homologygroups.txt</td>
<td>OrthoMCL/Synergy/OrthoFinder parameters</td>
<td>No</td>
</tr>

<tr>
<td>reference.txt</td>
<td>List of taxa used as reference for pan proteome extraction</td>
<td>No</td>
</tr>

<tr>
<td>$TAXONa.txt</td>
<td>$TAXONa long name</td>
<td>No</td>
</tr>

<tr>
<td>$TAXONa.fa(s)</td>
<td>$TAXONa proteome fasta file</td>
<td>Yes</td>
</tr>

<tr>
<td>$TAXONa.iprscan</td>
<td>$TAXONa proteome InterProScan output file</td>
<td>No</td>
</tr>

<tr>
<td>$TAXONb.txt</td>
<td>$TAXONb long name</td>
<td>No</td>
</tr>

<tr>
<td>$TAXONb.fa(s)</td>
<td>$TAXONb proteome fasta file</td>
<td>Yes</td>
</tr>

<tr>
<td>$TAXONb.iprscan</td>
<td>$TAXONb proteome InterProScan output file</td>
<td>No</td>
</tr>
</tbody>
</table>

</body>
</html>
