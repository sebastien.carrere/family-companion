#!/usr/bin/perl

use strict;
use FindBin;

use lib "$FindBin::RealBin/../../lib/ext/lipmutils";
use lib "$FindBin::RealBin/../../lib/int";

use ParamParser;
use WebBuilder;

use General;
use GeneralBioinfo;
use Apache::Htpasswd;
use Authentic;
use Authentic::EtcPasswd;
use Digest::MD5 qw/md5_hex/;

use constant DESC_FILE    => "$FindBin::RealBin/../../web/xml/portal.xml";
use constant OFFLINE_MODE => &FALSE;

require 'common.pl';

our $O_CONF;

MAIN:
{

    #
    # First, we configure $o_param
    #
    my $o_param = New ParamParser('INIT');
    $o_param->SetAuthorizedCharacters('[;&\'>/0-9a-zA-Z:_.@+\#\-\s=,"\?\(\)%~*!\[\]\{\}\\\]');
    $o_param->Update('CGIPM', 'O');

    $o_param->SetUnlessDefined('__wb_function', 'ProxyHome');

    &Main($o_param);

}

sub Main
{
    my ($o_webparam) = @_;

    $O_CONF = New ParamParser('INIT');
    $O_CONF->SetBehaviour('use_substitution_table');

    $O_CONF->Update("$FindBin::RealBin/../../site/cfg/site.cfg", 'O');

    $O_CONF->SetSubstitution('%S', \$O_CONF->Get('portal_web_server'));
    $O_CONF->SetSubstitution('%I', \$O_CONF->Get('portal_install_dir'));
    $O_CONF->SetSubstitution('%H', \$O_CONF->Get('portal_http_root'));

    $O_CONF->Update("$FindBin::RealBin/../../site/cfg/site.cfg",           'O');
    $O_CONF->Update("$FindBin::RealBin/../../site/cfg/authentication.cfg", 'O');
	
    if (-e "$FindBin::RealBin/../../data/cfg/local.cfg")
    {
        $O_CONF->Update("$FindBin::RealBin/../../data/cfg/local.cfg", 'O');
    }

    if (-e "$FindBin::RealBin/../../data/cfg/env.cfg")
    {
        $O_CONF->Update("$FindBin::RealBin/../../data/cfg/env.cfg", 'O');
    }


    my $o_authentic = &ProxyLogin();



    my $install_dir = $O_CONF->Get('portal_install_dir');
    my $http_root   = $O_CONF->Get('portal_http_root');

    my $o_webbuilder =
      New WebBuilder($o_webparam, $o_authentic, &DESC_FILE, '%I' => \$install_dir, '%H' => \$http_root);
    $o_webbuilder->SetBehaviour('encoded_url');
    $o_webbuilder->Realize();

}

sub ProxyLogin
{
    my $o_authentic;
    if (defined $ENV{$O_CONF->Get('remote_user_env_name')})
    {
        my $login         = $ENV{$O_CONF->Get('remote_user_env_name')};
        my $htpasswd_file = $O_CONF->Get('authentic_htpasswd_file');

        if (!-e $htpasswd_file)
        {
            &Die('Cannot get Htpasswd file', 'early');
        }

        my $o_htpasswsd        = new Apache::Htpasswd($htpasswd_file);
        my $encrypted_password = $o_htpasswsd->fetchPass($login);
        $o_authentic = New Authentic::EtcPasswd(
                                                file                => $O_CONF->Get('authentic_file'),
                                                -login              => $login,
                                                -password           => $encrypted_password,
                                                -encrypted_password => &TRUE,
                                                -cookie_name        => md5_hex($O_CONF->Get('authentic_cookie_name')),
                                                -cookie_expires     => '+1d',
                                                -cookie_secure      => $O_CONF->Get('authentic_cookie_secure')
                                                );

    }
    else
    {
        &Die("Cannot get User Environment variable", 'early');
    }
    return $o_authentic;

}

sub ProxyHome
{
    my $url = $O_CONF->Get('portal_http_root');
    print "<div class='alert alert-success'>You are logged in as " . $ENV{$O_CONF->Get('remote_user_env_name')};
    print "<a href='$url' target='_parent'><button class='button'>Access to your analyses</button></a>";
    print '</div>';

}
