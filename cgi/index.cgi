#!/usr/bin/perl

use strict;

use FindBin;

use lib "$FindBin::RealBin/../lib/ext/lipmutils";
use lib "$FindBin::RealBin/../lib/int";

use ParamParser;
use WebBuilder;

use General;
use GeneralBioinfo;
use Runner;
use URI::Escape;

use File::Temp qw/ tempfile tempdir /;
use CGI;
use Data::Dumper;
use File::Basename;
use File::Copy;
use Digest::MD5 qw(md5_hex);
use JSON;

use DBI;
use Array::Utils;
use URI::Encode;

use Authentic;
use Authentic::ShibLdap;
use Authentic::EtcPasswd;
use Authentic::Ldap;
use Net::LDAP;

use Proc::Killfam;


use Apache::Htpasswd;

my $RUNNER_STDOUT = '';
my $RUNNER_STDERR = '';
my $RUNNER_LOG    = '';

our $TITLE = 'family-companion';
our $ERROR_MESSAGE;
our $DEMO_URL;
our $SOURCECODE_URL;

require 'WebApp.pl';
require 'common.pl';

use constant DEBUG          => &TRUE;
use constant OFFLINE_MODE   => &FALSE;
use constant GENERATE_NAMES => &TRUE;
use constant COMPRESS       => &FALSE;
use constant NOCACHE       => &TRUE;

use constant DESC_FILE => "$FindBin::RealBin/../web/xml/portal.xml";
our $O_CONF;
our $TMPDIR = '/tmp';

MAIN:
{

    #
    # First, we configure $o_param
    #
    my $o_param = New ParamParser('INIT');
    $o_param->SetAuthorizedCharacters('[|;&\'>/0-9a-zA-Z:_.@+\#\-\s=,"\?\(\)%~*!\[\]\{\}\\\]');
    $o_param->Update('CGIPM', 'O');

	$o_param->SetUnlessDefined('__wb_function', 'ERROR'); 

    $o_param->Set('__wb_function', 'Login')             if ($o_param->IsDefined('login'));
    $o_param->Set('__wb_function', 'WSAPI')             if ($o_param->IsDefined('api'));
    $o_param->Set('__wb_function', 'WebService')        if ($o_param->IsDefined('blastmethod'));
    $o_param->Set('__wb_function', 'GetAuthentication') if ($o_param->IsDefined('authentication'));
	$o_param->Set('__wb_function', 'AskAccount')             if ($o_param->IsDefined('account'));
    $o_param->Set('__wb_function', 'ListAnalyses') if ($o_param->IsDefined('view'));
    $o_param->Set('__wb_function', 'Publish')      if ($o_param->IsDefined('publish'));
    $o_param->Set('__wb_function', 'Delete')       if ($o_param->IsDefined('delete'));
    $o_param->Set('__wb_function', 'AskAccount')             if ($o_param->IsDefined('account'));
    $o_param->Set('__wb_function', 'CreateAccount')             if ($o_param->IsDefined('create_account'));
    
    $o_param->SetUnlessDefined('__wba_ajax', 1);

    #$Error::Debug = 1;

    if ($o_param->IsDefined('download'))
    {
        #On on un header HTML particulier dans ce cas
        &Download($o_param->Get('download'));
    }
    else
    {
        &Main($o_param);
    }
}

sub ERROR
{
	 my ($o_webbuilder) = @_;
	 my $o_webparam = $o_webbuilder->GetObjectParam();
	__Error(\%ENV,\$o_webparam);	
}

sub Main
{
    my ($o_webparam) = @_;

    $o_webparam->Delete('userid');

	&AdjustConfig();
	$TMPDIR   = $O_CONF->Get('tmpdir');
    $DEMO_URL = $O_CONF->Get('demo_url');
    $SOURCECODE_URL = $O_CONF->Get('sourcecode_url');

    
    my $appli_url =
      ($o_webparam->IsDefined('__wb_redirecturl')) ? $o_webparam->Get('__wb_redirecturl') : __GetPublicUrl();

    my $o_authentic = &__Authentication($o_webparam, $appli_url);

    if (!defined $ENV{$O_CONF->Get('remote_user_env_name')} && $o_authentic->IsAuthenticated())
    {
        $ENV{$O_CONF->Get('remote_user_env_name')} = __GetUserId($o_authentic);
    }

    # Logout request
    if ($o_webparam->IsDefined('logout') && $o_webparam->Get('logout') == &TRUE)
    {
        $o_authentic->Logout();
    }

    my $install_dir = $O_CONF->Get('portal_install_dir');
    my $http_root   = $O_CONF->Get('portal_http_root');

    my $o_webbuilder =
      New WebBuilder($o_webparam, $o_authentic, &DESC_FILE, '%I' => \$install_dir, '%H' => \$http_root);
    $o_webbuilder->SetBehaviour('encoded_url');
    $o_webbuilder->Realize();
}

