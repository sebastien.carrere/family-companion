#!/bin/bash

git pull

docker version

if [ $? -eq 0 ]; then
	docker build --rm=true -t bbric/family-companion .
else
	echo "FAIL: docker seems not to be correctly installed - Please check the PREREQUISITES section"
fi


