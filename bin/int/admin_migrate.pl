#!/usr/bin/perl

=head1 NAME

    admin_migrate.pl - Move data from a user to another one

=head1 DESCRIPTION

    This program will move workspace data from a user id to another one.
    Helpfull when someone has a change in its email.
 
   

=head1 SYNOPSIS

    admin_migrate.pl --former_email <Current@email.address> --new_email <New@email.address> 

     Options:
       -verbose			

=head1 OPTIONS

=over 8

=item B<--former_email> : Old/Former email address

=item B<--new_email> : New email address

=back

=cut

use strict;
use warnings;
use Data::Dumper;
use FindBin;
use lib "$FindBin::RealBin/../../lib/int";
use lib "$FindBin::RealBin/../../lib/ext/lipmutils";


use Pod::Usage;
use Cwd 'abs_path';
use File::Basename;

use Digest::MD5 qw(md5_hex);
use ParamParser;
use General;

require 'common.pl';

our $O_CONF;

MAIN:
{

    &__Usage if (@ARGV == 0);

    my $user = $ENV{'USER'};
    if (!defined $ENV{'USER'} && defined $ENV{'SERVER_SOFTWARE'})
    {
        $user = 'www-data';
    }

    if ($user ne 'www-data')
    {
        die("ERROR - Must be run as  www-data user\n");
    }

    $O_CONF = New ParamParser('INIT');
    $O_CONF->SetBehaviour('use_substitution_table');
    $O_CONF->Update("$FindBin::RealBin/../../site/cfg/site.cfg", 'O');
    $O_CONF->SetSubstitution('%S', \$O_CONF->Get('portal_web_server'));
    $O_CONF->SetSubstitution('%I', \$O_CONF->Get('portal_install_dir'));
    $O_CONF->SetSubstitution('%H', \$O_CONF->Get('portal_http_root'));
    $O_CONF->Update("$FindBin::RealBin/../../site/cfg/site.cfg",           'O');
    $O_CONF->Update("$FindBin::RealBin/../../site/cfg/authentication.cfg", 'O');

    my $local_cfg = undef;
    $local_cfg = "$FindBin::RealBin/../../data/cfg/local.cfg" if (-e "$FindBin::RealBin/../../data/cfg/local.cfg");
    $O_CONF->Update($local_cfg, 'O') if defined "$local_cfg";
    $local_cfg = $ENV{'FC_DIR'} . '/data/cfg/local.cfg' if (defined $ENV{'FC_DIR'} && -e $ENV{'FC_DIR'} . '/data/cfg/local.cfg');
    $O_CONF->Update($local_cfg, 'O') if defined "$local_cfg";
    $local_cfg = $ENV{'FC_DIR'} . '/data/cfg/env.cfg'  if (defined $ENV{'FC_DIR'} && -e $ENV{'FC_DIR'} . '/data/cfg/env.cfg');
    $O_CONF->Update($local_cfg, 'O') if defined "$local_cfg";

	&Sanitize();

    my $o_param = New ParamParser('GETOPTLONG', \&__Usage, 'former_email=s', 'new_email=s', 'verbose');

    $o_param->AssertDefined('former_email');
    $o_param->AssertDefined('new_email');

    my $date = `date +%Y%m%d`;
    chomp $date;

    #COPY FORMER RAW DATA DIRECTORY TO NEW ONE
    my $former_email = $o_param->Get('former_email');
    die("[ERROR] - former_email >$former_email< does not have a canonical syntax")
      unless (IsEmailValid($former_email) == &TRUE);

    my $new_email = $o_param->Get('new_email');
    die("[ERROR] - new_email >$new_email< does not have a canonical syntax") unless (IsEmailValid($new_email) == &TRUE);

    my $former_hash = md5_hex(lc($former_email));
    my $new_hash    = md5_hex(lc($new_email));

    my $datarootdir = abs_path($O_CONF->Get('datarootdir'));

    my $former_rawdata_directory = $datarootdir . '/' . $former_hash;
    my $new_rawdata_directory    = $datarootdir . '/' . $new_hash;
    my $contact_file             = "$new_rawdata_directory/contact.txt";

    die("[ERROR] - Need read permission: $former_rawdata_directory") if (!-r $former_rawdata_directory);

    #! rename ou mv si newrep existe

    print STDOUT "[INFO] - Move workspace $former_rawdata_directory -> $new_rawdata_directory\n";
    if (-d $new_rawdata_directory)
    {
        &System("mv $former_rawdata_directory/* $new_rawdata_directory/; rm -rf $former_rawdata_directory");
    }
    else
    {
        &System("mv $former_rawdata_directory $new_rawdata_directory");

        # ecraser $contact_file avec nouveau mail
        print STDOUT "[INFO] - Edit contact file\n";
        my $fh_contact = GetStreamOut($contact_file);
        print $fh_contact lc($new_email);
        $fh_contact->close;
    }

    &System("cd $datarootdir; ln -s $new_hash $former_hash");

    if ($O_CONF->Get('authentication_mode') eq 'file')
    {
        print STDOUT "[INFO] - Edit authentication files because authentication_mode=file\n";
        my @a_authentication_files = ($O_CONF->Get('authentic_file'), $O_CONF->Get('authentic_htpasswd_file'));
        foreach my $authentication_file (@a_authentication_files)
        {
            my $backup = $authentication_file . '.bkp.' . $date;
            if (!-e $backup)
            {
                &System("mv $authentication_file $backup");
            }

            my $fh_in  = &GetStreamIn($backup);
            my $fh_out = &GetStreamOut($authentication_file);
            while (my $line = <$fh_in>)
            {
                chomp $line;
                if ($line =~ /$former_email/io)
                {
                    $line =~ s/$former_email/$new_email/iog;
                }
                print $fh_out "$line\n";
            }
            $fh_in->close();
            $fh_out->close();
        }

    }

    my $remote_user_env_name = $O_CONF->Get('remote_user_env_name');

    print STDOUT "[INFO] - Edit .htaccess files to set $remote_user_env_name\n";
    my @a_htaccess_files = `ls $new_rawdata_directory/*/*/.htaccess`;
    chomp @a_htaccess_files;
    foreach my $htaccess_file (@a_htaccess_files)
    {
        my $portal_name = basename(dirname($htaccess_file));
        if (defined $portal_name)
        {
            my $fh_out = &GetStreamOut($htaccess_file);
            print $fh_out <<END;
<If "%{ENV:$remote_user_env_name} !~ /^$new_email\$/i">
AuthType Basic
AuthName "Password Required"
AuthBasicProvider file
AuthUserFile "$new_rawdata_directory/.share/$portal_name.password"
Require valid-user
</If>
END
        }
    }

    # EDITER LES JSON DES ANALYSES ?

    exit 0;
}

sub __Usage
{
    pod2usage(-exitval => "NOEXIT", -verbose => 1);
}

__END__
