#!/usr/bin/perl

use strict;

use FindBin;
use lib $FindBin::RealBin . '/../../lib/ext/lipmutils';
use lib $FindBin::RealBin . '/../../lib/int/';

use General;
use ParamParser;
use GeneralBioinfo;
use Cwd;
use JSON;
use File::Basename;
use File::Temp qw/ tempfile tempdir /;
use Data::Dumper;


require 'common.pl';

my $DBNAME  = 'familyCompanion.db';
our $VERBOSE = &FALSE;


our $O_CONF;

my $BLASTBIN_DIR;

MAIN:
{
    $O_CONF = New ParamParser('INIT');
    $O_CONF->SetBehaviour('use_substitution_table');
    $O_CONF->SetSubstitution('%S', \$O_CONF->Get('portal_web_server'));
    $O_CONF->Update("$FindBin::RealBin/../../site/cfg/site.cfg", 'O');

	if (-e "$FindBin::RealBin/../../data/cfg/local.cfg")
    {
        $O_CONF->Update("$FindBin::RealBin/../../data/cfg/local.cfg", 'O');
    }

    if (-e "$FindBin::RealBin/../../data/cfg/env.cfg")
    {
        $O_CONF->Update("$FindBin::RealBin/../../data/cfg/env.cfg", 'O');
    }

    $BLASTBIN_DIR = $O_CONF->Get('blast_bindir');

    my $o_param = New ParamParser('GETOPTLONG', \&Usage, 'verbose', 'dir=s','lockfile=s');

    $VERBOSE = &TRUE if ($o_param->IsDefined('verbose'));
    $o_param->AssertDirExists('dir');
    $o_param->AssertFileExists('lockfile');

    my $outdir = Cwd::abs_path($o_param->Get('dir'));
    my $lockfile = Cwd::abs_path($o_param->Get('lockfile'));

    #Les paths dans results.json sont en relatif
    chdir $outdir;

    $o_param->Set('homologygroups_out', "$outdir/homologygroups_output/group_annotations.xls");
    $o_param->Set('analysis_cfg', "$outdir/results.json");

    $o_param->AssertFileExists('homologygroups_out', 'analysis_cfg');

    my $analysis_cfg = $o_param->Get('analysis_cfg');
    my $homologygroups_out = $o_param->Get('homologygroups_out');

    my $rh_analysis       = from_json(`cat $analysis_cfg`);
    my $contact           = $rh_analysis->{'analysis'}->{'contact'};
    my $title             = $rh_analysis->{'analysis'}->{'title'};
    my $rh_proteome_files = $rh_analysis->{'analysis'}->{'proteomes'};

    &CreateSchemaDB($outdir,$lockfile);

    my %h_homologygroups_annotations = ();
    my %h_protein_ids          = ();

    my ($homologygroups_tabfile, $rh_protein_groups) = &ParseHomologyGroupsResults($homologygroups_out, $outdir);
    my ($proteome_tabfile, $proteins_tabfile, $homologyGroup_proteome_association) =
      &ParseProteomes(\%h_protein_ids, $rh_proteome_files, $rh_protein_groups);

    &LoadHomologyGroupsResults($outdir, $homologygroups_tabfile);
    &LoadProteomes($outdir, $proteome_tabfile);
    &LoadGroupProteomeAssociation($outdir, $homologyGroup_proteome_association);
    &LoadProteins($outdir, $proteins_tabfile);
    &VacuumAndAnalyze($outdir);

    #MAKEBLASTDB
    &MakeBlastDB($outdir, $rh_proteome_files,$lockfile);

    my $analysis_url = __GetLocalUrl()
      . '?view='
      . basename($outdir)
      . '&owner='
      . $contact;
    &SendMail($contact,
              "[FAMILY-COMPANION] $title query builder is ready",
              "Check your analysis on the website at:\n$analysis_url");

	unlink ($lockfile);
    exit 0;

}

sub GetNewgroupId
{
    my $outdir = shift;
    return &__GetNewId($outdir, 'homologyGroup');
}

sub __GetNewId
{
    my $outdir = shift;
    my $table  = shift;
    my $cmd    = "sqlite3 $outdir/$DBNAME 'select max(id) from $table'";
    my $id     = `$cmd`;
    chomp $id;

    $id = -1 if ($id !~ /^\d+$/);
    $id++;
    return $id;
}

sub LoadGroupProteomeAssociation
{
    my ($outdir, $tabfile) = @_;
    &__Load($outdir, $tabfile, 'homologyGroup_proteome_association');
    return;

}

sub LoadHomologyGroupsResults
{
    my ($outdir, $tabfile) = @_;
    &__Load($outdir, $tabfile, 'homologyGroup');
    return;
}

sub VacuumAndAnalyze
{
	 my ($outdir) = @_;
	 &Log("VACUUM AND ANALYZE");
     my ($fh_cmd, $cmdfile) = tempfile(UNLINK => 1, DIR => $O_CONF->Get('tmpdir'));
    print $fh_cmd <<END;
VACUUM;
ANALYSE;
END
    my $cmd = "(sqlite3 $outdir/$DBNAME < $cmdfile >> $outdir/db.build.out ) 2> $outdir/db.build.err";

    &System($cmd);
    return;
}

sub __Load
{
    my ($outdir, $tabfile, $table) = @_;

    &Log("FILE - $table - $tabfile");
    my ($fh_cmd, $cmdfile) = tempfile(UNLINK => 1, DIR => $O_CONF->Get('tmpdir'));
    print $fh_cmd <<END;
.separator "\t"
.import $tabfile $table
END

    my $cmd = "(sqlite3 $outdir/$DBNAME < $cmdfile >> $outdir/db.build.out ) 2> $outdir/db.build.err";

    &System($cmd);
    return;

}

sub CreateSchemaDB
{
    my $outdir = shift;
    my $lockfile = shift;

    my $fh_sql_file = &GetStreamOut("$outdir/$DBNAME.create.sql");

    print $fh_sql_file <<END;
CREATE TABLE proteome (id INTEGER PRIMARY KEY ASC, code TEXT, organism TEXT, strain TEXT, release TEXT, description TEXT, sequence_number INTEGER);
CREATE UNIQUE INDEX proteome_code ON proteome(code);

CREATE TABLE homologyGroup (id INTEGER PRIMARY KEY ASC, accession TEXT, genes INTEGER, taxa INTEGER, annotation TEXT); 
CREATE UNIQUE INDEX homologyGroup_accession ON homologyGroup(accession);

CREATE TABLE protein (id INTEGER PRIMARY KEY ASC, accession TEXT, proteome_id INTEGER, homologyGroup_id INTEGER, annotation TEXT, FOREIGN KEY(proteome_id) REFERENCES proteome(id),FOREIGN KEY(homologyGroup_id) REFERENCES homologyGroup(id));
CREATE UNIQUE INDEX protein_accession ON protein(accession);

CREATE TABLE homologyGroup_proteome_association (homologyGroup_id INTEGER, proteome_id INTEGER, FOREIGN KEY(homologyGroup_id) REFERENCES homologyGroup(id),FOREIGN KEY(proteome_id) REFERENCES proteome(id));
CREATE UNIQUE INDEX homologyGroup_proteome_association_index ON homologyGroup_proteome_association(homologyGroup_id,proteome_id);

END
    $fh_sql_file->close;

    my $cmd = "(sqlite3 -init $outdir/$DBNAME.create.sql $outdir/$DBNAME '' >> $outdir/db.build.out ) 2> $outdir/db.build.err";
    &System($cmd);

    if ( ! -s  "$outdir/$DBNAME" )
    {
		&__Die("ERROR - Can not create database - sqlite3 not found ?",$outdir,$lockfile);
	}
    return;
}


sub MakeBlastDB
{
    my ($outdir, $rh_proteome_files,$lockfile) = @_;
    mkdir -p "$outdir/blastdb" unless -d "$outdir/blastdb";

    my @a_dblist = ();
    foreach my $proteome_code (keys %{$rh_proteome_files})
    {
        my $proteome_file = $rh_proteome_files->{$proteome_code}->{'fasta'};
        my $dbname        = "$outdir/blastdb/$proteome_code";
        push(@a_dblist, $dbname);
        my $cmd = "$BLASTBIN_DIR/makeblastdb -out $dbname -dbtype prot -parse_seqids -logfile $outdir/db.makeblastdb.log -in $proteome_file";
        &System("($cmd >> $outdir/db.build.out ) 2> $outdir/db.build.err");
        &System("echo '#$cmd' >> $outdir/db.build.log");
        &System("grep -i error $outdir/db.makeblastdb.log >> $outdir/db.build.err");
    }
    my $cmd =
      "$BLASTBIN_DIR/blastdb_aliastool -title ALL -out $outdir/blastdb/_ALL -logfile $outdir/db.blastdb_aliastool.log -dbtype prot -dblist '" . join(' ', @a_dblist) . "'";
    &System("($cmd >> $outdir/db.build.out ) 2> $outdir/db.build.err");
	&System("echo '#$cmd' >> $outdir/db.build.err");
    &System("grep -i error $outdir/db.blastdb_aliastool.log >> $outdir/db.build.err");

	my $test = `grep -i error $outdir/db.build.err`;
	chomp $test;
	if ($test ne '')
	{
		&System("grep -i error $outdir/db.build.err >> $outdir/error");
		&__Die("ERROR - MakeBlastDB " . `cat $outdir/db.build.err`,$outdir,$lockfile);
	}
    return;
}

sub ParseIprscan
{
    my ($iprscan_file, $rh_iprscan) = @_;
    my $fh_in = &GetStreamIn($iprscan_file);
    while (my $line = <$fh_in>)
    {
        chomp $line;
        my (@a_annotation) = split("\t", $line);
        $rh_iprscan->{$a_annotation[0]} = {} unless (defined $rh_iprscan->{$a_annotation[0]});

        my $annotation = $a_annotation[4] . '; ' . $a_annotation[5];
        $annotation .= '; ' . $a_annotation[11] if (defined $a_annotation[11]);
        $annotation .= '; ' . $a_annotation[12] if (defined $a_annotation[12]);
        $annotation .= '; ' . $a_annotation[13] if (defined $a_annotation[13]);
        $annotation .= '; ' . $a_annotation[14] if (defined $a_annotation[14]);
        $rh_iprscan->{$a_annotation[0]}->{$annotation} = 1;

    }
    return;
}

sub ParseProteomes
{
    my ($rh_protein_ids, $rh_proteome_files, $rh_protein_groups) = @_;

    my ($fh_proteins_tabfile,                   $proteins_tabfile)                   = tempfile(UNLINK => 1, DIR => $O_CONF->Get('tmpdir'));
    my ($fh_proteome_tabfile,                   $proteome_tabfile)                   = tempfile(UNLINK => 1, DIR => $O_CONF->Get('tmpdir'));
    my ($fh_homologyGroup_proteome_association, $homologyGroup_proteome_association) = tempfile(UNLINK => 1, DIR => $O_CONF->Get('tmpdir'));

    my $protein_id                           = 1;
    my $proteome_id                          = 1;
    my %h_homologyGroup_proteome_association = ();
    foreach my $proteome_code (keys %{$rh_proteome_files})
    {
        $rh_protein_ids->{$proteome_code} = {} unless defined $rh_protein_ids->{$proteome_code};

        my $proteome_file = $rh_proteome_files->{$proteome_code}->{'fasta'};

        my $iprscan_file = $rh_proteome_files->{$proteome_code}->{'iprscan'};

        my %h_iprscan = ();
        if (-e $iprscan_file)
        {
            ParseIprscan($iprscan_file, \%h_iprscan);
        }

        my %h_fasta = ();
        &FastaToHash($proteome_file, \%h_fasta, 'noseq');
        my $sequence_number = scalar(keys %h_fasta);
        foreach my $accession (sort keys %h_fasta)
        {
#CREATE TABLE protein (id INTEGER PRIMARY KEY ASC, accession TEXT, proteome_id INTEGER, annotation TEXT, FOREIGN KEY(proteome_id) REFERENCES proteome(id));
            my $annotation = $h_fasta{$accession}->{'header'};
            $annotation =~ s/\t/ /g;
            $annotation =~ s/"/'/g;

            my @a_annotation = ($annotation);
            push(@a_annotation, keys %{$h_iprscan{$accession}}) if (defined $h_iprscan{$accession});
            my $o_json = new JSON;
            $annotation = $o_json->encode(\@a_annotation);

            my $group_id = 0;
            if (defined $rh_protein_groups->{$proteome_code}->{$accession})
            {
                $group_id = $rh_protein_groups->{$proteome_code}->{$accession};
                if (!defined $h_homologyGroup_proteome_association{"$group_id.$proteome_id"})
                {
                    print $fh_homologyGroup_proteome_association "$group_id\t$proteome_id\n";
                    $h_homologyGroup_proteome_association{"$group_id.$proteome_id"} = 1;
                }
            }

            print $fh_proteins_tabfile "$protein_id\t$accession\t$proteome_id\t$group_id\t$annotation\n";
            $rh_protein_ids->{$proteome_code}->{$accession} = $protein_id;
            $protein_id++;
        }

        print $fh_proteome_tabfile "$proteome_id\t$proteome_code" . "\t"
          . $rh_proteome_files->{$proteome_code}->{'organism'} . "\t"
          . $rh_proteome_files->{$proteome_code}->{'strain'} . "\t"
          . $rh_proteome_files->{$proteome_code}->{'release'} . "\t"
          . $rh_proteome_files->{$proteome_code}->{'title'} . "\t"
          . $sequence_number . "\n";

        $proteome_id++;

    }
    $fh_proteins_tabfile->close;
    $fh_proteome_tabfile->close;
    $fh_homologyGroup_proteome_association->close;
    return ($proteome_tabfile, $proteins_tabfile, $homologyGroup_proteome_association);
}

sub LoadProteomes
{
    my ($outdir, $tabfile) = @_;
    &__Load($outdir, $tabfile, 'proteome');
    return;

}

sub LoadProteins
{
    my ($outdir, $tabfile) = @_;
    &__Load($outdir, $tabfile, 'protein');
    return;

}

sub ParseHomologyGroupsResults
{
    my ($homologygroups_out, $outdir) = @_;

    my ($fh_homologygroups_tabfile, $homologygroups_tabfile) = tempfile(UNLINK => 1, DIR => $O_CONF->Get('tmpdir'));

    my $homologygroups_id = &GetNewgroupId($outdir);
    if ($homologygroups_id == 0)
    {
        print $fh_homologygroups_tabfile "0\tspecific\tNULL\tNULL\tNULL\n";
        $homologygroups_id++;
    }

    #	CREATE TABLE group (id INTEGER PRIMARY KEY ASC, accession TEXT, genes INTEGER, taxa INTEGER, annotation TEXT);

    my $header = `head -1 $homologygroups_out`;
    chomp $header;

    my @a_headers        = split(/\t/, $header);
    my $i                = 0;
    my %h_protein_groups = ();

    my %h_headers = ();

    foreach my $header (@a_headers)
    {
        $header =~ s/^#//;
        my $field;
        if ($header eq 'GROUP')
        {
            $field = 'accession';
        }
        elsif ($header eq 'NUMBER OF SPECIES')
        {
            $field = 'taxa';
        }
        elsif ($header eq 'NUMBER OF PROTEINS')
        {
            $field = 'genes';
        }
        elsif ($header =~ /PROTEINS FROM (\S+)$/)
        {
            $field = $1;
            $h_protein_groups{$field} = {};
        }
        else
        {
            $field = 'annotation';
        }

        if ($field eq 'annotation')
        {
            $h_headers{$field} = [] unless defined $h_headers{$field};
            push(@{$h_headers{$field}}, $i);
        }
        else
        {
            $h_headers{$field} = $i;
        }
        $i++;
    }

    my $fh_in = &GetStreamIn($homologygroups_out);

    while (my $homologygroups_grp_line = <$fh_in>)
    {
        chomp $homologygroups_grp_line;
        next if ($homologygroups_grp_line =~ /^#/);
        my @a_fields = split(/\t/, $homologygroups_grp_line);

        my %h_annotations = ();

        foreach my $i (@{$h_headers{'annotation'}})
        {
            my @a_items = split(';', $a_fields[$i]);
            foreach my $item (@a_items)
            {
                $item =~ s/\s+$//;
                $item =~ s/^\s+//;
                $h_annotations{$item} = 1 if ($item ne '');
            }
        }

        my @a_annotation = sort keys %h_annotations;
        my $annotation   = encode_json(\@a_annotation);

        #CREATE TABLE group (id INTEGER PRIMARY KEY ASC, accession TEXT, genes INTEGER, taxa INTEGER, annotation TEXT);
        print $fh_homologygroups_tabfile $homologygroups_id . "\t"
          . $a_fields[$h_headers{'accession'}] . "\t"
          . $a_fields[$h_headers{'genes'}] . "\t"
          . $a_fields[$h_headers{'taxa'}] . "\t"
          . $annotation . "\n";

        foreach my $species_code (keys %h_protein_groups)
        {
            my @a_accessions = split(',', $a_fields[$h_headers{$species_code}]);
            foreach my $protein_accession (@a_accessions)
            {
                $h_protein_groups{$species_code}->{$protein_accession} = $homologygroups_id;
            }
        }

        $homologygroups_id++;
    }

    $fh_in->close;
    return ($homologygroups_tabfile, \%h_protein_groups);
}

sub __Die
{
	my $message = shift;
	my $outdir = shift;
	my $lockfile = shift;

	my $fh_killed = &GetStreamOut("$outdir/db.build.killed");
	print $fh_killed $message;
	$fh_killed->close;

	unlink ($lockfile);
	unlink ("$outdir/$DBNAME");

	
	
	die ("ERROR - $message");
	
}

sub Usage
{
    print STDOUT <<END;
	$0
	 --dir 			Output directory (where the familyCompanion.db will be created)
	 
	 Optionnal:
	 --verbose
END
    return;
}
