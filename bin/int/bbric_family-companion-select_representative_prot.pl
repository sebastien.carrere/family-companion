#!/usr/bin/perl

use strict;
use FindBin;
use lib "$FindBin::RealBin/../../lib/int/";
use lib "$FindBin::RealBin/../../lib/ext/lipmutils";
use File::Temp qw/ tempfile tempdir /;


use General;
use ParamParser;
use GeneralBioinfo;
use Storable;


require 'FamilyCompanion.pl';

our $WORKDIR;
our $O_CONF;
MAIN:
{
	my $o_param = New ParamParser('GETOPTLONG', \&Usage, 'workdir=s', 'start=i', 'size=i');
	$o_param->AssertDirExists('workdir');
	$o_param->AssertDefined('start','size');
	SetGlobals();
	$WORKDIR = $o_param->Get('workdir');
	my $start = $o_param->Get('start');
	my $size = $o_param->Get('size');
	
 	my $rh_homologygroups = retrieve ("$WORKDIR/rh_homologygroups");
	my $rh_species = retrieve ("$WORKDIR/rh_species");
	my $rh_seqidspeciescode = retrieve ("$WORKDIR/rh_seqidspeciescode");
	my $rh_analysis_params = retrieve ("$WORKDIR/rh_analysis_params");


	&GetPanProteomeIds($rh_homologygroups, $rh_species, $rh_analysis_params, $start,$size);
	
	
}


sub Usage
{
	
}



sub GetPanProteomeIds
{
    my ($rh_homologygroups, $rh_species, $rh_analysis_params, $start,$size) = @_;

    
    my $refexist;
    my @a_references = ();
    my @a_seqs1      = ();
    my @a_allseqs1   = ();

    my %h_list_group_select_seqid_code = ();
    my %h_nb_repre_group_by_proteome   = ();

    foreach my $code (keys %{$rh_analysis_params->{'proteomes'}})
    {
        if (   $rh_analysis_params->{'proteomes'}->{$code}->{'reference'} eq 'true'
            or $rh_analysis_params->{'proteomes'}->{$code}->{'reference'} == &TRUE)
        {
            push(@a_references, $code);
        }
        $h_nb_repre_group_by_proteome{$code} = 0;
    }

	#paralelliser la recherche de sequence representative
	# utils.pl avec des objets storage rh_homologygroups rh_species en splitant le nombres de clés de rh_homologous et --startid=trancheX
	my @a_groupids = sort (keys %{$rh_homologygroups});

	my $end = $start + $size;
	$end = scalar @a_groupids if ($end >  scalar @a_groupids);
	
	for (my $i = $start; $i < $end; $i ++)
	{
		my $groupid = $a_groupids[$i];
        @a_allseqs1 = ();
        foreach my $code (keys %{$rh_homologygroups->{$groupid}})
        {
            foreach my $seqid (@{$rh_homologygroups->{$groupid}->{$code}})
            {
                @a_seqs1 = ();
                push(@a_seqs1, $code, $seqid);
                push(@a_allseqs1, [@a_seqs1]);
            }
        }

        $h_list_group_select_seqid_code{$groupid} = &SelectSequenceRepGroupe($rh_species, \@a_allseqs1, \@a_references);
    }

	store (\%h_list_group_select_seqid_code, "$WORKDIR/h_list_group_select_seqid_code.$start");
	
    return;
}


=head3 function SelectSequenceRepGroupe

 Title        : SelectSequenceRepGroupe
 Usage        : @selec = &SelectSequenceRepGroupe($rh_species,\@a_allseqs1);
 Prerequisite : none
 Function     : Select representative sequence  [longest from cdhit 50 cluster]
 Returns      : array of [code,seqid]
 Args         : hash of species, array of array of [code,seqid]
 Globals      : none

=cut

sub SelectSequenceRepGroupe
{
    my ($rh_species, $ra_species_seqid_list, $ra_reference) = @_;
    my $selecSeqid = "";
    my $selecCode  = "";

    my $id = 0;
    my ($fh_tmp, $tmpfile) = File::Temp::tempfile(UNLINK => 1, DIR => $O_CONF->Get('tmpdir'));
    foreach my $ra_species_seqid (@{$ra_species_seqid_list})
    {

        my $species_code = $ra_species_seqid->[0];
        my $seqid        = $ra_species_seqid->[1];
        my $sequence     = $rh_species->{$species_code}->{'sequences'}->{$seqid}->{'sequence'};
        print $fh_tmp <<END;
>$id $species_code|$seqid acc=$seqid sp=$species_code
$sequence
END
        $id++;
    }
    $fh_tmp->close();

	#Calcul de la matrice de distance sur alignement du groupe
    my $distance_cmd = "mafft --thread 2 --auto --quiet $tmpfile | fprotdist -sequence stdin -out stdout -auto -noprogress";
    &Log("CMD -  $distance_cmd");

    my @a_distance_lines = `$distance_cmd`;
    chomp @a_distance_lines;

    #Calcul de la somme par ligne de la matrice de distance (score distance globale de chaque proteine avec les autres)
    foreach my $line (@a_distance_lines)
    {
        if ($line =~ /^\d+\s+\d\.\d+/)
        {
            my ($id, @a_distances) = split(/\s+/, $line);
            my $score = eval join '+', @a_distances;
            push(@{$ra_species_seqid_list->[$id]}, $score);

        }
    }

	#Tri sur ce score: la proteine avec le plus petit score (1ere du tri) ou la premiere protein issue d'une proteome de reference est choisie
    my @a_representative = ();
    foreach my $ra_species_seqid (sort {$a->[2] <=> $b->[2]} @{$ra_species_seqid_list})
    {
        my $species_code = $ra_species_seqid->[0];
        my $seqid        = $ra_species_seqid->[1];
        @a_representative = ($seqid, $species_code) if (scalar @a_representative == 0);
        my $score = $ra_species_seqid->[2];
        if (grep (/^$species_code$/, @{$ra_reference}))
        {
            @a_representative = ($seqid, $species_code);
            last;
        }
    }

	&System ("rm -f $tmpfile");
    return \@a_representative;

}
