#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use FindBin;
use lib "$FindBin::RealBin/../../lib/int";
use lib $FindBin::RealBin . '/../../lib/ext/lipmutils';

use Cwd 'abs_path';
use File::Basename;
use ParamParser;
use General;
use JSON;
use Proc::Killfam;
require 'WebApp.pl';


our $VERBOSE = &FALSE;
our $O_CONF;


use constant OFFLINE_MODE   => &FALSE;

MAIN:
{
	
	my $o_param = New ParamParser('GETOPTLONG', \&Usage, 'verbose',  'infile=s', 'type=s');

    $VERBOSE = &TRUE if ($o_param->IsDefined('verbose'));

    $o_param->AssertFileExists('infile');
    $o_param->AssertAllowedValue('type','binary', 'abundancy');
    my $infile = $o_param->Get('infile');
    my $type = $o_param->Get('type');
    
	&AdjustConfig();
	my $compress_mode = &__ConvertToInchlibData($infile, \$o_param);
	
	my $dirname = dirname ($infile);
	my $basename = basename ($infile);

	&MinifyJson("$infile.inchlib.json","$dirname/.$basename.inchlib.json");
	&PrintHtml($infile,$type,$compress_mode);
	unlink ("$infile.inchlib.json");
	unlink ("$infile.inchlib.csv");
	unlink ("$infile.inchlib.stdout");
	unlink ("$infile.inchlib.stderr");
	unlink ("$infile.inchlib.metadata");
	exit 0;

}


sub PrintHtml
{
	my ($infile, $type, $compress_mode) = @_;
	my $infile_basename = basename($infile);
	my $http_root = $O_CONF->Get('portal_http_root');
	my $max_compress = $O_CONF->Get('inchlib_clust_compress_threshold');
	my $title = "$type matrix clustering";
	if ($compress_mode == &TRUE)
	{
		my $lines = `grep -c -v '^#' $infile`;
		chomp $lines;
		$title .= " / $lines lines compressed in $max_compress lines";
		
	}
	my $fh_html = &GetStreamOut("$infile.html");
	
	print $fh_html <<END;
 <html>
    <head>
        <script src="$http_root/web/js/ext/jquery-min.js"></script>
        <script src="$http_root/web/js/ext/inchlib/konva.min.js"></script>
        <script src="$http_root/web/js/ext/inchlib/inchlib-1.2.0.min.js"></script>
        <script>
        \$(document).ready(function() { //run when the whole page is loaded
            window.inchlib = new InCHlib({ //instantiate InCHlib
                target: "inchlib", //ID of a target HTML element
                metadata: false, //turn on the metadata 
                column_metadata: false, //turn on the column metadata 
                max_height: 1200, //set maximum height of visualization in pixels
                width: 1000, //set width of visualization in pixels
                heatmap_colors: "Greens", //set color scale for clustered data
                metadata_colors: "Reds", //set color scale for metadata
            });

            inchlib.read_data_from_file(".$infile_basename.inchlib.json"); //read input json file
            inchlib.draw(); //draw cluster heatmap
        });
        </script>
    </head>

    <body>
    <h1>$title</h1>
        <div id="inchlib"></div>
    </body>
 </html>
END

	$fh_html->close();

	return;
}
sub Usage
{
	
}
