#!/usr/bin/perl

use strict;

use FindBin;
use lib $FindBin::RealBin . '/../../lib/ext/lipmutils';

use ParamParser;
use General;

my $O_CONF;
my $DOCKER_INSTALL = &FALSE;

MAIN:
{
    my $o_param = New ParamParser('GETOPTLONG', \&Usage, 'docker');

    $DOCKER_INSTALL = &TRUE if ($o_param->IsDefined('docker'));

    my $user = $ENV{'USER'};

    if ($user !~ /root|www-data/)
    {
        die("ERROR - Install must be done as root or www-data user [$user]\n");
    }

    my $conf_file = $FindBin::RealBin . '/../../site/cfg/site.cfg';

    die("ERROR - $conf_file not found - USE $conf_file.template to build it\n") if (!-e $conf_file);

    $O_CONF = New ParamParser('INIT');
    $O_CONF->SetBehaviour('use_substitution_table');

    $O_CONF->Update($conf_file, 'O');

    $O_CONF->SetSubstitution('%S', \$O_CONF->Get('portal_web_server'));
    $O_CONF->SetSubstitution('%I', \$O_CONF->Get('portal_install_dir'));
    $O_CONF->SetSubstitution('%H', \$O_CONF->Get('portal_http_root'));

    $O_CONF->Update($conf_file, 'O');

    my @a_install_files = `find $FindBin::RealBin/../../ -name '*.mask'`;
    chomp @a_install_files;

    foreach my $file (@a_install_files)
    {
        &Unmask($file);
    }

    #CRONTAB
    my $crontab_file = $O_CONF->Get('portal_install_dir') . '/site/cfg/crontab.cfg';
    if (-e $crontab_file)
    {
        system("crontab $crontab_file");
    }

    #MKDIRS
    my @a_dirs = (
                  $O_CONF->Get('datarootdir'),
                  $O_CONF->Get('datarootdir') . '/cfg',
                  $O_CONF->Get('portal_install_dir') . '/site/tmp',
                  $O_CONF->Get('portal_install_dir') . '/etc'
                  );
    foreach my $dir (@a_dirs)
    {
        &Mkdir($dir);
    }

    my $fh_out = &GetStreamOut($O_CONF->Get('portal_install_dir') . '/etc/.htaccess');
    print $fh_out "Require all denied\n";
    $fh_out->close();

    $fh_out = &GetStreamOut($O_CONF->Get('datarootdir') . '/cfg/.htaccess');
    print $fh_out "Require all denied\n";
    $fh_out->close();

    &Dependencies($DOCKER_INSTALL);
    system("chown www-data.www-data $conf_file");

    system("cd $FindBin::RealBin/../../web; ln -s index_prod.html index.html") unless (-e "$FindBin::RealBin/web/index.html");
    exit 0;
}

sub Usage
{
    print STDERR <<END;
	
	$0 [--docker]
	
	If --docker:
		Automatically install dependencies
	
	Else
		Show you dependencies
		
END
}

sub Dependencies
{

    my @a_cmd =
      (
        "apt-get -q update && apt-get install -yq cron at libtree-simple-perl libbio-perl-perl ncbi-blast+ sqlite liberror-perl libswitch-perl libxml-simple-perl libjson-perl libapache-session-perl libnet-ldap-perl libapache-htpasswd-perl ssmtp fasttree mafft biosquid python2.7 python-scipy python-sklearn python-numpy python-fastcluster mcl libarray-utils-perl libproc-processtable-perl liburi-encode-perl parallel dos2unix"
        );

    my $at_allow_cmd = 'cp /etc/at.deny /etc/at.deny.ori && grep -v "www-data" /etc/at.deny.ori > /etc/at.deny';

    if ($DOCKER_INSTALL == &TRUE)
    {
        foreach my $cmd (@a_cmd)
        {
            system($cmd);
        }
        if (-e "/etc/at.deny")
        {
            system($at_allow_cmd);
        }

    }
    else
    {
        foreach my $cmd (@a_cmd)
        {
            system("echo -e '\n\e[33m[DEPENDENCY] Run $cmd\e[39m'");
			system("echo -e '\n\e[33m[CONFIG] allow www-data to lauch at cmd $at_allow_cmd\e[39m'");
        }
    }
    return;
}

sub Mkdir
{
    my $dir = shift;
    if (!-d $dir)
    {
        print "[INFO] - Create www-data directory $dir\n";
        system("mkdir $dir; chown www-data.www-data -R $dir");
    }
    return;
}

sub Unmask
{
    my $file = shift;

    print "[INFO] - Load $file\n";
    my $target_file = $file;
    $target_file =~ s/\.mask//;

    my $fh_in  = &GetStreamIn($file);
    my $fh_out = &GetStreamOut($target_file);

    while (my $line = <$fh_in>)
    {
        chomp $line;
        while ($line =~ /#(\w+)#/)
        {
            my $key = $1;
            die("ERROR - $key not found in config file\n") if (!$O_CONF->IsDefined($key));
            my $value = $O_CONF->Get($key);
            $line =~ s/#$key#/$value/;
        }
        print $fh_out "$line\n";

    }
    $fh_in->close;
    $fh_out->close;

    print "[INFO] - Write $target_file\n";
    return;
}

