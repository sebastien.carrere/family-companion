#!/usr/bin/perl

use strict;
use FindBin;
use lib "$FindBin::RealBin/../../lib/int/";
use lib "$FindBin::RealBin/../../lib/ext/lipmutils";

use General;
use ParamParser;
use File::Basename;
use Apache::Htpasswd;
use File::Temp;
use Data::Dumper;

require 'common.pl';

our $O_CONF;

MAIN:
{
    my $user = $ENV{'USER'};
    if (!defined $ENV{'USER'} && defined $ENV{'SERVER_SOFTWARE'})
    {
        $user = 'www-data';
    }

    if ($user !~ /root|www-data/)
    {
        die("ERROR - Must be run as root (or sudo) or www-data user\n<pre>");
    }

    my $o_param =
      New ParamParser('GETOPTLONG', \&Usage, 'email=s', 'password=s', 'common_name=s', 'outdir=s', 'sendmail');

    $O_CONF = New ParamParser('INIT');
    $O_CONF->SetBehaviour('use_substitution_table');
    $O_CONF->Update("$FindBin::RealBin/../../site/cfg/site.cfg", 'O');

    $O_CONF->SetSubstitution('%S', \$O_CONF->Get('portal_web_server'));
    $O_CONF->SetSubstitution('%I', \$O_CONF->Get('portal_install_dir'));
    $O_CONF->SetSubstitution('%H', \$O_CONF->Get('portal_http_root'));

    $O_CONF->Update("$FindBin::RealBin/../../site/cfg/site.cfg",           'O');
    $O_CONF->Update("$FindBin::RealBin/../../site/cfg/authentication.cfg", 'O');

    my $local_cfg = undef;
    $local_cfg = "$FindBin::RealBin/../../data/cfg/local.cfg" if (-e "$FindBin::RealBin/../../data/cfg/local.cfg");
    $O_CONF->Update($local_cfg, 'O') if defined "$local_cfg";
    $local_cfg = $ENV{'FC_DIR'} . '/data/cfg/local.cfg' if (defined $ENV{'FC_DIR'} && -e $ENV{'FC_DIR'} . '/data/cfg/local.cfg');
    $O_CONF->Update($local_cfg, 'O') if defined "$local_cfg";
    $local_cfg = $ENV{'FC_DIR'} . '/data/cfg/env.cfg'  if (defined $ENV{'FC_DIR'} && -e $ENV{'FC_DIR'} . '/data/cfg/env.cfg');
    $O_CONF->Update($local_cfg, 'O') if defined "$local_cfg";

    &Sanitize();

    my $file          = $O_CONF->Get('authentic_file');
    my $htpasswd_file = $O_CONF->Get('authentic_htpasswd_file');

    my $file_basename          = basename($file);
    my $htpasswd_file_basename = basename($htpasswd_file);

    my $file_dir = dirname($file);
    if ($o_param->IsDefined('outdir'))
    {
        $o_param->AssertDirExists('outdir');
        $file_dir      = $o_param->Get('outdir');
        $file          = "$file_dir/$file_basename";
        $htpasswd_file = "$file_dir/$htpasswd_file_basename";
    }

    print STDOUT "[INFO] Outfiles: $file | $htpasswd_file\n";

    mkdir $file_dir unless -d $file_dir;

    $o_param->AssertDefined('email');
    $o_param->AssertDefined('password');
    $o_param->AssertDefined('common_name');

    my $email = lc($o_param->Get('email'));

    if ($email ne 'guest' && !IsEmailValid($email))
    {
        die "[ERROR] - $email is not a valid email address";
    }

    my $password    = $o_param->Get('password');
    my $common_name = $o_param->Get('common_name');

    my @a_users = ();
    @a_users = `cut -d ':' -f1 $file` if (-s $file);
    chomp @a_users;

    if (grep (/^$email$/, @a_users))
    {
        die("\nERROR - USER $email already exists - edit $file first\n");
    }

    system("touch $htpasswd_file") if (!-e $htpasswd_file);
    my $o_htpasswsd = new Apache::Htpasswd($htpasswd_file);
    $o_htpasswsd->htpasswd($email, $password);
    my $encrypted_password = $o_htpasswsd->fetchPass($email);

    my $fh_out = &GetStreamOut(">$file");
    print $fh_out "$email:$encrypted_password:$common_name:user:$email:/dev/null\n";
    $fh_out->close;

    system("chown www-data.www-data $htpasswd_file");

    #
    # Envoi de mail
    #
    my $url = __GetPublicUrl();

    if ($o_param->IsDefined('sendmail'))
    {
        my $admin_mail = $O_CONF->Get('admin_mail');

        my ($fh_mail, $mail_filename) = File::Temp::tempfile(UNLINK => 1, DIR => $O_CONF->Get('tmpdir'));
        print $fh_mail <<END;
Subject: [Family-Companion] New user account - $common_name
From: $admin_mail
To: $email
Cc: $admin_mail

Here are the credentials to access Family-Companion @ $url

Login: $email
Passwd: $password
Url: $url

END
        $fh_mail->close;
        my $ssmtp_conf = "$FindBin::RealBin/../../data/cfg/ssmtp.conf";
        $ssmtp_conf = $ENV{'FC_DIR'} . '/data/cfg/ssmtp.conf'  if (defined $ENV{'FC_DIR'} && -e $ENV{'FC_DIR'} . '/data/cfg/ssmtp.conf');

        if (-e $ssmtp_conf)
        {
            my $mail_cmd = "sendmail -C $ssmtp_conf -t < $mail_filename";
            my $ret      = system $mail_cmd;
            if ($ret ne 0)
            {
                die("ERROR - Send mail failed");
            }
        }
        else
        {
            print STDOUT "[WARNING] CAN NOT SEND EMAIL (>$ssmtp_conf< not found)\n";
            print STDOUT '[MAIL CONTENT]' . `cat $mail_filename`;
        }
    }
    else
    {
        print STDOUT <<END;
-------------------------------------------------------------		
Here are the credentials to access Family-Companion @ $url

Login: $email
Passwd: $password
Url: $url
-------------------------------------------------------------
END
    }
    exit 0;

}

sub Usage
{
    print <<END;

	$0 --email=<jon.doe\@somewhere.else> --password=<azekaze-aze> --common_name='Jon Doe'
	
END
}
