#!/usr/bin/perl


use strict;
use Bio::SearchIO;
use Storable;
use FindBin;

MAIN:
{
	my $blastfile = $ARGV[0];
	my $seqlen_storable = $ARGV[1];
	my %blast_flag =  %{retrieve ($ARGV[2])}; ;
	my $pv_cutoff = $ARGV[3];
	my %seq_len = %{retrieve ($seqlen_storable)};
	
	my $parseoutfile = "$blastfile.parse";
	
	open (PARSEOUT,">$parseoutfile");
	my $searchio = Bio::SearchIO->new(-file   => $blastfile,
									  -format => $blast_flag{'format'}) or die("Blast parsing failed!");

	
	while (my $result = $searchio->next_result()) {
		my $queryid=$result->query_name;
		my $querylen;
		if (defined $seq_len{$queryid}) {
			$querylen=$seq_len{$queryid};
		} else {
			$querylen=$result->query_length; # query length is not stored in BLAST m8 format, so querylen will be 0
		}
		while( my $hit = $result->next_hit ) {
			next unless numeric_pvalue($hit->significance) <= $pv_cutoff;
			my $subjectid=$hit->name;
			my $subjectlen;
			if (defined $seq_len{$subjectid}) {
				$subjectlen=$seq_len{$subjectid};
			} else {
				$subjectlen=$hit->length; # subject length is not stored in BLAST m8 format, so querylen will be 0
			}
			my $pvalue=numeric_pvalue($hit->significance);
			if ($blast_flag{'hsp'}) {
				my $simspanid=1;
				my $simspan='';
				my (@percentidentity,@hsplength);
				while( my $hsp = $hit->next_hsp ) {
					my $querystart=$hsp->start('query');
					my $queryend=$hsp->end('query');
					my $subjectstart=$hsp->start('sbjct');
					my $subjectend=$hsp->end('sbjct');
					$percentidentity[$simspanid]=$hsp->percent_identity;
					$hsplength[$simspanid]=$hsp->length('hit');
					$simspan.="$simspanid:$querystart-$queryend:$subjectstart-$subjectend.";
					$simspanid++;
				}
				my $sum_identical=0;
				my $sum_length=0;
				for (my $i=1;$i<$simspanid;$i++) {
					$sum_identical+=$percentidentity[$i]*$hsplength[$i];
					$sum_length+=$hsplength[$i];
				}
				my $percentIdent=int($sum_identical/$sum_length);
				print PARSEOUT "$queryid;$querylen;$subjectid;$subjectlen;$pvalue;$percentIdent;$simspan\n";
			} else {
				print PARSEOUT "$queryid;$querylen;$subjectid;$subjectlen;$pvalue;0;NULL\n";
			}
			
		}
	}
	close(PARSEOUT);
}


# Make pvalue numeric, used by subroutine blast_parse
# One Arguments:
# 1. String Variable: pvalue
# Last modified: 07/19/04
sub numeric_pvalue {
	my $p=$_[0];
	if ($p=~/^e-(\d+)/) {return "1e-".$1;}
	else {return $p}
} # numeric_pvalue

