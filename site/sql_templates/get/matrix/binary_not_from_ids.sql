SELECT
 homologyGroup.accession as line,
 group_concat(DISTINCT proteome.code) as column_array
 FROM
 proteome,
 homologyGroup,
 homologyGroup_proteome_association
 WHERE
 homologyGroup.id = homologyGroup_id
 AND proteome.id = proteome_id
 AND homologyGroup.id NOT IN (#MASK_IDS#) #MASK_FILTER_STATEMENT#
 GROUP BY
 homologyGroup.accession
 ORDER BY
 homologyGroup.accession
 #MASK_LIMIT# #MASK_OFFSET#
