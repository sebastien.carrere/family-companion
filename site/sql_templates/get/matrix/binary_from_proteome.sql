SELECT
 homologyGroup.accession as line,
 group_concat(DISTINCT proteome.code) as column_array
 FROM
 proteome,
 homologyGroup,
 homologyGroup_proteome_association
 WHERE homologyGroup.id = homologyGroup_id
 AND proteome.id = proteome_id
 AND proteome_id IN (
  SELECT id FROM proteome WHERE upper(code) IN ("#MASK_PROTEOME#") AND homologyGroup.id != 0
  )
 #MASK_FILTER_STATEMENT#
 GROUP BY
 homologyGroup.accession
 ORDER BY
 homologyGroup.accession
 #MASK_LIMIT# #MASK_OFFSET#
