SELECT
 homologyGroup.accession as line,
 group_concat(DISTINCT proteome.code) as column_array
 FROM
 proteome,
 homologyGroup,
 homologyGroup_proteome_association
 WHERE
 homologyGroup.id = homologyGroup_proteome_association.homologyGroup_id
 AND
 proteome.id = homologyGroup_proteome_association.proteome_id
 AND
 homologyGroup.id IN (#MASK_IDS#)
 GROUP BY homologyGroup.accession
 ORDER BY homologyGroup.accession
 #MASK_LIMIT# #MASK_OFFSET#
