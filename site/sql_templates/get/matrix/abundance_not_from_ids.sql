SELECT
 homologyGroup.accession as line,
 group_concat(proteome.code) as column_array
 FROM protein,
 proteome,
 homologyGroup,
 homologyGroup_proteome_association
 WHERE
 homologyGroup.id = homologyGroup_proteome_association.homologyGroup_id
 AND
 proteome.id = homologyGroup_proteome_association.proteome_id
 AND
 proteome.id = protein.proteome_id
 AND
 homologyGroup.id = protein.homologyGroup_id
 AND
 homologyGroup.id NOT IN (#MASK_IDS#) #MASK_FILTER_STATEMENT#
 GROUP BY
 homologyGroup.accession
 ORDER BY
 homologyGroup.accession
 #MASK_LIMIT# #MASK_OFFSET#
