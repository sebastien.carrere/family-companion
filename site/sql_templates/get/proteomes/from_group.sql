SELECT
 #MASK_FIELD#
 FROM
 proteome,
 homologyGroup,
 homologyGroup_proteome_association
 WHERE
 upper(homologyGroup.accession) IN ("#MASK_GROUP#")
 AND
 homologyGroup.id = homologyGroup_proteome_association.homologyGroup_id
 AND
 proteome.id = homologyGroup_proteome_association.proteome_id 
