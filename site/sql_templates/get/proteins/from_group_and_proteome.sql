SELECT
 #MASK_FIELD#,
 homologyGroup.accession as homologyGroup,
 proteome.code as proteome
 FROM
 protein,
 homologyGroup,
 proteome
 WHERE
 upper(homologyGroup.accession) IN ("#MASK_GROUP#")
 AND upper(proteome.code) IN ("#MASK_PROTEOME#")
 AND protein.homologyGroup_id = homologyGroup.id
 AND protein.proteome_id = proteome.id
 ORDER BY
 protein.homologyGroup_id
 #MASK_LIMIT# #MASK_OFFSET#
