SELECT
 #MASK_FIELD#,
 group_concat(DISTINCT proteome.code) as proteomes,
 group_concat(DISTINCT protein.accession) as proteins
FROM
 homologyGroup,
 proteome,
 homologyGroup_proteome_association,
 protein
WHERE
 protein.homologyGroup_id IN (SELECT homologyGroup.id FROM homologyGroup WHERE upper(homologyGroup.accession) IN ("#MASK_ACCESSION#"))
 AND proteome.id = homologyGroup_proteome_association.proteome_id
 AND homologyGroup.id = homologyGroup_proteome_association.homologyGroup_id
 AND homologyGroup.id = protein.homologyGroup_id
 AND protein.homologyGroup_id =  homologyGroup_proteome_association.homologyGroup_id
 AND protein.proteome_id = proteome.id
 GROUP BY homologyGroup.id
 #MASK_LIMIT# #MASK_OFFSET#
