SELECT
 #MASK_FIELD#,
 group_concat(DISTINCT proteome.code) as proteomes,
 group_concat(DISTINCT protein.accession) as proteins
 FROM
 homologyGroup,
 proteome,
 homologyGroup_proteome_association,
 protein
 WHERE
 protein.homologyGroup_id IN (#MASK_IDS#)
 AND
 homologyGroup.id = homologyGroup_proteome_association.homologyGroup_id
 AND
 proteome.id = homologyGroup_proteome_association.proteome_id
 AND
 homologyGroup.id = protein.homologyGroup_id
 AND
 protein.proteome_id = proteome.id
 #MASK_FILTER_STATEMENT#
 GROUP BY homologyGroup.id
 #MASK_LIMIT# #MASK_OFFSET#
