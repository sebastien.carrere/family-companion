SELECT
 homologyGroup_proteome_association.homologyGroup_id as id
 FROM
 proteome,
 homologyGroup_proteome_association
 WHERE
 upper(proteome.code) = "#MASK_PROTEOME#"
 AND
 proteome.id = homologyGroup_proteome_association.proteome_id
