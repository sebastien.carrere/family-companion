SELECT
 #MASK_FIELD#
 FROM
 homologyGroup,
 proteome,
 homologyGroup_proteome_association
 WHERE
 upper(proteome.code) = "#MASK_PROTEOME#"
 AND
 homologyGroup.id = homologyGroup_proteome_association.homologyGroup_id
 AND
 proteome.id = homologyGroup_proteome_association.proteome_id
 #MASK_FILTER_STATEMENT#
