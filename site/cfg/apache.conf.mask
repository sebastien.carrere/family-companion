<VirtualHost *:80>
	ServerAdmin webmaster@localhost
	DocumentRoot /var/www/
 
	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined

	RewriteEngine On
	
	
	#family-companion
	SetEnv FC_HOME #portal_install_dir#
	ScriptAlias "#portal_http_root#/cgi" "#portal_install_dir#/cgi"
	Alias "#portal_http_root#" "#portal_install_dir#" 
	RedirectMatch "^#portal_http_root#/*$" #portal_http_root#/web/

	RewriteRule ^#portal_http_root#/ws/(\w+)$ #portal_http_root#/cgi/index.cgi?$1=1 [T=application/x-httpd-cgi]
	RewriteCond %{QUERY_STRING} ^(.*)$
	RewriteRule ^#portal_http_root#/ws/(\w+)/(\w+)/(\w+) #portal_http_root#/cgi/index.cgi?__wb_function=WebService&analysis=$1&method=$2&resource=$3&%1 [T=application/x-httpd-cgi,R=307]

	ErrorDocument 401 #portal_http_root#/web/

	<Directory "#portal_install_dir#/cgi">
		 Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
		 AllowOverride All
	</Directory>
	<Directory "#portal_install_dir#/data">
		 Options -Indexes
		 AllowOverride All
	</Directory>

	<LocationMatch "#portal_http_root#/(lib|bin|site|etc)/">
		Require all denied
	</LocationMatch>

	<LocationMatch "#portal_http_root#/data/(cfg|etc)/">
		Require all denied
	</LocationMatch>

	
	<LocationMatch "#portal_http_root#/site/tmp">
		Require all granted
	</LocationMatch>
</VirtualHost>
