#!/bin/bash

error=0;

args=$@;

login=0;
local=0;

if [[ "$args" =~ (-login) ]]
then
	login=1;
fi

if [[ "$args" =~ (-local) ]]
then
	local=1;
	FC_ADMIN='guest'
	FC_SMTP='local'
	FC_PORT='1977'
	FC_HOSTNAME='localhost'
	FC_DIR="$HOME/.family-companion"
fi

echo -e "\e[1m\e[100m";


if [ -z ${FC_DIR+x} ]
then 
	echo -e "\n\e[31m[ERROR] - FC_DIR is unset\e[39m";
	error=1;
else
	if [ ! -d $FC_DIR/ ]
	then
		echo -e "\n\e[31m[ERROR] - FC_DIR >$FC_DIR< DOES NOT EXIST\e[39m";
		error=1;
	fi
fi

if [ -z ${FC_ADMIN+x} ]
then 
	echo -e "\n\e[31m[ERROR] - FC_ADMIN is unset : MUST BE SET TO Admin email address\e[39m";
	error=1;
fi

if [ ! -e $FC_DIR/data/cfg/ssmtp.conf ]
then
	if [ -z ${FC_SMTP+x} ]
	then
		echo -e "\n\e[31m[ERROR] - FC_SMTP is unset and SSMTP not configured: >$FC_DIR/data/cfg/ssmtp.conf< DOES NOT EXIST";
		echo -e "\e[33m>>>> [HELP] - Set FC_SMTP to your SMTP server address and FC_SMTP_LOGIN and FC_SMTP_PASSWD if authentication is required\e[39m";
		error=1;
	fi
fi

if [ -z ${FC_PORT+x} ]
then 
	echo -e "\n\e[31m[ERROR] - FC_PORT is unset : MUST BE SET TO the port you want to access family-companion app\e[39m";
	error=1;
fi

if [ -z ${FC_HOSTNAME+x} ]
then
	echo -e "\e[33m>>>> [HELP] - FC_HOSTNAME not set - default setting is localhost\e[39m";
	FC_HOSTNAME='localhost';
fi

echo -e "portal_web_server=http://$FC_HOSTNAME\n" > $FC_DIR/data/cfg/env.cfg;
echo -e "portal_port=$FC_PORT\n" >>  $FC_DIR/data/cfg/env.cfg;

if [ $error -ne 1 ]
then
	interactive=' -d ';
	if [ $login -eq 1 ]
	then
		interactive=' -i ';
	fi
	echo -e "\n\n\e[32mfamily-companion is running @ http://$HOSTNAME:$FC_PORT/family-companion\n\n\e[39m";
	docker run $interactive -p $FC_PORT:80 -p 587:587 -p 465:465 -p 25:25 -v $FC_DIR/data:/var/www/family-companion/data -v $FC_DIR/etc:/var/www/family-companion/etc -t bbric/family-companion
else
	echo -e "\n\n\e[31m[ERROR] - Service not started\n\n\e[39m";
fi



echo -e "\e[49m";
echo -e "\e[0m";
