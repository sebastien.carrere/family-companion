#!/bin/bash

docker ps | grep 'bbric/family-companion' | cut -d ' ' -f1 | xargs -i docker stop {}
