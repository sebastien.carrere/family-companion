Ext.define('BP.globals.Login', {

	singleton : true,
	/**
	 * Sends a request to the server to see if the user is authenticated
	 */
	checkLogin : function() {

		BP.globals.Ajax.send({
			url : '../cgi/index.cgi',
			params : {
				authentication : 1
			},
			method : 'POST',
			serverFailureMessage : "Impossible to check login",
			jsonFailureMessage : "Check login: bad response from the server",
			successFunction : function(json) {
				BP.globals.ConnectionData.mail = json["email"];

				if (!Ext.form.field.VTypes.email(json["email"])) {

					Ext.create("Ext.window.MessageBox").alert("Login Failed",
							"Connection lost, please login");

				}

				Ext.ComponentQuery.query("bannerPanel")[0].setLogin();

			},

		});

	},

	/**
	 * Logout function
	 */
	logout : function() {
		BP.globals.ConnectionData.mail = "";

		Ext.ComponentQuery.query("bannerPanel")[0].setLogin();

		var mainPanel = Ext.ComponentQuery.query("mainPanel")[0];

		mainPanel.removeAll(true);

		mainPanel.add({
			xtype : 'homePanel'
		});

	}

});