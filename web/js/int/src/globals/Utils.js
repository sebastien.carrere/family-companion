Ext.define("BP.globals.Utils", {
	singleton : true,

	/**
	 * Displays a short message that disappears after one second
	 */
	displayShortMessage : function(text, cpt) {

		Ext.create('Ext.window.Window', {
			header : false,
			bodyStyle : "font-color:green",
			border : 0,
			resizable : false,
			layout : 'fit',
			closable : false,
			items : { // Let's put an empty grid in just to illustrate fit
				// layout
				bodyCls : 'short-message',
				html : text
			}
		}).show(cpt, function() {
			this.el.ghost("b", {
				delay : 1000
			});
		});

	},
	/**
	 * Basename of a file
	 */
	basename : function(path) {

		return path.replace(/\\/g, '/').replace(/.*\//, '');

	},

	/**
	 * Returns the root url
	 * 
	 * @return {String} url
	 */
	getHostUrl : function() {
		var root = window.location.pathname.replace(/\/web.*/, "");

		var port =  window.location.port;
		var url = window.location.protocol + "//" + window.location.hostname

		if (port !== "")
		{
			url		+= ":" + port;
		}
		url		+= root;

		return url;
	},

	/**
	 * If the value is an array, returns items separated by <br />
	 * If a value is a url, transforms it into href
	 * 
	 * @param {String
	 *            or Array} value
	 */
	formatValue : function(value) {
		var newValue = value;

		if (Ext.isDefined(value)) {
			if (Ext.isArray(newValue)) {

				for (var i = 0; i < newValue.length; i++) {
					newValue[i] = this.formatValue(newValue[i]);
				}

				newValue = newValue.join("<br />");
			} else {
				if (!Ext.isNumeric(value)) {
					if (this.isHttpLink(value) || this.isFtpLink(value)) {
						newValue = this.toHttpHref(value);
					} else if (this.isDoiLink(value)) {
						newValue = this.toDoiHref(value);
					}
				}
			}
		}

		return newValue;

	},

	/**
	 * 
	 * Returns true if the value is a http link
	 * 
	 * @param {String}
	 *            value
	 * @return {Boolean}
	 */
	isHttpLink : function(value) {
		if (value.indexOf("http") == 0) {
			return true;
		}
		return false;
	},

	/**
	 * 
	 * Returns true if the value is a doi link
	 * 
	 * @param {String}
	 *            value
	 * @return {Boolean}
	 */
	isDoiLink : function(value) {
		if (value.indexOf("doi:") == 0) {
			return true;
		}
		return false;
	},

	/**
	 * 
	 * Returns true if the value is a ftp link
	 * 
	 * @param {String}
	 *            value
	 * @return {Boolean}
	 */
	isFtpLink : function(value) {
		if (value.indexOf("ftp:") == 0) {
			return true;
		}
		return false;
	},

	/**
	 * Returns a html link from a http url
	 * 
	 * @param {String}
	 *            value
	 * @return {String} a href html tag
	 */
	toHttpHref : function(value) {

		var newValue = "<a href='" + value + "' target=_blank>" + value
				+ "</a>";
		return newValue;

	},

	/**
	 * Returns a html link from a doi url
	 * 
	 * @param {String}
	 *            value
	 * @return {String} a href html tag
	 */
	toDoiHref : function(value) {

		var url = value.replace("doi:", "http://dx.doi.org/");

		var newValue = "<a href='" + url + "' target=_blank>" + value + "</a>";
		return newValue;

	},
/**
	 * Replace line breaks by <br />
	 * 
	 * @param {String}
	 *            value
	 * @return {String} 
	 */
	replaceNewLines : function(str) {
		return str.replace(/(?:\r\n|\r|\n)/g, '<br />');
	}


});
