Ext
		.define(
				"BP.globals.Result",
				{
					singleton : true,

					/**
					 * Displays the result in a new tab
					 */
					displayResult : function(url) {

						var my = this;

						var jsonPath = url + "/results.json";

						BP.globals.Ajax
								.send({
									url : jsonPath,
									waitMessage : "Displaying results...",
									successValue : undefined,

									successFunction : function(json) {
										if (Ext.isDefined(json.analysis)) {

											var resultsPanel = Ext
													.create("BP.view.panel.V_ResultsPanel");

											resultsPanel.setFromJson(json);

											var mainPanel = Ext.ComponentQuery
													.query("mainPanel")[0];
											mainPanel.add(resultsPanel);
											mainPanel
													.setActiveTab(resultsPanel);

											/*
											 * Check if the query builder is
											 * build
											 * 
											 * The analysis name is in the json
											 * path
											 * 
											 * ... / ... / analysis_name /
											 * results.json
											 * 
											 */
											var a_elts = jsonPath.split("/");
											var analysisName = a_elts[a_elts.length - 2];
											var urlCheck = BP.globals.Utils
													.getHostUrl()
													+ "/ws/"
													+ analysisName
													+ "/status/exists";

											BP.globals.Ajax
													.send({
														url : (DEBUG) ? "resources/test/json/checkQueryBuilder.json" : urlCheck,
														waitMessage : "check query builder",
														successValue : "true",

														successFunction : function(
																jsonCheck) {

															if (jsonCheck.exists == "false") {

																var button = Ext
																		.create(
																				"BP.view.button.V_BuildQueryBuilder",
																				{
																					analysisName : analysisName
																				});

																resultsPanel
																		.addDocked({
																			xtype : 'toolbar',
																			items : [ button ]
																		});

															} else {
																var button = Ext
																		.create("BP.view.button.V_QueryBuilder", {
																			analysisName : analysisName,
																			analysisTitle : json.analysis.title
																		});

																var button2 = Ext
																		.create("BP.view.button.V_RemoveQueryBuilder", {
																			analysisName : analysisName
																		});

																resultsPanel
																		.addDocked({
																			xtype : 'toolbar',
																			items : [
																					button,
																					//~ button2 
																					]
																		});
															}

															/*
															 * Displays the
															 * global parameter
															 * grid
															 */
															var globalParameters = [
																	{
																		'key' : 'Contact',
																		'value' : json.analysis.contact
																	},
																	{
																		'key' : 'Date',
																		'value' : json.analysis.date
																	} ];

															var storeGlobalParameters = Ext
																	.create(
																			"BP.store.S_KeyValue",
																			{
																				data : globalParameters
																			});

															var gridGlobalParameters = Ext
																	.create(
																			"BP.view.grid.V_KeyValueGrid",
																			{
																				title : "Global parameters",
																				store : storeGlobalParameters,
																				hideHeaders : true
																			});

															resultsPanel
																	.add(gridGlobalParameters);

															/*
															 * Displays the
															 * homologygroups
															 * parameter grid
															 */

															if (Ext
																	.isDefined(json.analysis.homologygroups)) {

																var homologygroupsParameters = [
																		{
																			'key' : 'Outfile',
																			'value' : my
																					.createLink(
																							json.analysis.homologygroups.outfile,
																							url)
																		},
																		{
																			'key' : 'Version',
																			'value' : json.analysis.homologygroups.version
																		},
																		{
																			'key' : 'Parameters',
																			'value' : json.analysis.homologygroups.parameters
																		} ];

																var storeHomologygroupsParameters = Ext
																		.create(
																				"BP.store.S_KeyValue",
																				{
																					data : homologygroupsParameters
																				});

																var gridHomologygroupsParameters = Ext
																		.create(
																				"BP.view.grid.V_KeyValueGrid",
																				{
																					title : "Homology groups parameters",
																					store : storeHomologygroupsParameters,
																					hideHeaders : true
																				});

																resultsPanel
																		.add(gridHomologygroupsParameters);
															}

															/*
															 * Grid of proteomes
															 */
															if (Ext
																	.isDefined(json.analysis.proteomes)) {

																var proteomes = json.analysis.proteomes;

																var dataProteomes = [];

																for ( var code in proteomes) {

																	var proteome = proteomes[code];
																	proteome["code"] = code;
																	proteome["fasta"] = my
																			.createLink(
																					proteome["fasta"],
																					url);

																	proteome["iprscan"] = "";

																	if (Ext
																			.isDefined(proteome["iprscan"])) {

																		proteome["iprscan"] = my
																				.createLink(
																						proteome["iprscan"],
																						url);
																	}
																	Ext.Array
																			.push(
																					dataProteomes,
																					proteome);
																}

																var storeProteomes = Ext
																		.create(
																				"BP.store.S_Proteome",
																				{
																					data : dataProteomes
																				});

																var gridProteomes = Ext
																		.create(
																				"BP.view.grid.V_GridProteomes",
																				{
																					title : "Proteomes",
																					store : storeProteomes
																				});

																resultsPanel
																		.add(gridProteomes);

															}

															/*
															 * Grid of results
															 */
															if (Ext
																	.isDefined(json.results)) {

																var results = json.results;

																var storeResults = Ext
																		.create(
																				"BP.store.S_TreeResult",
																				{
																					root : results.root
																				});

																var gridResult = Ext
																		.create(
																				"BP.view.grid.V_TreeGridResult",
																				{
																					title : "Results",
																					store : storeResults,
																					basePath : url
																				});

																resultsPanel
																		.add(gridResult);
																
																gridResult.updateLayout();
																
															}
														}
													});
										}
									}
								});

					},

					/**
					 * create a a href from
					 */
					createLink : function(relative_path, url) {

						var str = relative_path.replace("./", "");

						return ('<a target="_blank" href="' + url + "/" + str
								+ '">' + str + '</a>');

					},
					
					/**
					 * Sends a query to the query builder and displays a result panel
					 * 
					 * @param parentPanel : panel to insert the result panel
					 * @param idx : index to insert the result panel
					 * @param analysisName
					 * @param target : groups or proteins
					 * @param query : accession or annotation
					 * @param acc : query text
					 * @param pageSize : number of results in the grid
					 * 
					 */
					displayQueryResult : function(parentPanel, idx, analysisName, owner, target, query, acc, pageSize ) {
						
						
						var url = BP.globals.Utils.getHostUrl() + "/ws/" + analysisName
										+ "/get/" + target + "?" + query + "=" + acc;

						if (owner)
						{
							url += '&owner=' + owner;
						}
						
						BP.globals.Ajax.send({
							url : url,
							waitMessage : "Search " + target,
							successValue : "true",

							params : {
								limit : pageSize,
								start : 0
							},

							successFunction : function(json) {

								

								if (json.totalCount == 0) {
									Ext.Msg.alert("Message", "No result");
								} else {
									var resultGrid = Ext.create(
											"BP.view.querybuilder.V_QueryResultGrid", {
												json : json,
												pageSize : pageSize,
												page : 1,
												url : url
											});

									var resultPanel = Ext.create(
											"BP.view.querybuilder.V_QueryResultPanel", {
												title : "List of " + target + " with " + query
														+ " containing " + decodeURIComponent(acc),
												closable : true,
												collapsible : true
											});

									resultPanel.add(resultGrid);

									parentPanel.insert(idx, resultPanel);
									

								}
							}

						});
					}
					
					
				});
