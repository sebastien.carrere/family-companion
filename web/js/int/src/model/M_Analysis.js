Ext.define('BP.model.M_Analysis', {
	extend : 'Ext.data.Model',

	fields : [ {
		name : 'title',
		type : 'string'
	}, 
	{
		name : 'date',
		type : 'string'
	},
	
	{
		name : 'log',
		type : 'string'
	}, {
		name : 'url',
		type : 'string'
	}, {
		name : 'status',
		type : 'string'
	},
	{
		name : 'public',
		type : 'bool'
	}

	]
});