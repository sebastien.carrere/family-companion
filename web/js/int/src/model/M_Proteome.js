Ext.define('BP.model.M_Proteome', {
	extend : 'Ext.data.Model',

	fields : [ {
		name : 'code',
		type : 'string'
	}, {
		name : 'title',
		type : 'string'
	}, {
		name : 'fasta',
		type : 'string'
	}, {
		name : 'reference',
		type : 'boolean'
	}, {
		name : 'iprscan',
		type : 'string'
	} ]
});