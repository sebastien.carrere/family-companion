Ext.define('BP.store.S_Analyses', {
	extend : 'BP.store.S_GenericStore',

	requires : [],

	model : 'BP.model.M_Analysis',

	autoLoad : false,
	
	checkLogin : true,

	sorters : {
		property : 'date',
		direction : 'DESC'
	},

	initProxy : function() {
		return Ext.apply(this.callParent(), {
			type : 'ajax',
			url : '../cgi/index.cgi',
			extraParams : {
				"__wb_function" : "ListAnalyses",
				"owner" : BP.globals.ConnectionData.owner
			},

			reader : {
				type : 'json',
				rootProperty : 'analyses',
				successProperty : 'success'
			},
		});
	}

});