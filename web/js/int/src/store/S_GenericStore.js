/**
 * Generic class for all Archive stores
 * 
 * Load generic parameters and check if the user is logged if needed
 * 
 */
Ext.define('BP.store.S_GenericStore', {
	extend : 'Ext.data.Store',

	/**
	 * if true, check if the user is logged before sending the query
	 */
	checkLogin : false,
	

	constructor : function(config) {

		if (!Ext.isDefined(config)) {
			config = {};
		}
		config.proxy = this.initProxy();
		this.callParent(arguments);
	},


	// Inits the proxy
	initProxy : function() {
		
		var myStore = this;
		
		return {
			type : 'ajax',
			$configStrict : false,

			timeout : 240 * 1000,
			'afterRequest' : function(req, success) {
				
				var json = Ext.decode(req._operation._response.responseText);

				if (myStore.checkLogin && Ext.isDefined(json["login"])
						&& json["login"] == "false") {
					BP.globals.Login.logout();
				}

			},
			listeners : {
				/*
				 * function called when exception is found
				 */
				'exception' : function(proxy, response, operation, eOpts) {

					var win = Ext.create("Ext.window.MessageBox");

					if (response.status !== 200) {
						win.alert("Failed", "Server Error. Status: "
								+ response.status);
					} else {
						var responseText = Ext.decode(response.responseText);
						win.alert("Failed", responseText.message);
					}
				}
				
			}

		}
	}
});