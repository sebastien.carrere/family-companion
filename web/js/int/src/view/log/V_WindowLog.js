Ext.define('BP.view.log.V_WindowLog', {
    extend: 'Ext.window.Window',
    alias: 'widget.windowLog',


    width: 400,
    height: 400,
    

    layout:'fit',

    constrain : true,


    constructor: function(params) {

        var log = params.log;

        var v_panel = Ext.create("Ext.panel.Panel", { html: log, autoScroll : true });

        this.items = [v_panel];

        this.callParent([params]);


    }

});