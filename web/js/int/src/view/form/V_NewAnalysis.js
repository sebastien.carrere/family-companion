Ext
    .define(
        'BP.view.form.V_NewAnalysis', {
            extend: 'BP.view.form.V_CustomForm',
            requires: ['BP.globals.ConnectionData',
							'BP.view.form.V_ProteomesFieldSet',
							'BP.view.form.V_UploadFile',
							'BP.view.form.V_NumberField',
							'BP.view.form.V_CustomFieldSet'],

            alias: 'widget.newAnalysis',
            resizable: false,
            closable: false,
            autoScroll: true,
            bodyPadding: 5,
            border: false,
            title: "New analysis",

            fieldDefaults: {
                labelAlign: 'left',
                margin: '10 10 10 10',
                labelWidth: 100,
                allowBlank: true
            },

            items: [

                {
                    xtype: "container",
                    layout: "hbox",
                    items: [{
                            xtype: "button",
                            text: "Launch analysis",
                            action: "launchAnalysis",
                            margin: "0 0 0 10",
                            formBind: false
								}, {
                            xtype: "button",
                            text: "Reset",
                            action: "reset",
                            margin: "0 0 0 10",
                            formBind: false
								},
                        {
                            xtype: "button",
                            cls: "icon-bbric-info",
                            tooltip: 'File formats',
                            margin: "0 0 0 10",
                            handler: function() { window.open('https://framagit.org/BBRIC/family-companion/blob/527146eac34788169853c1fd053f7950229d140f/doc/fileFormats.md', '_blank'); }
								}

								]
							},
                {
                    xtype: "uploadFile",
                    flex: 1,
                    title: "Input data zip file",
                    allowBlank: true,
                    fileLabel: 'Select a zip file',
                    buttonLabel: 'Upload',
                    uploadAction: 'uploadZipFile',
                    pathVariable: "dataFile"
							},
                {
                    xtype: "customFieldSet",
                    title: "Analysis description",
                    collapsible: true,
                    collapsed: false,
                    titleWidth: 15,
                    items: [
                        {
                            xtype: "textfield",
                            regex: /^[;&'>0-9a-zA-Z:_.@+\#\-\s=,\"\?\(\)%~*!\[\]\{\}]*$/,
                            regexText: "<b>Error</b></br>allowed characters are [;&'>0-9a-zA-Z:_.@+\#\-\s=,\"\?\(\)%~*!\[\]\{\}]",
                            name: "analysis_title",
                            fieldLabel: 'Title',
                            allowBlank: false
										},
                        {
                            xtype: "textareafield",
                            regex: /^[;&'>0-9a-zA-Z:_.@+\#\-\s=,\"\?\(\)%~*!\[\]\{\}\ ]*$/,
                            regexText: "<b>Error</b></br>allowed characters are [;&'>0-9a-zA-Z:_.@+\#\-\s=,\"\?\(\)%~*!\[\]\{\}\ ]",
                            name: "analysis_description",
                            fieldLabel: 'Description'
										}]
							},
                {
                    xtype: "customFieldSet",
                    title: "General parameters",
                    titleWidth: 15,
                    collapsible: true,
                    collapsed: false,
                    items: [{
                        xtype: "checkboxfield",
                        name: "addIprscanResult",
                        action: "addIprscanResult",
                        fieldLabel: 'Add IprScan results ?'
								}]
							},
                {
                    xtype: "customFieldSet",
                    titleWidth: 15,
                    title: "Homology groups",
                    layout: {
                        type: 'hbox',
                        align: 'stretchmax'
                    },
                    collapsible: true,
                    collapsed: false,
                    items: [
                        {
                            xtype: "uploadFile",
                            title: "Homology groups result file",
                            flex: 1,
                            allowBlank: true,
                            labelWidth: 200,
                            fileLabel: 'Select an OrthoMCL/OrthoFinder/Synergy file',
                            buttonLabel: 'Upload homology group File',
                            uploadAction: 'uploadHomologyGroupsFile',
                            pathVariable: "pathHomologyGroups"
										},
                        {
                            xtype: 'splitter'
										},
                        {
                            xtype: 'component',
                            html: 'OR'
										},
                        {
                            xtype: 'splitter'
										},

                        {
                            xtype: "customFieldSet",
                            name: "homologygroups_parameters",
                            flex: 1,
                            title: "Parameters",
							height:200,
                            items: [
                                {
                                    xtype: "custom_numberfield",
                                    allowDecimals: true,
                                    name: "pvCutoff",
                                    fieldLabel: 'pv_cutoff',
                                    qtip: "P-Value or E-Value Cutoff in BLAST search and/or ortholog clustering, 1e-5 (DEFAULT).",
                                    value: 0.00001,
                                    decimalPrecision: 10,
                                    allowBlank: false
													},
                                {
                                    xtype: "custom_numberfield",
                                    allowDecimals: true,
                                    name: "piCutoff",
                                    fieldLabel: 'pi_cutoff',
                                    qtip: "Percent Identity Cutoff <0-100> in ortholog clustering, 0 (DEFAULT)",
                                    minValue: 0,
                                    maxValue: 100,
                                    value: 0,
                                    allowBlank: false
													},
                                {
                                    xtype: "custom_numberfield",
                                    allowDecimals: true,
                                    name: "pMatchCutoff",
                                    fieldLabel: 'pmatch_cutoff',
                                    qtip: "Percent Match Cutoff <0-100> in ortholog clustering, 0 (DEFAULT).",
                                    minValue: 0,
                                    maxValue: 100,
                                    value: 80,
                                    allowBlank: false

													},
                                {
                                    xtype: "custom_numberfield",
                                    allowDecimals: true,
                                    name: "inflation",
                                    fieldLabel: 'inflation',
                                    value: 1.5,
                                    qtip: "Markov Inflation Index, used in MCL algorithm, 1.5 (DEFAULT). Increasing this index increases cluster tightness, and the number of clusters.",
                                    allowBlank: false
													}]
										}]
							}, {
                    xtype: "proteomesFieldSet",
                    titleWidth: 15
							}, {
                    xtype: "container",
                    layout: "hbox",
                    items: [{
                            xtype: "button",
                            text: "Launch analysis",
                            action: "launchAnalysis",
                            formBind: false
								}, {
                            xtype: "button",
                            text: "Reset",
                            action: "reset",
                            formBind: false
								}

								]
							},

					],

            /**
             * Fills the form from json data
             */
            setFromJson: function(json) {

                if(json.zipdata) {

                    data = json.zipdata.proteomes;

                    var proteomes = [];

                    if(data) {

                        var nb = 0;
                        for(var key in data) {
                            var removeButton = true;

                            if(nb == 0) {
                                removeButton = false;
                            }

                            var proteomeFieldSet = Ext.create(
                                "BP.view.form.V_ProteomeFieldSet", {
                                    title: key,
                                    collapsed: true,
                                    removeButton: removeButton
                                });

                            var fastaPath = data[key]["fasta"];
                            if(!fastaPath) {
                                Ext.Msg
                                    .alert(
                                        'Failed',
                                        "The proteome " +
                                        key +
                                        " does not have fasta file path");
                                return;
                            }

                            /**
                             * sets the fasta path in the form
                             */
                            var fastaForm = proteomeFieldSet
                                .down('uploadFile');

                            var fastaFileField = fastaForm
                                .down("filefield");
                            fastaFileField.setDisabled(true);

                            var displayFieldFasta = fastaForm
                                .down("displayfield");
                            displayFieldFasta
                                .setValue(data[key]["fasta"]
                                    .replace(/\\/g, '/')
                                    .replace(/.*\//, ''));

                            var fastaPathField = fastaForm
                                .down("hiddenfield");
                            fastaPathField.setValue(data[key]["fasta"]);

                            /**
                             * Sets the iprscan path in the form
                             */
                            var iprScanPath = data[key]["iprscan"];

                            if(iprScanPath) {
                                this.query('[name="addIprscanResult"]')[0]
                                    .setValue(true);

                                var iprScanForm = proteomeFieldSet
                                    .down('iprScanUpload');

                                var iprScanFileField = iprScanForm
                                    .down("filefield");
                                iprScanFileField.setDisabled(true);

                                var displayField = iprScanForm
                                    .down("displayfield");

                                displayField
                                    .setValue(data[key]["iprscan"]
                                        .replace(/\\/g, '/')
                                        .replace(/.*\//, ''));

                                var path = data[key]["iprscan"];

                                var pathField = iprScanForm
                                    .down("hiddenfield");

                                pathField.setValue(path);

                            }

                            proteomeFieldSet.query('[name=code]')[0]
                                .setValue(key);

                            if(data[key]["title"]) {
                                proteomeFieldSet
                                    .query('[name="proteomeDescription"]')[0]
                                    .setValue(data[key]["title"]);
                            }

                            if(data[key]["reference"]) {

                                if(data[key]["reference"] > 0) {
                                    proteomeFieldSet
                                        .query('[name="isReference"]')[0]
                                        .setValue(true);
                                }

                            }

                            proteomes.push(proteomeFieldSet);

                            nb++;

                        }
                        if(proteomes.length > 0) {
                            var proteomesField = this
                                .query("proteomesFieldSet")[0];
                            proteomesField.removeAll();

                            Ext.Array.each(proteomes,
                                function(proteome) {
                                    proteomesField.add(proteome);
                                });

                        }

                    }

                    // homologygroups data
                    if(json.zipdata.homologygroups) {

                        if(json.zipdata.homologygroups.pi_cutoff) {
                            this.query('[name="piCutoff"]')[0]
                                .setValue(json.zipdata.homologygroups.pi_cutoff);
                        }

                        if(json.zipdata.homologygroups.pv_cutoff) {
                            this.query('[name="pvCutoff"]')[0]
                                .setValue(json.zipdata.homologygroups.pv_cutoff);
                        }

                        if(json.zipdata.homologygroups.pmatch_cutoff) {
                            this.query('[name="pMatchCutoff"]')[0]
                                .setValue(json.zipdata.homologygroups.pmatch_cutoff);
                        }

                        if(json.zipdata.homologygroups.inflation) {
                            this.query('[name="inflation"]')[0]
                                .setValue(json.zipdata.homologygroups.inflation);
                        }

                        if(json.zipdata.homologygroups.outfile) {
                            var homologygroupsForm = this
                                .down('uploadFile[uploadAction="uploadHomologyGroupsFile"]');

                            var homologygroupsFileField = homologygroupsForm
                                .down("filefield");
                            homologygroupsFileField.setDisabled(true);

                            var displayField = homologygroupsForm
                                .down("displayfield");

                            displayField
                                .setValue(json.zipdata.homologygroups.outfile
                                    .replace(/\\/g, '/')
                                    .replace(/.*\//, ''));

                            var path = json.zipdata.homologygroups.outfile;

                            var pathField = homologygroupsForm
                                .down("hiddenfield");

                            pathField.setValue(path);
                        }

                    }

                    // Title
                    if(json.zipdata.title) {
                        this.down('[name="analysis_title"]').setValue(
                            json.zipdata.title);
                    }

                    // Description
                    if(json.zipdata.description) {
                        this.down('[name="analysis_description"]').setValue(
                            json.zipdata.description);
                    }

                    Ext.Msg.alert("Zip processed", json.message +
                        '.<br />Form filled with the zip data.');

                }

            }

        });