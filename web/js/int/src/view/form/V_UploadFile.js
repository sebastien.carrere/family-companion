Ext.define('BP.view.form.V_UploadFile', {
	extend : 'BP.view.form.V_CustomFieldSet',
	requires : ['BP.view.form.V_CustomForm'],
	alias : 'widget.uploadFile',
	
	config : {
		allowBlank : true,
		fileLabel : "File",
		buttonLabel : "Upload File",
		uploadAction : "uploadFile",
		pathVariable: "path"
	},

	/**
	 * Constructor
	 */
	constructor : function(params) {

//		this.initConfig();

		if (Ext.isDefined(params.allowBlank)) {
			this.setAllowBlank(params.allowBlank);
		}

		if (Ext.isDefined(params.fileLabel)) {
			this.setFileLabel(params.fileLabel);
		}

		if (Ext.isDefined(params.buttonLabel)) {
			this.setButtonLabel(params.buttonLabel);
		}

		if (Ext.isDefined(params.uploadAction)) {
			this.setUploadAction(params.uploadAction);
		}
		
		if (Ext.isDefined(params.pathVariable)) {
			this.setPathVariable(params.pathVariable);
		}


		this.callParent([ params ]);

	},

	/**
	 * 
	 */
	initComponent : function() {

		this.items = [ {
			xtype : "customForm",
			border:0,
			items : [ {
				xtype : "filefield",
				buttonOnly:true,
				clearOnSubmit:false,
				name : "dataFile",
				fieldLabel : this.getFileLabel(),
				labelWidth : this.labelWidth,
				allowBlank : this.getAllowBlank()
			},{
				xtype: 'displayfield',
		        fieldLabel: 'File name',
		        name: 'filename',
		        value: 'No file defined'
			}, 
			{
				xtype : "hiddenfield",
				name : this.getPathVariable(),
				value : ""

			} ]
		} ];

		this.callParent(arguments);

	}

});