Ext
    .define(
        'BP.view.form.V_ProteomeFieldSet', {
            extend: 'BP.view.form.V_CustomFieldSet',
            requires: ['BP.view.form.V_IprScanUpload'],
            alias: 'widget.proteomeFieldSet',
            collapsible: true,
            collapsed: true,

            config: {
                /**
                 * If true, the remove button is enabled
                 */
                removeButton: true
            },

            layout: {
                type: 'hbox',
                align: 'stretchmax'
            },

            /**
             * constructor
             */
            constructor: function(params) {
                // this.initConfig();

                if(Ext.isDefined(params.removeButton)) {
                    this.setRemoveButton(params.removeButton);
                }

                var iprScanCheckBox = Ext.ComponentQuery.query("checkbox[name=addIprscanResult]")[0];

                var disabled = !iprScanCheckBox.getValue();

                var iprScanUpload = Ext.create(
                    "BP.view.form.V_IprScanUpload", {
                        disabled: disabled,
                        flex: 1,
                        title: "IprScan file",
                        allowBlank: false,
                        fileLabel: 'Select an IprScan file',
                        buttonLabel: 'Upload IprScan File',
                        uploadAction: 'uploadIprScanFile',
                        pathVariable: "pathIprScan"
                    });

                this.items = [

                    {
                        xtype: "customFieldSet",
                        flex: 1,
                        title: "Proteome description",
                        height: 200,
                        items: [
                            {
                                xtype: "textfield",
                                regex: /^[;&'>0-9a-zA-Z:_.@+\#\-\s=,\"\?\(\)%~*!\[\]\{\}]*$/,
                                regexText: "<b>Error</b></br>allowed characters are [;&'>0-9a-zA-Z:_.@+\#\-\s=,\"\?\(\)%~*!\[\]\{\}]",
                                name: "code",
                                fieldLabel: 'Code',
                                allowBlank: false
											},
                            {
                                xtype: "textareafield",
                                regex: /^[;&'>0-9a-zA-Z:_.@+\#\-\s=,\"\?\(\)%~*!\[\]\{\}\ ]*$/,
                                regexText: "<b>Error</b></br>allowed characters are [;&'>0-9a-zA-Z:_.@+\#\-\s=,\"\?\(\)%~*!\[\]\{\}\ ]",
                                name: "proteomeDescription",
                                fieldLabel: 'Proteome description'
											},

                            {
                                xtype: "checkboxfield",
                                name: "isReference",
                                fieldLabel: 'Is reference ?',
                                value: "off",
                                checked: false,
                                uncheckedValue: 'false',
                                inputValue: 'true'
											}

									]
								}, {
                        xtype: 'splitter'
								},

                    {
                        xtype: "uploadFile",
                        flex: 1,
                        title: "Proteome file",
                        allowBlank: false,
                        fileLabel: 'Select a fasta file',
                        buttonLabel: 'Upload Fasta File',
                        uploadAction: 'uploadFastaFile',
                        pathVariable: "pathProteome"
								}, {
                        xtype: 'splitter'
								},
                    iprScanUpload,

                    {
                        xtype: "button",
                        iconCls: "icon-cancel",
                        action: "removeProteome",
                        cls: 'my-btn',
                        disabled: false,
                        maxHeight: 24

								}

						];

                this.callParent([params]);

            },

            /**
             * 
             */
            initComponent: function() {

                var disabled = !this.getRemoveButton();

                this.callParent(arguments);

                var button = this
                    .query("button[action=removeProteome]")[0];

                button.setDisabled(disabled);

            }

        });