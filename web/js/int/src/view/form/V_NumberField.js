Ext.define('BP.view.form.V_NumberField', {
	extend : 'Ext.form.field.Number',
	requires : [],
	alias : 'widget.custom_numberfield',
	
	hideTrigger: true,
    keyNavEnabled: false,
    mouseWheelEnabled: false,
    width:200,

	listeners : {
		render : function(c) {
			
			Ext.create('Ext.tip.ToolTip', {
				anchor : "right",
				// margin : "0 0 0 100",
				target : c.getEl().id,
				trackMouse : false,
				// autoHide:false,
				// closable : true,
				title : name,
				html : c.qtip,
				width : 200
			});
			
		
		}
	}
});