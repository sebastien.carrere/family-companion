Ext.define('BP.view.form.V_ProteomesFieldSet', {
	extend : 'BP.view.form.V_CustomFieldSet',
	requires : [ "BP.view.form.V_ProteomeFieldSet" ],
	alias : 'widget.proteomesFieldSet',

	title : "Proteomes",
	collapsible : true,

	nb : 1,

	items : [
	{
		xtype : "proteomeFieldSet",
		title : "Proteome 1",
		collapsed : false,
		removeButton : false
	}, {
		xtype : "button",
		text : "Add Proteome",
		action : "addProteome"
	} ],

	initComponent : function() {

		this.callParent(arguments);

	}

});