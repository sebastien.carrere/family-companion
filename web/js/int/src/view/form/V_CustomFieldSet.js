Ext
		.define(
				'BP.view.form.V_CustomFieldSet',
				{
					extend : 'Ext.form.FieldSet',
					alias : 'widget.customFieldSet',

					margin:5,
					
					config : {
						titleWidth : 11
					},

					
					/**
					 * 
					 */
					constructor : function(params) {

//						this.initConfig();

						if (Ext.isDefined(params.titleWidth)) {
							this.setTitleWidth(params.titleWidth);
						}
						
						this.callParent([ params ]);
					},

					/**
					 * 
					 */
					initComponent : function() {
						this
								.on(
										'beforeadd',
										function(me, field) {
											if (!field.allowBlank
													&& field.xtype != "checkboxfield"
													&& field.xtype != "displayfield")
												field.labelSeparator += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
										});

						this.on('beforerender', function(me) {

							me.setTitle('<span style="font-size:'
									+ me.getTitleWidth() + ';">' + me.title
									+ '</span>');

						});

						this.callParent(arguments);
					}

				});