/**
 * Button to build query builder
 */
Ext.define('BP.view.button.V_QueryBuilder', {
	extend : 'Ext.button.Button',
	alias : 'widget.queryBuilderButton',

	text : "Query builder",
	
	
	
	layout : {
		type : "vbox",
		padding : 5,
		margin : 5,
		align : "stretch"
	}
	

});