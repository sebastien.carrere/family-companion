/**
 * Button to remove query builder
 */
Ext.define('BP.view.button.V_RemoveQueryBuilder', {
	extend : 'Ext.button.Button',
	alias : 'widget.removeQueryBuilderButton',

	text : "Remove Query builder"

});