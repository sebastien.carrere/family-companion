/**
 * Button to build query builder
 */
Ext.define('BP.view.button.V_BuildQueryBuilder', {
	extend : 'Ext.button.Button',
	alias : 'widget.buildQueryBuilderButton',

	text : "Build Query builder"

});