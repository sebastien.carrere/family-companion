Ext.define('BP.view.grid.V_TreeGridResult', {
	extend : 'Ext.tree.Panel',
	requires : [],
	alias : 'widget.gridResult',
	resizable : false,
	closable : false,
	padding : '5',
	title : "Results",
	autoScroll : false,

	useArrows : true,
	multiSelect : true,
	singleExpand : true,
	rootVisible : false,

	// path to build the link
	basePath : "",

	collapsible : true,
	collapsed : true,

	margin : 5,

	/*
	 * Events for the action columns
	 */
//	bubbleEvents : [ 'displayInfo' ],

	columns : [
//			{
//				xtype : 'actioncolumn',
//				text : 'links',
//				menuDisabled : true,
//				width : 100,
//				items : [ {
//					iconCls : iconCls,
//					tooltip : tooltip,
//					handler : function(g, rowIndex, colIndex, item, e, record) {
//
//						var win = Ext.create
//
//					}
//				} ]
//			},
			{
				text : "Status",
				dataIndex : "status",
				width : 50,
				renderer : function(currentCellValue, metadata, record,
						rowIndex, colIndex, store, view) {

					if (currentCellValue != "success") {
						cls = "error";
					} else {
						cls = "success";
					}

					metadata.tdCls = cls;

					return "";
				}
			},
			{
				text : 'Name',
				xtype : 'treecolumn',
				dataIndex : 'name',
				flex : 1,
				renderer : function(value, metaData, record, rowIndex,
						colIndex, store, view) {

					var message = record.data.message;
					if (message == "") {
						message = record.data.description;
					}

					message = Ext.String.htmlEncode(message);

					var path = view.up("panel")["basePath"];

					var link = record.data["link"];

					if (link != null && link != "") {
						return '<a target=_blank href="' + path + "/" + link
								+ '">' + value + '</a>';
					} else {
						return value;
					}

				}
			},
			{
				text : 'Description',
				dataIndex : 'description',
				flex : 2,
				renderer : function(value, metaData, record, rowIndex,
						colIndex, store, view) {

					var message = record.data.message;
					if (message == "") {
						message = record.data.description;
					}

					message = Ext.String.htmlEncode(message);

					return value;

				}
			}

	]

// initComponent : function() {
// Ext.apply(this, {
// columns : [ {
// text : 'Name',
// xtype : 'treecolumn',
// dataIndex : 'name',
// width : 200,
// renderer : function(value) {
// return '<span style="font-weight:bold;">' + value
// + "</span>";
// }
// } ]
// });
//
// this.callParent();
//
// }

});