Ext.define('BP.view.grid.V_KeyValueGrid', {
	extend : 'Ext.grid.Panel',
	requires : [],
	alias : 'widget.keyValueGrid',
	resizable : false,
	closable : false,
	padding : 5,
//	width : "100%",
	title : "Global parameters",
	autoScroll:false,
//	forceFit : true,,
	autoHeight:true,
	
	collapsible : true,
	collapsed : true,
	
	margin:5,
	
	
	columns : [ {
		text : 'key',
		dataIndex : 'key',
		width:100,
		renderer : function(value) {
			return '<span style="font-weight:bold;">'+value+"</span>";
		}
	}, {
		text : 'value',
		dataIndex : 'value',
		flex:1
	}

	]

});