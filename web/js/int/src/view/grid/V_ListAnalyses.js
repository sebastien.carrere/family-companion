Ext
    .define(
        'BP.view.grid.V_ListAnalyses', {
            extend: 'Ext.grid.Panel',
            requires: ['BP.globals.Utils'],
            alias: 'widget.listAnalyses',
            resizable: false,
            closable: false,
            autoScroll: true,
            bodyPadding: 5,
            //					width : "100%",

            store: 'S_Analyses',

            title: "List of analyses",

            /**
             * Event for the view action column
             */
            bubbleEvents: ['viewresult', 'deleteresult'],

            dockedItems: [{
                xtype: 'toolbar',
                items: [{
                    iconCls: 'icon-reload',
                    text: 'Reload',
                    action: 'reload'
						}]
					}],

            columns: [
                {
                    text: "status",
                    dataIndex: "status",
                    flex: 2,
                    align: "left",
                    renderer: function(currentCellValue, metadata,
                        record, rowIndex, colIndex, store, view) {
                        if(currentCellValue == "error") {
                            metadata.style = "background-color:red;";
                        } else if(currentCellValue == "log") {
                            metadata.style = "background-color:orange;";
                        } else if(currentCellValue == "success") {
                            metadata.style = "background-color:green;";
                        }
                        return "";
                    }
							},
                {
                    xtype: 'checkcolumn',
                    align: "left",
                    text: 'public',
                    dataIndex: 'public',
                    flex: 2,
                    listeners: {
                        checkchange: function(col, idx, isChecked) {

                            var view = this.up("listAnalyses").getView();

                            var record = view.getRecord(view.getNode(idx));

                            this.up("listAnalyses").fireEvent(
                                "publish", record,
                                idx);

                            this.up("listAnalyses").update();

                        }
                    },
							},
                {
                    xtype: 'actioncolumn',
                    text: "actions",
                    align: "left",
                    flex: 2,
                    items: [
                        {
                            iconCls: "icon-eye",
                            tooltip: 'View results files',
                            handler: function(grid, rowIndex,
                                colIndex, item, e, record,
                                row) {

                                /**
                                 * Controlled in C_ViewResult
                                 */
                                grid.up('panel').fireEvent(
                                    "viewresult", record,
                                    rowIndex);
                            }
										},
                        {
                            iconCls: "icon-delete",
                            tooltip: 'Delete analysis',
                            handler: function(grid, rowIndex,
                                colIndex, item, e, record,
                                row) {

                                Ext.MessageBox
                                    .confirm(
                                        'Confirm',
                                        'Are you sure you want to delete this analysis?',
                                        function(button) {

                                            if(button == "yes") {
                                                /**
                                                 * Controlled in C_ViewResult
                                                 */
                                                grid.up('panel').fireEvent(
                                                    "deleteresult", record,
                                                    rowIndex);
                                            }

                                        });

                            },

										},
                        {
                            iconCls: "icon-script_gear",
                            tooltip: 'Complete log',

                            handler: function(grid, rowIndex,
                                colIndex, item, e, record,
                                row) {

                                var v_win = Ext.create("BP.view.log.V_WindowLog", { title: record.data.title + "'s log", log: BP.globals.Utils.replaceNewLines(record.data.log) });
                                v_win.show();

                            },

										},

								]
							},
                {
                    text: "url",
                    align: "left",
                    dataIndex: "url",
                    flex: 1,
                    renderer: function(value) {
                        if(value != "") {

                            var a_path = value.split("/");

                            var path = a_path[a_path.length - 3] +
                                "/" +
                                a_path[a_path.length - 2] +
                                "/" +
                                a_path[a_path.length - 1]

                            return "<a href='" +
                                "../cgi/index.cgi?download=" +
                                path +
                                "' target=_blank>zip</a>";
                        }
                    },

							}

							, {
                    text: "title",
                    align: "left",
                    dataIndex: "title",
                    flex: 4
							}, {
                    text: "date",
                    align: "left",
                    dataIndex: "date",
                    flex: 4
							}, {
                    text: "log's last line",
                    align: "left",
                    dataIndex: "log",
                    flex: 8,
                    renderer: function(value, metaData, record,
                        rowIndex, colIndex, store, view) {

                        var a_lines = record.data.log.split("\n");

                        var lastLine = a_lines[a_lines.length - 2];

                        metaData.tdAttr = 'data-qtip="' +
                            lastLine + '"';

                        return lastLine;

                    }
							}

					]

        });