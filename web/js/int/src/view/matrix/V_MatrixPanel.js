Ext.define('BP.view.matrix.V_MatrixPanel', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.matrixPanel',
	requires : [],
	layout : {
		type : "hbox",
		align : "stretch"
	},
	autoScroll : true,
	resizable : false,
	closable : true,

	url : "",

	/**
	 * 
	 */
	constructor : function(params) {

		this.url = params.url;

		this.callParent([ params ]);

	},

	/**
	 * 
	 */
	initComponent : function() {

		var panel = this;

		this.callParent(arguments);

		var heatMap = Ext.create("BP.view.matrix.V_HeatMap", {
			"url" : this.url,
			header : false,
			title : "Abundancy matrix"
		});

		var grid = Ext.create("BP.view.matrix.V_Card", {
			store : new Ext.data.Store({
				model : "BP.model.M_KeyValue",
				visible : false
			})
		});

		this.add(heatMap);

	}

});