/**
 * The main application viewport
 */

Ext.define('BP.view.Viewport', {
	extend : 'Ext.container.Viewport',
	layout : 'border',

	requires : [ 'BP.view.panel.V_BannerPanel', 'BP.view.panel.V_MainPanel' ],

	items : [ {
		xtype : 'mainPanel'
	} ],

	initComponent : function() {

		// load the parameters from the query string in the url
		var parameters = Ext.Object.fromQueryString(location.search);

		var display_banner = true;

		if ("banner" in parameters) {
			// displays or not the banner
			var banner = parameters.banner;

			if (banner == 0) {
				display_banner = false;
			}
		}

		if (display_banner == true) {
			this.items.push({
				xtype : 'bannerPanel'
			});
		}

		this.callParent(arguments);
	}

});