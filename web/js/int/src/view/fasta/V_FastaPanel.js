/**
 * Fasta panel
 */
Ext
		.define(
				'BP.view.fasta.V_FastaPanel',
				{
					extend : 'Ext.panel.Panel',
					alias : 'widget.fastaPanel',
					requires : [],

					layout : {
						type : "vbox",
						align : "stretch"
					},
					autoScroll : true,
					resizable : true,

					collapsible : true,

					title : "fasta",

					/**
					 * Constructor
					 * 
					 * Must have a json key
					 * 
					 */
					constructor : function(params) {

						if (!Ext.isDefined(params.json)) {
							console
									.log("lacks the json parameters in the constructor of the fasta panel");
						}

						var o_json = params.json;

						params.title = o_json.protein_accession;

						var sequence = o_json.sequence;

						var a_links = o_json.links;

						var header = ">" + o_json.protein_accession;

						for ( var key in o_json) {
							if (key != "sequence" && key != "links"
									&& key != "header") {
								if (o_json[key] != "") {
									header += " " + key + "=" + o_json[key];
								}
							}
						}

						if (Ext.isDefined(a_links) && a_links.length > 0) {

							var a_items = [];

							Ext.each(a_links, function(l) {

								var item = {
									xtype : "button",
									tooltip : l.desc,
									link : l.href
								};

								if (l.rel == "groups") {
									item.action = "getGroups";
									item.iconCls = "icon-G";
									item.objectName = o_json.homologyGroup;
								} else if (l.rel == "proteins") {
									item.action = "getProteins";
									item.iconCls = "icon-P";
									item.objectName = o_json.protein_accession
											+ " info";
								} 

								a_items.push(item);

							});

							var toolbar = Ext.create("Ext.toolbar.Toolbar", {
								dock : 'top',
								items : a_items
							});
							params.dockedItems = [ toolbar ];
						}

						params.items = [ {
							xtype : "textarea",
							margin : 5,
							height : 200,
							value : header + "\n" + sequence,
							editable : false
						} ];

						this.callParent([ params ]);

					}

				});
