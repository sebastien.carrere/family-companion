/**
 * window to display fasta
 */
Ext.define('BP.view.fasta.V_FastaWindow', {
	extend : 'Ext.window.Window',
	alias : 'widget.fastaWindow',

	title : "Fasta",

	height : 600,
	width : 800,
	autoRender : true,
	autoScroll : true,

	constrain : true,

	closable : true,

	layout : {
		type : 'vbox',
		align : 'stretch',
		margin : 5,
		padding : 5,
	},

	/**
	 * 
	 * constructor
	 * 
	 * Must contains a json object
	 * 
	 */
	constructor : function(params) {

		var multiFastaPanel = Ext.create("BP.view.fasta.V_MultiFastaPanel", {
			json : params.json
		});

		params.items=[multiFastaPanel];
		
		this.callParent([ params ]);
		
		
	}

});