Ext
    .define(
        'BP.view.querybuilder.V_QueryResultGrid', {
            extend: 'Ext.grid.Panel',
            alias: 'widget.queryResultGrid',

            padding: 5,
            margin: 5,

            /*
             * Events for the action columns
             */
            bubbleEvents: ['getfasta', 'getgroups'],

            /**
             * Constructor : creates the grid from a json
             * 
             * @private
             * 
             */
            constructor: function(params) {

                var grid = this;

                if(!Ext.isDefined(params.json)) {
                    console
                        .log("lacks the json parameters in the constructor of the grid");
                }

                var o_json = params.json;

                var a_fields = o_json.fields;

                Ext.each(a_fields, function(f) {

                    if(f.type == "array") {
                        f.type = "string";
                    }

                    if(f.type == "float") {
                        f.sortType = 'asFloat';
                    }

                    f.convert = function(value) {
                        return BP.globals.Utils.formatValue(value);
                    };

                });

                var a_data = o_json;

                var a_links = o_json.links;

                var store = Ext.create("Ext.data.Store", {
                    fields: a_fields,
                    autoLoad: {
                        start: params.start,
                        limit: params.pageSize,
                        owner: BP.globals.ConnectionData.owner
                    },
                    pageSize: params.pageSize,
                    proxy: {
                        type: "ajax",
                        url: params.url,
                        extraParams: {
                            owner: BP.globals.ConnectionData.owner
                        },
                        reader: {
                            type: "json",
                            rootProperty: "table",
                            totalProperty: "totalCount"
                        }
                    }
                });

                params.store = store;

                var a_columns = [];

                Ext.each(a_fields, function(field) {

                    a_columns.push({
                        text: field.pretty_name,
                        dataIndex: field.name,
                        flex: 1
                    });

                });

                if(a_links.length > 0) {
                    var actionColumn = {
                        xtype: 'actioncolumn',
                        text: 'links',
                        menuDisabled: true,
                        width: 100,
                        items: []
                    }

                    Ext.each(a_links, function(link) {

                        var iconCls = "icon-link";

                        if(link.rel == "sequences") {
                            iconCls = "icon-F";
                        } else if(link.rel == "groups") {
                            iconCls = "icon-G";
                        } else if(link.rel == "proteins") {
                            iconCls = "icon-P";
                        } else if(link.rel == "alignment") {
                            iconCls = "icon-A";
                        }

                        var tooltip = link.desc;

                        actionColumn.items.push({
                            iconCls: iconCls,
                            tooltip: tooltip,
                            handler: function(g, rowIndex, colIndex,
                                item, e, record) {

                                if(link.rel == "sequences") {
                                    grid.fireEvent("getfasta", record);
                                } else if(link.rel == "groups") {
                                    grid.fireEvent("getgroups", record,
                                        grid);
                                } else if(link.rel == "proteins") {
                                    grid.fireEvent("getproteins",
                                        record, grid);
                                } else if(link.rel == "alignment") {
                                    grid.fireEvent("getalignment",
                                        record, grid);
                                }

                            }
                        });

                    });

                }

                a_columns.push(actionColumn);

                params.columns = a_columns;

                var rawDataLink = null;
                var abundancyMatrixLink = null;
                var binaryMatrixLink = null;

                var a_links_metadata = o_json.metadata.links;

                if(Ext.isDefined(a_links_metadata)) {
                    Ext.each(a_links_metadata,
                        function(l) {
                            if(l.rel == "rawdata" &&
                                Ext.isDefined(l.href)) {
                                rawDataLink = l.href;
                            } else if(l.rel == "abundancy_matrix" &&
                                Ext.isDefined(l.href)) {
                                abundancyMatrixLink = l.href;
                            } else if(l.rel == "binary_matrix" &&
                                Ext.isDefined(l.href)) {
                                binaryMatrixLink = l.href;
                            }
                        });
                }

                var a_buttons = [];

                if(rawDataLink != null) {
                    a_buttons.push({
                        xtype: "button",
                        tooltip: "Get raw data",
                        iconCls: "icon-page_excel",
                        handler: function() {
                            window.open(rawDataLink, "_blank");
                        }
                    });
                }

                if(abundancyMatrixLink != null) {
                    a_buttons.push({
                        xtype: "button",
                        tooltip: "Display abundancy matrix",
                        iconCls: "icon-application_view_icons",
                        handler: function() {

                            var matrixPanel = Ext.create(
                                "BP.view.matrix.V_HeatMap", {
                                    "url": abundancyMatrixLink,
                                    title: "Abundancy matrix"
                                });

                            var mainPanel = Ext.ComponentQuery
                                .query("mainPanel")[0];

                            mainPanel.add(matrixPanel);

                            mainPanel.setActiveItem(matrixPanel);

                        }
                    });
                }

                if(binaryMatrixLink != null) {
                    a_buttons.push({
                        xtype: "button",
                        tooltip: "Display binary matrix",
                        iconCls: "icon-application_view_detail",
                        handler: function() {
                            var heatMap = Ext.create(
                                "BP.view.matrix.V_HeatMap", {
                                    "url": binaryMatrixLink,
                                    title: "Binary matrix"
                                });

                            var mainPanel = Ext.ComponentQuery
                                .query("mainPanel")[0];

                            mainPanel.add(heatMap);

                            mainPanel.setActiveItem(heatMap);
                        }
                    });
                }

                params.dockedItems = [{
                    xtype: 'pagingtoolbar',
                    store: store,
                    dock: 'bottom',
                    displayInfo: true
						}];

                if(a_buttons.length > 0) {
                    params.dockedItems.push({
                        dock: 'top',
                        xtype: 'toolbar',
                        items: a_buttons
                    });
                }

                this.callParent([params]);

            }
        });