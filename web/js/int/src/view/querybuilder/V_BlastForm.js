Ext.define('BP.view.querybuilder.V_BlastForm', {
	extend : 'Ext.form.Panel',

	alias : 'widget.blastForm',

	collapsible : true,

	margin : 5,
	padding : 5,

	layout : {
		type : "vbox",
	},

	defaults : {
		padding : 5,
		margin : 5,

	},

	items : [ {
		xtype : "textareafield",
		fieldLabel : "Sequence",
		name : "blastquery",
		width : 400,
		height : 200,
		allowBlank : false

	}, {
		xtype : "combobox",
		width : 200,
		maxWidth : 200,
		fieldLabel : "Method",
		name : "blastmethod",
		queryMode : 'local',
		store : [ 'blastp', 'blastx' ],
		allowBlank : false
	}, {
		xtype : "numberfield",
		value : 0.001,
		allowBlank : false,
		fieldLabel : "E-value",
		name : "blastevalue",
		minValue : 0,
		width : 200,
		maxWidth : 200,
		maxValue : 10,
		decimalPrecision : 10,
		hideTrigger : true,
		keyNavEnabled : false,
		mouseWheelEnabled : false
	}, {
		xtype : "numberfield",
		value : 50,
		allowBlank : false,
		fieldLabel : "Page size",
		name : "pageSize",
		minValue : 1,
		width : 200,
		maxWidth : 200,
		maxValue : 200
	}, {
		xtype : "button",
		text : "blast",
		formBind : true,
		action : "searchByBlast"
	} ]
});
