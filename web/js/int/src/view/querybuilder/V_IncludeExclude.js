Ext
		.define(
				'BP.view.querybuilder.V_IncludeExclude',
				{
					extend : 'Ext.form.Panel',

					alias : 'widget.includeExcludeForm',

					collapsible : true,

					margin : 5,
					padding : 5,

					layout : {
						type : "vbox"
					},

					defaults : {
						padding : 5,
						margin : 5,

					},

					initComponent : function() {

						var panel = this;

						var queryBuilderPanel = this.up("queryBuilderPanel");

						var analysisName = queryBuilderPanel.analysisName;

						var columns = [ {
							text : "Proteome code",
							flex : 1,
							sortable : true,
							dataIndex : "code"
						} ];

						this.items = [

								{
									xtype : 'displayfield',
									fieldLabel : 'Drag & Drop proteomes from the central grid to the other grids to include or exclude proteomes',
									labelWidth : 500
								},

								{
									xtype : "container",
									width : 600,

									defaults : {
										padding : 5,
										margin : 5,

									},

									layout : {
										type : "hbox",
										border : false,
										align : "stretch",

									},

									items : [ {
										xtype : "grid",
										title : "Proteomes included",
										name : "includeGrid",
										flex : 1,
										hideHeaders : true,
										multiSelect : true,
										store : new Ext.data.Store({
											model : "BP.model.M_Proteome"
										}),
										columns : columns,
										viewConfig : {
											plugins : {
												ptype : 'gridviewdragdrop',
												ddGroup : 'DDgroup',
												enableDrop : true
											}
										}
									}, {
										xtype : "grid",
										title : "Proteomes",
										name : "mainGrid",
										hideHeaders : true,
										multiSelect : true,
										flex : 1,
										store : new Ext.data.Store({
											model : "BP.model.M_Proteome"
										}),
										columns : columns,
										viewConfig : {
											plugins : {
												ptype : 'gridviewdragdrop',
												ddGroup : 'DDgroup'
											}
										}
									}, {
										xtype : "grid",

										name : "excludeGrid",
										title : "Proteomes excluded",
										hideHeaders : true,
										multiSelect : true,
										flex : 1,
										store : new Ext.data.Store({
											model : "BP.model.M_Proteome"
										}),
										columns : columns,
										viewConfig : {
											plugins : {
												ptype : 'gridviewdragdrop',
												ddGroup : 'DDgroup',
												enableDrop : true
											}
										}
									} ]
								}, {
									xtype : "textfield",
									name : "annotation",
									width : 200,
									fieldLabel : "Group annotation contains"
								}, {
									xtype : "numberfield",
									value : 50,
									allowBlank : false,
									fieldLabel : "Page size",
									name : "pageSize",
									minValue : 1,
									width : 200,
									maxWidth : 200,
									maxValue : 200
								}, {
									xtype : "button",
									text : "Search groups",
									formBind : true,
									action : "includeExclude"
								}

						];

						this.callParent();
					}

				});
