Ext.define('BP.view.panel.V_MainPanel', {
	extend : 'Ext.tab.Panel',
	alias : 'widget.mainPanel',
	requires : [ "BP.view.form.V_NewAnalysis", "BP.globals.ConnectionData",
			"BP.view.grid.V_ListAnalyses", "BP.view.panel.V_Home" ],
	region : 'center',
	// layout : 'fit',
	// autoScroll : true,
	resizable : false,

	title : "Main panel",

	header : false,
	items : [ {
		xtype : 'homePanel'
	}, {
		xtype : 'helpPanel'
	} ],

	/**
	 * 
	 */
	initComponent : function() {

		var flag = Ext.form.field.VTypes.email(BP.globals.ConnectionData.mail);

		if (flag) {

			this.items.push({
				xtype : 'newAnalysis'
			});

			var store = Ext.getStore("S_Analyses");

			var myPanel = this;

			store.load();

			this.items.push({
				xtype : 'listAnalyses'
			});

		}

		this.callParent(arguments);

	},
	listeners : {
		'tabchange' : function(tabPanel, tab) {
			var a_windowCards = Ext.ComponentQuery.query("cardWindow");

			Ext.each(a_windowCards, function(w) {
				w.hide();
			});

		}
	}

});
