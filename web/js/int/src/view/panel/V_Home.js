Ext.define('BP.view.panel.V_Home', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.homePanel',
	
	autoScroll : true,
	resizable : true,
	
	title : "Home",
	
	loader: {
	    url: 'home.html',
	    autoLoad: true
	}
});
