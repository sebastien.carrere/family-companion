/**
 * 
 * @author LC
 * @description MetExplore banner
 */

Ext
		.define(
				'BP.view.panel.V_BannerPanel',
				{
					extend : 'Ext.Panel',
					alias : 'widget.bannerPanel',
					region : 'north',
					height : 100,
					resizable : false,

					title : "Banner panel",

					header : false,

					// split:true,

					requires : [ 'BP.globals.ConnectionData' ],

					layout : {
						type : 'hbox',
						align : 'stretch'
					},

					initComponent : function() {

						this.items = [
								{

									html : "<img src='resources/images/fc.png' width=100 style='float: left;'/>"
											+ "<div id='logotitre' style='margin-left: 120px;margin-top : 30px;'>Protein family analyses</div>"
											+ "<div style='margin-left: 120px;text-align:right;font-size:12px;margin-top : 10px;'><i>Family-Companion</i></div>",

									// html : "<img
									// src='resources/images/fc.png' width=100
									// />",
									width : 470,
									border : 0,
									margin : 0
								},
								// {
								// html : '<div id="logotitre"><div>Protein
								// family analyses</div><div
								// style="text-align:right;margin
								// :10"><i>OrthoMCL-Companion</i></div></div>',
								// width : 400,
								// border : 0,
								// margin: 20,
								// flex :2
								// }
								// ,
								{
									xtype : 'container',
									name : "login",
									layout : 'vbox',
									height : 100,
									// columnWidth:'200px',
									margin : 20,
									flex : 1,
									border : 0
								}

						];

						this.callParent(arguments);

						this.setLogin();

					},
					/**
					 * Set login container
					 */
					setLogin : function() {
						var flag = Ext.form.field.VTypes
								.email(BP.globals.ConnectionData.mail);

						var container = this.down("container[name=login]");

						container.removeAll(true, true);

						if (flag) {
							container.add({
								html : "<i>Logged as "
										+ BP.globals.ConnectionData.mail
										+ "</i>",
								border : 0
							});
						} else {
							container.add({
								xtype : 'button',
								text : "login",
								style : "text-align:right;",
								order : 0,
								action : "login"
							});
						}

					}

				});
