Ext.define('BP.view.panel.V_ResultPanel', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.resultPanel',
	requires : [ ],
	// layout : 'fit',
	autoScroll : true,
	resizable : true,
	closable : true,
	
	title:"",

	items : [
	         ],
	
	/**
	 * Build panel from Json result
	 */
	setFromJson : function (json) {
		
		this.setTitle(json.directory);
		
	}

});
	