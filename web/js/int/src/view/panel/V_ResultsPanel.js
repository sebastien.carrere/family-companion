Ext.define('BP.view.panel.V_ResultsPanel', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.resultsPanel',
	requires : [],
	
	autoScroll : true,
	resizable : false,
	closable : true,

	title : "",

	items : [],

	layout : {
		type : 'vbox',
		padding : '5',
		align : 'stretch'
	},

	/**
	 * Build panel from Json result
	 */
	setFromJson : function(json) {

		var analysisTitle = json.analysis.title;

		if (analysisTitle.length > 10) {
			analysisTitle = analysisTitle.substring(0, 10) + "...";
		}

		this.setTitle(analysisTitle);
	}

});
