/**
 * Controls fasta panel
 * <ul>
 * <li>Get Groups button</li>
 * <li>Get Proteins button</li>
 * </ul>
 */
Ext.define('BP.controller.C_FastaPanel', {
	extend : 'Ext.app.Controller',

	/**
	 * @private init function
	 */
	init : function() {
		this.control({
			'fastaPanel button[action=getGroups]' : {
				click : this.displayGrid
			},
			'fastaPanel button[action=getProteins]' : {
				click : this.displayGrid
			},

		});
	},

	/**
	 * Display group grid
	 */
	displayGrid : function(button) {

		var title = button.objectName;
		var waitMessage = "Searching groups";

		if (button.action == "getProteins") {
			waitMessage = "Searching proteins";
		}

		var panel = button.up("fastaPanel").down(
				"queryResultGrid[title=" + title + "]");

		console.log(panel);

		if (panel == null) {

			var fastaPanel = button.up("fastaPanel");

			var pageSize = 50;

			BP.globals.Ajax.send({
				url : button.link,
				waitMessage : waitMessage,
				successValue : "true",

				params : {
					limit : pageSize,
					start : 0
				},

				successFunction : function(json) {

					if (json.totalCount > 0) {

						var resultGrid = Ext.create(
								"BP.view.querybuilder.V_QueryResultGrid", {
									json : json,
									pageSize : pageSize,
									start : 0,
									url : button.link,
									title : title,
									closable : true
								});

						fastaPanel.add(resultGrid);
					} else {
						Ext.Msg.alert("Message", "No result");
					}
				}
			});
		}

	},

});