Ext.define('BP.controller.C_Proteome', {
	extend : 'Ext.app.Controller',

	init : function() {
		this.control({
			'button[action=addProteome]' : {
				click : this.addProteome
			},
			'button[action=removeProteome]' : {
				click : this.removeProteome
			}
		});

	},

	/**
	 * 
	 */
	addProteome : function(button) {

		var fieldSet = button.up("fieldset");

		fieldSet.nb = fieldSet.nb + 1;

		fieldSet.remove(button);

		Ext.each(Ext.ComponentQuery.query("proteomeFieldSet"), function(c) {

			c.collapse(true);
		});

		fieldSet.add({
			xtype : "proteomeFieldSet",
			title : 'Proteome ' + fieldSet.nb,
			collapsed : false,
			removeButton : true
		});
		fieldSet.add({
			xtype : "button",
			text : "Add Proteome",
			action : "addProteome"
		});

		BP.globals.Utils.displayShortMessage('New proteome', fieldSet);

	},

	/**
	 * 
	 */
	removeProteome : function(button) {

		proteomeField = button.up("fieldset");

		proteomesField = proteomeField.up("fieldset");

		proteomesField.remove(proteomeField);
		
		BP.globals.Utils.displayShortMessage('Remove proteome', proteomesField);

	}

	
});
