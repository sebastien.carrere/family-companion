Ext.define('BP.controller.C_Reset', {
	extend : 'Ext.app.Controller',

	requires : [  "BP.view.form.V_NewAnalysis" ],

	init : function() {
		this.control({
			'button[action=reset]' : {
				click : this.reset
			},
		});

	},
	
	reset: function(button) {
		var panel = button.up("form");
		
		var tabPanel = panel.up("tabpanel");
		
		tabPanel.remove(panel, true);
		tabPanel.insert(1, {xtype:"newAnalysis"});
		
		Ext.each(Ext.ComponentQuery.query("iprScanUpload"), function(c) {
			c.setDisabled(true);
		});
		
		tabPanel.setActiveTab(1);

	}
});