/**
 * 
 */
Ext.define('BP.controller.C_DeleteProteome', {
	extend : 'Ext.app.Controller',

	init : function() {
		this.control({
			'button[action=removeProteome]' : {
				click : this.removeProteome
			},
		});

	},

	removeProteome: function (button) {
		
		proteomeField = button.up("fieldset");
		
		proteomesField = proteomeField.up("fieldset");
		
		proteomesField.remove(proteomeField);
		
	}
	
});