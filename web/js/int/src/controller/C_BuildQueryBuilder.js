/**
 * 
 */
Ext.define('BP.controller.C_BuildQueryBuilder', {
	extend : 'Ext.app.Controller',

	/**
	 * @private init function
	 */
	init : function() {
		this.control({
			'buildQueryBuilderButton' : {
				click : this.build
			},
		});

	},

	/**
	 * Build the query builder of a button
	 */
	build : function(button) {

		var analysisName = button.analysisName;

		var url = BP.globals.Utils.getHostUrl() + "/ws/" + analysisName
				+ "/status/build";

		BP.globals.Ajax.send({
			url : url,
			waitMessage : "",
			successValue : "true",
			successFunction : function(json) {
				console.log(json);
				
				Ext.Msg.alert("Message", json["message"]);
				
				button.disable();
				
				
			}
		});

	}
});
