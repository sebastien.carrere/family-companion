/**
 * Controls the action column of the grid that list analyses Display a new panel
 * of results
 */
Ext
		.define(
				'BP.controller.C_ViewResult',
				{
					extend : 'Ext.app.Controller',

					requires : [ 'BP.view.panel.V_ResultsPanel' ],

					init : function() {
						this.control({
							'listAnalyses' : {
								viewresult : this.viewResult
							},
						});

					},

					/**
					 * 
					 */
					viewResult : function(record, rowIndex) {

						var data = record.store.getAt(rowIndex).data;

						var url_base = data.url;

						BP.globals.Result.displayResult(url_base);

					}

				});
