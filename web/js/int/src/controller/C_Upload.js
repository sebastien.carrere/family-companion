Ext.define('BP.controller.C_Upload', {
	extend : 'Ext.app.Controller',

	init : function() {
		this.control({
			'filefield' : {
				change : this.uploadFile
			}
		});

	},

	/**
	 * Upload a file
	 */
	uploadFile : function(field) {

		var formPanel = field.up("form");

		var form = formPanel.getForm();

		var fileFieldSet = formPanel.up("fieldset");

		var cgi_function = "Upload";

		if (fileFieldSet.uploadAction == "uploadZipFile") {
			cgi_function = "ZipUpload";
		}

		if (form.isValid()) {
			form.submit({
				url : '../cgi/index.cgi',
				params : {
					email : BP.globals.ConnectionData.email,
					__wb_function : cgi_function
				},
				waitMsg : 'Uploading...',
				success : function(fp, o) {
					var code = o.result.code;

					if (fileFieldSet.uploadAction == "uploadFastaFile") {
						var codeField = fileFieldSet.up("fieldset").down(
								"fieldset").down("textfield");

						codeField.setValue(code);
					}

					if (fileFieldSet.uploadAction == "uploadHomologyGroupsFile") {

						var parameterPanel = Ext.ComponentQuery.query("[name=homologygroups_parameters]")[0];
						
						parameterPanel.disable();

					}

					if (fileFieldSet.uploadAction == "uploadZipFile") {
						var newAnalysisPanel = Ext.ComponentQuery
								.query("newAnalysis")[0];

						if (o.result.success == 1) {
							newAnalysisPanel.setFromJson(o.result);
						} else {
							Ext.Msg.alert("Error",
									"Error during zip processing");
						}
					}

					var displayField = formPanel.down("displayfield");

					displayField.setValue(o.result.original_file_name);

					var path = o.result.path;

					var pathField = formPanel.down("hiddenfield");

					pathField.setValue(path);

				},
				failure : function(fp, o) {
					form.reset();
					
					if (fileFieldSet.uploadAction == "uploadHomologyGroupsFile") {

						
						var parameterPanel = Ext.ComponentQuery.query("[name=homologygroups_parameters]")[0];
						
						parameterPanel.enable();

					}
					
					Ext.Msg.alert('Failure', o.result.message);
				}
			});
		}

	}

});
