Ext.define('BP.controller.C_EnableIprScan', {
	extend : 'Ext.app.Controller',

	requires : [ 'BP.view.form.V_IprScanUpload' ],

	init : function() {
		this.control({
			'checkbox[name=addIprscanResult]' : {
				change : this.change
			},
		});

	},

	change : function(checkBox) {
		// change the default value of the class
		BP.view.form.V_IprScanUpload.prototype.disabled = !checkBox.getValue();

		
		Ext.each(Ext.ComponentQuery.query("iprScanUpload"), function(c) {
			c.setDisabled(! checkBox.getValue());
			
			
		});

	}
});
