Ext.define('BP.controller.C_QueryBuilder', {
	extend : 'Ext.app.Controller',

	/**
	 * @private init function
	 */
	init : function() {
		this.control({
			'queryBuilderButton' : {
				click : this.displayQueryBuilder
			},
		});

	},

	displayQueryBuilder : function(button) {

		var mainPanel = Ext.ComponentQuery.query("mainPanel")[0];

		var analysisTitle = button.analysisTitle;
		
		var analysisName = button.analysisName;

		if (analysisTitle.length > 10) {
			analysisTitle = analysisTitle.substring(0, 10) + "...";
		}

		var queryBuilderPanel = Ext.create(
				"BP.view.querybuilder.V_QueryBuilderPanel", {
					title : "Query builder (" + analysisTitle + ")",
					analysisName : analysisName
				});

		mainPanel.add(queryBuilderPanel);

		mainPanel.setActiveItem(queryBuilderPanel);

	}

});
