Ext.define('BP.controller.C_Blast', {
	extend : 'Ext.app.Controller',

	/**
	 * @private init function
	 */
	init : function() {
		this.control({
			'button[action=searchByBlast]' : {
				click : this.blast
			}
		});

	},

	blast : function(button) {
		var queryBuilderPanel = button.up("queryBuilderPanel");

		var blastPanel = button.up("blastForm");

		var analysisName = queryBuilderPanel.analysisName;

		var o_values = blastPanel.getValues();

		var blastQuery = encodeURIComponent(o_values.blastquery);
		var blastMethod = o_values.blastmethod;

		var blastEvalue = o_values.blastevalue;

		var pageSize = o_values.pageSize;

		var url = (DEBUG) ? "resources/test/json/gridQueryResult.json"
				: BP.globals.Utils.getHostUrl() + "/ws/" + analysisName
						+ "/search/blast?blastevalue=" + blastEvalue
						+ "&blastmethod=" + blastMethod + "&blastquery="
						+ blastQuery;

		BP.globals.Ajax.send({
			url : url,
			waitMessage : "Blast...",
			successValue : "true",

			params : {
				limit : pageSize,
				start : 0
			},

			successFunction : function(json) {

				if (json.totalCount == 0) {
					Ext.Msg.alert("Message", "No result");
				} else {
					var resultGrid = Ext.create(
							"BP.view.querybuilder.V_QueryResultGrid", {
								json : json,
								pageSize : pageSize,
								page : 1,
								url : url
							});

					var resultPanel = Ext.create(
							"BP.view.querybuilder.V_QueryResultPanel", {
								title : "Blast result",
								closable : true,
								collapsible : true
							});

					resultPanel.add(resultGrid);

					var idx = queryBuilderPanel.items.indexOf(blastPanel) + 1;

					queryBuilderPanel.insert(idx, resultPanel);

				}
			}

		});

	}

});
