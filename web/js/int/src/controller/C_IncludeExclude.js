Ext
		.define(
				'BP.controller.C_IncludeExclude',
				{
					extend : 'Ext.app.Controller',

					/**
					 * @private init function
					 */
					init : function() {
						this.control({
							'button[action=includeExclude]' : {
								click : this.run
							}
						});

					},

					run : function(button) {

						var queryBuilderPanel = button.up("queryBuilderPanel");

						var analysisName = queryBuilderPanel.analysisName;

						var panel = button.up("includeExcludeForm");

						var includeGrid = panel
								.down("grid[name='includeGrid']");

						var includeStore = includeGrid.getStore();

						var a_toInclude = [];

						includeStore.each(function(r) {

							a_toInclude.push(r.get("code"));

						});

						var excludeGrid = panel
								.down("grid[name='excludeGrid']");

						var excludeStore = excludeGrid.getStore();

						var a_toExclude = [];

						excludeStore.each(function(r) {

							a_toExclude.push(r.get("code"));

						});

						var o_values = panel.getValues();

						var pageSize = o_values.pageSize;

						var annotation = o_values.annotation;

						var url = (DEBUG) ? "resources/test/json/gridQueryResult.json"
								: BP.globals.Utils.getHostUrl() + "/ws/"
										+ analysisName
										+ "/get/groups?inproteome="
										+ a_toInclude.join(",");

						if (a_toExclude.length > 0) {
							url += "&outproteome=" + a_toExclude.join(",");
						}

						if (annotation != "") {
							url += "&annotation=" + annotation;
						}

						BP.globals.Ajax
								.send({
									url : url,
									waitMessage : "Search groups...",

									successValue : "true",

									params : {
										limit : pageSize,
										start : 0
									},

									successFunction : function(json) {

										if (json.totalCount == 0) {
											Ext.Msg.alert("Message",
													"No result");
										} else {
											var resultGrid = Ext
													.create(
															"BP.view.querybuilder.V_QueryResultGrid",
															{
																json : json,
																pageSize : pageSize,
																page : 1,
																url : url
															});

											var title = 'Groups';
											
											if (a_toInclude.length > 0)
											{
												title += ' with proteins from ' + a_toInclude.join(",");
											}
											
											if (a_toExclude.length > 0)
											{
												title += ' without proteins from ' + a_toExclude.join(",");
											}

											var resultPanel = Ext
													.create(
															"BP.view.querybuilder.V_QueryResultPanel",
															{
																title : title,
																closable : true,
																collapsible : true
															});

											resultPanel.add(resultGrid);

											var idx = queryBuilderPanel.items
													.indexOf(panel) + 1;

											queryBuilderPanel.insert(idx,
													resultPanel);
											
											resultPanel.focus();

										}
									}
								});

					}
				});
