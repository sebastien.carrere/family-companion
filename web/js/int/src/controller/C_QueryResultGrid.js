/**
 * Controls events on the query result grid ({@link BP.view.querybuilder.V_QueryResultGrid})
 * 
 * <ul>
 * <li>Get fasta file</li>
 * <li>Get groups table</li>
 * </ul>
 * 
 * 
 */
Ext.define('BP.controller.C_QueryResultGrid', {
	extend : 'Ext.app.Controller',

	/**
	 * @private init function
	 */
	init : function() {
		this.control({
			'queryResultGrid' : {
				getfasta : this.getFasta,
				getgroups : this.getGroups,
				getproteins : this.getProteins,
				getalignment : this.getAlignment
			},
		});

	},

	/**
	 * Get fasta file
	 */
	getFasta : function(record) {
		var url = null;

		var title = record.data.accession;

		Ext.each(record.data.links, function(l) {
			if (l.rel = "sequences") {
				url = l.href;
			}
		});

		if (url != null) {

			BP.globals.Ajax
					.send({
						url : url,
						waitMessage : "Get Fasta",
						successValue : "true",

						successFunction : function(json) {

							if (json.totalCount > 0) {

								var multiFastaPanel = Ext.create(
										"BP.view.fasta.V_MultiFastaPanel", {
											json : json,
											closable : true,
											title : title
										});

								var mainPanel = Ext.ComponentQuery
										.query("mainPanel")[0];

								mainPanel.add(multiFastaPanel);

								mainPanel.setActiveItem(multiFastaPanel);

							} else {
								Ext.Msg.alert("Message", "No result");
							}

						}
					});

		} else {
			Ext.Msg.alert("Failed", "No associated fasta file");
		}

	},

	/**
	 * Displays group table
	 */
	getGroups : function(record) {

		var waitMessage = "Searching group";
		var rel = "groups";
		var titlePanel = record.data.accession;

		this.displayGrid(record, waitMessage, rel, titlePanel);

	},

	/**
	 * Displays protein table
	 */
	getProteins : function(record) {

		var waitMessage = "Searching Proteins";
		var rel = "proteins";
		var titlePanel = record.data.accession + " proteins";

		this.displayGrid(record, waitMessage, rel, titlePanel);

	},

	/**
	 * 
	 * Display group or protein grid
	 * 
	 * @private
	 */
	displayGrid : function(record, waitMessage, rel, titlePanel) {
		var url = null;

		var a_links = record.data.links;

		Ext.each(a_links, function(l) {

			if (l.rel == rel) {
				url = l.href;
			}

		});

		var pageSize = 50;

		BP.globals.Ajax.send({
			url : url,
			waitMessage : waitMessage,
			successValue : "true",

			params : {
				limit : pageSize,
				start : 0
			},

			successFunction : function(json) {

				if (json.totalCount > 0) {

					var resultGrid = Ext.create(
							"BP.view.querybuilder.V_QueryResultGrid", {
								json : json,
								pageSize : pageSize,
								page : 1,
								url : url,
								title : titlePanel,
								closable : true
							});

					// var resultPanel = Ext.create(
					// "BP.view.querybuilder.V_QueryResultPanel", {
					// title : titlePanel,
					// closable : true,
					// collapsible : true
					// });

					// resultPanel.add(resultGrid);

					var mainPanel = Ext.ComponentQuery.query("mainPanel")[0];

					mainPanel.add(resultGrid);

					mainPanel.setActiveItem(resultGrid);

				} else {
					Ext.Msg.alert("Message", "No result");
				}

			}
		});
	},

	/**
	 * Display alignment
	 */
	getAlignment : function(record) {
		var url = null;

		var a_links = record.data.links;

		Ext.each(a_links, function(l) {

			if (l.rel == "alignment") {
				url = l.href;
			}

		});

		if (url == null) {
			Ext.Msg.alert("message", "no link for alignment");
		} else {
			var msaPanel = Ext.create("BP.view.msa.V_MsaPanel", {
				url : url
			});

			var mainPanel = Ext.ComponentQuery.query("mainPanel")[0];

			mainPanel.add(msaPanel);

			mainPanel.setActiveItem(msaPanel);

		}

	}

}

);
