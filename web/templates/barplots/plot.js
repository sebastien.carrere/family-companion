Ext.require([ 'Ext.chart.*', 'Ext.panel.*', 'Ext.Window',
		'Ext.fx.target.Sprite', 'Ext.layout.container.Fit',
		'Ext.window.MessageBox' ]);

Ext
		.onReady(function() {

			/**
			 * Main panel
			 */
			var mainPanel = Ext.create('Ext.Panel', {
				id : 'main-panel',
				baseCls : 'x-plain',
				renderTo : Ext.getBody(),
				layout : {
					type : 'table',
					columns : 2
				},
				// applied to child components
				defaults : {
					frame : true,
					// width : 400,
					// height : 400,
					margin : 20
				}

			});

			Ext.Array
					.each(
							data,
							function(item) {

								var xlabel = 'Counts';
								if (item.xlabel != null)
								{
									xlabel = item.xlabel;
								}
								
								var store = Ext.create('Ext.data.JsonStore', {
									fields : [ 'name', 'count', 'desc' ],
									data : item.data
								});

								var chart = Ext
										.create(
												'Ext.chart.Chart',
												{
													width : 500,
													height : 300,
													animate : true,
													store : store,
													axes : [ {
														type : 'Numeric',
														position : 'bottom',
														fields : [ 'count' ],
														// label : {
														// renderer :
														// Ext.util.Format.numberRenderer('0,0')
														// },
														title : xlabel,
														grid : true,
														minimum : 0
													}, {
														type : 'Category',
														position : 'left',
														fields : [ 'name' ],
														title : item.title
													} ],
													series : [ {
														type : 'bar',
														axis : 'bottom',
														highlight : true,
														tips : {
															trackMouse : true,
															width : 200,
															height : 130,
															renderer : function(
																	storeItem,
																	item) {
																
																this.setTitle(storeItem
																				.get('name'));
																
																this
																		.update(
																				storeItem
																						.get('count')
																				+ ' items'
																				+ '<br /><br />Description:<br />'
																				+ storeItem
																						.get('desc'));
															}
														},
														// label : {
														// display :
														// 'insideEnd',
														// field : 'data',
														// renderer :
														// Ext.util.Format.numberRenderer('0'),
														// orientation :
														// 'horizontal',
														// color : '#333',
														// 'text-anchor' :
														// 'middle'
														// },
														xField : 'name',
														yField : 'count'
													} ]
												});

								/**
								 * chart panel
								 */
								var panel = Ext
										.create(
												'Ext.Panel',
												{
													title : item.title,
													resizable : true,
													items : chart,
													layout : 'fit',
													dockedItems : [ {
														xtype : 'toolbar',
														items : [
																{
																	iconCls : 'icon-eye',
																	handler : function() {
																		window.open(item.path);
																	}
																},
																{
																	iconCls : 'icon-disk',
																	handler : function() {
																		Ext.MessageBox
																				.confirm(
																						'Confirm Download',
																						'Would you like to download the chart as an image?',
																						function(
																								choice) {
																							if (choice == 'yes') {
																								chart
																										.save({
																											type : 'image/png'
																										});
																							}
																						});
																	}
																} ]
													} ]
												});

								mainPanel.add(panel);

							});

		});
